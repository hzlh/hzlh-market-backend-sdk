<?php
namespace Market\Learn\Authentication\Repository\Authentication;

use Marmot\Framework\Classes\Repository;

use Market\Learn\Authentication\Model\Authentication;
use Market\Learn\Authentication\Adapter\Authentication\IAuthenticationAdapter;
use Market\Learn\Authentication\Adapter\Authentication\AuthenticationDBAdapter;
use Market\Learn\Authentication\Adapter\Authentication\AuthenticationMockAdapter;

use Common\Repository\OperatAbleRepositoryTrait;

class AuthenticationRepository extends Repository implements IAuthenticationAdapter
{
    use OperatAbleRepositoryTrait;

    protected $adapter;
    
    public function __construct()
    {
        $this->adapter = new AuthenticationDBAdapter();
    }

    public function setAdapter(IAuthenticationAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    protected function getActualAdapter() : IAuthenticationAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IAuthenticationAdapter
    {
        return new AuthenticationMockAdapter();
    }

    public function fetchOne($id) : Authentication
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
}
