<?php
namespace Market\Learn\Authentication\CommandHandler\Authentication;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class AuthenticationCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Learn\Authentication\Command\Authentication\AddAuthenticationCommand'=>
        'Market\Learn\Authentication\CommandHandler\Authentication\AddAuthenticationCommandHandler',
        'Market\Learn\Authentication\Command\Authentication\ResubmitAuthenticationCommand'=>
        'Market\Learn\Authentication\CommandHandler\Authentication\ResubmitAuthenticationCommandHandler',
        'Market\Learn\Authentication\Command\Authentication\ApproveAuthenticationCommand'=>
        'Market\Learn\Authentication\CommandHandler\Authentication\ApproveAuthenticationCommandHandler',
        'Market\Learn\Authentication\Command\Authentication\RejectAuthenticationCommand'=>
        'Market\Learn\Authentication\CommandHandler\Authentication\RejectAuthenticationCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
