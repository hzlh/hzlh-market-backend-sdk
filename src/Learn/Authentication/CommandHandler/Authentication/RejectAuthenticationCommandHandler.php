<?php
namespace Market\Learn\Authentication\CommandHandler\Authentication;

use Common\Model\IApproveAble;
use Common\CommandHandler\RejectCommandHandler;

use Market\Learn\Authentication\Repository\Authentication\AuthenticationRepository;

class RejectAuthenticationCommandHandler extends RejectCommandHandler
{
    use AuthenticationCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchAuthentication($id);
    }
}
