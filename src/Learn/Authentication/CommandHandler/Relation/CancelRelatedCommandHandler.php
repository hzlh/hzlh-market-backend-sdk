<?php
namespace Market\Learn\Authentication\CommandHandler\Relation;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Market\Learn\Authentication\Command\Relation\CancelRelatedCommand;

class CancelRelatedCommandHandler implements ICommandHandler
{
    use RelatedCommonCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof CancelRelatedCommand)) {
            throw new \InvalidArgumentException;
        }

        $serviceProvider = $this->getServiceProviderRepository()->fetchOne($command->serviceProviderId);
        $crew = $this->getCrewRepository()->fetchOne($command->crewId);

        if ($this->validateServiceProvider($serviceProvider) && $this->validateCrew($crew)) {
            $relation = $this->getRelation();
            $relation->setId($command->serviceProviderId);
            $relation->setServiceProvider($serviceProvider);

            return $relation->edit();
        }

        return false;
    }
}
