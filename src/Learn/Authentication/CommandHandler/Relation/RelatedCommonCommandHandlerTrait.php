<?php
namespace Market\Learn\Authentication\CommandHandler\Relation;

use Marmot\Core;

use Backend\Crew\Adapter\Crew\ICrewAdapter;
use Backend\Crew\Model\Crew;
use Backend\Crew\Model\NullCrew;
use Backend\Crew\Repository\Crew\CrewRepository;
use Market\Learn\Authentication\Model\Relation;
use Market\Learn\Authentication\Adapter\Authentication\IAuthenticationAdapter;
use Market\Learn\Authentication\Model\Authentication;
use Market\Learn\Authentication\Model\NullAuthentication;
use Market\Learn\Authentication\Repository\Authentication\AuthenticationRepository;
use Market\Learn\Authentication\Repository\Relation\RelationRepository;
use Market\Learn\Authentication\Adapter\Relation\IRelationAdapter;
use Strategy\Adapter\Type\ITypeAdapter;
use Strategy\Model\NullType;
use Strategy\Model\Type;
use Strategy\Repository\TypeRepository;

trait RelatedCommonCommandHandlerTrait
{
    protected function getServiceProviderRepository() : IAuthenticationAdapter
    {
        return new AuthenticationRepository();
    }

    protected function getStrategyTypeRepository() : ITypeAdapter
    {
        return new TypeRepository();
    }

    protected function getCrewRepository() : ICrewAdapter
    {
        return new CrewRepository();
    }

    protected function getRepository() : IRelationAdapter
    {
        return new RelationRepository();
    }

    protected function getRelation() : Relation
    {
        return new Relation();
    }

    protected function validateServiceProvider(Authentication $serviceProvider) : bool
    {
        if ($serviceProvider instanceof NullAuthentication) {
            Core::setLastError(PARAMETER_FORMAT_INCORRECT, array('pointer'=>'serviceProvider'));
            return false;
        }

        return true;
    }

    protected function validateStrategyType(Type $strategyType) : bool
    {
        if ($strategyType instanceof NullType) {
            Core::setLastError(PARAMETER_FORMAT_INCORRECT, array('pointer'=>'strategyType'));
            return false;
        }

        if ($strategyType->getStatus() != Type::STATUS['ENABLED']) {
            Core::setLastError(PARAMETER_FORMAT_INCORRECT, array('pointer'=>'strategyTypeStatus'));
            return false;
        }

        return true;
    }

    protected function validateCrew(Crew $crew) : bool
    {
        if ($crew instanceof NullCrew) {
            Core::setLastError(PARAMETER_FORMAT_INCORRECT, array('pointer'=>'crew'));
            return false;
        }

        return true;
    }
}
