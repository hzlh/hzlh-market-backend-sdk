<?php
namespace Market\Learn\Authentication\Controller;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IFetchAbleController;

use Market\Learn\Authentication\View\AuthenticationView;
use Market\Learn\Authentication\Repository\Authentication\AuthenticationRepository;

class FetchController extends Controller implements IFetchAbleController
{
    use JsonApiTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new AuthenticationRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : AuthenticationRepository
    {
        return $this->repository;
    }

    public function fetchOne(int $id)
    {
        $authentication = $this->getRepository()->fetchOne($id);

        if (!$authentication instanceof INull) {
            $this->renderView(new AuthenticationView($authentication));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function fetchList(string $ids)
    {
        $ids = explode(',', $ids);

        $authenticationList = array();

        $authenticationList = $this->getRepository()->fetchList($ids);

        if (!empty($authenticationList)) {
            $this->renderView(new AuthenticationView($authenticationList));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function filter()
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();

        list($authenticationList, $count) = $this->getRepository()->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = new AuthenticationView($authenticationList);
            $view->pagination(
                'talentMarketAuthentications',
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
