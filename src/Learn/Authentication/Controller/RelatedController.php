<?php
namespace Market\Learn\Authentication\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Market\Learn\Authentication\Adapter\Authentication\IAuthenticationAdapter;
use Market\Learn\Authentication\Command\Relation\RelatedCommand;
use Market\Learn\Authentication\Command\Relation\CancelRelatedCommand;
use Market\Learn\Authentication\CommandHandler\Relation\RelatedCommandHandlerFactory;
use Market\Learn\Authentication\Repository\Authentication\AuthenticationRepository;
use Market\Learn\Authentication\View\AuthenticationView;
use Market\Learn\Authentication\Model\Authentication;

class RelatedController extends Controller
{
    use JsonApiTrait;
    use AuthenticationValidateTrait;

    private $commandBus;

    private $serviceProviderRepository;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new RelatedCommandHandlerFactory());
        $this->serviceProviderRepository = new AuthenticationRepository();
    }

    protected function getCommandBus(): CommandBus
    {
        return $this->commandBus;
    }

    protected function getAuthenticationRepository(): IAuthenticationAdapter
    {
        return $this->serviceProviderRepository;
    }

    public function relate($id)
    {
        return $this->relateAction($id);
    }

    /**
     * func relateAction($serviceProviderId;)
     * 接参 策略类型id
     * 验证两个id
     * commandBus -> send
     * 查询企业信息
     * render -> view
     */
    protected function relateAction($id): bool
    {
        $data = $this->getRequest()->post('data');
        $relationships = $data['relationships'];

        $strategyTypeId = isset($data['attributes']['strategyTypeId']) ? $data['attributes']['strategyTypeId'] : 0;

        $crewId = $relationships['crew']['data'][0]['id'];

        if ($this->validateRelationScenario($id, $strategyTypeId, $crewId)) {
            $command = new RelatedCommand(
                $id,
                $strategyTypeId,
                $crewId
            );

            if ($this->getCommandBus()->send($command)) {
                $serviceProvider = $this->getAuthenticationRepository()->fetchOne($id);
                if ($serviceProvider instanceof Authentication) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new AuthenticationView($serviceProvider));
                    return true;
                }
            }
        }

        $this->displayError();

        return false;
    }

    public function cancelRelate($id)
    {
        return $this->cancelRelateAction($id);
    }

    protected function cancelRelateAction($id) : bool
    {
        if ($this->getRequest()->isPatchMethod()) {
            $data = $this->getRequest()->patch('data');
            $relationships = $data['relationships'];
            $crewId = $relationships['crew']['data'][0]['id'];
            $command = new CancelRelatedCommand(
                $id,
                $crewId
            );
            if ($this->getCommandBus()->send($command)) {
                $serviceProvider = $this->getAuthenticationRepository()->fetchOne($id);
                if ($serviceProvider instanceof Authentication) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new AuthenticationView($serviceProvider));
                    return true;
                }
            }
        }
        Core::setLastError(ROUTE_NOT_EXIST);
        return false;
    }
}
