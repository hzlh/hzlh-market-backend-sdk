<?php
namespace Market\Learn\Authentication\Adapter\Authentication\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class AuthenticationCache extends Cache
{
    /**
     * 构造函数初始化key和表名一致
     */
    public function __construct()
    {
        parent::__construct('market_learn_service_provider_authentication');
    }
}
