<?php
namespace Market\Learn\Authentication\Adapter\Authentication\Query;

use Marmot\Framework\Query\RowCacheQuery;

class AuthenticationRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'service_provider_authentication_id',
            new Persistence\AuthenticationCache(),
            new Persistence\AuthenticationDb()
        );
    }
}
