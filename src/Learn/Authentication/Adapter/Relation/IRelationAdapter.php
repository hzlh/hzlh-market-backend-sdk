<?php
namespace Market\Learn\Authentication\Adapter\Relation;

use Common\Adapter\IOperatAbleAdapter;
use Market\Learn\Authentication\Model\Relation;

interface IRelationAdapter extends IOperatAbleAdapter
{
    public function fetchOne($id) : Relation;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;
}
