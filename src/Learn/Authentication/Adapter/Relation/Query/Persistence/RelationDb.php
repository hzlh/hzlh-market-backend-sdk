<?php
namespace Market\Learn\Authentication\Adapter\Relation\Query\Persistence;

use Marmot\Framework\Classes\Db;

class RelationDb extends Db
{
    public function __construct()
    {
        parent::__construct('service_provider_relation');
    }
}
