<?php
namespace Market\Learn\Authentication\Model;

use BusinessNotice\Model\BusinessNotice;
use Common\Utils\JGPush\Device;
use Common\Utils\JGPush\Push as JPush;
use Member\Member\Adapter\Member\IMemberAdapter;
use Member\Member\Model\Member;
use Member\Member\Repository\Member\MemberRepository;

trait JPushNoticeTrait
{
    protected function getMemberRepository() : IMemberAdapter
    {
        return new MemberRepository();
    }

    protected function getMember(int $id) : Member
    {
        return $this->getMemberRepository()->fetchOne($id);
    }

    protected function jPush(int $memberId, BusinessNotice $notify): bool
    {
        $jPush = new JPush(
            JPush::NOTICE_TYPE_MESSAGE['BUSINESS'],
            '',
            marmot_encode($memberId)
        );

        return $jPush->push(
            JPush::PUSH_CATEGORY['ONE'],
            array(),
            array(
                'type' => JPush::NOTICE_TYPE_MAP[JPush::NOTICE_TYPE['BUSINESS']],
                'id'   => $notify->getRelationId(),
                'sendObject' => $notify->getSendObject(),
            )
        );
    }

    protected function setMemberJPushTags(int $memberId) : bool
    {
        $device = new Device($this->getMember($memberId)->getPushIdentity());
        return $device->addTag(
            array(
                Device::TAGS['SELLER']
            )
        );
    }
}
