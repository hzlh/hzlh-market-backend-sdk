<?php
namespace Market\Learn\Authentication\Model;

use Common\Utils\JGPush\Device;
use Common\Utils\JGPush\Push as JPush;
use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Common\Model\IOperatAble;
use Common\Model\IApproveAble;
use Common\Model\IResubmitAble;
use Common\Model\OperatAbleTrait;
use Common\Model\VerifyAbleTrait;
use Common\Model\ApproveAbleTrait;
use Common\Model\ResubmitAbleTrait;
use Common\Model\RejectReasonDocument;
use Common\Adapter\Document\RejectReasonDocumentAdapter;

use Member\Enterprise\Model\Enterprise;
use Market\Learn\Authentication\Repository\Authentication\AuthenticationRepository;

use Market\Learn\ServiceCategory\Model\ServiceCategory;
use Strategy\Model\Type;

class Authentication implements IObject, IApproveAble, IOperatAble, IResubmitAble
{
    use Object,
        ApproveAbleTrait,
        VerifyAbleTrait,
        OperatAbleTrait,
        ResubmitAbleTrait,
        JPushNoticeTrait;

    const NUMBER_PREFIX = 'FWS';

    const STATUS_NORMAL = 0;

    private $id;
    
    private $enterpriseName;
    
    private $enterprise;
    
    private $serviceCategory;

    private $qualificationImage;

    /**
     * @var Type $strategyType 策略类型
     */
    private $strategyType;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->enterpriseName = '';
        $this->enterprise = new Enterprise();
        $this->serviceCategory = new ServiceCategory();
        $this->strategyType = new Type();
        $this->qualificationImage = array();
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->rejectReason = new RejectReasonDocument();
        $this->status = self::STATUS_NORMAL;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->rejectReasonDocumentAdapter = new RejectReasonDocumentAdapter();
        $this->repository = new AuthenticationRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->enterpriseName);
        unset($this->enterprise);
        unset($this->serviceCategory);
        unset($this->areaId);
        unset($this->strategyType);
        unset($this->qualificationImage);
        unset($this->applyStatus);
        unset($this->rejectReason);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->rejectReasonDocumentAdapter);
        unset($this->authenticationRepository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function generateNumber() : string
    {
        return self::NUMBER_PREFIX.date('Ymd', $this->getCreateTime()).$this->getId();
    }

    public function setEnterpriseName(string $enterpriseName) : void
    {
        $this->enterpriseName = $enterpriseName;
    }

    public function getEnterpriseName() : string
    {
        return $this->enterpriseName;
    }
    
    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }
    
    public function setServiceCategory(ServiceCategory $serviceCategory) : void
    {
        $this->serviceCategory = $serviceCategory;
    }

    public function getServiceCategory() : ServiceCategory
    {
        return $this->serviceCategory;
    }

    public function getStrategyType() : Type
    {
        return $this->strategyType;
    }

    public function setStrategyType(Type $strategyType) : void
    {
        $this->strategyType = $strategyType;
    }
    
    public function setQualificationImage(array $qualificationImage) : void
    {
        $this->qualificationImage = $qualificationImage;
    }

    public function getQualificationImage() : array
    {
        return $this->qualificationImage;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : AuthenticationRepository
    {
        return $this->repository;
    }

    protected function addAction() : bool
    {
        if ($this->getEnterprise() instanceof INull) {
            Core::setLastError(ENTERPRISE_NOT_AUTHENTICATION);
            return false;
        }

        if ($this->getServiceCategory() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY);
            return false;
        }

        return $this->isExistServiceCategory() && $this->getRepository()->add($this);
    }

    protected function isExistServiceCategory() : bool
    {
        $filter = array();

        $filter['enterprise'] = $this->getEnterprise()->getId();
        $filter['serviceCategory'] = $this->getServiceCategory()->getId();


        list($list, $count) = $this->getRepository()->filter($filter);
        unset($list);

        if (!empty($count)) {
            Core::setLastError(PARAMETER_IS_UNIQUE, array());
            return false;
        }

        return true;
    }

    protected function editAction() : bool
    {
        return false;
    }

    protected function rejectAction() : bool
    {
        if (!$this->getRejectReasonDocumentAdapter()->add($this->getRejectReason())) {
            return false;
        }

        $this->setApplyStatus(self::APPLY_STATUS['REJECT']);
        $this->setUpdateTime(Core::$container->get('time'));
        $this->setStatusTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'rejectReason',
                'statusTime',
                'applyStatus',
                'updateTime'
            )
        );
    }

    protected function approveAction() : bool
    {
        $this->setApplyStatus(self::APPLY_STATUS['APPROVE']);
        $this->setUpdateTime(Core::$container->get('time'));
        $this->setStatusTime(Core::$container->get('time'));

        if ($this->getRepository()->edit(
            $this,
            array(
                'updateTime',
                'statusTime',
                'applyStatus'
            )
        )) {
            $this->setMemberJPushTags($this->getId());
            return true;
        }
        return false;
    }
    
    protected function resubmitAction() : bool
    {
        $this->setApplyStatus(self::APPLY_STATUS['PENDING']);
        $this->setUpdateTime(Core::$container->get('time'));
        $this->setStatusTime(Core::$container->get('time'));
        
        return $this->getRepository()->edit(
            $this,
            array(
                'qualificationImage',
                'updateTime',
                'statusTime',
                'applyStatus'
            )
        );
    }
}
