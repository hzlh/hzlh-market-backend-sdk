<?php
namespace Market\Learn\ServiceRequirement\Command\ServiceRequirement;

/**
 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
 */
class AddServiceRequirementCommand extends ServiceRequirementCommand
{
    public $title;

    public $detail;

    public $contactName;

    public $contactPhone;

    public $minPrice;

    public $maxPrice;

    public $validityStartTime;

    public $validityEndTime;

    public $memberId;

    public $serviceCategoryId;

    public function __construct(
        string $title,
        array $detail,
        string $contactName,
        string $contactPhone,
        float $minPrice,
        float $maxPrice,
        int $validityStartTime = 0,
        int $validityEndTime = 0,
        int $memberId = 0,
        int $serviceCategoryId = 0,
        int $id = 0
    ) {
        parent::__construct($id);

        $this->title = $title;
        $this->detail = $detail;
        $this->contactName = $contactName;
        $this->contactPhone = $contactPhone;
        $this->minPrice = $minPrice;
        $this->maxPrice = $maxPrice;
        $this->validityStartTime = $validityStartTime;
        $this->validityEndTime = $validityEndTime;
        $this->memberId = $memberId;
        $this->serviceCategoryId = $serviceCategoryId;
    }
}
