<?php
namespace Market\Learn\ServiceRequirement\CommandHandler\ServiceRequirement;

use Market\Learn\ServiceRequirement\Model\ServiceRequirement;
use Market\Learn\ServiceRequirement\Repository\ServiceRequirement\ServiceRequirementRepository;

trait ServiceRequirementCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new ServiceRequirementRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : ServiceRequirementRepository
    {
        return $this->repository;
    }
    
    protected function fetchServiceRequirement(int $id) : ServiceRequirement
    {
        return $this->getRepository()->fetchOne($id);
    }
}
