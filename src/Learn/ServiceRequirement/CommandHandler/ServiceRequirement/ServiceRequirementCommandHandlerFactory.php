<?php
namespace Market\Learn\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceRequirementCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Learn\ServiceRequirement\Command\ServiceRequirement\AddServiceRequirementCommand'=>
        'Market\Learn\ServiceRequirement\CommandHandler\ServiceRequirement\AddServiceRequirementCommandHandler',
        'Market\Learn\ServiceRequirement\Command\ServiceRequirement\ApproveServiceRequirementCommand'=>
        'Market\Learn\ServiceRequirement\CommandHandler\ServiceRequirement\ApproveServiceRequirementCommandHandler',
        'Market\Learn\ServiceRequirement\Command\ServiceRequirement\RejectServiceRequirementCommand'=>
        'Market\Learn\ServiceRequirement\CommandHandler\ServiceRequirement\RejectServiceRequirementCommandHandler',
        'Market\Learn\ServiceRequirement\Command\ServiceRequirement\RevokeServiceRequirementCommand'=>
        'Market\Learn\ServiceRequirement\CommandHandler\ServiceRequirement\RevokeServiceRequirementCommandHandler',
        'Market\Learn\ServiceRequirement\Command\ServiceRequirement\CloseServiceRequirementCommand'=>
        'Market\Learn\ServiceRequirement\CommandHandler\ServiceRequirement\CloseServiceRequirementCommandHandler',
        'Market\Learn\ServiceRequirement\Command\ServiceRequirement\DeleteServiceRequirementCommand'=>
        'Market\Learn\ServiceRequirement\CommandHandler\ServiceRequirement\DeleteServiceRequirementCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
