<?php
namespace Market\Learn\ServiceRequirement\CommandHandler\ServiceRequirement;

use Common\Model\IApproveAble;
use Common\CommandHandler\ApproveCommandHandler;

class ApproveServiceRequirementCommandHandler extends ApproveCommandHandler
{
    use ServiceRequirementCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchServiceRequirement($id);
    }
}
