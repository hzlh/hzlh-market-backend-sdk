<?php
namespace Market\Learn\ServiceRequirement\CommandHandler\ServiceRequirement;

use Common\Model\IApproveAble;
use Common\CommandHandler\RejectCommandHandler;

use Market\Learn\ServiceRequirement\Repository\ServiceRequirement\ServiceRequirementRepository;

class RejectServiceRequirementCommandHandler extends RejectCommandHandler
{
    use ServiceRequirementCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchServiceRequirement($id);
    }
}
