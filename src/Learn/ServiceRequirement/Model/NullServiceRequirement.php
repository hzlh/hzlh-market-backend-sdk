<?php
namespace Market\Learn\ServiceRequirement\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Common\Model\NullApproveAbleTrait;
use Common\Model\NullOperatAbleTrait;

class NullServiceRequirement extends ServiceRequirement implements INull
{
    use NullApproveAbleTrait, NullOperatAbleTrait;
    
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function revoke() : bool
    {
        return $this->resourceNotExist();
    }

    public function close() : bool
    {
        return $this->resourceNotExist();
    }

    public function delete() : bool
    {
        return $this->resourceNotExist();
    }
}
