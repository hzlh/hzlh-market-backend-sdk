<?php
namespace Market\Learn\ServiceRequirement\Controller;

use WidgetRules\Common\WidgetRules;

use WidgetRules\ServiceRequirement\WidgetRules as ServiceRequirementWidgetRules;

trait ServiceRequirementValidateTrait
{
    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function getServiceRequirementWidgetRules() : ServiceRequirementWidgetRules
    {
        return ServiceRequirementWidgetRules::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    protected function validateAddScenario(
        $title,
        $detail,
        $contactName,
        $contactPhone,
        $minPrice,
        $maxPrice,
        $validityStartTime,
        $validityEndTime,
        $memberId,
        $serviceCategoryId
    ) {
        return $this->getServiceRequirementWidgetRules()->title($title)
        && $this->getWidgetRules()->detail($detail)
        && $this->getWidgetRules()->realName($contactName, 'contactName')
        && $this->getWidgetRules()->cellphone($contactPhone, 'contactPhone')
        && (empty($minPrice) ? true : $this->getWidgetRules()->price($minPrice, 'minPrice'))
        && (empty($maxPrice) ? true : $this->getWidgetRules()->price($maxPrice, 'maxPrice'))
        && $this->getServiceRequirementWidgetRules()->priceThan($minPrice, $maxPrice)
        && (empty($validityStartTime) ? true :
        $this->getServiceRequirementWidgetRules()->date($validityStartTime, 'validityStartTime'))
        && (empty($validityEndTime) ? true :
        $this->getServiceRequirementWidgetRules()->date($validityEndTime, 'validityEndTime'))
        && $this->getWidgetRules()->formatNumeric($memberId, 'memberId')
        && $this->getWidgetRules()->formatNumeric($serviceCategoryId, 'serviceCategoryId');
    }
}
