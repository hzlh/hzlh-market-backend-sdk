<?php
namespace Market\Learn\ServiceRequirement\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IApproveAbleController;

use Market\Learn\ServiceRequirement\Model\ServiceRequirement;
use Market\Learn\ServiceRequirement\View\ServiceRequirementView;
use Market\Learn\ServiceRequirement\Repository\ServiceRequirement\ServiceRequirementRepository;
use Market\Learn\ServiceRequirement\Command\ServiceRequirement\RejectServiceRequirementCommand;
use Market\Learn\ServiceRequirement\Command\ServiceRequirement\ApproveServiceRequirementCommand;
use Market\Learn\ServiceRequirement\CommandHandler\ServiceRequirement\ServiceRequirementCommandHandlerFactory;

use WidgetRules\Common\WidgetRules;

class ApproveController extends Controller implements IApproveAbleController
{
    use JsonApiTrait;

    private $repository;

    private $commandBus;

    private $widgetRules;
    
    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRequirementRepository();
        $this->commandBus = new CommandBus(new ServiceRequirementCommandHandlerFactory());
        $this->widgetRules = WidgetRules::getInstance();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
        unset($this->widgetRules);
    }

    protected function getServiceRequirementRepository() : ServiceRequirementRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getWidgetRules() : WidgetRules
    {
        return $this->widgetRules;
    }

    public function approve(int $id)
    {
        $command = new ApproveServiceRequirementCommand($id);

        if ($this->getCommandBus()->send($command)) {
            $serviceRequirement = $this->getServiceRequirementRepository()->fetchOne($command->id);

            if ($serviceRequirement instanceof ServiceRequirement) {
                $this->renderView(new ServiceRequirementView($serviceRequirement));
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateRejectScenario(
        $rejectReason
    ) {
        return $this->getWidgetRules()->rejectReason($rejectReason);
    }

    /**
     * 申请信息审核驳回功能,通过PATCH传参
     * @param int id 申请信息id
     *
     * @return jsonApi
     */
    public function reject(int $id)
    {
        $data = $this->getRequest()->patch('data');

        $rejectReason = isset($data['attributes']['rejectReason'])
        ? $data['attributes']['rejectReason']
        : '';

        if ($this->validateRejectScenario($rejectReason)) {
            $command = new RejectServiceRequirementCommand(
                $rejectReason,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $serviceRequirement = $this->getServiceRequirementRepository()->fetchOne($command->id);

                if ($serviceRequirement instanceof ServiceRequirement) {
                    $this->renderView(new ServiceRequirementView($serviceRequirement));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
