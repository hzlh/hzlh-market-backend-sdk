<?php
namespace Market\Learn\ServiceCategory\Adapter\ParentCategory\Query\Persistence;

use Marmot\Framework\Classes\Db;

class ParentCategoryDb extends Db
{
    public function __construct()
    {
        parent::__construct('market_learn_parent_category');
    }
}
