<?php
namespace Market\Learn\ServiceCategory\Adapter\ServiceCategory;

use Market\Learn\ServiceCategory\Model\ServiceCategory;

use Common\Adapter\IOperatAbleAdapter;

interface IServiceCategoryAdapter extends IOperatAbleAdapter
{
    public function fetchOne($id) : ServiceCategory;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;
}
