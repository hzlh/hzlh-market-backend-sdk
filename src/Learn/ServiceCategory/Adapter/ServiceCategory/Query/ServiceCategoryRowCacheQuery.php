<?php
namespace Market\Learn\ServiceCategory\Adapter\ServiceCategory\Query;

use Marmot\Framework\Query\RowCacheQuery;

class ServiceCategoryRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'service_category_id',
            new Persistence\ServiceCategoryCache(),
            new Persistence\ServiceCategoryDb()
        );
    }
}
