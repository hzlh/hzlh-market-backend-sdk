<?php
namespace Market\Learn\ServiceCategory\Translator;

use Marmot\Interfaces\ITranslator;

use Market\Learn\ServiceCategory\Model\ParentCategory;
use Market\Learn\ServiceCategory\Model\NullParentCategory;

class ParentCategoryDBTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $parentCategory = null)
    {
        if (!isset($expression['parent_category_id'])) {
            return NullParentCategory::getInstance();
        }

        if ($parentCategory == null) {
            $parentCategory = new ParentCategory($expression['parent_category_id']);
        }
        
        if (isset($expression['name'])) {
            $parentCategory->setName($expression['name']);
        }
        if (isset($expression['status'])) {
            $parentCategory->setStatus($expression['status']);
        }
        if (isset($expression['status_time'])) {
            $parentCategory->setStatusTime($expression['status_time']);
        }
        if (isset($expression['create_time'])) {
            $parentCategory->setCreateTime($expression['create_time']);
        }
        if (isset($expression['update_time'])) {
            $parentCategory->setUpdateTime($expression['update_time']);
        }

        return $parentCategory;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($parentCategory, array $keys = array())
    {
        if (!$parentCategory instanceof ParentCategory) {
            return false;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'status',
                'statusTime',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['parent_category_id'] = $parentCategory->getId();
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $parentCategory->getName();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $parentCategory->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $parentCategory->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $parentCategory->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $parentCategory->getUpdateTime();
        }

        return $expression;
    }
}
