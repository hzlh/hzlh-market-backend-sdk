<?php
namespace Market\Learn\ServiceCategory\CommandHandler\ParentCategory;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ParentCategoryCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Learn\ServiceCategory\Command\ParentCategory\AddParentCategoryCommand'=>
        'Market\Learn\ServiceCategory\CommandHandler\ParentCategory\AddParentCategoryCommandHandler',
        'Market\Learn\ServiceCategory\Command\ParentCategory\EditParentCategoryCommand'=>
        'Market\Learn\ServiceCategory\CommandHandler\ParentCategory\EditParentCategoryCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
