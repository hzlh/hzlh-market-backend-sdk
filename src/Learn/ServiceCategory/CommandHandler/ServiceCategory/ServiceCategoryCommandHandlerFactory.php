<?php
namespace Market\Learn\ServiceCategory\CommandHandler\ServiceCategory;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceCategoryCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Learn\ServiceCategory\Command\ServiceCategory\AddServiceCategoryCommand'=>
        'Market\Learn\ServiceCategory\CommandHandler\ServiceCategory\AddServiceCategoryCommandHandler',
        'Market\Learn\ServiceCategory\Command\ServiceCategory\EditServiceCategoryCommand'=>
        'Market\Learn\ServiceCategory\CommandHandler\ServiceCategory\EditServiceCategoryCommandHandler',
        'Market\Learn\ServiceCategory\Command\ServiceCategory\EnableServiceCategoryCommand'=>
        'Market\Learn\ServiceCategory\CommandHandler\ServiceCategory\EnableServiceCategoryCommandHandler',
        'Market\Learn\ServiceCategory\Command\ServiceCategory\DisableServiceCategoryCommand'=>
        'Market\Learn\ServiceCategory\CommandHandler\ServiceCategory\DisableServiceCategoryCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
