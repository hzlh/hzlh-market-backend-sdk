<?php
namespace Market\Learn\ServiceCategory\CommandHandler\ServiceCategory;

use Market\Learn\ServiceCategory\Model\ServiceCategory;
use Market\Learn\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;

trait ServiceCategoryCommandHandlerTrait
{
    private $repository;

    public function __construct()
    {
        $this->repository = new ServiceCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : ServiceCategoryRepository
    {
        return $this->repository;
    }

    private function fetchServiceCategory($id) : ServiceCategory
    {
        return $this->getRepository()->fetchOne($id);
    }
}
