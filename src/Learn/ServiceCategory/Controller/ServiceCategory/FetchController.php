<?php
namespace Market\Learn\ServiceCategory\Controller\ServiceCategory;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IFetchAbleController;

use Market\Learn\ServiceCategory\View\ServiceCategoryView;
use Market\Learn\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;

class FetchController extends Controller implements IFetchAbleController
{
    use JsonApiTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceCategoryRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ServiceCategoryRepository
    {
        return $this->repository;
    }

    public function fetchOne(int $id)
    {
        $repository = $this->getRepository();
        $serviceCategory = $repository->fetchOne($id);

        if (!$serviceCategory instanceof INull) {
            $this->renderView(new ServiceCategoryView($serviceCategory));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function fetchList(string $ids)
    {
        $ids = explode(',', $ids);

        $serviceCategoryList = array();

        $repository = $this->getRepository();
        $serviceCategoryList = $repository->fetchList($ids);

        if (!empty($serviceCategoryList)) {
            $this->renderView(new ServiceCategoryView($serviceCategoryList));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function filter()
    {
        $repository = $this->getRepository();

        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();

        list($serviceCategoryList, $count) = $repository->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = new ServiceCategoryView($serviceCategoryList);
            $view->pagination(
                'marketServiceCategories',
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
