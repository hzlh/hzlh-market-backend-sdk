<?php
namespace Market\Learn\ServiceCategory\Controller\ServiceCategory;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IOperatAbleController;

use Market\Learn\ServiceCategory\Model\ServiceCategory;
use Market\Learn\ServiceCategory\View\ServiceCategoryView;
use Market\Learn\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;
use Market\Learn\ServiceCategory\Command\ServiceCategory\AddServiceCategoryCommand;
use Market\Learn\ServiceCategory\Command\ServiceCategory\EditServiceCategoryCommand;
use Market\Learn\ServiceCategory\CommandHandler\ServiceCategory\ServiceCategoryCommandHandlerFactory;

use WidgetRules\Common\WidgetRules;
use WidgetRules\ServiceCategory\WidgetRules as ServiceCategoryWidgetRules;

class OperationController extends Controller implements IOperatAbleController
{
    use JsonApiTrait;

    private $widgetRules;

    private $serviceCategoryWidgetRules;

    private $repository;

    private $commandBus;
    
    public function __construct()
    {
        parent::__construct();
        $this->widgetRules = new WidgetRules();
        $this->serviceCategoryWidgetRules = new ServiceCategoryWidgetRules();
        $this->repository = new ServiceCategoryRepository();
        $this->commandBus = new CommandBus(new ServiceCategoryCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->widgetRules);
        unset($this->serviceCategoryWidgetRules);
        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getWidgetRules() : WidgetRules
    {
        return $this->widgetRules;
    }

    protected function getServiceCategoryWidgetRules() : ServiceCategoryWidgetRules
    {
        return $this->serviceCategoryWidgetRules;
    }

    protected function getRepository() : ServiceCategoryRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    /**
     * 对应路由 /serviceCategories
     * 服务分类新增功能, 通过POST传参
     * @param jsonApi
     * @return jsonApi
     */
    public function add()
    {
        $data = $this->getRequest()->post('data');

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];
        
        $name = $attributes['name'];
        $qualificationName = $attributes['qualificationName'];
        $isQualification = $attributes['isQualification'];
        $isEnterpriseVerify = $attributes['isEnterpriseVerify'];
        $commission = $attributes['commission'];
        $status = $attributes['status'];
        $parentId = $relationships['parentCategory']['data'][0]['id'];

        if ($this->validateOperateScenario(
            $name,
            $qualificationName,
            $isQualification,
            $commission,
            $status,
            $isEnterpriseVerify
        ) && $this->validateAddScenario($parentId)) {
            $command = new AddServiceCategoryCommand(
                $name,
                $qualificationName,
                $isQualification,
                $isEnterpriseVerify,
                $commission,
                $status,
                $parentId
            );

            if ($this->getCommandBus()->send($command)) {
                $serviceCategory = $this->getRepository()->fetchOne($command->id);
                if ($serviceCategory instanceof ServiceCategory) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new ServiceCategoryView($serviceCategory));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * /serviceCategorys/{id:\d+}
     * 服务分类编辑功能, 通过PATCH传参
     * @param jsonApi
     * @return jsonApi
     */
    public function edit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        
        $name = $attributes['name'];
        $qualificationName = $attributes['qualificationName'];
        $isQualification = $attributes['isQualification'];
        $isEnterpriseVerify = $attributes['isEnterpriseVerify'];
        $commission = $attributes['commission'];
        $status = $attributes['status'];

        if ($this->validateOperateScenario(
            $name,
            $qualificationName,
            $isQualification,
            $commission,
            $status,
            $isEnterpriseVerify
        )) {
            $commandBus = $this->getCommandBus();

            $command = new EditServiceCategoryCommand(
                $name,
                $qualificationName,
                $isQualification,
                $isEnterpriseVerify,
                $commission,
                $status,
                $id
            );
            
            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $serviceCategory = $repository->fetchOne($id);
                if ($serviceCategory instanceof ServiceCategory) {
                    $this->render(new ServiceCategoryView($serviceCategory));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateOperateScenario(
        $name,
        $qualificationName,
        $isQualification,
        $commission,
        $status,
        $isEnterpriseVerify
    ) : bool {
        return $this->getServiceCategoryWidgetRules()->name($name)
        && $this->getServiceCategoryWidgetRules()->qualificationName($isQualification, $qualificationName)
        && $this->getServiceCategoryWidgetRules()->isQualification($isQualification)
        && $this->getServiceCategoryWidgetRules()->commission($commission)
        && $this->getWidgetRules()->formatNumeric($status, 'status')
        && $this->getServiceCategoryWidgetRules()->isEnterpriseVerify($isEnterpriseVerify);
    }

    protected function validateAddScenario(
        $parentId
    ) : bool {
        return $this->getWidgetRules()->formatNumeric($parentId, 'parentId');
    }
}
