<?php
namespace Market\Learn\ServiceCategory\Controller\ServiceCategory;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IEnableAbleController;

use Market\Learn\ServiceCategory\Model\ServiceCategory;
use Market\Learn\ServiceCategory\View\ServiceCategoryView;
use Market\Learn\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;
use Market\Learn\ServiceCategory\Command\ServiceCategory\EnableServiceCategoryCommand;
use Market\Learn\ServiceCategory\Command\ServiceCategory\DisableServiceCategoryCommand;
use Market\Learn\ServiceCategory\CommandHandler\ServiceCategory\ServiceCategoryCommandHandlerFactory;

class EnableController extends Controller implements IEnableAbleController
{
    use JsonApiTrait;

    private $repository;
    
    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceCategoryRepository();
        $this->commandBus = new CommandBus(new ServiceCategoryCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : ServiceCategoryRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    
    /**
     * 对应路由 /serviceCategorys/{id:\d+}/enable
     * 启用, 通过PATCH传参
     * @param int id 用户id
     * @return jsonApi
     */
    public function enable(int $id)
    {
        if (!empty($id)) {
            $command = new EnableServiceCategoryCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $serviceCategory  = $this->getRepository()->fetchOne($id);
                if ($serviceCategory instanceof ServiceCategory) {
                    $this->render(new ServiceCategoryView($serviceCategory));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /serviceCategorys/{id:\d+}/disable
     * 禁用, 通过PATCH传参
     * @param int id 用户id
     * @return jsonApi
     */
    public function disable(int $id)
    {
        if (!empty($id)) {
            $command = new DisableServiceCategoryCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $serviceCategory  = $this->getRepository()->fetchOne($id);
                if ($serviceCategory instanceof ServiceCategory) {
                    $this->render(new ServiceCategoryView($serviceCategory));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
}
