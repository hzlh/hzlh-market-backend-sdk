<?php
namespace Market\Learn\ServiceCategory\Command\ParentCategory;

use Marmot\Interfaces\ICommand;

class EditParentCategoryCommand implements ICommand
{
    public $name;

    public $id;

    public function __construct(
        string $name,
        int $id = 0
    ) {
        $this->name = $name;
        $this->id = $id;
    }
}
