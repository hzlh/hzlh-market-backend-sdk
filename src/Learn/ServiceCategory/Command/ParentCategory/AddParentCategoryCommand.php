<?php
namespace Market\Learn\ServiceCategory\Command\ParentCategory;

use Marmot\Interfaces\ICommand;

class AddParentCategoryCommand implements ICommand
{
    public $name;

    public $areaId;

    public $id;

    public function __construct(
        string $name,
        int $areaId = 0,
        int $id = 0
    ) {
        $this->name = $name;
        $this->areaId = $areaId;
        $this->id = $id;
    }
}
