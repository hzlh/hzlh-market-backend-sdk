<?php
namespace Market\Learn\ServiceCategory\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Common\Model\IOperatAble;
use Common\Model\OperatAbleTrait;
use Common\Model\IEnableAble;
use Common\Model\EnableAbleTrait;

use Market\Learn\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;

class ServiceCategory implements IObject, IOperatAble, IEnableAble
{
    use Object, OperatAbleTrait, EnableAbleTrait;

    const IS_QUALIFICATION = array(
        'NO' => 0,
        'YES' => 2
    );

    const IS_ENTERPRISE_VERIFY = array(
        'NO' => 0,
        'YES' => 2
    );

    private $id;

    private $name;

    private $parentCategory;

    private $qualificationName;

    private $isQualification;

    private $isEnterpriseVerify;

    private $commission;

    private $areaId;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->parentCategory = new ParentCategory();
        $this->qualificationName = '';
        $this->isQualification = self::IS_QUALIFICATION['NO'];
        $this->isEnterpriseVerify = self::IS_ENTERPRISE_VERIFY['YES'];
        $this->commission = 0.0;
        $this->areaId = 0;
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->repository = new ServiceCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->parentCategory);
        unset($this->qualificationName);
        unset($this->isQualification);
        unset($this->isEnterpriseVerify);
        unset($this->commission);
        unset($this->status);
        unset($this->areaId);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAreaId() : int
    {
        return $this->areaId;
    }

    public function setAreaId(int $areaId) : void
    {
        $this->areaId = $areaId;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setParentCategory(ParentCategory $parentCategory) : void
    {
        $this->parentCategory = $parentCategory;
    }

    public function getParentCategory() : ParentCategory
    {
        return $this->parentCategory;
    }
    
    public function setQualificationName(string $qualificationName) : void
    {
        $this->qualificationName = $qualificationName;
    }

    public function getQualificationName() : string
    {
        return $this->qualificationName;
    }
    
    public function setCommission(float $commission) : void
    {
        $this->commission = $commission;
    }

    public function getCommission() : float
    {
        return $this->commission;
    }
    
    public function setIsQualification(int $isQualification) : void
    {
        $this->isQualification =
        in_array($isQualification, self::IS_QUALIFICATION) ?
            $isQualification :
            self::IS_QUALIFICATION['NO'];
    }

    public function getIsQualification() : int
    {
        return $this->isQualification;
    }
    
    public function setIsEnterpriseVerify(int $isEnterpriseVerify) : void
    {
        $this->isEnterpriseVerify =
        in_array($isEnterpriseVerify, self::IS_ENTERPRISE_VERIFY) ?
            $isEnterpriseVerify :
            self::IS_ENTERPRISE_VERIFY['YES'];
    }

    public function getIsEnterpriseVerify() : int
    {
        return $this->isEnterpriseVerify;
    }
    
    protected function getRepository() : ServiceCategoryRepository
    {
        return $this->repository;
    }

    protected function addAction() : bool
    {
        return $this->isNameExist() && $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));
        
        return $this->isNameExist()
        && $this->getRepository()->edit($this, array(
                'name',
                'qualificationName',
                'isQualification',
                'isEnterpriseVerify',
                'commission',
                'status',
                'updateTime'
            ));
    }

    protected function isNameExist() : bool
    {
        $filter = array();

        $filter['unique'] = $this->getName();
        
        if (!empty($this->getId())) {
            $filter['id'] = $this->getId();
        }

        list($serviceCategoryList, $count) = $this->getRepository()->filter($filter);
        unset($serviceCategoryList);
        
        if (!empty($count)) {
            Core::setLastError(PARAMETER_IS_UNIQUE, array('pointer'=>'serviceCategoryName'));
            return false;
        }

        return true;
    }
}
