<?php
namespace Market\Learn\Service\Model;

use Market\Learn\Service\Repository\service\ServiceRepository;

trait ServiceStatisticTrait
{
    protected function getServiceRepository() : ServiceRepository
    {
        return new ServiceRepository();
    }

    protected function updateVolume(array $serviceIdsCount, array $serviceIds)
    {
        $serviceList = $this->getServiceRepository()->fetchList($serviceIds);

        foreach ($serviceList as $service) {
            $volume = isset($serviceIdsCount[$service->getId()]) ? $serviceIdsCount[$service->getId()] : 0;
            $volume = $service->getVolume() + $volume;

            if (!$service->updateVolume($volume)) {
                return false;
            }
        }

        return true;
    }
}
