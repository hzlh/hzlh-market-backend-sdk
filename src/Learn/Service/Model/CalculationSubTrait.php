<?php
namespace Market\Learn\Service\Model;

trait CalculationSubTrait
{
    public function subMinPrice(array $price)
    {
        return min(array_column($price, 'value'));
    }

    public function subMaxPrice(array $price)
    {
        return max(array_column($price, 'value'));
    }
}
