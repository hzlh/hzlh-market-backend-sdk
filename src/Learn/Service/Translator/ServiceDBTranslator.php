<?php
namespace Market\Learn\Service\Translator;

use Marmot\Interfaces\ITranslator;

use Market\Learn\Service\Model\Price;
use Market\Learn\Service\Model\Service;
use Market\Learn\Service\Model\NullService;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class ServiceDBTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $service = null)
    {
        if (!isset($expression['service_id'])) {
            return NullService::getInstance();
        }

        if ($service == null) {
            $service = new Service();
        }

        $service->setId($expression['service_id']);

        if (isset($expression['enterprise_id'])) {
            $service->getEnterprise()->setId($expression['enterprise_id']);
        }
        if (isset($expression['service_category_id'])) {
            $service->getServiceCategory()->setId($expression['service_category_id']);
        }
        if (isset($expression['title'])) {
            $service->setTitle($expression['title']);
        }
        if (isset($expression['tag'])) {
            $service->setTag($expression['tag']);
        }
        if (isset($expression['binaries'])) {
            $service->setBinaries($expression['binaries']);
        }
        $detail = array();
        if (is_string($expression['detail'])) {
            $detail = json_decode($expression['detail'], true);
        }
        if (is_array($expression['detail'])) {
            $detail = $expression['detail'];
        }
        $service->setDetail($detail);
        $price = array();
        if (is_string($expression['price'])) {
            $price = json_decode($expression['price'], true);
        }
        if (is_array($expression['price'])) {
            $price = $expression['price'];
        }
        $minPrice = isset($expression['min_price']) ? $expression['min_price'] : 0;
        $maxPrice = isset($expression['max_price']) ? $expression['max_price'] : 0;
        $service->setPrice(new Price($price, $minPrice, $maxPrice));
        $cover = array();
        if (is_string($expression['cover'])) {
            $cover = json_decode($expression['cover'], true);
        }
        if (is_array($expression['cover'])) {
            $cover = $expression['cover'];
        }
        $service->setCover($cover);
        $contract = array();
        if (is_string($expression['contract'])) {
            $contract = json_decode($expression['contract'], true);
        }
        if (is_array($expression['contract'])) {
            $contract = $expression['contract'];
        }
        $service->setContract($contract);

        $serviceObjects = array();
        if (is_string($expression['service_objects'])) {
            $serviceObjects = json_decode($expression['service_objects'], true);
        }
        if (is_array($expression['service_objects'])) {
            $serviceObjects = $expression['service_objects'];
        }
        $service->setServiceObjects($serviceObjects);

        if (isset($expression['volume'])) {
            $service->setVolume($expression['volume']);
        }
        if (isset($expression['attention_degree'])) {
            $service->setAttentionDegree($expression['attention_degree']);
        }
        if (isset($expression['page_views'])) {
            $service->setPageViews($expression['page_views']);
        }
        if (isset($expression['reject_reason'])) {
            $service->getRejectReason()->setId($expression['reject_reason']);
        }
        if (isset($expression['reject_reason'])) {
            $service->getRejectReason()->setId($expression['reject_reason']);
        }
        if (isset($expression['apply_status'])) {
            $service->setApplyStatus($expression['apply_status']);
        }
        if (isset($expression['status'])) {
            $service->setStatus($expression['status']);
        }
        if (isset($expression['status_time'])) {
            $service->setStatusTime($expression['status_time']);
        }
        if (isset($expression['create_time'])) {
            $service->setCreateTime($expression['create_time']);
        }
        if (isset($expression['update_time'])) {
            $service->setUpdateTime($expression['update_time']);
        }
        
        return $service;
    }

    public function objectToArray($service, array $keys = array())
    {
        if (!$service instanceof Service) {
            return false;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'enterprise',
                'serviceCategory',
                'title',
                'detail',
                'cover',
                'price',
                'minPrice',
                'maxPrice',
                'serviceObjects',
                'contract',
                'volume',
                'attentionDegree',
                'pageViews',
                'applyStatus',
                'rejectReason',
                'tag',
                'binaries',
                'createTime',
                'updateTime',
                'status',
                'statusTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['service_id'] = $service->getId();
        }
        if (in_array('enterprise', $keys)) {
            $expression['enterprise_id'] = $service->getEnterprise()->getId();
        }
        if (in_array('serviceCategory', $keys)) {
            $expression['service_category_id'] = $service->getServiceCategory()->getId();
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $service->getTitle();
        }
        if (in_array('detail', $keys)) {
            $expression['detail'] = $service->getDetail();
        }
        if (in_array('cover', $keys)) {
            $expression['cover'] = $service->getCover();
        }
        if (in_array('price', $keys)) {
            $expression['price'] = $service->getPrice()->getPrice();
        }
        if (in_array('minPrice', $keys)) {
            $expression['min_price'] = $service->getPrice()->getMinPrice();
        }
        if (in_array('maxPrice', $keys)) {
            $expression['max_price'] = $service->getPrice()->getMaxPrice();
        }
        if (in_array('serviceObjects', $keys)) {
            $expression['service_objects'] = $service->getServiceObjects();
        }
        if (in_array('contract', $keys)) {
            $expression['contract'] = $service->getContract();
        }
        if (in_array('volume', $keys)) {
            $expression['volume'] = $service->getVolume();
        }
        if (in_array('attentionDegree', $keys)) {
            $expression['attention_degree'] = $service->getAttentionDegree();
        }
        if (in_array('pageViews', $keys)) {
            $expression['page_views'] = $service->getPageViews();
        }
        if (in_array('binaries', $keys)) {
            $expression['binaries'] = $service->getBinaries();
        }
        if (in_array('tag', $keys)) {
            $expression['tag'] = $service->getTag();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['apply_status'] = $service->getApplyStatus();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['reject_reason'] = $service->getRejectReason()->getId();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $service->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $service->getUpdateTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $service->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $service->getStatusTime();
        }

        return $expression;
    }
}
