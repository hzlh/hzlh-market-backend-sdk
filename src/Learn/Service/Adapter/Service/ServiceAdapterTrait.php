<?php
namespace Market\Learn\Service\Adapter\Service;

use Member\Enterprise\Model\Enterprise;
use Member\Enterprise\Repository\Enterprise\EnterpriseRepository;

use Market\Learn\ServiceCategory\Model\ServiceCategory;
use Market\Learn\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;

use Snapshot\Model\ISnapshotAble;
use Snapshot\Repository\Snapshot\SnapshotRepository;

trait ServiceAdapterTrait
{
    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return new EnterpriseRepository();
    }

    protected function fetchEnterprise(int $id) : Enterprise
    {
        return $this->getEnterpriseRepository()->fetchOne($id);
    }

    protected function fetchEnterprises(array $ids) : array
    {
        return $this->getEnterpriseRepository()->fetchList($ids);
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return new ServiceCategoryRepository();
    }

    protected function fetchServiceCategory(int $id) : ServiceCategory
    {
        return $this->getServiceCategoryRepository()->fetchOne($id);
    }

    protected function fetchServiceCategories(array $ids) : array
    {
        return $this->getServiceCategoryRepository()->fetchList($ids);
    }

    protected function getSnapshotRepository() : SnapshotRepository
    {
        return new SnapshotRepository();
    }

    protected function snapshotList($ids) : array
    {
        $filter = array('snapshotObjectIds' => $ids, 'category' => ISnapshotAble::CATEGORY['MARKET_SERVICE']);
        $sort['createTime'] = -1;

        $list = $this->getSnapshotRepository()->filter($filter, $sort);

        $snapshotList = array();

        if (!empty($list) && isset($list[0])) {
            $snapshotList = $list[0];
        }
        
        return $snapshotList;
    }
}
