<?php
namespace Market\Learn\Service\Adapter\Service;

use Marmot\Core;
use Marmot\Basecode\Classes\Server;

use Common\Model\IEnableAble;
use Common\Adapter\FetchRestfulAdapterTrait;
use Common\Adapter\OperatAbleRestfulAdapterTrait;
use Common\Adapter\Document\RejectReasonDocumentAdapter;

use Market\Learn\Service\Model\Price;
use Market\Learn\Service\Model\Service;
use Market\Learn\Service\Model\NullService;
use Market\Learn\Service\Translator\ServiceDBTranslator;
use Market\Learn\Service\Adapter\Service\Query\ServiceRowCacheQuery;

use Market\Learn\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;

use Member\Member\Repository\Member\MemberRepository;

/**
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class ServiceDBAdapter implements IServiceAdapter
{
    use OperatAbleRestfulAdapterTrait, FetchRestfulAdapterTrait, ServiceAdapterTrait;

    const CONTRACT_EXISTENCE = 1;

    const TAG = 0;

    private $translator;

    private $rowCacheQuery;

    private $rejectReasonDocumentAdapter;

    private $serviceCategoryRepository;

    private $memberRepository;

    public function __construct()
    {
        $this->translator = new ServiceDBTranslator();
        $this->rowCacheQuery = new ServiceRowCacheQuery();
        $this->rejectReasonDocumentAdapter = new RejectReasonDocumentAdapter();
        $this->serviceCategoryRepository = new ServiceCategoryRepository();
        $this->memberRepository = new MemberRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowCacheQuery);
        unset($this->rejectReasonDocumentAdapter);
        unset($this->serviceCategoryRepository);
        unset($this->memberRepository);
    }
    
    protected function getDBTranslator() : ServiceDBTranslator
    {
        return $this->translator;
    }
    
    protected function getRowCacheQuery() : ServiceRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getMemberRepository() : MemberRepository
    {
        return $this->memberRepository;
    }
    
    protected function getRejectReasonDocumentAdapter() : RejectReasonDocumentAdapter
    {
        return $this->rejectReasonDocumentAdapter;
    }
    
    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return $this->serviceCategoryRepository;
    }

    public function fetchOne($id) : Service
    {
        $info = array();

        $info = $this->getRowCacheQuery()->getOne($id);

        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullService::getInstance();
        }

        $service = $this->getDBTranslator()->arrayToObject($info);

        if ($service instanceof Service) {
            if (!empty($service->getRejectReason()->getId())) {
                $this->getRejectReasonDocumentAdapter()->fetchOne($service->getRejectReason());
            }
        }

        $enterprise = $this->fetchEnterprise($service->getEnterprise()->getId());
        if (!empty($enterprise)) {
            $service->setEnterprise($enterprise);
        }

        $serviceCategory = $this->fetchServiceCategory($service->getServiceCategory()->getId());
        if (!empty($serviceCategory)) {
            $service->setServiceCategory($serviceCategory);
        }

        $snapshotList = $this->snapshotList($service->getId());
        if (!empty($snapshotList[0])) {
            $service->addSnapshot($snapshotList[0]);
        }

        return $service;
    }

    public function fetchList(array $ids) : array
    {
        $serviceList = array();
        $enterpriseIds = array();
        $serviceCategoryIds = array();
        
        $serviceInfoList = $this->getRowCacheQuery()->getList($ids);

        if (empty($serviceInfoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $translator = $this->getDBTranslator();
        foreach ($serviceInfoList as $serviceInfo) {
            $service = $translator->arrayToObject($serviceInfo);

            $enterpriseIds[$service->getId()] = $service->getEnterprise()->getId();
            $serviceCategoryIds[$service->getId()] = $service->getServiceCategory()->getId();

            if (!empty($service->getRejectReason()->getId())) {
                $rejectReasonDocuments[] = $service->getRejectReason();
            }

            $serviceList[] = $service;
        }

        if (!empty($rejectReasonDocuments)) {
            $this->getRejectReasonDocumentAdapter()->fetchList($rejectReasonDocuments);
        }
        
        $enterprises = $this->fetchEnterprises($enterpriseIds);
        if (!empty($enterprises)) {
            foreach ($serviceList as $key => $service) {
                if (isset($enterprises[$key])) {
                    $service->setEnterprise($enterprises[$key]);
                }
            }
        }

        $serviceCategories = $this->fetchServiceCategories($serviceCategoryIds);
        if (!empty($serviceCategories)) {
            foreach ($serviceList as $key => $service) {
                if (isset($serviceCategories[$key])) {
                    $service->setServiceCategory($serviceCategories[$key]);
                }
            }
        }

        return $serviceList;
    }
    
    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $service = new Service();

            if (isset($filter['applyStatus'])) {
                $applyStatus = $filter['applyStatus'];
                if (is_numeric($applyStatus)) {
                    $service->setApplyStatus($filter['applyStatus']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $service,
                        array('applyStatus')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($applyStatus, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $service,
                        array('applyStatus')
                    );
                    $condition .= $conjection.key($info).' IN ('.$applyStatus.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $status = $filter['status'];
                if (is_numeric($status)) {
                    $service->setStatus($filter['status']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $service,
                        array('status')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($status, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $service,
                        array('status')
                    );
                    $condition .= $conjection.key($info).' IN ('.$status.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['areaId'])) {
                $service->setAreaId($filter['areaId']);
                $info = $this->getDBTranslator()->objectToArray(
                    $service,
                    array('areaId')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['title']) && !empty($filter['title'])) {
                $service->setTitle($filter['title']);
                $info = $this->getDBTranslator()->objectToArray($service, array('title'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['enterprise']) && !empty($filter['enterprise'])) {
                $service->getEnterprise()->setId($filter['enterprise']);
                $info = $this->getDBTranslator()->objectToArray(
                    $service,
                    array('enterprise')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['parentCategory']) && !empty($filter['parentCategory'])) {
                $ids = array();
                $serviceCategoryFilter['parentCategory'] = $filter['parentCategory'];
                $serviceCategoryFilter['status'] = IEnableAble::STATUS['ENABLED'];
                $sort['id'] = -1;
                $page = 0;
                $size = 100;

                list($serviceCategories, $count) = $this->getServiceCategoryRepository()->filter(
                    $serviceCategoryFilter,
                    $sort,
                    $page,
                    $size
                );

                if (!empty($count)) {
                    foreach ($serviceCategories as $serviceCategory) {
                        $ids[] = $serviceCategory->getId();
                    }

                    $filter['serviceCategory'] = implode(',', $ids);
                }
            }
            if (isset($filter['serviceCategory']) && !empty($filter['serviceCategory'])) {
                $serviceCategory = $filter['serviceCategory'];
                if (is_numeric($serviceCategory)) {
                    $service->getServiceCategory()->setId($filter['serviceCategory']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $service,
                        array('serviceCategory')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($serviceCategory, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $service,
                        array('serviceCategory')
                    );
                    $condition .= $conjection.key($info).' IN ('.$serviceCategory.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['startTime']) && !empty($filter['startTime'])) {
                $service->setCreateTime($filter['startTime']);
                $info = $this->getDBTranslator()->objectToArray(
                    $service,
                    array('createTime')
                );
                
                $condition .= $conjection.key($info).' > '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['endTime']) && !empty($filter['endTime'])) {
                $service->setCreateTime($filter['endTime']);
                $info = $this->getDBTranslator()->objectToArray(
                    $service,
                    array('createTime')
                );
                $condition .= $conjection.key($info).' < '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['serviceObjects'])) {
                $info = $this->getDBTranslator()->objectToArray($service, array('serviceObjects'));
                $condition .= $conjection.' JSON_CONTAINS('.key($info).', \''.json_encode(
                    explode(',', $filter['serviceObjects'])
                ).'\')';
                $conjection = ' AND ';
            }
            if (isset($filter['minPriceRange']) && !empty($filter['minPriceRange'])) {
                $price = new Price(array(), $filter['minPriceRange']);
                $service->setPrice($price);
                $info = $this->getDBTranslator()->objectToArray(
                    $service,
                    array('minPrice')
                );
                $condition .= $conjection.key($info).' >= '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['maxPriceRange']) && !empty($filter['maxPriceRange'])) {
                $price = new Price(array(), $filter['maxPriceRange']);
                $service->setPrice($price);
                $info = $this->getDBTranslator()->objectToArray(
                    $service,
                    array('minPrice')
                );
                $condition .= $conjection.key($info).' <= '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['contract']) && $filter['contract'] == self::CONTRACT_EXISTENCE) {
                $info = $this->getDBTranslator()->objectToArray($service, array('contract'));
                $condition .= $conjection.' JSON_EXTRACT('.key($info).', "$.name"'.')'.' IS NOT NULL';
                $conjection = ' AND ';
            }
            if (isset($filter['number'])) {
                $id = substr($filter['number'], 10);
                $id = is_numeric($id) ? $id : 0;
               
                $service->setId($id);
                $info = $this->getDBTranslator()->objectToArray(
                    $service,
                    array('id')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['tag'])) {
                if ($filter['tag'] == self::TAG) {
                    $condition .= $conjection."tag != ''";
                }
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        $memberId = Server::get('HTTP_MEMBER_ID', 0);
  
        if (!empty($sort)) {
            $service = new Service();
            foreach ($sort as $key => $val) {
                if (!empty($memberId)) {
                    $member = $this->getMemberRepository()->fetchOne($memberId);
                    $binaries = $member->getBinaries();
                    $condition .= $conjection.'-bit_count('.$binaries.'&`binaries`)';
                    $conjection = ',';
                }
                if ($key == 'status') {
                    $info = $this->getDBTranslator()->objectToArray($service, array('status'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'applyStatus') {
                    $info = $this->getDBTranslator()->objectToArray($service, array('applyStatus'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'updateTime') {
                    $info = $this->getDBTranslator()->objectToArray($service, array('updateTime'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'minPrice') {
                    $info = $this->getDBTranslator()->objectToArray($service, array('minPrice'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'maxPrice') {
                    $info = $this->getDBTranslator()->objectToArray($service, array('maxPrice'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'volume') {
                    $info = $this->getDBTranslator()->objectToArray($service, array('volume'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'id') {
                    $info = $this->getDBTranslator()->objectToArray($service, array('id'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
            }
        }
        
        return $condition;
    }
}
