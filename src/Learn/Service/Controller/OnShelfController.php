<?php
namespace Market\Learn\Service\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IOnShelfAbleController;

use Market\Learn\Service\Model\Service;
use Market\Learn\Service\View\ServiceView;
use Market\Learn\Service\Repository\Service\ServiceRepository;
use Market\Learn\Service\Command\Service\OnShelfServiceCommand;
use Market\Learn\Service\Command\Service\OffStockServiceCommand;
use Market\Learn\Service\CommandHandler\Service\ServiceCommandHandlerFactory;

class OnShelfController extends Controller implements IOnShelfAbleController
{
    use JsonApiTrait;

    private $repository;
    
    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRepository();
        $this->commandBus = new CommandBus(new ServiceCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : ServiceRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    
    /**
     * 对应路由 /services/{id:\d+}/onShelf
     * 上架, 通过PATCH传参
     * @param int id 用户id
     * @return jsonApi
     */
    public function onShelf(int $id)
    {
        if (!empty($id)) {
            $command = new OnShelfServiceCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $service  = $this->getRepository()->fetchOne($id);
                if ($service instanceof Service) {
                    $this->render(new ServiceView($service));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /services/{id:\d+}/offStock
     * 下架, 通过PATCH传参
     * @param int id 用户id
     * @return jsonApi
     */
    public function offStock(int $id)
    {
        if (!empty($id)) {
            $command = new OffStockServiceCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $service  = $this->getRepository()->fetchOne($id);
                if ($service instanceof Service) {
                    $this->render(new ServiceView($service));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
