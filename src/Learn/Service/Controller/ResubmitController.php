<?php
namespace Market\Learn\Service\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IResubmitAbleController;

use Market\Learn\Service\Model\Service;
use Market\Learn\Service\View\ServiceView;
use Market\Learn\Service\Repository\Service\ServiceRepository;
use Market\Learn\Service\Command\Service\ResubmitServiceCommand;
use Market\Learn\Service\CommandHandler\Service\ServiceCommandHandlerFactory;

class ResubmitController extends Controller implements IResubmitAbleController
{
    use JsonApiTrait, ServiceValidateTrait;

    private $repository;

    private $commandBus;
    
    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRepository();
        $this->commandBus = new CommandBus(new ServiceCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : ServiceRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    /**
     * 对应路由 /services/{id:\d+}/resubmit
     * 重新提交, 通过PATCH传参
     * @param int id
     * @param jsonApi
     * @return jsonApi
     */
    public function resubmit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $detail = $attributes['detail'];
        $price = $attributes['price'];
        $cover = $attributes['cover'];
        $contract = $attributes['contract'];
        $serviceObjects = $attributes['serviceObjects'];
        $tag = $attributes['tag'];

        $serviceCategoryId = $relationships['serviceCategory']['data'][0]['id'];

        if ($this->validateCommonScenario(
            $title,
            $detail,
            $price,
            $cover,
            $contract,
            $serviceObjects,
            $serviceCategoryId
        )) {
            $command = new ResubmitServiceCommand(
                $title,
                $tag,
                $detail,
                $cover,
                $price,
                $contract,
                $serviceObjects,
                $serviceCategoryId,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $service = $this->getRepository()->fetchOne($command->id);
                if ($service instanceof Service) {
                    $this->render(new ServiceView($service));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
