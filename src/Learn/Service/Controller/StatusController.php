<?php
namespace Market\Learn\Service\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Market\Learn\Service\Model\Service;
use Market\Learn\Service\View\ServiceView;
use Market\Learn\Service\Repository\Service\ServiceRepository;
use Market\Learn\Service\Command\Service\CloseServiceCommand;
use Market\Learn\Service\Command\Service\RevokeServiceCommand;
use Market\Learn\Service\Command\Service\DeleteServiceCommand;
use Market\Learn\Service\CommandHandler\Service\ServiceCommandHandlerFactory;

class StatusController extends Controller
{
    use JsonApiTrait;

    private $repository;

    private $commandBus;
    
    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRepository();
        $this->commandBus = new CommandBus(new ServiceCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : ServiceRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    /**
     * 对应路由 /services/{id}/revoke
     * 撤销功能, 通过POST传参
     * @param jsonApi
     * @return jsonApi
     */
    public function revoke(int $id)
    {
        $commandBus = $this->getCommandBus();

        $command = new RevokeServiceCommand($id);

        if ($commandBus->send($command)) {
            $service = $this->getRepository()->fetchOne($command->id);
            if ($service instanceof Service) {
                $this->render(new ServiceView($service));
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /services/{id}/close
     * 关闭功能, 通过POST传参
     * @param jsonApi
     * @return jsonApi
     */
    public function close(int $id)
    {
        $commandBus = $this->getCommandBus();

        $command = new CloseServiceCommand($id);

        if ($commandBus->send($command)) {
            $service = $this->getRepository()->fetchOne($command->id);
            if ($service instanceof Service) {
                $this->render(new ServiceView($service));
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /services/{id}/delete
     * 删除功能, 通过POST传参
     * @param jsonApi
     * @return jsonApi
     */
    public function delete(int $id)
    {
        $commandBus = $this->getCommandBus();

        $command = new DeleteServiceCommand($id);

        if ($commandBus->send($command)) {
            $service = $this->getRepository()->fetchOne($command->id);
            if ($service instanceof Service) {
                $this->render(new ServiceView($service));
                return true;
            }
        }

        $this->displayError();
        return false;
    }
}
