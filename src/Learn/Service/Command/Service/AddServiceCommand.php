<?php
namespace Market\Learn\Service\Command\Service;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class AddServiceCommand extends ServiceOperationCommand
{
    public $enterpriseId;

    public function __construct(
        string $title,
        string $tag,
        array $detail,
        array $cover,
        array $price,
        array $contract,
        array $serviceObjects,
        int $serviceCategoryId = 0,
        int $enterpriseId = 0,
        int $id = 0
    ) {
        $this->title = $title;
        $this->tag = $tag;
        $this->detail = $detail;
        $this->cover = $cover;
        $this->price = $price;
        $this->contract = $contract;
        $this->serviceObjects = $serviceObjects;
        $this->serviceCategoryId = $serviceCategoryId;
        $this->enterpriseId = $enterpriseId;
        $this->id = $id;
    }
}
