<?php
namespace Market\Learn\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Learn\Service\Command\Service\AddServiceCommand'=>
        'Market\Learn\Service\CommandHandler\Service\AddServiceCommandHandler',
        'Market\Learn\Service\Command\Service\EditServiceCommand'=>
        'Market\Learn\Service\CommandHandler\Service\EditServiceCommandHandler',
        'Market\Learn\Service\Command\Service\ResubmitServiceCommand'=>
        'Market\Learn\Service\CommandHandler\Service\ResubmitServiceCommandHandler',
        'Market\Learn\Service\Command\Service\ApproveServiceCommand'=>
        'Market\Learn\Service\CommandHandler\Service\ApproveServiceCommandHandler',
        'Market\Learn\Service\Command\Service\RejectServiceCommand'=>
        'Market\Learn\Service\CommandHandler\Service\RejectServiceCommandHandler',
        'Market\Learn\Service\Command\Service\RevokeServiceCommand'=>
        'Market\Learn\Service\CommandHandler\Service\RevokeServiceCommandHandler',
        'Market\Learn\Service\Command\Service\CloseServiceCommand'=>
        'Market\Learn\Service\CommandHandler\Service\CloseServiceCommandHandler',
        'Market\Learn\Service\Command\Service\DeleteServiceCommand'=>
        'Market\Learn\Service\CommandHandler\Service\DeleteServiceCommandHandler',
        'Market\Learn\Service\Command\Service\OnShelfServiceCommand'=>
        'Market\Learn\Service\CommandHandler\Service\OnShelfServiceCommandHandler',
        'Market\Learn\Service\Command\Service\OffStockServiceCommand'=>
        'Market\Learn\Service\CommandHandler\Service\OffStockServiceCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';
  
        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
