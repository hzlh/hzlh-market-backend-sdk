<?php
namespace Market\Learn\Service\CommandHandler\Service;

use Market\Learn\Service\Model\Service;
use Market\Learn\Service\Repository\Service\ServiceRepository;

trait ServiceCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new ServiceRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : ServiceRepository
    {
        return $this->repository;
    }
    
    protected function fetchService(int $id) : Service
    {
        return $this->getRepository()->fetchOne($id);
    }
}
