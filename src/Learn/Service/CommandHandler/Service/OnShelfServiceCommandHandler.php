<?php
namespace Market\Learn\Service\CommandHandler\Service;

use Common\Model\IOnShelfAble;
use Common\CommandHandler\OnShelfCommandHandler;

class OnShelfServiceCommandHandler extends OnShelfCommandHandler
{
    use ServiceCommandHandlerTrait;
    
    protected function fetchIOnShelfObject($id) : IOnShelfAble
    {
        return $this->fetchService($id);
    }
}
