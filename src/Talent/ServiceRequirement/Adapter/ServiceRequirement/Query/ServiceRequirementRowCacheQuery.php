<?php
namespace Market\Talent\ServiceRequirement\Adapter\ServiceRequirement\Query;

use Marmot\Framework\Query\RowCacheQuery;

class ServiceRequirementRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'service_requirement_id',
            new Persistence\ServiceRequirementCache(),
            new Persistence\ServiceRequirementDb()
        );
    }
}
