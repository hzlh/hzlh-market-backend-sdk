<?php
namespace Market\Talent\ServiceRequirement\Adapter\ServiceRequirement\Query\Persistence;

use Marmot\Framework\Classes\Db;

class ServiceRequirementDb extends Db
{
    public function __construct()
    {
        parent::__construct('market_talent_service_requirement');
    }
}
