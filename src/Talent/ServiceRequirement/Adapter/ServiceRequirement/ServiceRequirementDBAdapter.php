<?php
namespace Market\Talent\ServiceRequirement\Adapter\ServiceRequirement;

use Marmot\Core;

use Common\Model\IEnableAble;
use Common\Adapter\FetchRestfulAdapterTrait;
use Common\Adapter\OperatAbleRestfulAdapterTrait;
use Common\Adapter\Document\RejectReasonDocumentAdapter;

use Market\Talent\ServiceRequirement\Model\ServiceRequirement;
use Market\Talent\ServiceRequirement\Model\NullServiceRequirement;
use Market\Talent\ServiceRequirement\Translator\ServiceRequirementDBTranslator;
use Market\Talent\ServiceRequirement\Adapter\ServiceRequirement\Query\ServiceRequirementRowCacheQuery;

use Member\Member\Model\Member;
use Member\Member\Repository\Member\MemberRepository;

use Market\Talent\ServiceCategory\Model\ServiceCategory;
use Market\Talent\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class ServiceRequirementDBAdapter implements IServiceRequirementAdapter
{
    use OperatAbleRestfulAdapterTrait, FetchRestfulAdapterTrait;

    private $translator;

    private $rowCacheQuery;

    private $memberRepository;

    private $serviceCategoryRepository;

    private $rejectReasonDocumentAdapter;

    public function __construct()
    {
        $this->translator = new ServiceRequirementDBTranslator();
        $this->rowCacheQuery = new ServiceRequirementRowCacheQuery();
        $this->memberRepository = new MemberRepository();
        $this->serviceCategoryRepository = new ServiceCategoryRepository();
        $this->rejectReasonDocumentAdapter = new RejectReasonDocumentAdapter();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowCacheQuery);
        unset($this->memberRepository);
        unset($this->serviceCategoryRepository);
        unset($this->rejectReasonDocumentAdapter);
    }
    
    protected function getDBTranslator() : ServiceRequirementDBTranslator
    {
        return $this->translator;
    }
    
    protected function getRowCacheQuery() : ServiceRequirementRowCacheQuery
    {
        return $this->rowCacheQuery;
    }
    
    protected function getRejectReasonDocumentAdapter() : RejectReasonDocumentAdapter
    {
        return $this->rejectReasonDocumentAdapter;
    }

    protected function getMemberRepository() : MemberRepository
    {
        return $this->memberRepository;
    }

    protected function fetchMember(int $id) : Member
    {
        return $this->getMemberRepository()->fetchOne($id);
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return $this->serviceCategoryRepository;
    }

    protected function fetchServiceCategory(int $id) : ServiceCategory
    {
        return $this->getServiceCategoryRepository()->fetchOne($id);
    }

    public function fetchOne($id) : ServiceRequirement
    {
        $info = array();

        $info = $this->getRowCacheQuery()->getOne($id);

        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullServiceRequirement::getInstance();
        }

        $serviceRequirement = $this->getDBTranslator()->arrayToObject($info);

        if ($serviceRequirement instanceof ServiceRequirement) {
            if (!empty($serviceRequirement->getRejectReason()->getId())) {
                $this->getRejectReasonDocumentAdapter()->fetchOne($serviceRequirement->getRejectReason());
            }
        }

        $member = $this->fetchMember($serviceRequirement->getMember()->getId());
        if (!empty($member)) {
            $serviceRequirement->setMember($member);
        }

        $serviceCategory = $this->fetchServiceCategory($serviceRequirement->getServiceCategory()->getId());
        if (!empty($serviceCategory)) {
            $serviceRequirement->setServiceCategory($serviceCategory);
        }

        return $serviceRequirement;
    }

    public function fetchList(array $ids) : array
    {
        $serviceRequirementList = array();
        
        $serviceRequirementInfoList = $this->getRowCacheQuery()->getList($ids);

        if (empty($serviceRequirementInfoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $translator = $this->getDBTranslator();
        foreach ($serviceRequirementInfoList as $serviceRequirementInfo) {
            $serviceRequirement = $translator->arrayToObject($serviceRequirementInfo);

            $memberIds[$serviceRequirement->getId()] = $serviceRequirement->getMember()->getId();
            $serviceCategoryIds[$serviceRequirement->getId()] = $serviceRequirement->getServiceCategory()->getId();

            if (!empty($serviceRequirement->getRejectReason()->getId())) {
                $rejectReasonDocuments[] = $serviceRequirement->getRejectReason();
            }

            $serviceRequirementList[] = $serviceRequirement;
        }

        if (!empty($rejectReasonDocuments)) {
            $this->getRejectReasonDocumentAdapter()->fetchList($rejectReasonDocuments);
        }
        
        $member = $this->getMemberRepository()->fetchList($memberIds);
        if (!empty($member)) {
            foreach ($serviceRequirementList as $key => $serviceRequirement) {
                if (isset($member[$key])) {
                    $serviceRequirement->setMember($member[$key]);
                }
            }
        }

        $serviceCategory = $this->getServiceCategoryRepository()->fetchList($serviceCategoryIds);
        if (!empty($serviceCategory)) {
            foreach ($serviceRequirementList as $key => $serviceRequirement) {
                if (isset($serviceCategory[$key])) {
                    $serviceRequirement->setServiceCategory($serviceCategory[$key]);
                }
            }
        }

        return $serviceRequirementList;
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $serviceRequirement = new ServiceRequirement();

            if (isset($filter['applyStatus'])) {
                $applyStatus = $filter['applyStatus'];
                if (is_numeric($applyStatus)) {
                    $serviceRequirement->setApplyStatus($filter['applyStatus']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceRequirement,
                        array('applyStatus')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($applyStatus, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceRequirement,
                        array('applyStatus')
                    );
                    $condition .= $conjection.key($info).' IN ('.$applyStatus.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['member']) && !empty($filter['member'])) {
                $serviceRequirement->getMember()->setId($filter['member']);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceRequirement,
                    array('member')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $status = $filter['status'];
                if (is_numeric($status)) {
                    $serviceRequirement->setStatus($filter['status']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceRequirement,
                        array('status')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($status, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceRequirement,
                        array('status')
                    );
                    $condition .= $conjection.key($info).' IN ('.$status.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['title']) && !empty($filter['title'])) {
                $serviceRequirement->setTitle($filter['title']);
                $info = $this->getDBTranslator()->objectToArray($serviceRequirement, array('title'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['parentCategory']) && !empty($filter['parentCategory'])) {
                $ids = array();
                $filter['parentCategory'] = $filter['parentCategory'];
                $filter['status'] = IEnableAble::STATUS['ENABLED'];
                $sort['id'] = -1;
                $page = 0;
                $size = 100;

                list($serviceCategories, $count) = $this->getServiceCategoryRepository()->filter(
                    $filter,
                    $sort,
                    $page,
                    $size
                );

                if (!empty($count)) {
                    foreach ($serviceCategories as $serviceCategory) {
                        $ids[] = $serviceCategory->getId();
                    }

                    $filter['serviceCategory'] = implode(',', $ids);
                }
            }
            if (isset($filter['serviceCategory']) && !empty($filter['serviceCategory'])) {
                $serviceCategory = $filter['serviceCategory'];
                if (is_numeric($serviceCategory)) {
                    $serviceRequirement->getServiceCategory()->setId($filter['serviceCategory']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceRequirement,
                        array('serviceCategory')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($serviceCategory, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceRequirement,
                        array('serviceCategory')
                    );
                    $condition .= $conjection.key($info).' IN ('.$serviceCategory.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['validityStartTime']) && !empty($filter['validityStartTime'])) {
                $serviceRequirement->setValidityStartTime($filter['validityStartTime']);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceRequirement,
                    array('validityStartTime')
                );
                
                $condition .= $conjection.'('.key($info).' > '.current($info);
                $conjection = ' OR ';
                $condition .= $conjection.key($info).' = 0)';
                $conjection = ' AND ';
            }
            if (isset($filter['validityEndTime']) && !empty($filter['validityEndTime'])) {
                $serviceRequirement->setValidityEndTime($filter['validityEndTime']);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceRequirement,
                    array('validityEndTime')
                );
                $condition .= $conjection.'('.key($info).' < '.current($info);
                $conjection = ' OR ';
                $condition .= $conjection.key($info).' = 0)';
                $conjection = ' AND ';
            }
            //处理定时任务,判断过期的需求
            if (isset($filter['validityPeriod']) && !empty($filter['validityPeriod'])) {
                $serviceRequirement->setValidityEndTime($filter['validityPeriod']);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceRequirement,
                    array('validityEndTime')
                );
                $condition .= $conjection.key($info).' < '.current($info);
                $conjection = ' AND ';
                $condition .= $conjection.key($info).' <> 0';
                $conjection = ' AND ';
            }
            if (isset($filter['minPrice']) && !empty($filter['minPrice'])) {
                $serviceRequirement->setMinPrice($filter['minPrice']);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceRequirement,
                    array('minPrice')
                );
                $condition .= $conjection.key($info).' >= '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['maxPrice']) && !empty($filter['maxPrice'])) {
                $serviceRequirement->setMaxPrice($filter['maxPrice']);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceRequirement,
                    array('maxPrice')
                );
                $condition .= $conjection.key($info).' <= '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['number'])) {
                $id = substr($filter['number'], 10);
                $id = is_numeric($id) ? $id : 0;
               
                $serviceRequirement->setId($id);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceRequirement,
                    array('id')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $serviceRequirement = new ServiceRequirement();
            foreach ($sort as $key => $val) {
                if ($key == 'status') {
                    $info = $this->getDBTranslator()->objectToArray($serviceRequirement, array('status'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'applyStatus') {
                    $info = $this->getDBTranslator()->objectToArray($serviceRequirement, array('applyStatus'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'updateTime') {
                    $info = $this->getDBTranslator()->objectToArray($serviceRequirement, array('updateTime'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'id') {
                    $info = $this->getDBTranslator()->objectToArray($serviceRequirement, array('id'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
            }
        }

        return $condition;
    }
}
