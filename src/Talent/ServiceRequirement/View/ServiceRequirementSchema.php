<?php
namespace Market\Talent\ServiceRequirement\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class ServiceRequirementSchema extends SchemaProvider
{
    protected $resourceType = 'marketServiceRequirements';

    public function getId($serviceRequirement)
    {
        return $serviceRequirement->getId();
    }

    public function getAttributes($serviceRequirement) : array
    {
        $rejectReason = $serviceRequirement->getRejectReason()->getData();
        $rejectReasonSchema = !empty($rejectReason) ? $rejectReason['rejectReason'] : '';

        return [
            'number'  => $serviceRequirement->generateNumber(),
            'title'  => $serviceRequirement->getTitle(),
            'detail'  => $serviceRequirement->getDetail(),
            'contactName'  => $serviceRequirement->getContactsInfo()->getName(),
            'contactPhone'  => $serviceRequirement->getContactsInfo()->getPhone(),
            'minPrice'  => $serviceRequirement->getMinPrice(),
            'maxPrice'  => $serviceRequirement->getMaxPrice(),
            'validityStartTime'  => $serviceRequirement->getValidityStartTime(),
            'validityEndTime'  => $serviceRequirement->getValidityEndTime(),
            'applyStatus' => $serviceRequirement->getApplyStatus(),
            'rejectReason'  => $rejectReasonSchema,
            'status' => $serviceRequirement->getStatus(),
            'createTime' => $serviceRequirement->getCreateTime(),
            'updateTime' => $serviceRequirement->getUpdateTime(),
            'statusTime' => $serviceRequirement->getStatusTime()
        ];
    }

    public function getRelationships($serviceRequirement, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'serviceCategory' => [self::DATA => $serviceRequirement->getServiceCategory()],
            'member' => [self::DATA => $serviceRequirement->getMember()]
        ];
    }
}
