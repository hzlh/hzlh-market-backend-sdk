<?php
namespace Market\Talent\ServiceRequirement\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Common\Model\IOperatAble;
use Common\Model\IApproveAble;
use Common\Model\OperatAbleTrait;
use Common\Model\VerifyAbleTrait;
use Common\Model\ApproveAbleTrait;
use Common\Model\RejectReasonDocument;
use Common\Adapter\Document\RejectReasonDocumentAdapter;

use Member\Member\Model\Member;

use Market\Talent\ServiceRequirement\Repository\ServiceRequirement\ServiceRequirementRepository;

use Market\Talent\ServiceCategory\Model\ServiceCategory;

class ServiceRequirement implements IObject, IApproveAble, IOperatAble
{
    use Object, ApproveAbleTrait, VerifyAbleTrait, OperatAbleTrait;

    const NUMBER_PREFIX = 'XQ';
    
    const STATUS = array(
        'NORMAL' => 0, //正常
        'REVOKED' => -2, //撤销
        'CLOSED' => -4, //关闭
        'DELETED' => -6, //删除
    );

    private $id;

    private $member;

    private $serviceCategory;
    
    private $title;

    private $detail;
    /**
     * @var ContactsInfo $contactsInfo 联系人信息
     */
    private $contactsInfo;

    private $minPrice;

    private $maxPrice;

    private $validityStartTime;

    private $validityEndTime;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->member = new Member();
        $this->serviceCategory = new ServiceCategory();
        $this->title = '';
        $this->detail = array();
        $this->contactsInfo = new ContactsInfo();
        $this->minPrice = 0.0;
        $this->maxPrice = 0.0;
        $this->validityStartTime = 0;
        $this->validityEndTime = 0;
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->rejectReason = new RejectReasonDocument();
        $this->status = self::STATUS['NORMAL'];
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->rejectReasonDocumentAdapter = new RejectReasonDocumentAdapter();
        $this->repository = new ServiceRequirementRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->member);
        unset($this->serviceCategory);
        unset($this->title);
        unset($this->detail);
        unset($this->minPrice);
        unset($this->maxPrice);
        unset($this->validityStartTime);
        unset($this->validityEndTime);
        unset($this->applyStatus);
        unset($this->rejectReason);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->rejectReasonDocumentAdapter);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function generateNumber() : string
    {
        return self::NUMBER_PREFIX.date('Ymd', $this->getCreateTime()).$this->getId();
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setServiceCategory(ServiceCategory $serviceCategory) : void
    {
        $this->serviceCategory = $serviceCategory;
    }

    public function getServiceCategory() : ServiceCategory
    {
        return $this->serviceCategory;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setDetail(array $detail) : void
    {
        $this->detail = $detail;
    }

    public function getDetail() : array
    {
        return $this->detail;
    }
    
    public function setContactsInfo(ContactsInfo $contactsInfo) : void
    {
        $this->contactsInfo = $contactsInfo;
    }

    public function getContactsInfo() : ContactsInfo
    {
        return $this->contactsInfo;
    }
    
    public function setMinPrice(float $minPrice) : void
    {
        $this->minPrice = $minPrice;
    }

    public function getMinPrice() : float
    {
        return $this->minPrice;
    }
    
    public function setMaxPrice(float $maxPrice) : void
    {
        $this->maxPrice = $maxPrice;
    }

    public function getMaxPrice() : float
    {
        return $this->maxPrice;
    }
    
    public function setValidityStartTime(int $validityStartTime) : void
    {
        $this->validityStartTime = $validityStartTime;
    }

    public function getValidityStartTime() : int
    {
        return $this->validityStartTime;
    }
    
    public function setValidityEndTime(int $validityEndTime) : void
    {
        $this->validityEndTime = $validityEndTime;
    }

    public function getValidityEndTime() : int
    {
        return $this->validityEndTime;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ?
                        $status :
                        self::STATUS['NORMAL'];
    }

    protected function getRepository() : ServiceRequirementRepository
    {
        return $this->repository;
    }
    
    protected function rejectAction() : bool
    {
        if (!$this->getRejectReasonDocumentAdapter()->add($this->getRejectReason())) {
            return false;
        }

        $this->setApplyStatus(self::APPLY_STATUS['REJECT']);
        $this->setUpdateTime(Core::$container->get('time'));
        $this->setStatusTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'rejectReason',
                'statusTime',
                'applyStatus',
                'updateTime'
            )
        );
    }

    public function revoke() : bool
    {
        if (!$this->isPending() || !$this->isNormal()) {
            Core::setLastError(RESOURCE_STATUS_NOT_PENDING);
            return false;
        }
        
        $this->setStatus(self::STATUS['REVOKED']);
        $this->setUpdateTime(Core::$container->get('time'));
        $this->setStatusTime(Core::$container->get('time'));
        
        return $this->getRepository()->edit(
            $this,
            array(
                'status',
                'updateTime',
                'statusTime'
            )
        );
    }

    public function close() : bool
    {
        if (!$this->isApprove() || !$this->isNormal()) {
            Core::setLastError(RESOURCE_STATUS_NOT_NORMAL);
            return false;
        }
        
        $this->setStatus(self::STATUS['CLOSED']);
        $this->setUpdateTime(Core::$container->get('time'));
        $this->setStatusTime(Core::$container->get('time'));
        
        return $this->getRepository()->edit(
            $this,
            array(
                'status',
                'updateTime',
                'statusTime'
            )
        );
    }

    public function delete() : bool
    {
        if (!$this->isReject() && !$this->isRevoked() && !$this->isClosed()) {
            Core::setLastError(RESOURCE_STATUS_NOT_REJECT);
            return false;
        }
        
        $this->setStatus(self::STATUS['DELETED']);
        $this->setUpdateTime(Core::$container->get('time'));
        $this->setStatusTime(Core::$container->get('time'));
        
        return $this->getRepository()->edit(
            $this,
            array(
                'status',
                'updateTime',
                'statusTime'
            )
        );
    }

    protected function approveAction() : bool
    {
        $this->setApplyStatus(self::APPLY_STATUS['APPROVE']);
        $this->setUpdateTime(Core::$container->get('time'));
        $this->setStatusTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'updateTime',
                'statusTime',
                'applyStatus'
            )
        );
        ;
    }
    
    protected function addAction() : bool
    {
        //真实代码不需要此部分,此段代码只是用于数据迁移
        // if (!empty($this->getRejectReason()->getData())) {
        //     if (!$this->getRejectReasonDocumentAdapter()->add($this->getRejectReason())) {
        //         return false;
        //     }
        // }

        return $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return false;
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::STATUS['NORMAL'];
    }

    public function isRevoked() : bool
    {
        return $this->getStatus() == self::STATUS['REVOKED'];
    }

    public function isClosed() : bool
    {
        return $this->getStatus() == self::STATUS['CLOSED'];
    }
}
