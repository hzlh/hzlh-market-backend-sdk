<?php
namespace Market\Talent\ServiceRequirement\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Market\Talent\ServiceRequirement\Model\ServiceRequirement;
use Market\Talent\ServiceRequirement\View\ServiceRequirementView;
use Market\Talent\ServiceRequirement\Repository\ServiceRequirement\ServiceRequirementRepository;
use Market\Talent\ServiceRequirement\Command\ServiceRequirement\CloseServiceRequirementCommand;
use Market\Talent\ServiceRequirement\Command\ServiceRequirement\RevokeServiceRequirementCommand;
use Market\Talent\ServiceRequirement\Command\ServiceRequirement\DeleteServiceRequirementCommand;
use Market\Talent\ServiceRequirement\CommandHandler\ServiceRequirement\ServiceRequirementCommandHandlerFactory;

class StatusController extends Controller
{
    use JsonApiTrait;

    private $repository;

    private $commandBus;
    
    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRequirementRepository();
        $this->commandBus = new CommandBus(new ServiceRequirementCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : ServiceRequirementRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    /**
     * 对应路由 /serviceRequirements/{id}/revoke
     * 撤销功能, 通过POST传参
     * @param jsonApi
     * @return jsonApi
     */
    public function revoke(int $id)
    {
        $commandBus = $this->getCommandBus();

        $command = new RevokeServiceRequirementCommand($id);

        if ($commandBus->send($command)) {
            $serviceRequirement = $this->getRepository()->fetchOne($command->id);
            if ($serviceRequirement instanceof ServiceRequirement) {
                $this->render(new ServiceRequirementView($serviceRequirement));
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /serviceRequirements/{id}/close
     * 关闭功能, 通过POST传参
     * @param jsonApi
     * @return jsonApi
     */
    public function close(int $id)
    {
        $commandBus = $this->getCommandBus();

        $command = new CloseServiceRequirementCommand($id);

        if ($commandBus->send($command)) {
            $serviceRequirement = $this->getRepository()->fetchOne($command->id);
            if ($serviceRequirement instanceof ServiceRequirement) {
                $this->render(new ServiceRequirementView($serviceRequirement));
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /serviceRequirements/{id}/delete
     * 删除功能, 通过POST传参
     * @param jsonApi
     * @return jsonApi
     */
    public function delete(int $id)
    {
        $commandBus = $this->getCommandBus();

        $command = new DeleteServiceRequirementCommand($id);

        if ($commandBus->send($command)) {
            $serviceRequirement = $this->getRepository()->fetchOne($command->id);
            if ($serviceRequirement instanceof ServiceRequirement) {
                $this->render(new ServiceRequirementView($serviceRequirement));
                return true;
            }
        }

        $this->displayError();
        return false;
    }
}
