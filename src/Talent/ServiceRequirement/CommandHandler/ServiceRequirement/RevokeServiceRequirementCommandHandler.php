<?php
namespace Market\Talent\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Talent\ServiceRequirement\Command\ServiceRequirement\RevokeServiceRequirementCommand;

class RevokeServiceRequirementCommandHandler implements ICommandHandler
{
    use ServiceRequirementCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof RevokeServiceRequirementCommand)) {
            throw new \InvalidArgumentException;
        }

        $serviceRequirement = $this->fetchServiceRequirement($command->id);

        return $serviceRequirement->revoke();
    }
}
