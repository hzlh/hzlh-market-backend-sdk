<?php
namespace Market\Talent\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Talent\ServiceRequirement\Model\ContactsInfo;
use Market\Talent\ServiceRequirement\Model\ServiceRequirement;
use Market\Talent\ServiceRequirement\Command\ServiceRequirement\AddServiceRequirementCommand;

use Member\Member\Model\Member;
use Member\Member\Repository\Member\MemberRepository;

use Market\Talent\ServiceCategory\Model\ServiceCategory;
use Market\Talent\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;

class AddServiceRequirementCommandHandler implements ICommandHandler
{
    private $serviceRequirement;

    private $memberRepository;

    private $serviceCategoryRepository;

    public function __construct()
    {
        $this->serviceRequirement = new ServiceRequirement();
        $this->memberRepository = new MemberRepository();
        $this->serviceCategoryRepository = new ServiceCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->serviceRequirement);
        unset($this->memberRepository);
        unset($this->serviceCategoryRepository);
    }

    protected function getServiceRequirement() : ServiceRequirement
    {
        return $this->serviceRequirement;
    }

    protected function getMemberRepository() : MemberRepository
    {
        return $this->memberRepository;
    }

    protected function fetchMember(int $id) : Member
    {
        return $this->getMemberRepository()->fetchOne($id);
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return $this->serviceCategoryRepository;
    }

    protected function fetchServiceCategory(int $id) : ServiceCategory
    {
        return $this->getServiceCategoryRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddServiceRequirementCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->fetchMember($command->memberId);
        $serviceCategory = $this->fetchServiceCategory($command->serviceCategoryId);
       
        $serviceRequirement = $this->getServiceRequirement();
        $serviceRequirement->setTitle($command->title);
        $serviceRequirement->setDetail($command->detail);
        $serviceRequirement->setContactsInfo(new ContactsInfo(
            $command->contactName,
            $command->contactPhone
        ));
        $serviceRequirement->setMinPrice($command->minPrice);
        $serviceRequirement->setMaxPrice($command->maxPrice);
        $serviceRequirement->setValidityStartTime($command->validityStartTime);
        $serviceRequirement->setValidityEndTime($command->validityEndTime);
        $serviceRequirement->setMember($member);
        $serviceRequirement->setServiceCategory($serviceCategory);

        if ($serviceRequirement->add()) {
            $command->id = $serviceRequirement->getId();
            return true;
        }
        
        return false;
    }
}
