<?php
namespace Market\Talent\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceRequirementCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Talent\ServiceRequirement\Command\ServiceRequirement\AddServiceRequirementCommand'=>
        'Market\Talent\ServiceRequirement\CommandHandler\ServiceRequirement\AddServiceRequirementCommandHandler',
        'Market\Talent\ServiceRequirement\Command\ServiceRequirement\ApproveServiceRequirementCommand'=>
        'Market\Talent\ServiceRequirement\CommandHandler\ServiceRequirement\ApproveServiceRequirementCommandHandler',
        'Market\Talent\ServiceRequirement\Command\ServiceRequirement\RejectServiceRequirementCommand'=>
        'Market\Talent\ServiceRequirement\CommandHandler\ServiceRequirement\RejectServiceRequirementCommandHandler',
        'Market\Talent\ServiceRequirement\Command\ServiceRequirement\RevokeServiceRequirementCommand'=>
        'Market\Talent\ServiceRequirement\CommandHandler\ServiceRequirement\RevokeServiceRequirementCommandHandler',
        'Market\Talent\ServiceRequirement\Command\ServiceRequirement\CloseServiceRequirementCommand'=>
        'Market\Talent\ServiceRequirement\CommandHandler\ServiceRequirement\CloseServiceRequirementCommandHandler',
        'Market\Talent\ServiceRequirement\Command\ServiceRequirement\DeleteServiceRequirementCommand'=>
        'Market\Talent\ServiceRequirement\CommandHandler\ServiceRequirement\DeleteServiceRequirementCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
