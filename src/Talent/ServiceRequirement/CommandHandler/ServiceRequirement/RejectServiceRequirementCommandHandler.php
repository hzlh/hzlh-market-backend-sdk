<?php
namespace Market\Talent\ServiceRequirement\CommandHandler\ServiceRequirement;

use Common\Model\IApproveAble;
use Common\CommandHandler\RejectCommandHandler;

use Market\Talent\ServiceRequirement\Repository\ServiceRequirement\ServiceRequirementRepository;

class RejectServiceRequirementCommandHandler extends RejectCommandHandler
{
    use ServiceRequirementCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchServiceRequirement($id);
    }
}
