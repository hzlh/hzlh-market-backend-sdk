<?php
namespace Market\Talent\Product\Model;

use Common\Model\IOperatAble;
use Common\Model\IOnShelfAble;
use Common\Model\IApproveAble;
use Common\Model\IResubmitAble;

use Snapshot\Model\ISnapshotAble;

interface IProduct extends IOperatAble, ISnapshotAble, IOnShelfAble, IApproveAble, IResubmitAble
{
}
