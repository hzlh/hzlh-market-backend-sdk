<?php
namespace Market\Talent\ServiceCategory\Controller\ParentCategory;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IFetchAbleController;

use Market\Talent\ServiceCategory\View\ParentCategoryView;
use Market\Talent\ServiceCategory\Repository\ParentCategory\ParentCategoryRepository;

class FetchController extends Controller implements IFetchAbleController
{
    use JsonApiTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ParentCategoryRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ParentCategoryRepository
    {
        return $this->repository;
    }

    public function fetchOne(int $id)
    {
        $parentCategory = $this->getRepository()->fetchOne($id);

        if (!$parentCategory instanceof INull) {
            $this->renderView(new ParentCategoryView($parentCategory));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function fetchList(string $ids)
    {
        $ids = explode(',', $ids);

        $parentCategoryList = array();

        $parentCategoryList = $this->getRepository()->fetchList($ids);

        if (!empty($parentCategoryList)) {
            $this->renderView(new ParentCategoryView($parentCategoryList));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function filter()
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();

        list($parentCategoryList, $count) = $this->getRepository()->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = new ParentCategoryView($parentCategoryList);
            $view->pagination(
                'marketParentCategories',
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
