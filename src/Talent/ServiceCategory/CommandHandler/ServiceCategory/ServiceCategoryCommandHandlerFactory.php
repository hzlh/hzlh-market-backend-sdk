<?php
namespace Market\Talent\ServiceCategory\CommandHandler\ServiceCategory;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceCategoryCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Talent\ServiceCategory\Command\ServiceCategory\AddServiceCategoryCommand'=>
        'Market\Talent\ServiceCategory\CommandHandler\ServiceCategory\AddServiceCategoryCommandHandler',
        'Market\Talent\ServiceCategory\Command\ServiceCategory\EditServiceCategoryCommand'=>
        'Market\Talent\ServiceCategory\CommandHandler\ServiceCategory\EditServiceCategoryCommandHandler',
        'Market\Talent\ServiceCategory\Command\ServiceCategory\EnableServiceCategoryCommand'=>
        'Market\Talent\ServiceCategory\CommandHandler\ServiceCategory\EnableServiceCategoryCommandHandler',
        'Market\Talent\ServiceCategory\Command\ServiceCategory\DisableServiceCategoryCommand'=>
        'Market\Talent\ServiceCategory\CommandHandler\ServiceCategory\DisableServiceCategoryCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
