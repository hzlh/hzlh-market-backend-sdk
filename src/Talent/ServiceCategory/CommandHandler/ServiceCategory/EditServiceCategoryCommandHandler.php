<?php
namespace Market\Talent\ServiceCategory\CommandHandler\ServiceCategory;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Talent\ServiceCategory\Command\ServiceCategory\EditServiceCategoryCommand;

class EditServiceCategoryCommandHandler implements ICommandHandler
{
    use ServiceCategoryCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditServiceCategoryCommand)) {
            throw new \InvalidArgumentException;
        }

        $serviceCategory = $this->fetchServiceCategory($command->id);

        $serviceCategory->setName($command->name);
        $serviceCategory->setQualificationName($command->qualificationName);
        $serviceCategory->setIsQualification($command->isQualification);
        $serviceCategory->setIsEnterpriseVerify($command->isEnterpriseVerify);
        $serviceCategory->setCommission($command->commission);
        $serviceCategory->setStatus($command->status);

        return $serviceCategory->edit();
    }
}
