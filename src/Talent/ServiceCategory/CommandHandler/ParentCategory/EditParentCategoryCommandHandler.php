<?php
namespace Market\Talent\ServiceCategory\CommandHandler\ParentCategory;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use Market\Talent\ServiceCategory\Model\ParentCategory;
use Market\Talent\ServiceCategory\Command\ParentCategory\EditParentCategoryCommand;
use Market\Talent\ServiceCategory\Repository\ParentCategory\ParentCategoryRepository;

class EditParentCategoryCommandHandler implements ICommandHandler
{
    private $repository;

    public function __construct()
    {
        $this->repository = new ParentCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : ParentCategoryRepository
    {
        return $this->repository;
    }

    private function fetchParentCategory($id) : ParentCategory
    {
        return $this->getRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditParentCategoryCommand)) {
            throw new \InvalidArgumentException;
        }

        $parentCategory = $this->fetchParentCategory($command->id);

        $parentCategory->setName($command->name);

        return $parentCategory->edit();
    }
}
