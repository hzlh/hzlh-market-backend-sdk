<?php
namespace Market\Talent\ServiceCategory\CommandHandler\ParentCategory;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Talent\ServiceCategory\Model\ParentCategory;
use Market\Talent\ServiceCategory\Command\ParentCategory\AddParentCategoryCommand;

class AddParentCategoryCommandHandler implements ICommandHandler
{
    private $parentCategory;

    public function __construct()
    {
        $this->parentCategory = new ParentCategory();
    }

    public function __destruct()
    {
        unset($this->parentCategory);
    }

    protected function getParentCategory()
    {
        return $this->parentCategory;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddParentCategoryCommand)) {
            throw new \InvalidArgumentException;
        }

        $parentCategory = $this->getParentCategory();
        $parentCategory->setName($command->name);

        if ($parentCategory->add()) {
            $command->id = $parentCategory->getId();
            return true;
        }
        
        return false;
    }
}
