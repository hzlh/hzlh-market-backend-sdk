<?php
namespace Market\Talent\ServiceCategory\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Common\Model\NullOperatAbleTrait;

class NullParentCategory extends ParentCategory implements INull
{
    use NullOperatAbleTrait;
    
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function isNameExist() : bool
    {
        return $this->resourceNotExist();
    }
}
