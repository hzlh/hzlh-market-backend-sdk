<?php
namespace Market\Talent\ServiceCategory\Model;

use Common\Model\IOperatAble;
use Common\Model\OperatAbleTrait;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Market\Talent\ServiceCategory\Repository\ParentCategory\ParentCategoryRepository;

class ParentCategory implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    const STATUS_NORMAL = 0;

    private $id;

    private $name;

    private $areaId;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->areaId = 0;
        $this->status = self::STATUS_NORMAL;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->repository = new ParentCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->areaId);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getAreaId() : int
    {
        return $this->areaId;
    }

    public function setAreaId(int $areaId) : void
    {
        $this->areaId = $areaId;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : ParentCategoryRepository
    {
        return $this->repository;
    }

    public function addAction() : bool
    {
        return $this->isNameExist() && $this->getRepository()->add($this);
    }

    public function editAction() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));
        
        return $this->isNameExist()
        && $this->getRepository()->edit($this, array(
                'name',
                'updateTime'
            ));
    }

    protected function isNameExist() : bool
    {
        $filter = array();

        $filter['unique'] = $this->getName();
        if (!empty($this->getId())) {
            $filter['id'] = $this->getId();
        }

        list($parentCategoryList, $count) = $this->getRepository()->filter($filter);
        unset($parentCategoryList);
        
        if (!empty($count)) {
            Core::setLastError(PARAMETER_IS_UNIQUE, array('pointer'=>'parentCategoryName'));
            return false;
        }

        return true;
    }
}
