<?php
namespace Market\Talent\ServiceCategory\Command\ServiceCategory;

use Marmot\Interfaces\ICommand;

class AddServiceCategoryCommand implements ICommand
{
    public $name;

    public $qualificationName;

    public $isQualification;

    public $isEnterpriseVerify;

    public $commission;

    public $status;

    public $parentId;

    public $areaId;

    public $id;

    public function __construct(
        string $name,
        string $qualificationName,
        int $isQualification,
        int $isEnterpriseVerify,
        float $commission,
        int $status,
        int $parentId,
        int $id = 0
    ) {
        $this->name = $name;
        $this->qualificationName = $qualificationName;
        $this->isQualification = $isQualification;
        $this->isEnterpriseVerify = $isEnterpriseVerify;
        $this->commission = $commission;
        $this->status = $status;
        $this->parentId = $parentId;
        $this->id = $id;
    }
}
