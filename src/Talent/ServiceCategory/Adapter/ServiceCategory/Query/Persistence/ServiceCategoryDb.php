<?php
namespace Market\Talent\ServiceCategory\Adapter\ServiceCategory\Query\Persistence;

use Marmot\Framework\Classes\Db;

class ServiceCategoryDb extends Db
{
    public function __construct()
    {
        parent::__construct('market_talent_service_category');
    }
}
