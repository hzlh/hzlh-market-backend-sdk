<?php
namespace Market\Talent\ServiceCategory\Adapter\ServiceCategory\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class ServiceCategoryCache extends Cache
{
    /**
     * 构造函数初始化key和表名一致
     */
    public function __construct()
    {
        parent::__construct('market_talent_service_category');
    }
}
