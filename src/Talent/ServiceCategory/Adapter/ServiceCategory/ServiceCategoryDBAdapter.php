<?php
namespace Market\Talent\ServiceCategory\Adapter\ServiceCategory;

use Market\Talent\ServiceCategory\Model\ServiceCategory;
use Market\Talent\ServiceCategory\Model\NullServiceCategory;
use Market\Talent\ServiceCategory\Translator\ServiceCategoryDBTranslator;
use Market\Talent\ServiceCategory\Adapter\ServiceCategory\Query\ServiceCategoryRowCacheQuery;

use Market\Talent\ServiceCategory\Repository\ParentCategory\ParentCategoryRepository;

use Marmot\Core;

use Common\Adapter\FetchRestfulAdapterTrait;
use Common\Adapter\OperatAbleRestfulAdapterTrait;

class ServiceCategoryDBAdapter implements IServiceCategoryAdapter
{
    use OperatAbleRestfulAdapterTrait, FetchRestfulAdapterTrait;

    private $translator;

    private $rowCacheQuery;

    private $parentCategoryRepository;

    public function __construct()
    {
        $this->translator = new ServiceCategoryDBTranslator();
        $this->rowCacheQuery = new ServiceCategoryRowCacheQuery();
        $this->parentCategoryRepository = new ParentCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowCacheQuery);
        unset($this->parentCategoryRepository);
    }
    
    protected function getDBTranslator() : ServiceCategoryDBTranslator
    {
        return $this->translator;
    }
    
    protected function getRowCacheQuery() : ServiceCategoryRowCacheQuery
    {
        return $this->rowCacheQuery;
    }
    
    protected function getParentCategoryRepository() : ParentCategoryRepository
    {
        return $this->parentCategoryRepository;
    }

    public function fetchOne($id) : ServiceCategory
    {
        $info = array();

        $info = $this->getRowCacheQuery()->getOne($id);

        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullServiceCategory::getInstance();
        }

        $serviceCategory = $this->getDBTranslator()->arrayToObject($info);

        $parentCategory = $this->getParentCategoryRepository()
        ->fetchOne($serviceCategory->getParentCategory()->getId());

        if (!empty($parentCategory)) {
            $serviceCategory->setParentCategory($parentCategory);
        }

        return $serviceCategory;
    }

    public function fetchList(array $ids) : array
    {
        $serviceCategoryList = array();
        
        $serviceCategoryInfoList = $this->getRowCacheQuery()->getList($ids);
        if (empty($serviceCategoryInfoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $translator = $this->getDBTranslator();
        foreach ($serviceCategoryInfoList as $serviceCategoryInfo) {
            $object = $translator->arrayToObject($serviceCategoryInfo);

            $parentIds[$object->getId()] = $object->getParentCategory()->getId();

            $serviceCategoryList[] = $object;
        }

        $parentCategory = $this->getParentCategoryRepository()->fetchList($parentIds);
        if (!empty($parentCategory)) {
            foreach ($serviceCategoryList as $key => $serviceCategory) {
                if (isset($parentCategory[$key])) {
                    $serviceCategory->setParentCategory($parentCategory[$key]);
                }
            }
        }

        return $serviceCategoryList;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $serviceCategory = new ServiceCategory();

            if (isset($filter['status'])) {
                $status = $filter['status'];
                if (is_numeric($status)) {
                    $serviceCategory->setStatus($filter['status']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceCategory,
                        array('status')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($status, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceCategory,
                        array('status')
                    );
                    $condition .= $conjection.key($info).' IN ('.$status.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['areaId'])) {
                $serviceCategory->setAreaId($filter['areaId']);
                $info = $this->getDBTranslator()->objectToArray($serviceCategory, array('areaId'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['name']) && !empty($filter['name'])) {
                $serviceCategory->setName($filter['name']);
                $info = $this->getDBTranslator()->objectToArray($serviceCategory, array('name'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['unique'])) {
                $serviceCategory->setName($filter['unique']);
                $info = $this->getDBTranslator()->objectToArray($serviceCategory, array('name'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['id'])) {
                $serviceCategory->setId($filter['id']);
                $info = $this->getDBTranslator()->objectToArray($serviceCategory, array('id'));
                $condition .= $conjection.key($info).' <> '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['parentCategory']) && !empty($filter['parentCategory'])) {
                $parentCategory = $filter['parentCategory'];
                $info = $this->getDBTranslator()->objectToArray($serviceCategory, array('parentCategory'));
                $condition .= $conjection.key($info).' IN ('.$parentCategory.')';
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $serviceCategory = new ServiceCategory();
            if (isset($sort['updateTime'])) {
                $info = $this->getDBTranslator()->objectToArray($serviceCategory, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['id'])) {
                $info = $this->getDBTranslator()->objectToArray($serviceCategory, array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}
