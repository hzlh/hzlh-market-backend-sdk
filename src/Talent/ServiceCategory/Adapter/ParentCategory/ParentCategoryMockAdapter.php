<?php
namespace Market\Talent\ServiceCategory\Adapter\ParentCategory;

use Common\Model\IOperatAble;
use Common\Adapter\OperatAbleRestfulAdapterTrait;

use Market\Talent\ServiceCategory\Model\ParentCategory;
use Market\Talent\ServiceCategory\Utils\ParentCategory\MockFactory;
use Market\Talent\ServiceCategory\Translator\ParentCategoryDBTranslator;
use Market\Talent\ServiceCategory\Adapter\ParentCategory\Query\ParentCategoryRowCacheQuery;

class ParentCategoryMockAdapter implements IParentCategoryAdapter
{
    use OperatAbleRestfulAdapterTrait;
    
    private $translator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->translator = new ParentCategoryDBTranslator();
        $this->rowCacheQuery = new ParentCategoryRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDBTranslator() : ParentCategoryDBTranslator
    {
        return $this->translator;
    }
    
    protected function getRowCacheQuery() : ParentCategoryRowCacheQuery
    {
        return $this->rowCacheQuery;
    }
    
    public function add(IOperatAble $parentCategory, array $keys = array()) : bool
    {
        unset($parentCategory);
        unset($keys);

        return true;
    }

    public function edit(IOperatAble $parentCategory, array $keys = array()) : bool
    {
        unset($parentCategory);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : ParentCategory
    {
        return MockFactory::generateParentCategory($id);
    }

    public function fetchList(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateParentCategory($id);
        }

        return $parentCategoryList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
