<?php
namespace Market\Talent\ServiceCategory\Adapter\ParentCategory\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class ParentCategoryCache extends Cache
{
    /**
     * 构造函数初始化key和表名一致
     */
    public function __construct()
    {
        parent::__construct('market_talent_parent_category');
    }
}
