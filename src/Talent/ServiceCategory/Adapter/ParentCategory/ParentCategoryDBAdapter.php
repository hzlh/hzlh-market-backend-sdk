<?php
namespace Market\Talent\ServiceCategory\Adapter\ParentCategory;

use Marmot\Core;

use Market\Talent\ServiceCategory\Model\ParentCategory;
use Market\Talent\ServiceCategory\Model\NullParentCategory;
use Market\Talent\ServiceCategory\Translator\ParentCategoryDBTranslator;
use Market\Talent\ServiceCategory\Adapter\ParentCategory\Query\ParentCategoryRowCacheQuery;

use Common\Adapter\FetchRestfulAdapterTrait;
use Common\Adapter\OperatAbleRestfulAdapterTrait;

class ParentCategoryDBAdapter implements IParentCategoryAdapter
{
    use OperatAbleRestfulAdapterTrait, FetchRestfulAdapterTrait;

    private $translator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->translator = new ParentCategoryDBTranslator();
        $this->rowCacheQuery = new ParentCategoryRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDBTranslator() : ParentCategoryDBTranslator
    {
        return $this->translator;
    }
    
    protected function getRowCacheQuery() : ParentCategoryRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    public function fetchOne($id) : ParentCategory
    {
        $info = array();

        $info = $this->getRowCacheQuery()->getOne($id);
        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullParentCategory::getInstance();
        }

        $parentCategory = $this->getDBTranslator()->arrayToObject($info);

        return $parentCategory;
    }

    public function fetchList(array $ids) : array
    {
        $parentCategoryList = array();
        
        $parentCategoryInfoList = $this->getRowCacheQuery()->getList($ids);
        if (empty($parentCategoryInfoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $translator = $this->getDBTranslator();
        foreach ($parentCategoryInfoList as $parentCategoryInfo) {
            $parentCategoryList[] = $translator->arrayToObject($parentCategoryInfo);
        }

        return $parentCategoryList;
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $parentCategory = new ParentCategory();

            if (isset($filter['name']) && !empty($filter['name'])) {
                $parentCategory->setName($filter['name']);
                $info = $this->getDBTranslator()->objectToArray($parentCategory, array('name'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['unique'])) {
                $parentCategory->setName($filter['unique']);
                $info = $this->getDBTranslator()->objectToArray($parentCategory, array('name'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['areaId'])) {
                $parentCategory->setAreaId($filter['areaId']);
                $info = $this->getDBTranslator()->objectToArray($parentCategory, array('areaId'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['id'])) {
                $parentCategory->setId($filter['id']);
                $info = $this->getDBTranslator()->objectToArray($parentCategory, array('id'));
                $condition .= $conjection.key($info).' <> '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $parentCategory = new ParentCategory();
            if (isset($sort['updateTime'])) {
                $info = $this->getDBTranslator()->objectToArray($parentCategory, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['id'])) {
                $info = $this->getDBTranslator()->objectToArray($parentCategory, array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}
