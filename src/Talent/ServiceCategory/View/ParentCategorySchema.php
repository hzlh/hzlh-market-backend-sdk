<?php
namespace Market\Talent\ServiceCategory\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class ParentCategorySchema extends SchemaProvider
{
    protected $resourceType = 'marketParentCategories';

    public function getId($parentCategory) : int
    {
        return $parentCategory->getId();
    }

    public function getAttributes($parentCategory) : array
    {
        return [
            'name' => $parentCategory->getName(),
            'status' => $parentCategory->getStatus(),
            'createTime' => $parentCategory->getCreateTime(),
            'updateTime' => $parentCategory->getUpdateTime(),
            'statusTime' => $parentCategory->getStatusTime()
        ];
    }
}
