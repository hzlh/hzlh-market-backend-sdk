<?php
namespace Market\Talent\Service\Adapter\Service\Query\Persistence;

use Marmot\Framework\Classes\Db;

class ServiceDb extends Db
{
    public function __construct()
    {
        parent::__construct('market_talent_service');
    }
}
