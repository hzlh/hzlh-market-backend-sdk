<?php
namespace Market\Talent\Service\Adapter\Service\Query;

use Marmot\Framework\Query\RowCacheQuery;

class ServiceRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'service_id',
            new Persistence\ServiceCache(),
            new Persistence\ServiceDb()
        );
    }
}
