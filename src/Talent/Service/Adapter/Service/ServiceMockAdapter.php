<?php
namespace Market\Talent\Service\Adapter\Service;

use Market\Talent\Service\Model\Service;
use Market\Talent\Service\Utils\MockFactory;
use Market\Talent\Service\Translator\ServiceDBTranslator;
use Market\Talent\Service\Adapter\Service\Query\ServiceRowCacheQuery;

use Common\Model\IOperatAble;
use Common\Adapter\OperatAbleRestfulAdapterTrait;

class ServiceMockAdapter implements IServiceAdapter
{
    use OperatAbleRestfulAdapterTrait;
    
    private $translator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->translator = new ServiceDBTranslator();
        $this->rowCacheQuery = new ServiceRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDBTranslator() : ServiceDBTranslator
    {
        return $this->translator;
    }
    
    protected function getRowCacheQuery() : ServiceRowCacheQuery
    {
        return $this->rowCacheQuery;
    }
    
    public function add(IOperatAble $service, array $keys = array()) : bool
    {
        unset($service);
        unset($keys);

        return true;
    }

    public function edit(IOperatAble $service, array $keys = array()) : bool
    {
        unset($service);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : Service
    {
        return MockFactory::generateService($id);
    }

    public function fetchList(array $ids) : array
    {
        $serviceList = array();

        foreach ($ids as $id) {
            $serviceList[] = MockFactory::generateService($id);
        }

        return $serviceList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }
}
