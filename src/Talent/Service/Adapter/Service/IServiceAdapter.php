<?php
namespace Market\Talent\Service\Adapter\Service;

use Market\Talent\Service\Model\Service;

use Common\Adapter\IOperatAbleAdapter;

interface IServiceAdapter extends IOperatAbleAdapter
{
    public function fetchOne($id) : Service;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;
}
