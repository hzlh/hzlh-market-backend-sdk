<?php
namespace Market\Talent\Service\Repository\Service;

use Marmot\Framework\Classes\Repository;

use Common\Repository\OperatAbleRepositoryTrait;

use Market\Talent\Service\Model\Service;
use Market\Talent\Service\Adapter\Service\IServiceAdapter;
use Market\Talent\Service\Adapter\Service\ServiceDBAdapter;
use Market\Talent\Service\Adapter\Service\ServiceMockAdapter;

class ServiceRepository extends Repository implements IServiceAdapter
{
    use OperatAbleRepositoryTrait;
    
    private $adapter;
    
    public function __construct()
    {
        $this->adapter = new ServiceDBAdapter();
    }

    public function setAdapter(IServiceAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    protected function getActualAdapter() : IServiceAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IServiceAdapter
    {
        return new ServiceMockAdapter();
    }

    public function fetchOne($id) : Service
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
}
