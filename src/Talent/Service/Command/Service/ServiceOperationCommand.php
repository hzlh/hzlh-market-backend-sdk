<?php
namespace Market\Talent\Service\Command\Service;

use Marmot\Interfaces\ICommand;

abstract class ServiceOperationCommand implements ICommand
{
    public $title;

    public $tag;

    public $detail;

    public $cover;

    public $price;

    public $contract;

    public $serviceObjects;

    public $serviceCategoryId;

    public $id;

    public function __construct(
        string $title,
        string $tag,
        array $detail,
        array $cover,
        array $price,
        array $contract,
        array $serviceObjects,
        int $serviceCategoryId = 0,
        int $id = 0
    ) {
        $this->title = $title;
        $this->tag = $tag;
        $this->detail = $detail;
        $this->cover = $cover;
        $this->price = $price;
        $this->contract = $contract;
        $this->serviceObjects = $serviceObjects;
        $this->serviceCategoryId = $serviceCategoryId;
        $this->id = $id;
    }
}
