<?php
namespace Market\Talent\Service\Controller;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IFetchAbleController;

use Market\Talent\Service\View\ServiceView;
use Market\Talent\Service\Repository\Service\ServiceRepository;

use Marmot\Basecode\Classes\Server;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class FetchController extends Controller implements IFetchAbleController
{
    use JsonApiTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ServiceRepository
    {
        return $this->repository;
    }

    public function fetchOne(int $id)
    {
        $ip = Server::get('HTTP_IP', '');
        $memberId = Server::get('HTTP_MEMBER_ID', 0);
        $sessionId = Server::get('HTTP_SESSION_ID', '');

        $service = $this->getRepository()->fetchOne($id);
        if (!$service instanceof INull) {
            if (!empty($ip) && !empty($sessionId)) {
                $service->addPageViews($ip, $memberId, $sessionId);
            }
            
            $this->renderView(new ServiceView($service));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function fetchList(string $ids)
    {
        $ids = explode(',', $ids);

        $serviceList = array();

        $serviceList = $this->getRepository()->fetchList($ids);

        if (!empty($serviceList)) {
            $this->renderView(new ServiceView($serviceList));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function filter()
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();

        list($serviceList, $count) = $this->getRepository()->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = new ServiceView($serviceList);
            $view->pagination(
                'marketServices',
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
