<?php
namespace Market\Talent\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Talent\Service\Command\Service\AddServiceCommand'=>
        'Market\Talent\Service\CommandHandler\Service\AddServiceCommandHandler',
        'Market\Talent\Service\Command\Service\EditServiceCommand'=>
        'Market\Talent\Service\CommandHandler\Service\EditServiceCommandHandler',
        'Market\Talent\Service\Command\Service\ResubmitServiceCommand'=>
        'Market\Talent\Service\CommandHandler\Service\ResubmitServiceCommandHandler',
        'Market\Talent\Service\Command\Service\ApproveServiceCommand'=>
        'Market\Talent\Service\CommandHandler\Service\ApproveServiceCommandHandler',
        'Market\Talent\Service\Command\Service\RejectServiceCommand'=>
        'Market\Talent\Service\CommandHandler\Service\RejectServiceCommandHandler',
        'Market\Talent\Service\Command\Service\RevokeServiceCommand'=>
        'Market\Talent\Service\CommandHandler\Service\RevokeServiceCommandHandler',
        'Market\Talent\Service\Command\Service\CloseServiceCommand'=>
        'Market\Talent\Service\CommandHandler\Service\CloseServiceCommandHandler',
        'Market\Talent\Service\Command\Service\DeleteServiceCommand'=>
        'Market\Talent\Service\CommandHandler\Service\DeleteServiceCommandHandler',
        'Market\Talent\Service\Command\Service\OnShelfServiceCommand'=>
        'Market\Talent\Service\CommandHandler\Service\OnShelfServiceCommandHandler',
        'Market\Talent\Service\Command\Service\OffStockServiceCommand'=>
        'Market\Talent\Service\CommandHandler\Service\OffStockServiceCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';
  
        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
