<?php
namespace Market\Talent\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;

use Common\Model\IApproveAble;

use Market\Talent\Service\Model\Price;
use Market\Talent\Service\Model\Service;
use Market\Talent\Service\Model\CalculationSubTrait;

use Market\Talent\ServiceCategory\Model\ServiceCategory;
use Market\Talent\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;

trait ServiceCommonCommandHandlerTrait
{
    use CalculationSubTrait;
    
    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return new ServiceCategoryRepository();
    }

    protected function fetchServiceCategory(int $id) : ServiceCategory
    {
        return $this->getServiceCategoryRepository()->fetchOne($id);
    }

    protected function executeAction(ICommand $command, Service $service) : Service
    {
      
        $service->setTitle($command->title);
        $service->setDetail($command->detail);
        $service->setCover($command->cover);
        $service->setContract($command->contract);
        $service->setServiceObjects($command->serviceObjects);
        $service->setPrice($this->price($command->price));
        $service->setServiceCategory($this->fetchServiceCategory($command->serviceCategoryId));
        $service->setTag($command->tag);
        $service->generateBinary($command->tag);

        $service->setApplyStatus(IApproveAble::APPLY_STATUS['APPROVE']);

        return $service;
    }

    private function price($price) : Price
    {
        $minPrice = $this->subMinPrice($price);
        $maxPrice = $this->subMaxPrice($price);

        return new Price($price, $minPrice, $maxPrice);
    }
}
