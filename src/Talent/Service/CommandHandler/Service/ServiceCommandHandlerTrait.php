<?php
namespace Market\Talent\Service\CommandHandler\Service;

use Market\Talent\Service\Model\Service;
use Market\Talent\Service\Repository\Service\ServiceRepository;

trait ServiceCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new ServiceRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : ServiceRepository
    {
        return $this->repository;
    }
    
    protected function fetchService(int $id) : Service
    {
        return $this->getRepository()->fetchOne($id);
    }
}
