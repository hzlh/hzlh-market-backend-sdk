<?php
namespace Market\Talent\Service\CommandHandler\Service;

use Common\Model\IOnShelfAble;
use Common\CommandHandler\OffStockCommandHandler;

class OffStockServiceCommandHandler extends OffStockCommandHandler
{
    use ServiceCommandHandlerTrait;
    
    protected function fetchIOnShelfObject($id) : IOnShelfAble
    {
        return $this->fetchService($id);
    }
}
