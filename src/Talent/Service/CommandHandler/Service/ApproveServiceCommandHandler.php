<?php
namespace Market\Talent\Service\CommandHandler\Service;

use Common\Model\IApproveAble;
use Common\CommandHandler\ApproveCommandHandler;

class ApproveServiceCommandHandler extends ApproveCommandHandler
{
    use ServiceCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchService($id);
    }
}
