<?php
namespace Market\Talent\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Talent\Service\Command\Service\ResubmitServiceCommand;

class ResubmitServiceCommandHandler implements ICommandHandler
{
    use ServiceCommandHandlerTrait, ServiceCommonCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $service = $this->fetchService($command->id);

        $service = $this->executeAction($command, $service);
       
        return $service->resubmit();
    }
}
