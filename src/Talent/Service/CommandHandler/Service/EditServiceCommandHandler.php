<?php
namespace Market\Talent\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Talent\Service\Command\Service\EditServiceCommand;

class EditServiceCommandHandler implements ICommandHandler
{
    use ServiceCommandHandlerTrait, ServiceCommonCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $service = $this->fetchService($command->id);

        $service = $this->executeAction($command, $service);
       
        return $service->edit();
    }
}
