<?php
namespace Market\Talent\Service\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Common\Model\IApproveAble;

use Market\Talent\Authentication\Repository\Authentication\AuthenticationRepository;

trait ValidateTrait
{
    protected function getAuthenticationRepository() : AuthenticationRepository
    {
        return new AuthenticationRepository();
    }

    protected function validate()
    {
        if ($this->getServiceCategory() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY);
            return false;
        }

        // if ($this->getEnterprise() instanceof INull) {
        //     Core::setLastError(PARAMETER_IS_EMPTY);
        //     return false;
        // }

        // if (!$this->isAuthenticate()) {
        //     Core::setLastError(ENTERPRISE_NOT_AUTHENTICATION_SERVICE_CATEGORY);
        //     return false;
        // }

        return true;
    }

    protected function isAuthenticate() : bool
    {
        $filter = array(
            'enterprise' => $this->getEnterprise()->getId(),
            'serviceCategory' => $this->getServiceCategory()->getId(),
            'applyStatus' => IApproveAble::APPLY_STATUS['APPROVE']
        );
        
        list($authentications, $count) = $this->getAuthenticationRepository()->filter($filter);
        
        unset($authentications);

        if (empty($count)) {
            return false;
        }

        return true;
    }
}
