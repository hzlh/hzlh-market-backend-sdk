<?php
namespace Market\Talent\Authentication\Translator;

use Marmot\Interfaces\ITranslator;

use Market\Talent\Authentication\Model\Authentication;
use Market\Talent\Authentication\Model\NullAuthentication;

class AuthenticationDBTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $authentication = null)
    {
        if (!isset($expression['service_provider_authentication_id'])) {
            return NullAuthentication::getInstance();
        }

        if ($authentication == null) {
            $authentication = new Authentication($expression['service_provider_authentication_id']);
        }

        if (isset($expression['enterprise_id'])) {
            $authentication->getEnterprise()->setId($expression['enterprise_id']);
        }
        if (isset($expression['enterprise_name'])) {
            $authentication->setEnterpriseName($expression['enterprise_name']);
        }
        if (isset($expression['service_category_id'])) {
            $authentication->getServiceCategory()->setId($expression['service_category_id']);
        }
        $qualificationImage = array();
        if (!empty($expression['qualification_image'])) {
            if (is_string($expression['qualification_image'])) {
                $qualificationImage = json_decode($expression['qualification_image'], true);
            }
            if (is_array($expression['qualification_image'])) {
                $qualificationImage = $expression['qualification_image'];
            }
        }
        $authentication->setQualificationImage($qualificationImage);
        $authentication->getRejectReason()->setId($expression['reject_reason']);
        $authentication->setApplyStatus($expression['apply_status']);
        $authentication->setStatus($expression['status']);
        $authentication->setStatusTime($expression['status_time']);
        $authentication->setCreateTime($expression['create_time']);
        $authentication->setUpdateTime($expression['update_time']);

        return $authentication;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($authentication, array $keys = array())
    {
        if (!$authentication instanceof Authentication) {
            return false;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'enterprise',
                'enterpriseName',
                'serviceCategory',
                'qualificationImage',
                'rejectReason',
                'applyStatus',
                'status',
                'statusTime',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['service_provider_authentication_id'] = $authentication->getId();
        }
        if (in_array('enterprise', $keys)) {
            $expression['enterprise_id'] = $authentication->getEnterprise()->getId();
        }
        if (in_array('enterpriseName', $keys)) {
            $expression['enterprise_name'] = $authentication->getEnterpriseName();
        }
        if (in_array('serviceCategory', $keys)) {
            $expression['service_category_id'] = $authentication->getServiceCategory()->getId();
        }
        if (in_array('qualificationImage', $keys)) {
            $expression['qualification_image'] = $authentication->getQualificationImage();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['reject_reason'] = $authentication->getRejectReason()->getId();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['apply_status'] = $authentication->getApplyStatus();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $authentication->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $authentication->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $authentication->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $authentication->getUpdateTime();
        }

        return $expression;
    }
}
