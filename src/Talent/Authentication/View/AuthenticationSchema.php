<?php
namespace Market\Talent\Authentication\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class AuthenticationSchema extends SchemaProvider
{
    protected $resourceType = 'authentications';

    public function getId($authentication) : int
    {
        return $authentication->getId();
    }

    public function getAttributes($authentication) : array
    {
        $rejectReason = $authentication->getRejectReason()->getData();
        $rejectReasonSchema = isset($rejectReason['rejectReason']) ? $rejectReason['rejectReason'] : '';

        return [
            'number'  => $authentication->generateNumber(),
            'enterpriseName' => $authentication->getEnterpriseName(),
            'qualificationImage' => $authentication->getQualificationImage(),
            'rejectReason' => $rejectReasonSchema,
            'applyStatus' => $authentication->getApplyStatus(),
            'status' => $authentication->getStatus(),
            'createTime' => $authentication->getCreateTime(),
            'updateTime' => $authentication->getUpdateTime(),
            'statusTime' => $authentication->getStatusTime()
        ];
    }

    public function getRelationships($authentication, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);

        return [
            'enterprise' => [self::DATA => $authentication->getEnterprise()],
            'serviceCategory' => [self::DATA => $authentication->getServiceCategory()],
            'strategyType' => [self::DATA => $authentication->getStrategyType()]
        ];
    }
}
