<?php
namespace Market\Talent\Authentication\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IApproveAbleController;

use Market\Talent\Authentication\Model\Authentication;
use Market\Talent\Authentication\View\AuthenticationView;
use Market\Talent\Authentication\Repository\Authentication\AuthenticationRepository;
use Market\Talent\Authentication\Command\Authentication\RejectAuthenticationCommand;
use Market\Talent\Authentication\Command\Authentication\ApproveAuthenticationCommand;
use Market\Talent\Authentication\CommandHandler\Authentication\AuthenticationCommandHandlerFactory;

use WidgetRules\Common\WidgetRules;

class ApproveController extends Controller implements IApproveAbleController
{
    use JsonApiTrait;

    private $repository;

    private $commandBus;

    private $widgetRules;
    
    public function __construct()
    {
        parent::__construct();
        $this->repository = new AuthenticationRepository();
        $this->commandBus = new CommandBus(new AuthenticationCommandHandlerFactory());
        $this->widgetRules = WidgetRules::getInstance();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
        unset($this->widgetRules);
    }

    protected function getAuthenticationRepository() : AuthenticationRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getWidgetRules() : WidgetRules
    {
        return $this->widgetRules;
    }

    public function approve(int $id)
    {
        $command = new ApproveAuthenticationCommand($id);

        if ($this->getCommandBus()->send($command)) {
            $authentication = $this->getAuthenticationRepository()->fetchOne($command->id);

            if ($authentication instanceof Authentication) {
                $this->renderView(new AuthenticationView($authentication));
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 申请信息审核驳回功能,通过PATCH传参
     * @param int id 申请信息id
     *
     * @return jsonApi
     */
    public function reject(int $id)
    {
        $data = $this->getRequest()->patch('data');

        $rejectReason = isset($data['attributes']['rejectReason'])
        ? $data['attributes']['rejectReason']
        : '';

        if ($this->validateRejectScenario($rejectReason)) {
            $command = new RejectAuthenticationCommand(
                $rejectReason,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $authentication = $this->getAuthenticationRepository()->fetchOne($command->id);

                if ($authentication instanceof Authentication) {
                    $this->renderView(new AuthenticationView($authentication));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateRejectScenario(
        $rejectReason
    ) {
        return $this->getWidgetRules()->rejectReason($rejectReason);
    }
}
