<?php
namespace Market\Talent\Authentication\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IOperatAbleController;

use Market\Talent\Authentication\Model\Authentication;
use Market\Talent\Authentication\View\AuthenticationView;
use Market\Talent\Authentication\Repository\Authentication\AuthenticationRepository;
use Market\Talent\Authentication\Command\Authentication\AddAuthenticationCommand;
use Market\Talent\Authentication\CommandHandler\Authentication\AuthenticationCommandHandlerFactory;

class OperationController extends Controller implements IOperatAbleController
{
    use JsonApiTrait, AuthenticationValidateTrait;

    private $repository;

    private $commandBus;
    
    public function __construct()
    {
        parent::__construct();
        $this->repository = new AuthenticationRepository();
        $this->commandBus = new CommandBus(new AuthenticationCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : AuthenticationRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    /**
     * 对应路由 /authentications
     * 新闻新增功能, 通过POST传参
     * @param jsonApi
     * @return jsonApi
     */
    public function add()
    {
        $data = $this->getRequest()->post('data');
        $relationships = $data['relationships'];

        $enterpriseId = $relationships['enterprise']['data'][0]['id'];
        
        $qualificationList = $relationships['qualifications']['data'];

        $qualifications = array();
        foreach ($qualificationList as $qualification) {
            $qualifications[] =
            array(
                'serviceCategoryId' => $qualification['relationships']['serviceCategory']['data'][0]['id'],
                'image' => isset($qualification['attributes']['image']) ?
                    $qualification['attributes']['image'] :
                    array()
            );
        }

        if ($this->validateAddScenario($qualifications, $enterpriseId)) {
            $commandBus = $this->getCommandBus();

            $command = new AddAuthenticationCommand(
                $qualifications,
                $enterpriseId
            );

            if ($commandBus->send($command)) {
                $authentication = $this->getRepository()->fetchOne($command->id);
      
                if ($authentication instanceof Authentication) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new AuthenticationView($authentication));
                    return true;
                }
            }
        }
echo 1;
        $this->displayError();
        return false;
    }

    public function edit(int $id)
    {
        unset($id);
        $this->displayError();
        return false;
    }
}
