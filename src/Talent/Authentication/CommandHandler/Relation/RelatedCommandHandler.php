<?php
namespace Market\Talent\Authentication\CommandHandler\Relation;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Market\Talent\Authentication\Command\Relation\RelatedCommand;

class RelatedCommandHandler implements ICommandHandler
{
    use RelatedCommonCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof RelatedCommand)) {
            throw new \InvalidArgumentException;
        }

        $serviceProvider = $this->getServiceProviderRepository()->fetchOne($command->serviceProviderId);
        $strategyType = $this->getStrategyTypeRepository()->fetchOne($command->strategyTypeId);
        $crew = $this->getCrewRepository()->fetchOne($command->crewId);

        if ($this->validateServiceProvider($serviceProvider)
            && $this->validateCrew($crew)
            && $this->validateStrategyType($strategyType)
        ) {
            $relation = $this->getRelation();
            $relation->setId($command->serviceProviderId);
            $relation->setServiceProvider($serviceProvider);
            $relation->setStrategyType($strategyType);
            $relation->setCrew($crew);

            $relationInfo = $this->getRepository()->fetchOne($command->serviceProviderId);

            if (!$relationInfo->getId()) {
                return $relation->add();
            }

            return $relation->edit();
        }

        return false;
    }
}
