<?php
namespace Market\Talent\Authentication\CommandHandler\Relation;

use Marmot\Framework\Classes\NullCommandHandler;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;

class RelatedCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Talent\Authentication\Command\Relation\RelatedCommand'=>
            'Market\Talent\Authentication\CommandHandler\Relation\RelatedCommandHandler',
        'Market\Talent\Authentication\Command\Relation\CancelRelatedCommand'=>
            'Market\Talent\Authentication\CommandHandler\Relation\CancelRelatedCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
