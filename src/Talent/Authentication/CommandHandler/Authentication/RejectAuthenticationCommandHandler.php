<?php
namespace Market\Talent\Authentication\CommandHandler\Authentication;

use Common\Model\IApproveAble;
use Common\CommandHandler\RejectCommandHandler;

use Market\Talent\Authentication\Repository\Authentication\AuthenticationRepository;

class RejectAuthenticationCommandHandler extends RejectCommandHandler
{
    use AuthenticationCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchAuthentication($id);
    }
}
