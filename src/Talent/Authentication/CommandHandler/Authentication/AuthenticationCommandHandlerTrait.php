<?php
namespace Market\Talent\Authentication\CommandHandler\Authentication;

use Market\Talent\Authentication\Model\Authentication;
use Market\Talent\Authentication\Repository\Authentication\AuthenticationRepository;

trait AuthenticationCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new AuthenticationRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : AuthenticationRepository
    {
        return $this->repository;
    }
    
    protected function fetchAuthentication(int $id) : Authentication
    {
        return $this->getRepository()->fetchOne($id);
    }
}
