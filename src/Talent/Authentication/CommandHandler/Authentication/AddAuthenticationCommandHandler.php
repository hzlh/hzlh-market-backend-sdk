<?php
namespace Market\Talent\Authentication\CommandHandler\Authentication;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Talent\Authentication\Model\Authentication;
use Market\Talent\Authentication\Command\Authentication\AddAuthenticationCommand;

use Member\Enterprise\Model\Enterprise;
use Member\Enterprise\Repository\Enterprise\EnterpriseRepository;

use Market\Talent\ServiceCategory\Model\ServiceCategory;
use Market\Talent\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;

class AddAuthenticationCommandHandler implements ICommandHandler
{
    private $enterpriseRepository;

    private $serviceCategoryRepository;

    public function __construct()
    {
        $this->enterpriseRepository = new EnterpriseRepository();
        $this->serviceCategoryRepository = new ServiceCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->enterpriseRepository);
        unset($this->serviceCategoryRepository);
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }

    protected function fetchEnterprise(int $id) : Enterprise
    {
        return $this->getEnterpriseRepository()->fetchOne($id);
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return $this->serviceCategoryRepository;
    }

    protected function fetchServiceCategoryList(array $ids) : array
    {
        return $this->getServiceCategoryRepository()->fetchList($ids);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddAuthenticationCommand)) {
            throw new \InvalidArgumentException;
        }

        $enterprise = $this->fetchEnterprise($command->enterpriseId);

        $qualificationList = $this->qualificationList($command->qualifications);

        foreach ($qualificationList as $value) {
            $authentication = new Authentication();
            $authentication->setEnterprise($enterprise);
            $authentication->setEnterpriseName($enterprise->getName());
            $authentication->setServiceCategory($value['serviceCategory']);
            $authentication->setQualificationImage($value['image']);

            if (!$authentication->add()) {
                return false;
            }

            $command->id = $authentication->getId();
        }

        if ($command->id) {
            return true;
        }
        
        return false;
    }

    private function qualificationList($qualifications) : array
    {
        $ids = array();
        $images = array();
        $qualificationList = array();

        foreach ($qualifications as $qualification) {
            $ids[] = $qualification['serviceCategoryId'];
            $images[$qualification['serviceCategoryId']] = $qualification['image'];
        }

        $serviceCategoryList = $this->fetchServiceCategoryList($ids);

        foreach ($serviceCategoryList as $serviceCategory) {
            $image = isset($images[$serviceCategory->getId()]) ? $images[$serviceCategory->getId()] : array();

            $qualificationList[] = array(
                'serviceCategory' => $serviceCategory,
                'image' => $image
            );
        }

        return $qualificationList;
    }
}
