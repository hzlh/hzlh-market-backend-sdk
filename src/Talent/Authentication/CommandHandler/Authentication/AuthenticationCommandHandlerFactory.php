<?php
namespace Market\Talent\Authentication\CommandHandler\Authentication;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class AuthenticationCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Talent\Authentication\Command\Authentication\AddAuthenticationCommand'=>
        'Market\Talent\Authentication\CommandHandler\Authentication\AddAuthenticationCommandHandler',
        'Market\Talent\Authentication\Command\Authentication\ResubmitAuthenticationCommand'=>
        'Market\Talent\Authentication\CommandHandler\Authentication\ResubmitAuthenticationCommandHandler',
        'Market\Talent\Authentication\Command\Authentication\ApproveAuthenticationCommand'=>
        'Market\Talent\Authentication\CommandHandler\Authentication\ApproveAuthenticationCommandHandler',
        'Market\Talent\Authentication\Command\Authentication\RejectAuthenticationCommand'=>
        'Market\Talent\Authentication\CommandHandler\Authentication\RejectAuthenticationCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
