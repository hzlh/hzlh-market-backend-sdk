<?php
namespace Market\Talent\Authentication\CommandHandler\Authentication;

use Common\Model\IApproveAble;
use Common\CommandHandler\ApproveCommandHandler;

class ApproveAuthenticationCommandHandler extends ApproveCommandHandler
{
    use AuthenticationCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchAuthentication($id);
    }
}
