<?php
namespace Market\Talent\Authentication\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;
use Marmot\Core;

use Common\Model\IOperatAble;
use Common\Model\OperatAbleTrait;
use Market\Talent\Authentication\Adapter\Relation\IRelationAdapter;
use Market\Talent\Authentication\Repository\Authentication\AuthenticationRepository;
use Market\Talent\Authentication\Repository\Relation\RelationRepository;
use Backend\Crew\Model\Crew;
use Strategy\Model\NullType;
use Strategy\Model\Type;

class Relation implements IOperatAble, IObject
{
    use Object, OperatAbleTrait;

    const STATUS = array(
        'ENABLE' => 0,
        'DISABLE' => -2
    );

    private $id;

    private $serviceProvider;

    private $strategyType;

    private $crew;

    private $repository;

    private $serviceProviderRepository;

    public function __construct($id = 0)
    {
        $this->id = $id;
        $this->serviceProvider = new NullAuthentication();
        $this->strategyType = new NullType();
        $this->crew = new Crew();

        $this->status = self::STATUS['ENABLE'];
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->repository = new RelationRepository();
        $this->serviceProviderRepository = new AuthenticationRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->serviceProvider);
        unset($this->strategyType);
        unset($this->enterprriseRepository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function getId()
    {
        return $this->id;
    }

    public function setServiceProvider(Authentication $serviceProvider) : void
    {
        $this->serviceProvider = $serviceProvider;
    }
    public function getServiceProvider() : Authentication
    {
        return $this->serviceProvider;
    }

    public function setStrategyType(Type $strategyType) : void
    {
        $this->strategyType = $strategyType;
    }
    public function getStrategyType() : Type
    {
        return $this->strategyType;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }
    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function getRepository() : IRelationAdapter
    {
        return $this->repository;
    }

    public function setStatus(int $status) : void
    {
        $status = in_array($status, self::STATUS) ? $status : self::STATUS['ENABLE'];
        $this->status = $status;
    }

    protected function addAction() : bool
    {
        return $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return $this->getRepository()->edit(
            $this,
            array(
                'id',
                'serviceProvider',
                'strategyType',
                'updateTime'
            )
        );
    }
}
