<?php
namespace Market\Talent\Authentication\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Common\Model\NullOperatAbleTrait;

class NullRelation extends Relation implements INull
{
    use NullOperatAbleTrait;

    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
