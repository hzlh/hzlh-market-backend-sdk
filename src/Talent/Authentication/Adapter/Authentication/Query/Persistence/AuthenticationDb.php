<?php
namespace Market\Talent\Authentication\Adapter\Authentication\Query\Persistence;

use Marmot\Framework\Classes\Db;

class AuthenticationDb extends Db
{
    public function __construct()
    {
        parent::__construct('market_talent_service_provider_authentication');
    }
}
