<?php
namespace Market\Talent\Authentication\Command\Authentication;

use Marmot\Interfaces\ICommand;

abstract class AuthenticationCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id = 0
    ) {
        $this->id = $id;
    }
}
