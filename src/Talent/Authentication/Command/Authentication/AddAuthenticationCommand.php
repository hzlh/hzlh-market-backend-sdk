<?php
namespace Market\Talent\Authentication\Command\Authentication;

class AddAuthenticationCommand extends AuthenticationCommand
{
    public $qualifications;

    public $enterpriseId;

    public function __construct(
        array $qualifications,
        int $enterpriseId = 0,
        int $id = 0
    ) {
        parent::__construct($id);

        $this->qualifications = $qualifications;
        $this->enterpriseId = $enterpriseId;
    }
}
