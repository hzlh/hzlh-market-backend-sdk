<?php
namespace Market\Order\Model;

use Marmot\Core;
use Market\Order\Repository\OrderCommodity\OrderCommodityRepository;
use Snapshot\Model\Snapshot;
use Market\Service\Service\Model\NullService;

class OrderCommodity
{
    private $id;

    private $snapshot;

    private $number;

    private $skuIndex;

    private $commodity;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->snapshot = new Snapshot();
        $this->number = 0;
        $this->skuIndex = 0;
        $this->commodity = new NullService();
        $this->repository = new OrderCommodityRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->snapshot);
        unset($this->number);
        unset($this->skuIndex);
        unset($this->commodity);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setSnapshot(Snapshot $snapshot)
    {
        $this->snapshot = $snapshot;
    }

    public function getSnapshot() : Snapshot
    {
        return $this->snapshot;
    }
    
    public function setNumber(int $number)
    {
        $this->number = $number;
    }

    public function getNumber() : int
    {
        return $this->number;
    }
    
    public function setSkuIndex(int $skuIndex)
    {
        $this->skuIndex = $skuIndex;
    }

    public function getSkuIndex() : int
    {
        return $this->skuIndex;
    }
    
    public function setCommodity(ITradeAble $commodity)
    {
        $this->commodity = $commodity;
    }

    public function getCommodity() : ITradeAble
    {
        return $this->commodity;
    }

    public function getRepository() : OrderCommodityRepository
    {
        return $this->repository;
    }

    public function add(Order $order) : bool
    {
        if (!$this->isValidate()) {
            return false;
        }

        return $this->getRepository()->add($order, $this, array(
            'id',
            'snapshot',
            'number',
            'skuIndex'
        ));
    }

    private function isValidate() : bool
    {
        return $this->isLatestSnapshot() && $this->isSkuIndexExist();
    }

    private function isLatestSnapshot() : bool
    {
        $snapshotList = $this->getCommodity()->getSnapshots();

        $snapshotIds = array();

        foreach ($snapshotList as $snapshot) {
            $snapshotIds[] = $snapshot->getId();
        }

        $snapshotId = $this->getSnapshot()->getId();

        if (!in_array($snapshotId, $snapshotIds)) {
            Core::setLastError(COMMODITY_FAILURE);
            return false;
        }

        $latestId = max($snapshotIds);
        
        if ($latestId != $snapshotId) {
            Core::setLastError(COMMODITY_FAILURE);
            return false;
        }

        return true;
    }

    private function isSkuIndexExist() : bool
    {
        $skuList = $this->getCommodity()->getPrice()->getPrice();

        if (!array_key_exists($this->getSkuIndex(), $skuList)) {
            Core::setLastError(SKU_FAILURE);
            return false;
        }

        return true;
    }
}
