<?php
namespace Market\Order\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullOrderAddress extends OrderAddress implements INull
{
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function add(Order $order) : bool
    {
        return $this->resourceNotExist();
    }
}
