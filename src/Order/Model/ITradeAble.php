<?php
namespace Market\Order\Model;

use Market\Service\Service\Model\Price;

interface ITradeAble
{
    const ORDER_CATEGORY = array(
        'NULL' =>0,
        'SERVICE'=>1
    );

    public function getPrice() : Price;
    
    public function getSnapshots() : array;
}
