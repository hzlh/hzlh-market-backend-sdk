<?php
namespace Market\Order\Model;

use Marmot\Core;
use Snapshot\Model\Snapshot;

use Member\DeliveryAddress\Model\DeliveryAddress;

use Market\Order\Repository\OrderAddress\OrderAddressRepository;

class OrderAddress
{
    private $id;

    private $snapshot;

    private $cellphone;

    private $deliveryAddress;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->snapshot = new Snapshot();
        $this->deliveryAddress = new DeliveryAddress();
        $this->cellphone = '';
        $this->repository = new OrderAddressRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->snapshot);
        unset($this->deliveryAddress);
        unset($this->cellphone);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setSnapshot(Snapshot $snapshot)
    {
        $this->snapshot = $snapshot;
    }

    public function getSnapshot() : Snapshot
    {
        return $this->snapshot;
    }
    
    public function setDeliveryAddress(DeliveryAddress $deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    public function getDeliveryAddress() : DeliveryAddress
    {
        return $this->deliveryAddress;
    }
    
    public function setCellphone(string $cellphone)
    {
        $this->cellphone = $cellphone;
    }

    public function getCellphone() : string
    {
        return $this->cellphone;
    }

    public function getRepository() : OrderAddressRepository
    {
        return $this->repository;
    }

    public function add(Order $order) : bool
    {
        if (!$this->isLatestSnapshot()) {
            return false;
        }

        return $this->getRepository()->add($order, $this);
    }

    private function isLatestSnapshot() : bool
    {
        $snapshotList = $this->getDeliveryAddress()->getSnapshots();

        $snapshotIds = array();

        foreach ($snapshotList as $snapshot) {
            $snapshotIds[] = $snapshot->getId();
        }

        $snapshotId = $this->getSnapshot()->getId();

        if (!in_array($snapshotId, $snapshotIds)) {
            Core::setLastError(DELIVERY_ADDRESS_FAILURE);
            return false;
        }

        $latestId = max($snapshotIds);
        
        if ($latestId != $snapshotId) {
            Core::setLastError(DELIVERY_ADDRESS_FAILURE);
            return false;
        }

        return true;
    }

    public function edit() : bool
    {
        if (!$this->isLatestSnapshot()) {
            return false;
        }

        return $this->getRepository()->edit($this);
    }
}
