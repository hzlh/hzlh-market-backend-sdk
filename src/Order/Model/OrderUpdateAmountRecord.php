<?php
namespace Market\Order\Model;

use Marmot\Core;

use Market\Order\Repository\OrderUpdateAmountRecord\OrderUpdateAmountRecordRepository;

class OrderUpdateAmountRecord
{
    private $id;

    private $amount;

    private $remark;

    private $createTime;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->amount = 0.0;
        $this->remark = '';
        $this->createTime = Core::$container->get('time');
        $this->repository = new OrderUpdateAmountRecordRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->amount);
        unset($this->remark);
        unset($this->createTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setAmount(float $amount) : void
    {
        $this->amount = $amount;
    }

    public function getAmount() : float
    {
        return $this->amount;
    }
    
    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }

    public function setCreateTime(int $createTime) : void
    {
        $this->createTime = $createTime;
    }

    public function getCreateTime() : int
    {
        return $this->createTime;
    }

    public function getRepository() : OrderUpdateAmountRecordRepository
    {
        return $this->repository;
    }

    public function add(Order $order) : bool
    {
        return $this->getRepository()->add($order, $this);
    }
}
