<?php
namespace Market\Order\Model;

use Member\MemberCoupon\Model\MemberCoupon;

abstract class CommonOrder extends Order
{
    private $orderCommodities;

    private $orderAddress;

    private $memberCoupons;

    public function __construct()
    {
        parent::__construct();
        $this->orderCommodities = array();
        $this->orderAddress = new OrderAddress();
        $this->memberCoupons = array();
    }

    public function __destruct()
    {
        unset($this->orderCommodities);
        unset($this->orderAddress);
        unset($this->memberCoupons);
    }

    public function addOrderCommodity(OrderCommodity $orderCommodity) : void
    {
        $this->orderCommodities[] = $orderCommodity;
    }

    public function getOrderCommodities() : array
    {
        return $this->orderCommodities;
    }

    public function setOrderAddress(OrderAddress $orderAddress) : void
    {
        $this->orderAddress = $orderAddress;
    }

    public function getOrderAddress() : OrderAddress
    {
        return $this->orderAddress;
    }

    public function addMemberCoupon(MemberCoupon $memberCoupon) : void
    {
        $this->memberCoupons[] = $memberCoupon;
    }

    public function getMemberCoupons() : array
    {
        return $this->memberCoupons;
    }

    public function clearMemberCoupons() : void
    {
        $this->memberCoupons = [];
    }
}
