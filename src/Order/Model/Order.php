<?php
namespace Market\Order\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Member\Enterprise\Model\Enterprise;
use Member\Member\Model\MemberAccount;

use Market\Order\Adapter\Order\IOrderAdapter;

use Market\Payment\Model\Payment;

abstract class Order implements IObject
{
    use Object;

    const STATUS = array(
        "PENDING" => 0,  //待付款
        "PAID" => 2,  //已付款
        "PERFORMANCE_BEGIN" => 4,  //履约开始
        "PERFORMANCE_END" => 6,  //履约结束
        "BUYER_CONFIRMATION" => 8,  //买家确认
        "PLATFORM_CONFIRMATION" => 9,  //平台自动确认
        "ORDER_COMPLETION" => 10,  //订单完成
        "BUYER_CANCEL_ORDER" => -2,  //买家订单取消
        "SELLER_CANCEL_ORDER" => -3,  //卖家订单取消
        "PLATFORM_CANCEL_ORDER" => -4,  //平台订单取消
        "PAY_FAILURE" => -6, //支付失败
        "REFUNDING" => -8, //退款中
        "REFUNDED" => -10 //已退款
    );
    
    const BUYER_ORDER_STATUS = array(
        "NORMAL" => 0,  //正常
        "DELETE" => -2,  //删除
        "PERMANENT_DELETE" => -4,  //永久删除
    );

    const SELLER_ORDER_STATUS = array(
        "NORMAL" => 0,  //正常
        "DELETE" => -2,  //删除
        "PERMANENT_DELETE" => -4,  //永久删除
    );

    //买家取消订单原因
    const BUYER_CANCEL_REASON = array(
        "OTHER" => 0,  //其他
        "NOT_WANT_BUY" => 1,  //我不想买了
        "INFORMATION_FILLIN_WRONG" => 2,  //信息填写错误
        "SHORTAGE" => 3,  //缺货
        "PUSH_THE_BUTTON_WRONG" => 4,  //按错了
        "PAYMENT_PROBLEM" => 5,  //付款遇到问题
    );

    //卖家取消订单原因
    const SELLER_CANCEL_REASON = array(
        "OTHER" => 0,  //其他
        "UNABLE_TO_CONTACT_BUYER" => 1,  //无法联系上商家
        "BUYER_WRONG_OR_REMAKE" => 2,  //买家误拍或重拍
        "BUYER_INSINCERE" => 3,  //买家无诚意完成交易
        "SHORTAGE" => 4,  //缺货无法交易
    );

    const TIME_RECORD = array(
        self::STATUS['PENDING'] => '',
        self::STATUS['PAID'] => '',
        self::STATUS['PERFORMANCE_BEGIN'] => '',
        self::STATUS['PERFORMANCE_END'] => '',
        self::STATUS['BUYER_CONFIRMATION'] => '',
        self::STATUS['PLATFORM_CONFIRMATION'] => '',
        self::STATUS['ORDER_COMPLETION'] => '',
        self::STATUS['BUYER_CANCEL_ORDER'] => '',
        self::STATUS['SELLER_CANCEL_ORDER'] => '',
        self::STATUS['PLATFORM_CANCEL_ORDER'] => '',
        self::STATUS['PAY_FAILURE'] => '',
        self::STATUS['REFUNDING'] => '',
        self::STATUS['REFUNDED'] => ''
    );

    const NATURAL_PERSON_COUNT = 1;

    const ORDERNO_PREFIX = 1001;
    const MEMBER_ID_COUNT = 6;
    const MEMBER_ID_COMPLEMENT_DIGIT = 0;
    const RANDOM_NUMBER_MIN = 00000;
    const RANDOM_NUMBER_MAX = 99999;
    const TRANSACTION_RANDOM_NUMBER_MIN = 0000000000;
    const TRANSACTION_RANDOM_NUMBER_MAX = 9999999999;

    private $id;

    private $orderno;

    private $buyerMemberAccount;

    private $sellerEnterprise;

    private $totalPrice;

    private $paidAmount;

    private $collectedAmount;

    private $payment;

    private $category;

    private $cancelReason;

    private $buyerOrderStatus;

    private $sellerOrderStatus;

    private $remark;

    private $failureReason;

    private $timeRecord;

    private $commission;//佣金

    private $subsidy;//补贴

    private $realAmount;//计算佣金和补贴的商家实收

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->orderno = '';
        $this->buyerMemberAccount = new MemberAccount();
        $this->sellerEnterprise = new Enterprise();
        $this->totalPrice = 0.0;
        $this->paidAmount = 0.0;
        $this->collectedAmount = 0.0;
        $this->commission = 0.0;
        $this->subsidy = 0.0;
        $this->realAmount = 0.0;
        $this->payment = new Payment();
        $this->category = ITradeAble::ORDER_CATEGORY['NULL'];
        $this->cancelReason = self::BUYER_CANCEL_REASON['OTHER'];
        $this->status = self::STATUS['PENDING'];
        $this->remark = '';
        $this->failureReason = array();
        $this->timeRecord = self::TIME_RECORD;
        $this->buyerOrderStatus = self::BUYER_ORDER_STATUS['NORMAL'];
        $this->sellerOrderStatus = self::SELLER_ORDER_STATUS['NORMAL'];
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->orderno);
        unset($this->buyerMemberAccount);
        unset($this->sellerEnterprise);
        unset($this->totalPrice);
        unset($this->paidAmount);
        unset($this->collectedAmount);
        unset($this->payment);
        unset($this->category);
        unset($this->cancelReason);
        unset($this->status);
        unset($this->remark);
        unset($this->failureReason);
        unset($this->timeRecord);
        unset($this->buyerOrderStatus);
        unset($this->sellerOrderStatus);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function generateTransactionNumber() : string
    {
        $createTime = date('Ymd', $this->getCreateTime());

        $randomNumber = mt_rand(self::TRANSACTION_RANDOM_NUMBER_MIN, self::TRANSACTION_RANDOM_NUMBER_MAX);

        return $createTime.self::ORDERNO_PREFIX.$randomNumber;
    }

    public function generateOrderno() : void
    {
        $createTime = date('Ymd', $this->getCreateTime());
        $memberId = str_pad(
            $this->getBuyerMemberAccount()->getId(),
            self::MEMBER_ID_COUNT,
            self::MEMBER_ID_COMPLEMENT_DIGIT,
            STR_PAD_LEFT
        );

        $randomNumber = mt_rand(self::RANDOM_NUMBER_MIN, self::RANDOM_NUMBER_MAX);

        $this->setOrderno(self::ORDERNO_PREFIX.$memberId.$createTime.$randomNumber);
    }

    public function setOrderno(string $orderno) : void
    {
        $this->orderno = $orderno;
    }

    public function getOrderno() : string
    {
        return $this->orderno;
    }

    public function setBuyerMemberAccount(MemberAccount $buyerMemberAccount) : void
    {
        $this->buyerMemberAccount = $buyerMemberAccount;
    }

    public function getBuyerMemberAccount() : MemberAccount
    {
        return $this->buyerMemberAccount;
    }
    
    public function setSellerEnterprise(Enterprise $sellerEnterprise) : void
    {
        $this->sellerEnterprise = $sellerEnterprise;
    }

    public function getSellerEnterprise() : Enterprise
    {
        return $this->sellerEnterprise;
    }

    public function setTotalPrice(float $totalPrice) : void
    {
        $this->totalPrice = $totalPrice;
    }

    public function getTotalPrice() : float
    {
        return $this->totalPrice;
    }

    public function setPaidAmount(float $paidAmount) : void
    {
        $this->paidAmount = $paidAmount;
    }

    public function getPaidAmount() : float
    {
        return $this->paidAmount;
    }

    public function setCommission(float $commission) : void
    {
        $this->commission = $commission;
    }

    public function getCommission() : float
    {
        return bcmul($this->getCollectedAmount(), bcdiv($this->commission, 100, 2), 2);
    }

    public function getCommissionProportion() : float
    {
        return $this->commission;
    }

    public function setSubsidy(float $subsidy) : void
    {
        $this->subsidy  = $subsidy;
    }

    public function getSubsidy() : float
    {
        return bcmul($this->getCollectedAmount(), bcdiv($this->subsidy, 100, 2), 2);
    }

    public function getSubsidyProportion() : float
    {
        return $this->subsidy;
    }

    public function setRealAmount(float $realAmount) : void
    {
        $this->realAmount = $realAmount;
    }

    public function getRealAmount() : float
    {
        return $this->realAmount;
    }

    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }
    
    public function setCollectedAmount(float $collectedAmount) : void
    {
        $this->collectedAmount = $collectedAmount;
    }

    public function getCollectedAmount() : float
    {
        return $this->collectedAmount;
    }

    public function setPayment(Payment $payment) : void
    {
        $this->payment = $payment;
    }

    public function getPayment() : Payment
    {
        return $this->payment;
    }
    
    public function setCategory(int $category)
    {
        $this->category = in_array($category, ITradeAble::ORDER_CATEGORY) ?
        $category :
        ITradeAble::ORDER_CATEGORY['NULL'];
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setCancelReason(int $cancelReason)
    {
        $this->cancelReason = $cancelReason;
    }

    public function getCancelReason() : int
    {
        return $this->cancelReason;
    }
    
    public function setFailureReason(array $failureReason)
    {
        $this->failureReason = $failureReason;
    }

    public function getFailureReason() : array
    {
        return $this->failureReason;
    }

    public function setTimeRecord(array $timeRecord)
    {
        $this->timeRecord = $timeRecord;
    }

    public function getTimeRecord() : array
    {
        return $this->timeRecord;
    }

    public function setStatus(int $status) : void
    {
        $this->status= in_array($status, self::STATUS) ? $status : self::STATUS['PENDING'];
    }

    public function setBuyerOrderStatus(int $buyerOrderStatus) : void
    {
        $this->buyerOrderStatus = in_array($buyerOrderStatus, self::BUYER_ORDER_STATUS) ?
                            $buyerOrderStatus :
                            self::BUYER_ORDER_STATUS['NORMAL'];
    }

    public function getBuyerOrderStatus() : int
    {
        return $this->buyerOrderStatus;
    }

    public function setSellerOrderStatus(int $sellerOrderStatus) : void
    {
        $this->sellerOrderStatus = in_array($sellerOrderStatus, self::SELLER_ORDER_STATUS) ?
                            $sellerOrderStatus :
                            self::SELLER_ORDER_STATUS['NORMAL'];
    }

    public function getSellerOrderStatus() : int
    {
        return $this->sellerOrderStatus;
    }
    
    public function placeOrder() : bool
    {
        return $this->placeOrderAction();
    }

    abstract protected function placeOrderAction() : bool;

    public function pay() : bool
    {
        if (!($this->isPending() || $this->isPayFailure())) {
            Core::setLastError(ORDER_STATUS_NOT_PENDING);
            return false;
        }

        $timeRecord = $this->getTimeRecord();
        $timeRecord[self::STATUS['PAID']] = Core::$container->get('time');

        $this->setStatus(self::STATUS['PAID']);
        $this->setTimeRecord($timeRecord);
        $this->setUpdateTime(Core::$container->get('time'));
        $this->setStatusTime(Core::$container->get('time'));

        return $this->payAction();
    }
    
    public function isPayFailure() : bool
    {
        return $this->getStatus() == self::STATUS['PAY_FAILURE'];
    }

    public function buyerCancel() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(ORDER_STATUS_NOT_PENDING);
            return false;
        }

        return $this->updateStatus(self::STATUS['BUYER_CANCEL_ORDER']);
    }

    public function sellerCancel() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(ORDER_STATUS_NOT_PENDING);
            return false;
        }

        return $this->updateStatus(self::STATUS['SELLER_CANCEL_ORDER']);
    }

    public function isPending() : bool
    {
        return $this->getStatus() == self::STATUS['PENDING'];
    }

    abstract protected function payAction() : bool;

    protected function updateStatus(int $status) : bool
    {
        $timeRecord = $this->getTimeRecord();
        $timeRecord[$status] = Core::$container->get('time');

        $this->setTimeRecord($timeRecord);

        $this->setStatus($status);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));
        return $this->getRepository()->edit(
            $this,
            array(
                'statusTime',
                'status',
                'updateTime',
                'cancelReason',
                'timeRecord'
            )
        );
    }

    abstract protected function getRepository();

    public function performanceBegin() : bool
    {
        if (!$this->isPaid()) {
            Core::setLastError(ORDER_STATUS_NOT_PAID);
            return false;
        }

        return $this->updateStatus(self::STATUS['PERFORMANCE_BEGIN']);
    }

    public function isPaid() : bool
    {
        return $this->getStatus() == self::STATUS['PAID'];
    }

    public function performanceEnd() : bool
    {
        if (!$this->isPerformanceBegin()) {
            Core::setLastError(ORDER_STATUS_NOT_PERFORMANCE_BEGIN);
            return false;
        }

        return $this->updateStatus(self::STATUS['PERFORMANCE_END']);
    }

    protected function isPerformanceBegin() : bool
    {
        return $this->getStatus() == self::STATUS['PERFORMANCE_BEGIN'];
    }

    protected function isPerformanceEnd() : bool
    {
        return $this->getStatus() == self::STATUS['PERFORMANCE_END'];
    }

    public function isBuyerConfirmation() : bool
    {
        return $this->getStatus() == self::STATUS['BUYER_CONFIRMATION'];
    }

    public function buyerConfirmation() : bool
    {
        if (!$this->isPerformanceEnd()) {
            Core::setLastError(ORDER_STATUS_NOT_PERFORMANCE_END);
            return false;
        }

        return $this->updateStatus(self::STATUS['BUYER_CONFIRMATION']);
    }

    public function isCencelOrder() : bool
    {
        return $this->getStatus() == self::STATUS['BUYER_CANCEL_ORDER'] ||
               $this->getStatus() == self::STATUS['SELLER_CANCEL_ORDER'] ||
               $this->getStatus() == self::STATUS['PLATFORM_CANCEL_ORDER'];
    }

    public function isCompleted() : bool
    {
        return $this->getStatus() == self::STATUS['BUYER_CONFIRMATION'] ||
               $this->getStatus() == self::STATUS['PLATFORM_CONFIRMATION'] ||
               $this->getStatus() == self::STATUS['ORDER_COMPLETION'];
    }

    public function buyerDelete() : bool
    {
        if (!$this->isCencelOrder() && !$this->isCompleted()) {
            Core::setLastError(ORDER_STATUS_NOT_CENCEL_OR_COMPLETED);
            return false;
        }

        return $this->updateBuyerOrderStatus(self::BUYER_ORDER_STATUS['DELETE']);
    }

    public function buyerPermanentDelete() : bool
    {
        if (!$this->isBuyerDelete()) {
            Core::setLastError(ORDER_STATUS_NOT_DELETE);
            return false;
        }

        return $this->updateBuyerOrderStatus(self::BUYER_ORDER_STATUS['PERMANENT_DELETE']);
    }

    public function isBuyerDelete() : bool
    {
        return $this->getBuyerOrderStatus() == self::BUYER_ORDER_STATUS['DELETE'];
    }

    protected function updateBuyerOrderStatus(int $status) : bool
    {
        $this->setBuyerOrderStatus($status);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));
        return $this->getRepository()->edit(
            $this,
            array(
                'statusTime',
                'buyerOrderStatus',
                'updateTime'
            )
        );
    }

    public function sellerDelete() : bool
    {
        if (!$this->isCencelOrder() && !$this->isCompleted()) {
            Core::setLastError(ORDER_STATUS_NOT_CENCEL_OR_COMPLETED);
            return false;
        }

        return $this->updateSellerOrderStatus(self::SELLER_ORDER_STATUS['DELETE']);
    }

    public function sellerPermanentDelete() : bool
    {
        if (!$this->isSellerDelete()) {
            Core::setLastError(ORDER_STATUS_NOT_DELETE);
            return false;
        }

        return $this->updateSellerOrderStatus(self::SELLER_ORDER_STATUS['PERMANENT_DELETE']);
    }

    public function isSellerDelete() : bool
    {
        return $this->getSellerOrderStatus() == self::SELLER_ORDER_STATUS['DELETE'];
    }

    protected function updateSellerOrderStatus(int $status) : bool
    {
        $this->setSellerOrderStatus($status);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));
        return $this->getRepository()->edit(
            $this,
            array(
                'statusTime',
                'sellerOrderStatus',
                'updateTime'
            )
        );
    }
}
