<?php
namespace Market\Order\CommandHandler;

use Market\Order\Model\OrderAddress;

use Snapshot\Model\Snapshot;
use Snapshot\Repository\RepositoryFactory;
use Snapshot\Repository\Snapshot\SnapshotRepository;

trait OrderAddressCommandHandlerTrait
{
    protected function getOrderAddress() : OrderAddress
    {
        return new OrderAddress();
    }

    protected function executeOrderAddress($orderAddressArray) : OrderAddress
    {
        $orderAddress = $this->getOrderAddress();
        $orderAddress->setCellphone($orderAddressArray['cellphone']);
        $snapshot = $this->fetchSnapshot($orderAddressArray['snapshotId']);

        $orderAddress->setSnapshot($snapshot);
        $deliveryAddress = $this->fetchSnapshotObject(
            $snapshot->getCategory(),
            $snapshot->getSnapshotObject()->getId()
        );

        $orderAddress->setDeliveryAddress($deliveryAddress);

        if (!empty($deliveryAddress->getCellphone())) {
            $orderAddress->setCellphone($deliveryAddress->getCellphone());
        }

        return $orderAddress;
    }
}
