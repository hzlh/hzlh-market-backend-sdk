<?php
namespace Market\Order\CommandHandler;

use Market\Order\Model\OrderCommodity;

use Snapshot\Model\Snapshot;
use Snapshot\Repository\RepositoryFactory;
use Snapshot\Repository\Snapshot\SnapshotRepository;

trait OrderCommodityCommandHandlerTrait
{
    protected function getOrderCommodity() : OrderCommodity
    {
        return new OrderCommodity();
    }

    protected function getSnapshotRepository() : SnapshotRepository
    {
        return new SnapshotRepository();
    }

    protected function getRepositoryFactory() : RepositoryFactory
    {
        return new RepositoryFactory();
    }
    
    protected function fetchSnapshotObject($category, $snapshotObjectId)
    {
        $repository = $this->getRepositoryFactory()->getRepository($category);

        return $repository->fetchOne($snapshotObjectId);
    }

    protected function fetchSnapshot(int $id) : Snapshot
    {
        return $this->getSnapshotRepository()->fetchOne($id);
    }

    protected function executeOrderCommodities($orderCommoditiesArray) : array
    {
        $orderCommodities = array();

        foreach ($orderCommoditiesArray as $orderCommodityArray) {
            $orderCommodity = $this->getOrderCommodity();
            $orderCommodity->setNumber($orderCommodityArray['number']);
            $orderCommodity->setSkuIndex($orderCommodityArray['skuIndex']);
            $snapshot = $this->fetchSnapshot($orderCommodityArray['snapshotId']);

            $orderCommodity->setSnapshot($snapshot);
            $commodity = $this->fetchSnapshotObject($snapshot->getCategory(), $snapshot->getSnapshotObject()->getId());
            $orderCommodity->setCommodity($commodity);

            $orderCommodities[] = $orderCommodity;
        }

        return $orderCommodities;
    }
}
