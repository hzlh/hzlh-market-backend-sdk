<?php
namespace Market\Order\CommandHandler;

use Member\MemberCoupon\Model\MemberCoupon;
use Member\MemberCoupon\Repository\MemberCoupon\MemberCouponRepository;

trait OrderMemberCouponCommandHandlerTrait
{
    protected function getMemberCouponRepository() : MemberCouponRepository
    {
        return new MemberCouponRepository();
    }

    protected function fetchMemberCoupon(int $id) : MemberCoupon
    {
        return $this->getMemberCouponRepository()->fetchOne($id);
    }

    protected function executeMemberCoupons($memberCouponIds) : array
    {
        $memberCouponList = array();

        foreach ($memberCouponIds as $memberCouponId) {
            $memberCoupon = $this->fetchMemberCoupon($memberCouponId);
            
            $memberCouponList[] = $memberCoupon;
        }

        return $memberCouponList;
    }
}
