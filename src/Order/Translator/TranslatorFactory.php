<?php
namespace Market\Order\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\NullTranslator;

use Market\Order\Model\ITradeAble;

class TranslatorFactory
{
    const MAPS = array(
        ITradeAble::ORDER_CATEGORY['SERVICE']=>
        'Market\Service\ServiceOrder\Translator\ServiceOrderDBTranslator',
    );

    public function getTranslator(int $category) : ITranslator
    {
        $translator = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($translator) ? new $translator : NullTranslator::getInstance();
    }
}
