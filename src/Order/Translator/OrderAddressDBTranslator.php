<?php
namespace Market\Order\Translator;

use Marmot\Interfaces\ITranslator;

use Market\Order\Model\Contact;
use Market\Order\Model\OrderAddress;
use Market\Order\Model\NullOrderAddress;

class OrderAddressDBTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $orderAddress = null)
    {
        if (!isset($expression['order_address_id'])) {
            return NullOrderAddress::getInstance();
        }

        if ($orderAddress == null) {
            $orderAddress = new OrderAddress();
        }
        
        $orderAddress->setId($expression['order_address_id']);

        if (isset($expression['snapshot_id'])) {
            $orderAddress->getSnapshot()->setId($expression['snapshot_id']);
        }
        if (isset($expression['cellphone'])) {
            $orderAddress->setCellphone($expression['cellphone']);
        }

        return $orderAddress;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($orderAddress, array $keys = array())
    {
        if (!$orderAddress instanceof OrderAddress) {
            return false;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'snapshot',
                'cellphone'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['order_address_id'] = $orderAddress->getId();
        }
        if (in_array('snapshot', $keys)) {
            $expression['snapshot_id'] = $orderAddress->getSnapshot()->getId();
        }
        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $orderAddress->getCellphone();
        }

        return $expression;
    }
}
