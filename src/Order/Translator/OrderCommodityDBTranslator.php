<?php
namespace Market\Order\Translator;

use Marmot\Interfaces\ITranslator;

use Market\Order\Model\Contact;
use Market\Order\Model\OrderCommodity;
use Market\Order\Model\NullOrderCommodity;

class OrderCommodityDBTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $orderCommodity = null)
    {
        if (!isset($expression['order_commodity_id'])) {
            return NullOrderCommodity::getInstance();
        }

        if ($orderCommodity == null) {
            $orderCommodity = new OrderCommodity();
        }
        
        $orderCommodity->setId($expression['order_commodity_id']);

        if (isset($expression['snapshot_id'])) {
            $orderCommodity->getSnapshot()->setId($expression['snapshot_id']);
        }
        if (isset($expression['number'])) {
            $orderCommodity->setNumber($expression['number']);
        }
        if (isset($expression['sku_index'])) {
            $orderCommodity->setSkuIndex($expression['sku_index']);
        }
        if (isset($expression['commodity_id'])) {
            $orderCommodity->getCommodity()->setId($expression['commodity_id']);
        }

        return $orderCommodity;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($orderCommodity, array $keys = array())
    {
        if (!$orderCommodity instanceof OrderCommodity) {
            return false;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'snapshot',
                'number',
                'skuIndex',
                'commodity'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['order_commodity_id'] = $orderCommodity->getId();
        }
        if (in_array('snapshot', $keys)) {
            $expression['snapshot_id'] = $orderCommodity->getSnapshot()->getId();
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $orderCommodity->getNumber();
        }
        if (in_array('skuIndex', $keys)) {
            $expression['sku_index'] = $orderCommodity->getSkuIndex();
        }
        if (in_array('commodity', $keys)) {
            $expression['commodity_id'] = $orderCommodity->getCommodity()->getId();
        }

        return $expression;
    }
}
