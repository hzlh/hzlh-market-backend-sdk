<?php
namespace Market\Order\Translator;

use Marmot\Interfaces\ITranslator;

use Market\Order\Model\Order;

use Market\Payment\Model\Payment;

/**
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
*/
abstract class OrderDBTranslator implements ITranslator
{
     /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function arrayToObject(array $expression, $order = null)
    {
        if (isset($expression['order_id'])) {
            $order->setId($expression['order_id']);
        }
        if (isset($expression['buyer_member_account_id'])) {
            $order->getBuyerMemberAccount()->setId($expression['buyer_member_account_id']);
        }
        if (isset($expression['seller_enterprise_id'])) {
            $order->getSellerEnterprise()->setId($expression['seller_enterprise_id']);
        }
        if (isset($expression['orderno'])) {
            $order->setOrderno($expression['orderno']);
        }
        if (isset($expression['total_price'])) {
            $order->setTotalPrice($expression['total_price']);
        }
        if (isset($expression['paid_amount'])) {
            $order->setPaidAmount($expression['paid_amount']);
        }
        if (isset($expression['collected_amount'])) {
            $order->setCollectedAmount($expression['collected_amount']);
        }
        if (isset($expression['commission'])) {
            $order->setCommission($expression['commission']);
        }
        if (isset($expression['subsidy'])) {
            $order->setSubsidy($expression['subsidy']);
        }
        if (isset($expression['real_amount'])) {
            $order->setRealAmount($expression['real_amount']);
        }
        if (isset($expression['failure_reason'])) {
            $failureReason = array();
            if (is_string($expression['failure_reason'])) {
                $failureReason = json_decode($expression['failure_reason'], true);
            }
            if (is_array($expression['failure_reason'])) {
                $failureReason = $expression['failure_reason'];
            }

            $order->setFailureReason($failureReason);
        }
        if (isset($expression['time_record'])) {
            $timeRecord = array();
            if (is_string($expression['time_record'])) {
                $timeRecord = json_decode($expression['time_record'], true);
            }
            if (is_array($expression['time_record'])) {
                $timeRecord = $expression['time_record'];
            }

            $order->setTimeRecord($timeRecord);
        }
        $type = isset($expression['payment_type'])? $expression['payment_type'] : 0;
        $transactionNumber = isset($expression['transaction_number'])? $expression['transaction_number'] : '';
        $transactionInfo = array();
        if (is_string($expression['transaction_info'])) {
            $transactionInfo = json_decode($expression['transaction_info'], true);
        }
        if (is_array($expression['transaction_info'])) {
            $transactionInfo = $expression['transaction_info'];
        }
        $time = isset($expression['payment_time'])? $expression['payment_time'] : 0;
        $order->setPayment(new Payment($type, $time, $transactionNumber, $transactionInfo));
        
        if (isset($expression['category'])) {
            $order->setCategory($expression['category']);
        }
        if (isset($expression['create_time'])) {
            $order->setCreateTime($expression['create_time']);
        }
        if (isset($expression['update_time'])) {
            $order->setUpdateTime($expression['update_time']);
        }
        if (isset($expression['remark'])) {
            $order->setRemark($expression['remark']);
        }
        if (isset($expression['status'])) {
            $order->setStatus($expression['status']);
        }
        if (isset($expression['cancel_reason'])) {
            $order->setCancelReason($expression['cancel_reason']);
        }
        if (isset($expression['buyer_order_status'])) {
            $order->setBuyerOrderStatus($expression['buyer_order_status']);
        }
        if (isset($expression['seller_order_status'])) {
            $order->setSellerOrderStatus($expression['seller_order_status']);
        }
        if (isset($expression['status_time'])) {
            $order->setStatusTime($expression['status_time']);
        }

        return $order;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($order, array $keys = array())
    {
        if (!$order instanceof Order) {
            return false;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'orderno',
                'buyerMemberAccount',
                'sellerEnterprise',
                'totalPrice',
                'paidAmount',
                'collectedAmount',
                'commission',
                'subsidy',
                'realAmount',
                'paymentType',
                'transactionNumber',
                'paymentTime',
                'category',
                'status',
                'remark',
                'failureReason',
                'timeRecord',
                'cancelReason',
                'buyerOrderStatus',
                'sellerOrderStatus',
                'createTime',
                'updateTime',
                'statusTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['order_id'] = $order->getId();
        }
        if (in_array('orderno', $keys)) {
            $expression['orderno'] = $order->getOrderno();
        }
        if (in_array('buyerMemberAccount', $keys)) {
            $expression['buyer_member_account_id'] = $order->getBuyerMemberAccount()->getId();
        }
        if (in_array('sellerEnterprise', $keys)) {
            $expression['seller_enterprise_id'] = $order->getSellerEnterprise()->getId();
        }
        if (in_array('totalPrice', $keys)) {
            $expression['total_price'] = $order->getTotalPrice();
        }
        if (in_array('paidAmount', $keys)) {
            $expression['paid_amount'] = $order->getPaidAmount();
        }
        if (in_array('collectedAmount', $keys)) {
            $expression['collected_amount'] = $order->getCollectedAmount();
        }
        if (in_array('commission', $keys)) {
            $expression['commission'] = $order->getCommissionProportion();
        }
        if (in_array('subsidy', $keys)) {
            $expression['subsidy'] = $order->getSubsidyProportion();
        }
        if (in_array('realAmount', $keys)) {
            $expression['real_amount'] = $order->getRealAmount();
        }
        if (in_array('paymentType', $keys)) {
            $expression['payment_type'] = $order->getPayment()->getType();
        }
        if (in_array('transactionNumber', $keys)) {
            $expression['transaction_number'] = $order->getPayment()->getTransactionNumber();
        }
        if (in_array('transactionInfo', $keys)) {
            $expression['transaction_info'] = $order->getPayment()->getTransactionInfo();
        }
        if (in_array('paymentTime', $keys)) {
            $expression['payment_time'] = $order->getPayment()->getTime();
        }
        if (in_array('category', $keys)) {
            $expression['category'] = $order->getCategory();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $order->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $order->getUpdateTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $order->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $order->getStatusTime();
        }
        if (in_array('remark', $keys)) {
            $expression['remark'] = $order->getRemark();
        }
        if (in_array('failureReason', $keys)) {
            $expression['failure_reason'] = $order->getFailureReason();
        }
        if (in_array('timeRecord', $keys)) {
            $expression['time_record'] = $order->getTimeRecord();
        }
        if (in_array('cancelReason', $keys)) {
            $expression['cancel_reason'] = $order->getCancelReason();
        }
        if (in_array('buyerOrderStatus', $keys)) {
            $expression['buyer_order_status'] = $order->getBuyerOrderStatus();
        }
        if (in_array('sellerOrderStatus', $keys)) {
            $expression['seller_order_status'] = $order->getSellerOrderStatus();
        }

        return $expression;
    }
}
