<?php
namespace Market\Order\Translator;

use Marmot\Interfaces\ITranslator;

use Market\Order\Model\OrderUpdateAmountRecord;
use Market\Order\Model\NullOrderUpdateAmountRecord;

class OrderUpdateAmountRecordDBTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $orderUpdateAmountRecord = null)
    {
        if (!isset($expression['order_update_amount_record_id'])) {
            return NullOrderUpdateAmountRecord::getInstance();
        }

        if ($orderUpdateAmountRecord == null) {
            $orderUpdateAmountRecord = new OrderUpdateAmountRecord();
        }
        
        $orderUpdateAmountRecord->setId($expression['order_id']);

        if (isset($expression['amount'])) {
            $orderUpdateAmountRecord->setAmount($expression['amount']);
        }

        if (isset($expression['remark'])) {
            $orderUpdateAmountRecord->setRemark($expression['remark']);
        }

        if (isset($expression['create_time'])) {
            $orderUpdateAmountRecord->setCreateTime($expression['create_time']);
        }

        return $orderUpdateAmountRecord;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($orderUpdateAmountRecord, array $keys = array())
    {
        if (!$orderUpdateAmountRecord instanceof OrderUpdateAmountRecord) {
            return false;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'amount',
                'remark',
                'createTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['order_update_amount_record_id'] = $orderUpdateAmountRecord->getId();
            $expression['order_id'] = $orderUpdateAmountRecord->getId();
        }
        if (in_array('amount', $keys)) {
            $expression['amount'] = $orderUpdateAmountRecord->getAmount();
        }
        if (in_array('remark', $keys)) {
            $expression['remark'] = $orderUpdateAmountRecord->getRemark();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $orderUpdateAmountRecord->getCreateTime();
        }

        return $expression;
    }
}
