<?php
namespace Market\Order\Translator;

use Market\Order\Model\CommonOrder;

abstract class CommonOrderDBTranslator extends OrderDBTranslator
{
    protected function getOrderCommodityDBTranslator() : OrderCommodityDBTranslator
    {
        new OrderCommodityDBTranslator();
    }

    public function arrayToObject(array $expression, $commonOrder = null)
    {
        $commonOrder = parent::arrayToObject($expression, $commonOrder);

        if (isset($expression['order_commodity_array'])) {
            foreach ($expression['order_commodity_array'] as $val) {
                $val = (array)$val;
                $orderCommodity = $this->getOrderCommodityDBTranslator()->arrayToObject($val);
                $commonOrder->addOrderCommodity($orderCommodity);
            }
        }

        if (isset($expression['order_address_id'])) {
            $commonOrder->getOrderAddress()->setId($expression['order_address_id']);
        }
        
        return $commonOrder;
    }

    public function objectToArray($commonOrder, array $keys = array())
    {
        $orderExpression = parent::objectToArray($commonOrder, $keys);

        if (!$commonOrder instanceof CommonOrder) {
            return false;
        }

        if (empty($keys)) {
            $keys = array(
                'orderCommodityArray',
                'orderAddress'
            );
        }

        $expression = array();

        if (in_array('orderCommodityArray', $keys)) {
            foreach ($commonOrder->getOrderCommodities() as $orderCommodity) {
                $expression['order_commodity_array'][] =
                $this->getOrderCommodityDBTranslator()->objectToArray($orderCommodity);
            }
        }
        if (in_array('orderAddress', $keys)) {
            $expression['order_address_id'] = $commonOrder->getOrderAddress()->getId();
        }

        $expression = array_merge($expression, $orderExpression);

        return $expression;
    }
}
