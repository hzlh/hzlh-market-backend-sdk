<?php
namespace Market\Order\Adapter\OrderMemberCoupon;

use Market\Order\Model\Order;

use Member\MemberCoupon\Model\MemberCoupon;

interface IOrderMemberCouponAdapter
{
    public function add(Order $order, MemberCoupon $memberCoupon) : bool;

    public function delete(Order $order, MemberCoupon $memberCoupon) : bool;
    
    public function orderMemberCouponList(
        array $filter = array()
    ) : array;
}
