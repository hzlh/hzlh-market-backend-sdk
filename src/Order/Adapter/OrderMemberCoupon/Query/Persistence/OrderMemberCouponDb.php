<?php
namespace Market\Order\Adapter\OrderMemberCoupon\Query\Persistence;

use Marmot\Framework\Classes\Db;

class OrderMemberCouponDb extends Db
{
    public function __construct()
    {
        parent::__construct('market_order_member_coupon');
    }
}
