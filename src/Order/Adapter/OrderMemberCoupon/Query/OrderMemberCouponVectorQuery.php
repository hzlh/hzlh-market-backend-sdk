<?php
namespace Market\Order\Adapter\OrderMemberCoupon\Query;

use Marmot\Framework\Query\DBVectorQuery;

class OrderMemberCouponVectorQuery extends DBVectorQuery
{
    public function __construct()
    {
        parent::__construct(
            new Persistence\OrderMemberCouponDb()
        );
    }
}
