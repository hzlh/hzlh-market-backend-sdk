<?php
namespace Market\Order\Adapter\OrderMemberCoupon;

use Marmot\Core;

use Market\Order\Model\Order;
use Market\Order\Model\ITradeAble;
use Market\Order\Translator\TranslatorFactory;
use Market\Order\Adapter\OrderMemberCoupon\Query\OrderMemberCouponVectorQuery;

use Member\MemberCoupon\Model\MemberCoupon;
use Member\MemberCoupon\Model\NullMemberCoupon;
use Member\MemberCoupon\Translator\MemberCouponDBTranslator;
use Member\MemberCoupon\Repository\MemberCoupon\MemberCouponRepository;

use Market\Service\ServiceOrder\Model\ServiceOrder;

class OrderMemberCouponDBAdapter implements IOrderMemberCouponAdapter
{
    protected $memberCouponDBTranslator;

    protected $orderMemberCouponVectorQuery;

    protected $translatorFactory;

    protected $memberCouponRepository;

    public function __construct()
    {
        $this->memberCouponDBTranslator = new MemberCouponDBTranslator();
        $this->orderMemberCouponVectorQuery = new OrderMemberCouponVectorQuery();
        $this->translatorFactory = new TranslatorFactory();
        $this->memberCouponRepository = new MemberCouponRepository();
    }

    public function __destruct()
    {
        unset($this->memberCouponDBTranslator);
        unset($this->orderCouponVectorQuery);
        unset($this->translatorFactory);
        unset($this->memberCouponRepository);
    }
    
    protected function getMemberCouponDBTranslator() : MemberCouponDBTranslator
    {
        return $this->memberCouponDBTranslator;
    }

    protected function getOrderMemberCouponVectorQuery() : OrderMemberCouponVectorQuery
    {
        return $this->orderMemberCouponVectorQuery;
    }
    
    protected function getTranslatorFactory() : TranslatorFactory
    {
        return $this->translatorFactory;
    }
    
    protected function fetchTranslator($category)
    {
        return $this->getTranslatorFactory()->getTranslator($category);
    }

    protected function getMemberCouponRepository() : MemberCouponRepository
    {
        return $this->memberCouponRepository;
    }
    
    public function add(Order $order, MemberCoupon $memberCoupon) : bool
    {
        $info = array();

        $orderTranslator = $this->fetchTranslator($order->getCategory());

        $orderInfo = $orderTranslator->objectToArray($order, array('id'));

        $memberCouponInfo = $this->getMemberCouponDBTranslator()->objectToArray($memberCoupon, array('id'));

        $info = array_merge($orderInfo, $memberCouponInfo);
        $row = $this->getOrderMemberCouponVectorQuery()->add($info);

        if (!$row) {
            return false;
        }

        return true;
    }

    public function delete(Order $order, MemberCoupon $memberCoupon) : bool
    {
        $info = array();

        $orderTranslator = $this->fetchTranslator($order->getCategory());

        $orderInfo = $orderTranslator->objectToArray($order, array('id'));

        $memberCouponInfo = $this->getMemberCouponDBTranslator()->objectToArray($memberCoupon, array('id'));

        $info = array_merge($orderInfo, $memberCouponInfo);
        $row = $this->getOrderMemberCouponVectorQuery()->delete($info);

        if (!$row) {
            return false;
        }

        return true;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $serviceOrder = new ServiceOrder();

            $orderTranslator = $this->fetchTranslator(ITradeAble::ORDER_CATEGORY['SERVICE']);

            if (isset($filter['orderIds'])) {
                $orderIds = $filter['orderIds'];
                $info = $orderTranslator->objectToArray($serviceOrder, array('id'));
                if (is_numeric($orderIds)) {
                    $condition .= key($info).' = '.$orderIds;
                }
                if (is_array($orderIds)) {
                    $condition .= key($info).' IN ('.implode(',', $orderIds).')';
                }
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    public function orderMemberCouponList(
        array $filter = array()
    ) : array {
        $condition = '';
        $condition = $this->formatFilter($filter);

        $listTemp = $this->getOrderMemberCouponVectorQuery()->find($condition);

        if (empty($listTemp)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $memberCouponIds = $memberCoupons = array();
        $memberCouponInfo = $this->getMemberCouponDBTranslator()->objectToArray(new MemberCoupon(), array('id'));

        foreach ($listTemp as $each) {
            $memberCouponIds[] = $each[key($memberCouponInfo)];
        }

        $memberCoupons = $this->getMemberCouponRepository()->fetchList($memberCouponIds);
        $memberCouponTemp = array();
        foreach ($memberCoupons as $memberCoupon) {
            $memberCouponTemp[$memberCoupon->getId()] = $memberCoupon;
        }

        $orderMemberCouponList = array();
        foreach ($listTemp as $value) {
            $orderMemberCouponList[$value['order_id']][] = isset($memberCouponTemp[$value['member_coupon_id']]) ?
            $memberCouponTemp[$value['member_coupon_id']] : NullMemberCoupon::getInstance();
        }
        
        return $orderMemberCouponList;
    }
}
