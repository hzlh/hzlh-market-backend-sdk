<?php
namespace Market\Order\Adapter\Order;

use Member\Enterprise\Model\Enterprise;
use Member\Enterprise\Repository\Enterprise\EnterpriseRepository;

use Member\Member\Model\MemberAccount;
use Member\Member\Repository\MemberAccount\MemberAccountRepository;

use Market\Order\Repository\OrderAddress\OrderAddressRepository;
use Market\Order\Repository\OrderCommodity\OrderCommodityRepository;
use Market\Order\Repository\OrderMemberCoupon\OrderMemberCouponRepository;

trait OrderAdapterTrait
{
    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return new EnterpriseRepository();
    }

    protected function fetchEnterprise(int $id) : Enterprise
    {
        return $this->getEnterpriseRepository()->fetchOne($id);
    }

    protected function getMemberAccountRepository() : MemberAccountRepository
    {
        return new MemberAccountRepository();
    }

    protected function fetchMemberAccount(int $id) : MemberAccount
    {
        return $this->getMemberAccountRepository()->fetchOne($id);
    }

    protected function getOrderAddressRepository() : OrderAddressRepository
    {
        return new OrderAddressRepository();
    }

    protected function getOrderCommodityRepository() : OrderCommodityRepository
    {
        return new OrderCommodityRepository();
    }
    
    protected function getOrderMemberCouponRepository() : OrderMemberCouponRepository
    {
        return new OrderMemberCouponRepository();
    }
    
    protected function orderAddressList($ids) : array
    {
        $filter = array('orderIds' => $ids);
        $size = count($ids);
        return $this->getOrderAddressRepository()->orderAddressList($filter, $size);
    }
    
    protected function orderCommodityList($ids) : array
    {
        $filter = array('orderIds' => $ids);
        $size = count($ids);

        return $this->getOrderCommodityRepository()->orderCommodityList($filter, $size);
    }

    protected function orderMemberCouponList($ids) : array
    {
        $filter = array('orderIds' => $ids);
        return $this->getOrderMemberCouponRepository()->orderMemberCouponList($filter);
    }
}
