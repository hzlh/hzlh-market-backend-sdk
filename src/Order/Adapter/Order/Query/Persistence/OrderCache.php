<?php
namespace Market\Order\Adapter\Order\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class OrderCache extends Cache
{

    const KEY = 'market_order';
    /**
     * 构造函数初始化key和表名一致
     */
    public function __construct()
    {
        parent::__construct(self::KEY);
    }

    public function getCacheKey($id = 0)
    {
        if (empty($id)) {
            return self::KEY;
        }

        return self::KEY . '_' . $id;
    }
}
