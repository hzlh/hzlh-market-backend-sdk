<?php
namespace Market\Order\Adapter\Order\Query\Persistence;

use Marmot\Framework\Classes\Db;

class OrderDb extends Db
{
    public function __construct()
    {
        parent::__construct('market_order');
    }
}
