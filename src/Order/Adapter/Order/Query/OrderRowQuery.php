<?php
namespace Market\Order\Adapter\Order\Query;

use Marmot\Framework\Query\RowQuery;

class OrderRowQuery extends RowQuery
{
    public function __construct()
    {
        parent::__construct(
            'order_id',
            new Persistence\OrderDb()
        );
    }
}
