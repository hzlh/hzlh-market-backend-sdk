<?php
namespace Market\Order\Adapter\Order\Query;

use Marmot\Framework\Query\RowCacheQuery;

class OrderRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'order_id',
            new Persistence\OrderCache(),
            new Persistence\OrderDb()
        );
    }
}
