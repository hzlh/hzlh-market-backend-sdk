<?php
namespace Market\Order\Adapter\OrderSearch\Query;

use Marmot\Framework\Query\RowQuery;

class OrderSearchRowQuery extends RowQuery
{
    public function __construct()
    {
        parent::__construct(
            'order_id',
            new Persistence\OrderSearchDb()
        );
    }

    /**
     * @param array $data 添加数据
     * 此处因为在设计的时候没有主键id,所以返回的是受影响行数,所以复写此方法
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function add(array $data, $lasetInsertId = false)
    {
        $result = $this->getDbLayer()->insert($data, $lasetInsertId);
        if (!$result) {
            return false;
        }
        
        return $result;
    }
}
