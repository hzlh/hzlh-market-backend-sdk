<?php
namespace Market\Order\Adapter\OrderSearch\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class OrderSearchCache extends Cache
{
    /**
     * 构造函数初始化key和表名一致
     */
    public function __construct()
    {
        parent::__construct('market_order_search');
    }
}
