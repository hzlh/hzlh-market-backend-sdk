<?php
namespace Market\Order\Adapter\OrderSearch;

use Market\Order\Model\Order;

interface IOrderSearchAdapter
{
    public function add(Order $order) : bool;

    public function orderSearchGetOrder(
        array $filter = array()
    ) : array;
}
