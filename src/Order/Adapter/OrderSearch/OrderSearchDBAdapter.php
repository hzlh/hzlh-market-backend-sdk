<?php
namespace Market\Order\Adapter\OrderSearch;

use Marmot\Core;

use Market\Order\Model\Order;
use Market\Order\Translator\TranslatorFactory;
use Market\Order\Adapter\OrderSearch\Query\OrderSearchRowQuery;

class OrderSearchDBAdapter implements IOrderSearchAdapter
{
    protected $orderSearchRowQuery;

    protected $translatorFactory;

    public function __construct()
    {
        $this->orderSearchRowQuery = new OrderSearchRowQuery();
        $this->translatorFactory = new TranslatorFactory();
    }

    public function __destruct()
    {
        unset($this->orderSearchRowQuery);
        unset($this->translatorFactory);
    }

    protected function getOrderSearchRowQuery() : OrderSearchRowQuery
    {
        return $this->orderSearchRowQuery;
    }
    
    public function add(Order $order) : bool
    {
        $info = array();

        $orderCommodities = $order->getOrderCommodities();
        $commodities = array();
        $serviceCategories = array();
        $serviceNames = array();
        $sellerEnterpriseName = $order->getSellerEnterprise()->getName();
        $deliveryAddressCellphone = $order->getOrderAddress()->getCellphone();

        foreach ($orderCommodities as $orderCommodity) {
            $commodities[] = $orderCommodity->getCommodity();
        }

        foreach ($commodities as $commodity) {
            $serviceCategories[] = $commodity->getServiceCategory()->getId();
            $serviceNames[] = $commodity->getTitle();
        }

        $info['order_id'] = $order->getId();
        $info['service_categories'] = json_encode($serviceCategories, JSON_UNESCAPED_UNICODE);
        $info['service_names'] = json_encode($serviceNames, JSON_UNESCAPED_UNICODE);
        $info['seller_enterprise_name'] = $sellerEnterpriseName;
        $info['delivery_address_cellphone'] = $deliveryAddressCellphone;

        $id = $this->getOrderSearchRowQuery()->add($info);

        if (!$id) {
            return false;
        }

        return true;
    }

    private function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            if (isset($filter['serviceCategories'])) {
                $serviceCategories = explode(',', $filter['serviceCategories']);
                $serviceCategoriesArray = array();
                foreach ($serviceCategories as $value) {
                    $serviceCategoriesArray[] = intval($value);
                }

                $condition .= $conjection.' JSON_CONTAINS(service_categories, \''.json_encode(
                    $serviceCategoriesArray
                ).'\')';
                $conjection = ' AND ';
            }

            if (isset($filter['serviceNames'])) {
                $condition .= $conjection.'service_names LIKE \'%'.$filter['serviceNames'].'%\'';
                $conjection = ' AND ';
            }

            if (isset($filter['sellerEnterpriseName'])) {
                $condition .= $conjection.'seller_enterprise_name LIKE \'%'.$filter['sellerEnterpriseName'].'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['deliveryAddressCellphone'])) {
                $condition .= $conjection.'delivery_address_cellphone = \''.$filter['deliveryAddressCellphone'].'\'';
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    public function orderSearchGetOrder(
        array $filter = array()
    ) : array {

        $condition = '';
        $offset = 0;
        $size = 100000;

        $condition = $this->formatFilter($filter);
     
        //查询数据
        $list = $this->getOrderSearchRowQuery()->find($condition, $offset, $size);

        if (empty($list)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $orderIds = array();

        foreach ($list as $each) {
            $orderIds[] = $each['order_id'];
        }

        return array_unique($orderIds);
    }
}
