<?php
namespace Market\Order\Adapter\OrderAddress;

use Market\Order\Model\Order;
use Market\Order\Model\OrderAddress;

interface IOrderAddressAdapter
{
    public function add(Order $order, OrderAddress $orderAddress) : bool;

    public function edit(OrderAddress $orderAddress) : bool;

    public function fetchOne($id) : OrderAddress;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function orderAddressList(
        array $filter = array()
    ) : array;
}
