<?php
namespace Market\Order\Adapter\OrderAddress\Query\Persistence;

use Marmot\Framework\Classes\Db;

class OrderAddressDb extends Db
{
    /**
     * 构造函数初始化key和表名一致
     */
    public function __construct()
    {
        parent::__construct('market_order_address');
    }
}
