<?php
namespace Market\Order\Adapter\OrderAddress\Query;

use Marmot\Framework\Query\RowCacheQuery;

class OrderAddressRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'order_address_id',
            new Persistence\OrderAddressCache(),
            new Persistence\OrderAddressDb()
        );
    }
}
