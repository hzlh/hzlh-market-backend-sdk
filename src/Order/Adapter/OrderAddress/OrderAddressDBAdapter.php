<?php
namespace Market\Order\Adapter\OrderAddress;

use Marmot\Core;

use Market\Order\Model\Order;
use Market\Order\Model\ITradeAble;
use Market\Order\Model\OrderAddress;
use Market\Order\Model\NullOrderAddress;
use Market\Order\Translator\TranslatorFactory;
use Market\Order\Translator\OrderAddressDBTranslator;
use Market\Order\Adapter\OrderAddress\Query\OrderAddressRowCacheQuery;

use Market\Service\ServiceOrder\Model\ServiceOrder;

use Snapshot\Repository\RepositoryFactory;
use Snapshot\Repository\Snapshot\SnapshotRepository;

class OrderAddressDBAdapter implements IOrderAddressAdapter
{
    protected $orderAddressDBTranslator;

    protected $orderAddressRowCacheQuery;

    protected $translatorFactory;

    protected $snapshotRepository;

    protected $repositoryFactory;

    public function __construct()
    {
        $this->orderAddressDBTranslator = new OrderAddressDBTranslator();
        $this->orderAddressRowCacheQuery = new OrderAddressRowCacheQuery();
        $this->snapshotRepository = new SnapshotRepository();
        $this->repositoryFactory = new RepositoryFactory();
        $this->translatorFactory = new TranslatorFactory();
    }

    public function __destruct()
    {
        unset($this->orderAddressDBTranslator);
        unset($this->orderAddressRowCacheQuery);
        unset($this->snapshotRepository);
        unset($this->repositoryFactory);
        unset($this->translatorFactory);
    }
    
    protected function getOrderAddressDBTranslator() : OrderAddressDBTranslator
    {
        return $this->orderAddressDBTranslator;
    }

    protected function getOrderAddressRowCacheQuery() : OrderAddressRowCacheQuery
    {
        return $this->orderAddressRowCacheQuery;
    }
    
    protected function getSnapshotRepository() : SnapshotRepository
    {
        return $this->snapshotRepository;
    }
    
    protected function getRepositoryFactory() : RepositoryFactory
    {
        return $this->repositoryFactory;
    }

    protected function getSnapshotObjectRepository($category)
    {
        return $this->getRepositoryFactory()->getRepository($category);
    }
    
    protected function getTranslatorFactory() : TranslatorFactory
    {
        return $this->translatorFactory;
    }
    
    protected function fetchTranslator($category)
    {
        return $this->getTranslatorFactory()->getTranslator($category);
    }

    public function add(Order $order, OrderAddress $orderAddress) : bool
    {
        $info = array();

        $orderTranslator = $this->fetchTranslator($order->getCategory());

        $orderInfo = $orderTranslator->objectToArray($order, array('id'));

        $orderAddressInfo = $this->getOrderAddressDBTranslator()->objectToArray($orderAddress);

        $info = array_merge($orderInfo, $orderAddressInfo);
        $id = $this->getOrderAddressRowCacheQuery()->add($info);

        if (!$id) {
            return false;
        }

        return true;
    }

    public function edit(OrderAddress $orderAddress) : bool
    {
        $info = array();

        $conditionArray[$this->getOrderAddressRowCacheQuery()->getPrimaryKey()] = $orderAddress->getId();

        $info = $this->getOrderAddressDBTranslator()->objectToArray($orderAddress);

        $result = $this->getOrderAddressRowCacheQuery()->update($info, $conditionArray);

        if (!$result) {
            return false;
        }

        return true;
    }

    public function fetchOne($id) : OrderAddress
    {
        $info = array();

        $info = $this->getOrderAddressRowCacheQuery()->getOne($id);
        
        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullOrderAddress::getInstance();
        }

        $orderAddress = $this->getOrderAddressDBTranslator()->arrayToObject($info);

        if ($orderAddress instanceof OrderAddress) {
            $snapshot = $this->getSnapshotRepository()->fetchOne($orderAddress->getSnapshot()->getId());
            $orderAddress->setSnapshot($snapshot);

            $snapshotObjectRepository = $this->getSnapshotObjectRepository($snapshot->getCategory());
            $snapshotObject = $snapshotObjectRepository->fetchOne($snapshot->getSnapshotObject()->getId());
            $orderAddress->setDeliveryAddress($snapshotObject);
        }

        return $orderAddress;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $serviceOrder = new ServiceOrder();

            $orderTranslator = $this->fetchTranslator(ITradeAble::ORDER_CATEGORY['SERVICE']);

            if (isset($filter['orderIds'])) {
                $orderIds = $filter['orderIds'];
                $info = $orderTranslator->objectToArray($serviceOrder, array('id'));
                if (is_numeric($orderIds)) {
                    $condition .= key($info).' = '.$orderIds;
                }
                if (is_array($orderIds)) {
                    $condition .= key($info).' IN ('.implode(',', $orderIds).')';
                }
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    private function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $orderAddress = new OrderAddress();

            if (isset($sort['id'])) {
                $info = $this->getOrderAddressDBTranslator()->objectToArray(
                    $orderAddress,
                    array('id')
                );
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }

    public function fetchList(array $ids) : array
    {
        $orderAddressInfoList = array();
        
        $orderAddressInfoList = $this->getOrderAddressRowCacheQuery()->getList($ids);

        if (empty($orderAddressInfoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $orderAddressList = array();

        foreach ($orderAddressInfoList as $orderAddressInfo) {
            $orderAddress = $this->getOrderAddressDBTranslator()->arrayToObject($orderAddressInfo);
            $snapshot = $this->getSnapshotRepository()->fetchOne($orderAddress->getSnapshot()->getId());

            $orderAddress->setSnapshot($snapshot);

            $orderAddress->setDeliveryAddress($snapshot->getSnapshotObject());

            $orderAddressList[] = $orderAddress;
        }

        return $orderAddressList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        $rowCacheQuery = $this->getOrderAddressRowCacheQuery();

        $condition = $this->formatFilter($filter);
        $condition .= $this->formatSort($sort);

        //查询数据
        $orderAddressList = $rowCacheQuery->find($condition, $offset, $size);

        if (empty($orderAddressList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $ids = array();
        $primaryKey = $rowCacheQuery->getPrimaryKey();
        foreach ($orderAddressList as $info) {
            $ids[] = $info[$primaryKey];
        }

        $count = 0;

        $count = sizeof($ids);
        if ($count  == $size || $offset > 0) {
            $count = $rowCacheQuery->count($condition);
        }

        return array($this->fetchList($ids), $count);
    }

    public function orderAddressList(
        array $filter = array()
    ) : array {
        $rowCacheQuery = $this->getOrderAddressRowCacheQuery();
        $condition = '';
        $offset = 0;
        $size = 100000;
        
        $condition = $this->formatFilter($filter);

        //查询数据
        $listTemp = $this->getOrderAddressRowCacheQuery()->find($condition, $offset, $size);

        if (empty($listTemp)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $ids = array();
        $primaryKey = $rowCacheQuery->getPrimaryKey();
        foreach ($listTemp as $info) {
            $ids[] = $info[$primaryKey];
        }

        $orderAddressInfoList = $this->getOrderAddressRowCacheQuery()->getList($ids);

        $orderAddressList = array();
        $orderAddressListTemp = $this->fetchList($ids);

        foreach ($orderAddressListTemp as $orderAddress) {
            $orderAddressList[$orderAddress->getId()] = $orderAddress;
        }

        foreach ($orderAddressInfoList as $val) {
            $list[$val['order_id']] = isset($orderAddressList[$val['order_address_id']]) ?
            $orderAddressList[$val['order_address_id']] :
            NullOrderAddress::getInstance();
        }

        return $list;
    }
}
