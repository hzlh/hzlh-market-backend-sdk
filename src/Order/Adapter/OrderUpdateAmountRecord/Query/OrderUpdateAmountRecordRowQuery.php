<?php
namespace Market\Order\Adapter\OrderUpdateAmountRecord\Query;

use Marmot\Framework\Query\RowQuery;

class OrderUpdateAmountRecordRowQuery extends RowQuery
{
    public function __construct()
    {
        parent::__construct(
            'order_update_amount_record_id',
            new Persistence\OrderUpdateAmountRecordDb()
        );
    }

    /**
     * @param array $data 添加数据
     * 此处因为在设计的时候没有主键id,所以返回的是受影响行数,所以复写此方法
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function add(array $data, $lasetInsertId = false)
    {
        $result = $this->getDbLayer()->insert($data, $lasetInsertId);
        if (!$result) {
            return false;
        }
        
        return $result;
    }
}
