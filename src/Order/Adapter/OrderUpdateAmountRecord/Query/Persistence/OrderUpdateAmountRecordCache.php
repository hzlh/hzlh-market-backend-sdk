<?php
namespace Market\Order\Adapter\OrderUpdateAmountRecord\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class OrderUpdateAmountRecordCache extends Cache
{
    /**
     * 构造函数初始化key和表名一致
     */
    public function __construct()
    {
        parent::__construct('market_order_update_amount_record');
    }
}
