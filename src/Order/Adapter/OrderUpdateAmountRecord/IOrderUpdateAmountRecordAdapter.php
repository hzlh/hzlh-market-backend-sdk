<?php
namespace Market\Order\Adapter\OrderUpdateAmountRecord;

use Market\Order\Model\Order;
use Market\Order\Model\OrderUpdateAmountRecord;

interface IOrderUpdateAmountRecordAdapter
{
    public function add(Order $order, OrderUpdateAmountRecord $orderUpdateAmountRecord) : bool;

    public function fetchOne($id) : OrderUpdateAmountRecord;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;
}
