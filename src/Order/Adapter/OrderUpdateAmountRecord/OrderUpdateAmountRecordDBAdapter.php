<?php
namespace Market\Order\Adapter\OrderUpdateAmountRecord;

use Marmot\Core;

use Market\Order\Model\Order;
use Market\Order\Model\OrderUpdateAmountRecord;
use Market\Order\Translator\TranslatorFactory;
use Market\Order\Translator\OrderUpdateAmountRecordDBTranslator;
use Market\Order\Adapter\OrderUpdateAmountRecord\Query\OrderUpdateAmountRecordRowQuery;

class OrderUpdateAmountRecordDBAdapter implements IOrderUpdateAmountRecordAdapter
{
    protected $translator;

    protected $rowQuery;

    protected $translatorFactory;

    public function __construct()
    {
        $this->translator = new OrderUpdateAmountRecordDBTranslator();
        $this->rowQuery = new OrderUpdateAmountRecordRowQuery();
        $this->translatorFactory = new TranslatorFactory();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowQuery);
        unset($this->translatorFactory);
    }
    
    protected function getOrderUpdateAmountRecordDBTranslator() : OrderUpdateAmountRecordDBTranslator
    {
        return $this->translator;
    }

    protected function getOrderUpdateAmountRecordRowQuery() : OrderUpdateAmountRecordRowQuery
    {
        return $this->rowQuery;
    }
    
    protected function getTranslatorFactory() : TranslatorFactory
    {
        return $this->translatorFactory;
    }
    
    protected function fetchTranslator($category)
    {
        return $this->getTranslatorFactory()->getTranslator($category);
    }

    public function add(Order $order, OrderUpdateAmountRecord $orderUpdateAmountRecord) : bool
    {
        $info = array();

        $orderTranslator = $this->fetchTranslator($order->getCategory());

        $orderInfo = $orderTranslator->objectToArray($order, array('id'));

        $orderUpdateAmountRecordInfo = $this->getOrderUpdateAmountRecordDBTranslator()->objectToArray(
            $orderUpdateAmountRecord,
            array('amount', 'remark', 'createTime')
        );

        $info = array_merge($orderInfo, $orderUpdateAmountRecordInfo);

        $id = $this->getOrderUpdateAmountRecordRowQuery()->add($info);

        if (!$id) {
            return false;
        }

        return true;
    }

    public function fetchOne($id) : OrderUpdateAmountRecord
    {
        $info = array();

        $info = $this->getOrderUpdateAmountRecordRowQuery()->getOne($id);
        
        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullOrderUpdateAmountRecord::getInstance();
        }

        $orderUpdateAmountRecord = $this->getOrderUpdateAmountRecordDBTranslator()->arrayToObject($info);

        return $orderUpdateAmountRecord;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            if (isset($filter['order']) && !empty($filter['order'])) {
                $info['order_id'] = $filter['order'];
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    private function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $orderUpdateAmountRecord = new OrderUpdateAmountRecord();

            if (isset($sort['id'])) {
                $info = $this->getOrderUpdateAmountRecordDBTranslator()->objectToArray(
                    $orderUpdateAmountRecord,
                    array('id')
                );
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }

    public function fetchList(array $ids) : array
    {
        $updateAmountRecordInfoList = array();
        
        $updateAmountRecordInfoList = $this->getOrderUpdateAmountRecordRowQuery()->getList($ids);

        if (empty($updateAmountRecordInfoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $orderUpdateAmountRecordList = array();

        foreach ($updateAmountRecordInfoList as $orderUpdateAmountRecordInfo) {
            $orderUpdateAmountRecord = $this->getOrderUpdateAmountRecordDBTranslator()->arrayToObject(
                $orderUpdateAmountRecordInfo
            );

            $orderUpdateAmountRecordList[] = $orderUpdateAmountRecord;
        }

        return $orderUpdateAmountRecordList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        $rowCacheQuery = $this->getOrderUpdateAmountRecordRowQuery();

        $condition = $this->formatFilter($filter);
        $condition .= $this->formatSort($sort);

        //查询数据
        $orderUpdateAmountRecordList = $rowCacheQuery->find($condition, $offset, $size);

        if (empty($orderUpdateAmountRecordList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array(array(), 0);
        }

        $ids = array();
        $primaryKey = $rowCacheQuery->getPrimaryKey();
        foreach ($orderUpdateAmountRecordList as $info) {
            $ids[] = $info[$primaryKey];
        }

        $count = 0;

        $count = sizeof($ids);
        if ($count  == $size || $offset > 0) {
            $count = $rowCacheQuery->count($condition);
        }

        return array($this->fetchList($ids), $count);
    }
}
