<?php
namespace Market\Order\Adapter\OrderCommodity;

use Market\Order\Model\Order;
use Market\Order\Model\OrderCommodity;

interface IOrderCommodityAdapter
{
    public function add(Order $order, OrderCommodity $orderCommodity, array $keys) : bool;

    public function fetchOne($id) : OrderCommodity;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function orderCommodityList(
        array $filter = array()
    ) : array;
}
