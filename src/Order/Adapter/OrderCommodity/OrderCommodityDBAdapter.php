<?php
namespace Market\Order\Adapter\OrderCommodity;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Market\Order\Model\Order;
use Market\Order\Model\ITradeAble;
use Market\Order\Model\OrderCommodity;
use Market\Order\Model\NullOrderCommodity;
use Market\Order\Translator\OrderDBTranslator;
use Market\Order\Translator\TranslatorFactory;
use Market\Order\Translator\OrderCommodityDBTranslator;
use Market\Order\Adapter\OrderCommodity\Query\OrderCommodityRowCacheQuery;

use Snapshot\Repository\Snapshot\SnapshotRepository;
use Snapshot\Repository\RepositoryFactory;

use Market\Service\ServiceOrder\Model\ServiceOrder;

class OrderCommodityDBAdapter implements IOrderCommodityAdapter
{
    protected $orderCommodityDBTranslator;

    protected $orderCommodityRowCacheQuery;

    protected $snapshotRepository;

    protected $repositoryFactory;

    protected $translatorFactory;

    public function __construct()
    {
        $this->orderCommodityDBTranslator = new OrderCommodityDBTranslator();
        $this->orderCommodityRowCacheQuery = new OrderCommodityRowCacheQuery();
        $this->snapshotRepository = new SnapshotRepository();
        $this->repositoryFactory = new RepositoryFactory();
        $this->translatorFactory = new TranslatorFactory();
    }

    public function __destruct()
    {
        unset($this->orderCommodityDBTranslator);
        unset($this->orderCommodityRowCacheQuery);
        unset($this->orderDBTranslator);
        unset($this->snapshotRepository);
        unset($this->repositoryFactory);
        unset($this->translatorFactory);
    }
    
    protected function getOrderCommodityDBTranslator() : OrderCommodityDBTranslator
    {
        return $this->orderCommodityDBTranslator;
    }

    protected function getOrderCommodityRowCacheQuery() : OrderCommodityRowCacheQuery
    {
        return $this->orderCommodityRowCacheQuery;
    }
    
    protected function getSnapshotRepository() : SnapshotRepository
    {
        return $this->snapshotRepository;
    }
    
    protected function getRepositoryFactory() : RepositoryFactory
    {
        return $this->repositoryFactory;
    }

    protected function getSnapshotObjectRepository($category)
    {
        return $this->getRepositoryFactory()->getRepository($category);
    }

    protected function getTranslatorFactory() : TranslatorFactory
    {
        return $this->translatorFactory;
    }
    
    protected function fetchTranslator($category)
    {
        return $this->getTranslatorFactory()->getTranslator($category);
    }

    public function add(Order $order, OrderCommodity $orderCommodity, array $keys) : bool
    {
        $info = array();

        $orderTranslator = $this->fetchTranslator($order->getCategory());

        $orderInfo = $orderTranslator->objectToArray($order, array('id'));

        $orderCommodityInfo = $this->getOrderCommodityDBTranslator()->objectToArray($orderCommodity, $keys);

        $info = array_merge($orderInfo, $orderCommodityInfo);
        $id = $this->getOrderCommodityRowCacheQuery()->add($info);

        if (!$id) {
            return false;
        }

        return true;
    }

    public function fetchOne($id) : OrderCommodity
    {
        $info = array();

        $info = $this->getOrderCommodityRowCacheQuery()->getOne($id);
        
        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullOrderCommodity::getInstance();
        }

        $orderCommodity = $this->getOrderCommodityDBTranslator()->arrayToObject($info);

        if ($orderCommodity instanceof OrderCommodity) {
            $snapshot = $this->getSnapshotRepository()->fetchOne($orderCommodity->getSnapshot()->getId());
            $orderCommodity->setSnapshot($snapshot);

            $snapshotObjectRepository = $this->getSnapshotObjectRepository($snapshot->getCategory());
            $snapshotObject = $snapshotObjectRepository->fetchOne($snapshot->getSnapshotObject()->getId());
            $orderCommodity->setCommodity($snapshotObject);
        }

        return $orderCommodity;
    }

    private function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $orderCommodity = new OrderCommodity();

            if (isset($sort['id'])) {
                $info = $this->getOrderCommodityDBTranslator()->objectToArray(
                    $orderCommodity,
                    array('id')
                );
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }

    private function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $order = new ServiceOrder();

            $orderTranslator = $this->fetchTranslator(ITradeAble::ORDER_CATEGORY['SERVICE']);
            
            if (isset($filter['orderIds'])) {
                $orderIds = $filter['orderIds'];
                $info = $orderTranslator->objectToArray($order, array('id'));
                if (is_numeric($orderIds)) {
                    $condition .= key($info).' = '.$orderIds;
                }
                if (is_array($orderIds)) {
                    $condition .= key($info).' IN ('.implode(',', $orderIds).')';
                }
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    public function fetchList(array $ids) : array
    {
        $orderCommodityInfoList = $this->getOrderCommodityRowCacheQuery()->getList($ids);

        if (empty($orderCommodityInfoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $orderCommodityList = array();

        $translator = $this->getOrderCommodityDBTranslator();
        foreach ($orderCommodityInfoList as $orderCommodityInfo) {
            $orderCommodity = $translator->arrayToObject($orderCommodityInfo);
            
            $snapshot = $this->getSnapshotRepository()->fetchOne($orderCommodity->getSnapshot()->getId());

            $orderCommodity->setSnapshot($snapshot);

            // $snapshotObjectRepository = $this->getSnapshotObjectRepository($snapshot->getCategory());

            // $snapshotObject = $snapshotObjectRepository->fetchOne($snapshot->getSnapshotObject()->getId());

            $orderCommodity->setCommodity($snapshot->getSnapshotObject());
            $commodityId = $snapshot->getId().'_'.$snapshot->getSnapshotObject()->getId();
            $orderCommodity->getCommodity()->setId($commodityId);

            $orderCommodityList[] = $orderCommodity;
        }

        return $orderCommodityList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        $rowCacheQuery = $this->getOrderCommodityRowCacheQuery();

        $condition = $this->formatFilter($filter);
        $condition .= $this->formatSort($sort);

        //查询数据
        $orderCommodityList = $rowCacheQuery->find($condition, $offset, $size);

        if (empty($orderCommodityList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $ids = array();
        $primaryKey = $rowCacheQuery->getPrimaryKey();
        foreach ($orderCommodityList as $info) {
            $ids[] = $info[$primaryKey];
        }

        $count = 0;

        $count = sizeof($ids);
        if ($count  == $size || $offset > 0) {
            $count = $rowCacheQuery->count($condition);
        }

        return array($this->fetchList($ids), $count);
    }
    
    public function orderCommodityList(
        array $filter = array()
    ) : array {
        $rowCacheQuery = $this->getOrderCommodityRowCacheQuery();
        $condition = '';
        $offset = 0;
        $size = 100000;

        $condition = $this->formatFilter($filter);

        //查询数据
        $listTemp = $this->getOrderCommodityRowCacheQuery()->find($condition, $offset, $size);

        if (empty($listTemp)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $ids = array();
        $primaryKey = $rowCacheQuery->getPrimaryKey();
        foreach ($listTemp as $info) {
            $ids[] = $info[$primaryKey];
        }

        $orderCommodityInfoList = $this->getOrderCommodityRowCacheQuery()->getList($ids);

        $orderCommodityList = array();
        $orderCommodityListTemp = $this->fetchList($ids);

        foreach ($orderCommodityListTemp as $orderCommodity) {
            $orderCommodityList[$orderCommodity->getId()] = $orderCommodity;
        }
   
        foreach ($orderCommodityInfoList as $val) {
            $list[$val['order_id']][] = isset($orderCommodityList[$val['order_commodity_id']]) ?
            $orderCommodityList[$val['order_commodity_id']] :
            NullOrderCommodity::getInstance();
        }

        return $list;
    }
}
