<?php
namespace Market\Order\Adapter\OrderCommodity\Query;

use Marmot\Framework\Query\RowCacheQuery;

class OrderCommodityRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'order_commodity_id',
            new Persistence\OrderCommodityCache(),
            new Persistence\OrderCommodityDb()
        );
    }
}
