<?php
namespace Market\Order\Adapter\OrderCommodity\Query\Persistence;

use Marmot\Framework\Classes\Db;

class OrderCommodityDb extends Db
{
    /**
     * 构造函数初始化key和表名一致
     */
    public function __construct()
    {
        parent::__construct('market_order_commodity');
    }
}
