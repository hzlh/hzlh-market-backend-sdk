<?php
namespace Market\Order\Command;

abstract class PlaceOrderCommonOrderCommand extends PlaceOrderCommand
{
    public $orderCommodities;

    public $orderAddress;

    public $memberCouponIds;

    public $remark;
    
    public function __construct(
        array $orderCommodities,
        array $orderAddress,
        array $memberCouponIds,
        string $remark,
        int $buyerMemberAccountId = 0,
        int $id = 0
    ) {
        parent::__construct(
            $buyerMemberAccountId,
            $id
        );
        $this->orderCommodities = $orderCommodities;
        $this->orderAddress = $orderAddress;
        $this->memberCouponIds = $memberCouponIds;
        $this->remark = $remark;
    }
}
