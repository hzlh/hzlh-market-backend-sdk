<?php
namespace Market\Order\Command;

use Marmot\Interfaces\ICommand;

abstract class PlaceOrderCommand implements ICommand
{
    public $buyerMemberAccountId;

    public $id;

    public function __construct(
        int $buyerMemberAccountId = 0,
        int $id = 0
    ) {
        $this->buyerMemberAccountId = $buyerMemberAccountId;
        $this->id = $id;
    }
}
