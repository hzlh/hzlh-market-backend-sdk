<?php
namespace Market\Order\Command;

use Marmot\Interfaces\ICommand;

abstract class OrderCommand implements ICommand
{
    public $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
