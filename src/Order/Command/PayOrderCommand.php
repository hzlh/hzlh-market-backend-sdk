<?php
namespace Market\Order\Command;

use Marmot\Interfaces\ICommand;

abstract class PayOrderCommand implements ICommand
{
    public $paymentType;

    public $id;

    public function __construct(
        int $paymentType = 0,
        int $id = 0
    ) {
        $this->paymentType = $paymentType;
        $this->id = $id;
    }
}
