<?php
namespace Market\Order\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class OrderAddressSchema extends SchemaProvider
{
    protected $resourceType = 'marketOrderAddress';

    public function getId($orderAddress)
    {
        return $orderAddress->getId();
    }

    public function getAttributes($orderAddress) : array
    {
        return [
            'cellphone'  => $orderAddress->getCellphone()
        ];
    }

    public function getRelationships($orderAddress, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'snapshot' => [self::DATA => $orderAddress->getSnapshot()],
            'deliveryAddress' => [self::DATA => $orderAddress->getDeliveryAddress()]
        ];
    }
}
