<?php
namespace Market\Order\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class OrderCommoditySchema extends SchemaProvider
{
    protected $resourceType = 'marketOrderCommodities';

    public function getId($orderCommodity)
    {
        return $orderCommodity->getId();
    }

    public function getAttributes($orderCommodity) : array
    {
        return [
            'number'  => $orderCommodity->getNumber(),
            'skuIndex'  => $orderCommodity->getSkuIndex(),
        ];
    }

    public function getRelationships($orderCommodity, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'snapshot' => [self::DATA => $orderCommodity->getSnapshot()],
            'commodity' => [self::DATA => $orderCommodity->getCommodity()]
        ];
    }
}
