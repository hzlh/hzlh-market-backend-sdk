<?php
namespace Market\Order\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class OrderUpdateAmountRecordSchema extends SchemaProvider
{
    protected $resourceType = 'marketOrderUpdateAmountRecords';

    public function getId($orderUpdateAmountRecord)
    {
        return $orderUpdateAmountRecord->getId();
    }

    public function getAttributes($orderUpdateAmountRecord) : array
    {
        return [
            'amount'  => $orderUpdateAmountRecord->getAmount(),
            'remark'  => $orderUpdateAmountRecord->getRemark(),
            'createTime'  => $orderUpdateAmountRecord->getCreateTime(),
        ];
    }
}
