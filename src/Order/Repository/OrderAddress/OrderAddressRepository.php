<?php
namespace Market\Order\Repository\OrderAddress;

use Market\Order\Model\Order;
use Market\Order\Model\OrderAddress;
use Market\Order\Adapter\OrderAddress\IOrderAddressAdapter;
use Market\Order\Adapter\OrderAddress\OrderAddressDBAdapter;

class OrderAddressRepository implements IOrderAddressAdapter
{
    private $adapter;
    
    public function __construct()
    {
        $this->adapter = new OrderAddressDBAdapter();
    }

    public function setAdapter(IOrderAddressAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    protected function getAdapter() : IOrderAddressAdapter
    {
        return $this->adapter;
    }

    public function add(Order $order, OrderAddress $orderAddress) : bool
    {
        return $this->getAdapter()->add($order, $orderAddress);
    }

    public function edit(OrderAddress $orderAddress) : bool
    {
        return $this->getAdapter()->edit($orderAddress);
    }

    public function fetchOne($id) : OrderAddress
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function orderAddressList(
        array $filter = array()
    ) : array {
        return $this->getAdapter()->orderAddressList($filter);
    }
}
