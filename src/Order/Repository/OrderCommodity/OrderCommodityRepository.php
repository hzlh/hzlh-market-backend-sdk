<?php
namespace Market\Order\Repository\OrderCommodity;

use Market\Order\Model\Order;
use Market\Order\Model\OrderCommodity;
use Market\Order\Adapter\OrderCommodity\IOrderCommodityAdapter;
use Market\Order\Adapter\OrderCommodity\OrderCommodityDBAdapter;

class OrderCommodityRepository implements IOrderCommodityAdapter
{
    private $adapter;
    
    public function __construct()
    {
        $this->adapter = new OrderCommodityDBAdapter();
    }

    public function setAdapter(IOrderCommodityAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    protected function getAdapter() : IOrderCommodityAdapter
    {
        return $this->adapter;
    }

    public function add(Order $order, OrderCommodity $orderCommodity, array $keys) : bool
    {
        return $this->getAdapter()->add($order, $orderCommodity, $keys);
    }

    public function fetchOne($id) : OrderCommodity
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function orderCommodityList(
        array $filter = array()
    ) : array {
        return $this->getAdapter()->orderCommodityList($filter);
    }
}
