<?php
namespace Market\Order\Repository\OrderSearch;

use Market\Order\Model\Order;
use Market\Order\Adapter\OrderSearch\IOrderSearchAdapter;
use Market\Order\Adapter\OrderSearch\OrderSearchDBAdapter;

class OrderSearchRepository implements IOrderSearchAdapter
{
    private $adapter;
    
    public function __construct()
    {
        $this->adapter = new OrderSearchDBAdapter();
    }

    public function setAdapter(IOrderSearchAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    protected function getAdapter() : IOrderSearchAdapter
    {
        return $this->adapter;
    }

    public function add(Order $order) : bool
    {
        return $this->getAdapter()->add($order);
    }

    public function orderSearchGetOrder(
        array $filter = array()
    ) : array {
        return $this->getAdapter()->orderSearchGetOrder($filter);
    }
}
