<?php
namespace Market\Order\Repository\OrderMemberCoupon;

use Market\Order\Model\Order;
use Market\Order\Adapter\OrderMemberCoupon\IOrderMemberCouponAdapter;
use Market\Order\Adapter\OrderMemberCoupon\OrderMemberCouponDBAdapter;

use Member\MemberCoupon\Model\MemberCoupon;

class OrderMemberCouponRepository implements IOrderMemberCouponAdapter
{
    private $adapter;
    
    public function __construct()
    {
        $this->adapter = new OrderMemberCouponDBAdapter();
    }

    public function setAdapter(IOrderMemberCouponAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    protected function getAdapter() : IOrderMemberCouponAdapter
    {
        return $this->adapter;
    }

    public function add(Order $order, MemberCoupon $memberCoupon) : bool
    {
        return $this->getAdapter()->add($order, $memberCoupon);
    }

    public function delete(Order $order, MemberCoupon $memberCoupon) : bool
    {
        return $this->getAdapter()->delete($order, $memberCoupon);
    }
    
    public function orderMemberCouponList(
        array $filter = array()
    ) : array {
        return $this->getAdapter()->orderMemberCouponList($filter);
    }
}
