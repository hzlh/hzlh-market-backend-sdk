<?php
namespace Market\Order\Repository\OrderUpdateAmountRecord;

use Market\Order\Model\Order;
use Market\Order\Model\OrderUpdateAmountRecord;
use Market\Order\Adapter\OrderUpdateAmountRecord\IOrderUpdateAmountRecordAdapter;
use Market\Order\Adapter\OrderUpdateAmountRecord\OrderUpdateAmountRecordDBAdapter;

class OrderUpdateAmountRecordRepository implements IOrderUpdateAmountRecordAdapter
{
    private $adapter;
    
    public function __construct()
    {
        $this->adapter = new OrderUpdateAmountRecordDBAdapter();
    }

    public function setAdapter(IOrderUpdateAmountRecordAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    protected function getAdapter() : IOrderUpdateAmountRecordAdapter
    {
        return $this->adapter;
    }

    public function add(Order $order, OrderUpdateAmountRecord $orderUpdateAmountRecord) : bool
    {
        return $this->getAdapter()->add($order, $orderUpdateAmountRecord);
    }

    public function fetchOne($id) : OrderUpdateAmountRecord
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
}
