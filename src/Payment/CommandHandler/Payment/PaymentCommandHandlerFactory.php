<?php
namespace Market\Payment\CommandHandler\Payment;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class PaymentCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Payment\Command\Payment\PaymentCommand'=>
        'Market\Payment\CommandHandler\Payment\PaymentCommandHandler',
        'Market\Payment\Command\Payment\PaymentFailureCommand'=>
        'Market\Payment\CommandHandler\Payment\PaymentFailureCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
