<?php
namespace Market\Payment\CommandHandler\Payment;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Payment\Model\Payment;
use Market\Payment\Model\IPayAble;
use Market\Payment\Model\PaymentFactory;
use Market\Payment\Command\Payment\PaymentCommand;

use Member\PaymentPassword\Model\ValidatePaymentPasswordTrait;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class PaymentCommandHandler implements ICommandHandler
{
    use ValidatePaymentPasswordTrait;
    
    private $paymentFactory;

    public function __construct()
    {
        $this->paymentFactory = new PaymentFactory();
    }

    public function __destruct()
    {
        unset($this->paymentFactory);
    }

    protected function getPaymentFactory() : PaymentFactory
    {
        return $this->paymentFactory;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof PaymentCommand)) {
            throw new \InvalidArgumentException;
        }

        $transactionObject = $this->getPaymentFactory()->getModel($command->paymentId);
        $service = $this->getPaymentFactory()->getService($command->paymentId);
        $transactionNumber = $command->transactionNumber;
        $transactionInfo = $command->transactionInfo;
        $temp = explode('_', $command->paymentId);
        $type = $temp[0];
        $paymentAmount = 0;

        if ($type == IPayAble::TYPE['ORDER']) {
            $paymentAmount = !empty($transactionObject->getPaidAmount()) ? $transactionObject->getPaidAmount() : 0;
        }

        if ($type == IPayAble::TYPE['DEPOSIT']) {
            $paymentAmount = !empty($transactionObject->getAmount()) ? $transactionObject->getAmount() : 0;
        }

        if ($command->paymentType == Payment::PAYMENT_TYPE['WECHAT']) {
            $totalFee = isset($transactionInfo['total_fee']) ? ($transactionInfo['total_fee']/100) : 0;
            if ($paymentAmount != $totalFee) {
                Core::setLastError(PAYMENT_AMOUNT_MISMATCH);
                return false;
            }
        }

        if ($command->paymentType == Payment::PAYMENT_TYPE['ALIPAY']) {
            $totalAmount = isset($transactionInfo['total_amount']) ? $transactionInfo['total_amount'] : 0;
            if ($paymentAmount != $totalAmount) {
                Core::setLastError(PAYMENT_AMOUNT_MISMATCH);
                return false;
            }
        }

        if ($command->paymentType == Payment::PAYMENT_TYPE['BALANCE']) {
            $paymentPassword = $this->fetchPaymentPassword($transactionObject->getBuyerMemberAccount()->getId());

            if (empty($command->paymentPassword) ||
                !$this->paymentPasswordIsCorrect($paymentPassword, $command->paymentPassword)
            ) {
                Core::setLastError(PASSWORD_INCORRECT);
                return false;
            }

            $transactionNumber = $transactionObject->generateTransactionNumber();
        }

        $payment = new Payment(
            $command->paymentType,
            Core::$container->get('time'),
            $transactionNumber,
            $command->transactionInfo
        );
        $transactionObject->setPayment($payment);

        $service = new $service($transactionObject);

        return $service->pay();
    }
}
