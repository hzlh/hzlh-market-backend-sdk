<?php
namespace Market\Payment\CommandHandler\Payment;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Payment\Model\Payment;
use Market\Payment\Model\PaymentFactory;
use Market\Payment\Command\Payment\PaymentFailureCommand;

class PaymentFailureCommandHandler implements ICommandHandler
{

    private $paymentFactory;

    public function __construct()
    {
        $this->paymentFactory = new PaymentFactory();
    }

    public function __destruct()
    {
        unset($this->paymentFactory);
    }

    protected function getPaymentFactory() : PaymentFactory
    {
        return $this->paymentFactory;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof PaymentFailureCommand)) {
            throw new \InvalidArgumentException;
        }

        $transactionObject = $this->getPaymentFactory()->getModel($command->paymentId);

        $transactionObject->setFailureReason($command->failureReason);

        return $transactionObject->paymentFailure();
    }
}
