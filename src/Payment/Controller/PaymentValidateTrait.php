<?php
namespace Market\Payment\Controller;

use WidgetRules\Common\WidgetRules;
use WidgetRules\Payment\WidgetRules as PaymentWidgetRules;

trait PaymentValidateTrait
{
    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function getPaymentWidgetRules() : PaymentWidgetRules
    {
        return PaymentWidgetRules::getInstance();
    }

    protected function validatePayScenario(
        $paymentType,
        $transactionNumber,
        $transactionInfo
    ) {
        return $this->getPaymentWidgetRules()->paymentType($paymentType)
        && $this->getWidgetRules()->formatString($transactionNumber, 'transactionNumber')
        && $this->getWidgetRules()->formatArray($transactionInfo, 'transactionInfo');
    }

    protected function validatePaymentFailureScenario(
        $failureReason
    ) {
        return $this->getWidgetRules()->formatArray($failureReason, 'failureReason');
    }
}
