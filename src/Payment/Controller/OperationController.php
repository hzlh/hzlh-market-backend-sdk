<?php
namespace Market\Payment\Controller;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Market\Payment\Model\Payment;
use Market\Payment\Model\PaymentFactory;
use Market\Payment\Command\Payment\PaymentCommand;
use Market\Payment\Command\Payment\PaymentFailureCommand;
use Market\Payment\CommandHandler\Payment\PaymentCommandHandlerFactory;

class OperationController extends Controller
{
    use JsonApiTrait, PaymentValidateTrait;

    private $paymentFactory;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->paymentFactory = new PaymentFactory();
        $this->commandBus = new CommandBus(new PaymentCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->paymentFactory);
        unset($this->commandBus);
    }

    protected function getPaymentFactory() : PaymentFactory
    {
        return $this->paymentFactory;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    
    /**
     * /payments/{paymentId}
     * 充值功能, 通过POST传参
     * @param jsonApi
     * @return jsonApi
     */
    public function pay(string $paymentId)
    {
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];

        $paymentType = $attributes['paymentType'];
        $transactionNumber = isset($attributes['transactionNumber']) ? $attributes['transactionNumber'] : '';
        $transactionInfo = isset($attributes['transactionInfo']) ? $attributes['transactionInfo'] : array();
        $paymentPassword = isset($attributes['paymentPassword']) ? $attributes['paymentPassword'] : 0;
    
        if ($this->validatePayScenario(
            $paymentType,
            $transactionNumber,
            $transactionInfo
        )) {
            $command = new PaymentCommand(
                $paymentId,
                $transactionNumber,
                $transactionInfo,
                $paymentType,
                $paymentPassword
            );

            if ($this->getCommandBus()->send($command)) {
                $payment = $this->getPaymentFactory()->getModel($command->paymentId);
                if (!$payment instanceof INull) {
                    $this->getResponse()->setStatusCode(201);
                    $paymentView = $this->getPaymentFactory()->getView($command->paymentId);
                    $this->render(new $paymentView($payment));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * /payments/{paymentId}/paymentFailure
     * 支付失败功能, 通过PATCH传参
     * @param jsonApi
     * @return jsonApi
     */
    public function paymentFailure(string $paymentId)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];

        $failureReason = isset($attributes['failureReason']) ? $attributes['failureReason'] : array();
    
        if ($this->validatePaymentFailureScenario(
            $failureReason
        )) {
            $command = new PaymentFailureCommand(
                $paymentId,
                $failureReason
            );

            if ($this->getCommandBus()->send($command)) {
                $payment = $this->getPaymentFactory()->getModel($command->paymentId);
                if (!$payment instanceof INull) {
                    $paymentView = $this->getPaymentFactory()->getView($command->paymentId);
                    $this->render(new $paymentView($payment));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
