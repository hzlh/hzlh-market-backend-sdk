<?php
namespace Market\Payment\Model;

interface IPayAble
{
    const TYPE = array(
        'ORDER' => 1,
        'DEPOSIT' => 2
    );

    public function pay() : bool;
    
    public function getPaymentId() : string;
}
