<?php
namespace Market\Payment\Model;

class Payment
{
    const PAYMENT_TYPE = array(
        'NULL' => 0,
        'WECHAT' => 1,
        'ALIPAY' => 2,
        'CHANPAY' => 3,
        'BALANCE' => 4,
    );

    const PAYMENT_TYPE_CN = array(
        self::PAYMENT_TYPE['NULL'] => '第三方支付',
        self::PAYMENT_TYPE['WECHAT'] => '微信',
        self::PAYMENT_TYPE['ALIPAY'] => '支付宝',
        self::PAYMENT_TYPE['CHANPAY'] => '畅捷',
        self::PAYMENT_TYPE['BALANCE'] => '余额',
    );
    
    private $type;

    private $time;

    private $transactionNumber;

    private $transactionInfo;

    public function __construct(
        int $type = self::PAYMENT_TYPE['NULL'],
        int $time = 0,
        string $transactionNumber = '',
        array $transactionInfo = array()
    ) {
        $this->type = $type;
        $this->time = $time;
        $this->transactionNumber = $transactionNumber;
        $this->transactionInfo = $transactionInfo;
    }

    public function __destruct()
    {
        unset($this->type);
        unset($this->time);
        unset($this->transactionNumber);
        unset($this->transactionInfo);
    }

    public function getType() : int
    {
        return $this->type;
    }

    public function getTime() : int
    {
        return $this->time;
    }

    public function getTransactionNumber() : string
    {
        return $this->transactionNumber;
    }

    public function getTransactionInfo() : array
    {
        return $this->transactionInfo;
    }
}
