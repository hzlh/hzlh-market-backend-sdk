<?php
namespace Market\Payment\Model;

use Common\Repository\NullRepository;

class PaymentFactory
{
    const MAPS = array(
        IPayAble::TYPE['ORDER']=>'\Market\Service\ServiceOrder\Repository\ServiceOrder\ServiceOrderRepository',
        IPayAble::TYPE['DEPOSIT']=>'\Member\Deposit\Repository\Deposit\DepositRepository',
    );
    
    const VIEW_MAPS = array(
        IPayAble::TYPE['ORDER']=>'\Market\Service\ServiceOrder\View\ServiceOrderView',
        IPayAble::TYPE['DEPOSIT']=>'\Member\Deposit\View\DepositView',
    );
    
    const SERIVCE_MAPS = array(
        IPayAble::TYPE['ORDER']=>'\Market\Service\ServiceOrder\Service\ServiceOrderService',
        IPayAble::TYPE['DEPOSIT']=>'\Member\Deposit\Service\DepositService',
    );

    public function getModel(string $paymentId) : IPayAble
    {
        $temp = explode('_', $paymentId);
        $type = $temp[0];
        $id = $temp[1];

        $repository = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';
        $repository = class_exists($repository) ? new $repository : NullRepository::getInstance();

        return $repository->fetchOne($id);
    }

    public function getView(string $paymentId) : string
    {
        $temp = explode('_', $paymentId);
        $type = $temp[0];

        return isset(self::VIEW_MAPS[$type]) ? self::VIEW_MAPS[$type] : '';
    }

    public function getService(string $paymentId) : string
    {
        $temp = explode('_', $paymentId);
        $type = $temp[0];

        return isset(self::SERIVCE_MAPS[$type]) ? self::SERIVCE_MAPS[$type] : '';
    }
}
