<?php
namespace Market\Payment\Command\Payment;

use Marmot\Interfaces\ICommand;

class PaymentFailureCommand implements ICommand
{
    public $paymentId;

    public $failureReason;

    public function __construct(
        string $paymentId,
        array $failureReason
    ) {
        $this->paymentId = $paymentId;
        $this->failureReason = $failureReason;
    }
}
