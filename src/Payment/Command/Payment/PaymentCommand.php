<?php
namespace Market\Payment\Command\Payment;

use Marmot\Interfaces\ICommand;

class PaymentCommand implements ICommand
{
    public $paymentId;

    public $transactionNumber;

    public $transactionInfo;

    public $paymentType;
    
    public $paymentPassword;

    public function __construct(
        string $paymentId,
        string $transactionNumber,
        array $transactionInfo,
        int $paymentType = 0,
        int $paymentPassword = 0
    ) {
        $this->paymentId = $paymentId;
        $this->transactionNumber = $transactionNumber;
        $this->transactionInfo = $transactionInfo;
        $this->paymentType = $paymentType;
        $this->paymentPassword = $paymentPassword;
    }
}
