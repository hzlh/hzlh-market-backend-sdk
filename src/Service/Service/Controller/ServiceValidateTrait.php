<?php
namespace Market\Service\Service\Controller;

use WidgetRules\Common\WidgetRules;

use WidgetRules\Service\WidgetRules as ServiceWidgetRules;

trait ServiceValidateTrait
{
    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function getServiceWidgetRules() : ServiceWidgetRules
    {
        return ServiceWidgetRules::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    protected function validateCommonScenario(
        $title,
        $detail,
        $price,
        $cover,
        $contract,
        $serviceObjects,
        $serviceCategoryId
    ) {
        return $this->getServiceWidgetRules()->title($title)
        && $this->getWidgetRules()->detail($detail)
        && $this->getServiceWidgetRules()->price($price)
        && $this->getWidgetRules()->image($cover, 'cover')
        && (empty($contract) ? true : $this->getServiceWidgetRules()->contract($contract))
        && (empty($serviceObjects) ? true : $this->getServiceWidgetRules()->serviceObjects($serviceObjects))
        && $this->getWidgetRules()->formatNumeric($serviceCategoryId, 'serviceCategoryId');
    }

    protected function validateAddScenario($enterpriseId)
    {
        return $this->getWidgetRules()->formatNumeric($enterpriseId, 'enterpriseId');
    }
}
