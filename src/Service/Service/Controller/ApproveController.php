<?php
namespace Market\Service\Service\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IApproveAbleController;

use Market\Service\Service\Model\Service;
use Market\Service\Service\View\ServiceView;
use Market\Service\Service\Repository\Service\ServiceRepository;
use Market\Service\Service\Command\Service\RejectServiceCommand;
use Market\Service\Service\Command\Service\ApproveServiceCommand;
use Market\Service\Service\CommandHandler\Service\ServiceCommandHandlerFactory;

use WidgetRules\Common\WidgetRules;

class ApproveController extends Controller implements IApproveAbleController
{
    use JsonApiTrait;

    private $repository;

    private $commandBus;

    private $widgetRules;
    
    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRepository();
        $this->commandBus = new CommandBus(new ServiceCommandHandlerFactory());
        $this->widgetRules = WidgetRules::getInstance();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
        unset($this->widgetRules);
    }

    protected function getServiceRepository() : ServiceRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getWidgetRules() : WidgetRules
    {
        return $this->widgetRules;
    }

    public function approve(int $id)
    {
        $command = new ApproveServiceCommand($id);

        if ($this->getCommandBus()->send($command)) {
            $service = $this->getServiceRepository()->fetchOne($command->id);

            if ($service instanceof Service) {
                $this->renderView(new ServiceView($service));
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateRejectScenario(
        $rejectReason
    ) {
        return $this->getWidgetRules()->rejectReason($rejectReason);
    }

    /**
     * 申请信息审核驳回功能,通过PATCH传参
     * @param int id 申请信息id
     *
     * @return jsonApi
     */
    public function reject(int $id)
    {
        $data = $this->getRequest()->patch('data');

        $rejectReason = isset($data['attributes']['rejectReason'])
        ? $data['attributes']['rejectReason']
        : '';

        if ($this->validateRejectScenario($rejectReason)) {
            $command = new RejectServiceCommand(
                $rejectReason,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $service = $this->getServiceRepository()->fetchOne($command->id);

                if ($service instanceof Service) {
                    $this->renderView(new ServiceView($service));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
