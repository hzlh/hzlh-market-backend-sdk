<?php
namespace Market\Service\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\Service\Model\Service;
use Market\Service\Service\Command\Service\AddServiceCommand;

use Member\Enterprise\Model\Enterprise;
use Member\Enterprise\Repository\Enterprise\EnterpriseRepository;

class AddServiceCommandHandler implements ICommandHandler
{
    use ServiceCommonCommandHandlerTrait;
    
    private $service;

    private $enterpriseRepository;

    public function __construct()
    {
        $this->service = new Service();
        $this->enterpriseRepository = new EnterpriseRepository();
    }

    public function __destruct()
    {
        unset($this->service);
        unset($this->enterpriseRepository);
    }

    protected function getService() : Service
    {
        return $this->service;
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }

    protected function fetchEnterprise(int $id) : Enterprise
    {
        return $this->getEnterpriseRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $enterprise = $this->fetchEnterprise($command->enterpriseId);

        $service = $this->getService();
        $service->setEnterprise($enterprise);
        $service = $this->executeAction($command, $service);

        if ($service->add()) {
            $command->id = $service->getId();
            return true;
        }
        
        return false;
    }
}
