<?php
namespace Market\Service\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\Service\Command\Service\RevokeServiceCommand;

class RevokeServiceCommandHandler implements ICommandHandler
{
    use ServiceCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof RevokeServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $service = $this->fetchService($command->id);

        return $service->revoke();
    }
}
