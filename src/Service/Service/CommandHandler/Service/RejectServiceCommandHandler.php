<?php
namespace Market\Service\Service\CommandHandler\Service;

use Common\Model\IApproveAble;
use Common\CommandHandler\RejectCommandHandler;

use Market\Service\Service\Repository\Service\ServiceRepository;

class RejectServiceCommandHandler extends RejectCommandHandler
{
    use ServiceCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchService($id);
    }
}
