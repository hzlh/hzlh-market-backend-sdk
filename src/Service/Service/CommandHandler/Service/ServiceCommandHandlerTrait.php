<?php
namespace Market\Service\Service\CommandHandler\Service;

use Market\Service\Service\Model\Service;
use Market\Service\Service\Repository\Service\ServiceRepository;

trait ServiceCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new ServiceRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : ServiceRepository
    {
        return $this->repository;
    }
    
    protected function fetchService(int $id) : Service
    {
        return $this->getRepository()->fetchOne($id);
    }
}
