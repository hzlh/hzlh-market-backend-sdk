<?php
namespace Market\Service\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Service\Service\Command\Service\AddServiceCommand'=>
        'Market\Service\Service\CommandHandler\Service\AddServiceCommandHandler',
        'Market\Service\Service\Command\Service\EditServiceCommand'=>
        'Market\Service\Service\CommandHandler\Service\EditServiceCommandHandler',
        'Market\Service\Service\Command\Service\ResubmitServiceCommand'=>
        'Market\Service\Service\CommandHandler\Service\ResubmitServiceCommandHandler',
        'Market\Service\Service\Command\Service\ApproveServiceCommand'=>
        'Market\Service\Service\CommandHandler\Service\ApproveServiceCommandHandler',
        'Market\Service\Service\Command\Service\RejectServiceCommand'=>
        'Market\Service\Service\CommandHandler\Service\RejectServiceCommandHandler',
        'Market\Service\Service\Command\Service\RevokeServiceCommand'=>
        'Market\Service\Service\CommandHandler\Service\RevokeServiceCommandHandler',
        'Market\Service\Service\Command\Service\CloseServiceCommand'=>
        'Market\Service\Service\CommandHandler\Service\CloseServiceCommandHandler',
        'Market\Service\Service\Command\Service\DeleteServiceCommand'=>
        'Market\Service\Service\CommandHandler\Service\DeleteServiceCommandHandler',
        'Market\Service\Service\Command\Service\OnShelfServiceCommand'=>
        'Market\Service\Service\CommandHandler\Service\OnShelfServiceCommandHandler',
        'Market\Service\Service\Command\Service\OffStockServiceCommand'=>
        'Market\Service\Service\CommandHandler\Service\OffStockServiceCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
