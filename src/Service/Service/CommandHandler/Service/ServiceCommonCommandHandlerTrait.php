<?php
namespace Market\Service\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;

use Market\Service\Service\Model\Price;
use Market\Service\Service\Model\Service;
use Market\Service\Service\Model\CalculationSubTrait;

use Market\Service\ServiceCategory\Model\ServiceCategory;
use Market\Service\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;

trait ServiceCommonCommandHandlerTrait
{
    use CalculationSubTrait;
    
    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return new ServiceCategoryRepository();
    }

    protected function fetchServiceCategory(int $id) : ServiceCategory
    {
        return $this->getServiceCategoryRepository()->fetchOne($id);
    }

    protected function executeAction(ICommand $command, Service $service) : Service
    {
        $service->setTitle($command->title);
        $service->setDetail($command->detail);
        $service->setCover($command->cover);
        $service->setContract($command->contract);
        $service->setServiceObjects($command->serviceObjects);
        $service->setPrice($this->price($command->price));
        $service->setServiceCategory($this->fetchServiceCategory($command->serviceCategoryId));
        $service->setTag($command->tag);
        $service->generateBinary($command->tag);

        return $service;
    }

    private function price($price) : Price
    {
        $minPrice = $this->subMinPrice($price);
        $maxPrice = $this->subMaxPrice($price);

        return new Price($price, $minPrice, $maxPrice);
    }
}
