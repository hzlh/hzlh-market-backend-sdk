<?php
namespace Market\Service\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\Service\Command\Service\DeleteServiceCommand;

class DeleteServiceCommandHandler implements ICommandHandler
{
    use ServiceCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof DeleteServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $service = $this->fetchService($command->id);

        return $service->delete();
    }
}
