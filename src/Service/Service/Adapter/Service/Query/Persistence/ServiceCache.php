<?php
namespace Market\Service\Service\Adapter\Service\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class ServiceCache extends Cache
{
    /**
     * 构造函数初始化key和表名一致
     */
    public function __construct()
    {
        parent::__construct('market_service');
    }
}
