<?php
namespace Market\Service\Service\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class ServiceSchema extends SchemaProvider
{
    protected $resourceType = 'marketServices';

    public function getId($service)
    {
        return $service->getId();
    }

    public function getAttributes($service) : array
    {
        $rejectReason = $service->getRejectReason()->getData();
        $rejectReasonSchema = !empty($rejectReason) ? $rejectReason['rejectReason'] : '';

        return [
            'number'  => $service->generateNumber(),
            'title'  => $service->getTitle(),
            'detail'  => $service->getDetail(),
            'cover'  => $service->getCover(),
            'contract'  => $service->getContract(),
            'price'  => $service->getPrice()->getPrice(),
            'minPrice'  => $service->getPrice()->getMinPrice(),
            'maxPrice'  => $service->getPrice()->getMaxPrice(),
            'serviceObjects'  => $service->getServiceObjects(),
            'volume'  => $service->getVolume(),
            'attentionDegree'  => $service->getAttentionDegree(),
            'tag' => $service->getTag(),
            'binaries' => $service->getBinaries(),
            'pageViews'  => $service->getPageViews(),
            'applyStatus' => $service->getApplyStatus(),
            'rejectReason'  => $rejectReasonSchema,
            'status' => $service->getStatus(),
            'createTime' => $service->getCreateTime(),
            'updateTime' => $service->getUpdateTime(),
            'statusTime' => $service->getStatusTime()
        ];
    }

    public function getRelationships($service, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'serviceCategory' => [self::DATA => $service->getServiceCategory()],
            'snapshots' => [self::DATA => $service->getSnapshots()],
            'enterprise' => [self::DATA => $service->getEnterprise()]
        ];
    }
}
