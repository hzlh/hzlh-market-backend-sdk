<?php
namespace Market\Service\Service\Command\Service;

use Marmot\Interfaces\ICommand;

abstract class ServiceCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id = 0
    ) {
        $this->id = $id;
    }
}
