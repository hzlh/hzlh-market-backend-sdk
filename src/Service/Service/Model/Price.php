<?php
namespace Market\Service\Service\Model;

class Price
{
    private $price;

    private $minPrice;

    private $maxPrice;

    public function __construct(
        array $price = array(),
        float $minPrice = 0.0,
        float $maxPrice = 0.0
    ) {
        $this->price = $price;
        $this->minPrice = $minPrice;
        $this->maxPrice = $maxPrice;
    }

    public function __destruct()
    {
        unset($this->price);
        unset($this->minPrice);
        unset($this->maxPrice);
    }

    public function getPrice() : array
    {
        return $this->price;
    }

    public function getMinPrice() : float
    {
        return $this->minPrice;
    }

    public function getMaxPrice() : float
    {
        return $this->maxPrice;
    }
}
