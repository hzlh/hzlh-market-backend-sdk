<?php
namespace Market\Service\Service\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Common\Model\TagTrait;
use Common\Model\OperatAbleTrait;
use Common\Model\OnShelfAbleTrait;
use Common\Model\VerifyAbleTrait;
use Common\Model\ApproveAbleTrait;
use Common\Model\ResubmitAbleTrait;
use Common\Model\RejectReasonDocument;
use Common\Adapter\Document\RejectReasonDocumentAdapter;

use Member\Enterprise\Model\Enterprise;

use Member\Member\Model\Member;

use Market\Service\ServiceCategory\Model\ServiceCategory;

use Market\Product\Model\IProduct;

use Snapshot\Model\Snapshot;
use Snapshot\Model\SnapshotTrait;
use Snapshot\Model\ISnapshotAble;
use Snapshot\Model\SnapshotInfoDocument;

use Market\Service\Service\Translator\ServiceDBTranslator;
use Market\Service\Service\Repository\Service\ServiceRepository;

use Backend\Policy\Model\IPolicyRelationAble;

use BrowseRecord\Model\BrowseRecord;
use BrowseRecord\Model\IBrowseAble;

use Evaluation\Evaluation\Model\IEvaluation;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Service implements IObject, IProduct, IPolicyRelationAble, IEvaluation
{
    use Object,
        ApproveAbleTrait,
        VerifyAbleTrait,
        OperatAbleTrait,
        SnapshotTrait,
        OnShelfAbleTrait,
        ResubmitAbleTrait,
        TagTrait,
        ValidateTrait;

    const NUMBER_PREFIX = 'FW';

    const SERVICE_STATUS = array(
        'ON_SHELF' => 0, //上架
        'OFF_STOCK' => -2, //下架
        'REVOKED' => -4, //撤销
        'CLOSED' => -6, //关闭
        'DELETED' => -8, //删除
    );

    const SERVICE_OBJECTS = array(
        'NULL' => 0,
        'MICRO_ENTERPRISE' => 1, //微型企业
        'SMALL_ENTERPRISES' => 2, //小型企业
        'MEDIUM_ENTERPRISE' => 3, //中型企业
        'LARGE_ENTERPRISES' => 4, //大型企业
        'ENTREPRENEURIAL_INDIVIDUAL' => 5, //创业个人
        'INDIVIDUAL_MERCHANTS' => 6, //个体商户
        'OTHER' => 7, //其他
    );

    private $id;

    private $enterprise;

    private $serviceCategory;
    
    private $title;

    private $detail;

    private $price;

    private $cover;

    private $areaId;

    private $serviceObjects;

    private $contract;

    private $volume;

    private $attentionDegree;

    private $pageViews;

    private $snapshots;

    private $repository;

    private $translator;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->enterprise = new Enterprise();
        $this->serviceCategory = new ServiceCategory();
        $this->title = '';
        $this->detail = array();
        $this->price = new Price();
        $this->cover = array();
        $this->serviceObjects = array();
        $this->contract = array();
        $this->volume = 0;
        $this->attentionDegree = 0;
        $this->pageViews = 0;
        $this->snapshots = array();
        $this->tag = '';
        $this->binaries = 0;
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->rejectReason = new RejectReasonDocument();
        $this->status = self::SERVICE_STATUS['ON_SHELF'];
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->rejectReasonDocumentAdapter = new RejectReasonDocumentAdapter();
        $this->repository = new ServiceRepository();
        $this->translator = new ServiceDBTranslator();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->enterprise);
        unset($this->serviceCategory);
        unset($this->title);
        unset($this->detail);
        unset($this->price);
        unset($this->cover);
        unset($this->serviceObjects);
        unset($this->contract);
        unset($this->volume);
        unset($this->attentionDegree);
        unset($this->pageViews);
        unset($this->snapshots);
        unset($this->applyStatus);
        unset($this->rejectReason);
        unset($this->status);
        unset($this->tag);
        unset($this->areaId);
        unset($this->binaries);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->rejectReasonDocumentAdapter);
        unset($this->repository);
        unset($this->translator);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function generateNumber() : string
    {
        return self::NUMBER_PREFIX.date('Ymd', $this->getCreateTime()).$this->getId();
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    public function setServiceCategory(ServiceCategory $serviceCategory) : void
    {
        $this->serviceCategory = $serviceCategory;
    }

    public function getServiceCategory() : ServiceCategory
    {
        return $this->serviceCategory;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setDetail(array $detail) : void
    {
        $this->detail = $detail;
    }

    public function getDetail() : array
    {
        return $this->detail;
    }
    
    public function setPrice(Price $price) : void
    {
        $this->price = $price;
    }

    public function getPrice() : Price
    {
        return $this->price;
    }
    
    public function setCover(array $cover) : void
    {
        $this->cover = $cover;
    }

    public function getCover() : array
    {
        return $this->cover;
    }
    
    public function setContract(array $contract) : void
    {
        $this->contract = $contract;
    }

    public function getContract() : array
    {
        return $this->contract;
    }
    
    public function setServiceObjects(array $serviceObjects) : void
    {
        $this->serviceObjects = $serviceObjects;
    }

    public function getServiceObjects() : array
    {
        return $this->serviceObjects;
    }
    
    public function setVolume(int $volume) : void
    {
        $this->volume = $volume;
    }

    public function getVolume() : int
    {
        return $this->volume;
    }
    
    public function setAttentionDegree(int $attentionDegree) : void
    {
        $this->attentionDegree = $attentionDegree;
    }

    public function getAttentionDegree() : int
    {
        return $this->attentionDegree;
    }

    public function setPageViews(int $pageViews) : void
    {
        $this->pageViews = $pageViews;
    }

    public function getPageViews() : int
    {
        return $this->pageViews;
    }
    
    public function getAttribute()
    {
        return $this->getServiceCategory()->getName();
    }

    public function getProductPrice() : float
    {
        return $this->getPrice()->getMinPrice();
    }

    public function addSnapshot(Snapshot $snapshot) : void
    {
        $this->snapshots[] = $snapshot;
    }

    public function getSnapshots() : array
    {
        return $this->snapshots;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::SERVICE_STATUS) ?
                        $status :
                        self::SERVICE_STATUS['ON_SHELF'];
    }

    protected function getRepository() : ServiceRepository
    {
        return $this->repository;
    }

    protected function getTranslator() : ServiceDBTranslator
    {
        return $this->translator;
    }

    protected function addAction() : bool
    {
        if (!$this->validate()) {
            return false;
        }

        return $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        if (!$this->isOffStock() && !$this->isReject()) {
            Core::setLastError(RESOURCE_STATUS_OFF_STOCK);
            return false;
        }

        if (!$this->validate()) {
            return false;
        }

        $this->setUpdateTime(Core::$container->get('time'));
        $this->setApplyStatus(self::APPLY_STATUS['PENDING']);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setStatus(self::SERVICE_STATUS['ON_SHELF']);

        return $this->getRepository()->edit($this);
    }

    protected function capturAction() : Snapshot
    {
        $snapshot = new Snapshot();
        $service = $this->getTranslator()->objectToArray($this);

        $snapshot->setCategory(ISnapshotAble::CATEGORY['MARKET_SERVICE']);
        $snapshot->setSnapshotObject($this);

        $snapshotInfoDocument = new SnapshotInfoDocument();
        $snapshotInfoDocument->setData($service);
        $snapshot->setInfo($snapshotInfoDocument);

        return $snapshot;
    }

    protected function approveAction() : bool
    {
        if (!$this->isOnShelf()) {
            Core::setLastError(RESOURCE_STATUS_ON_SHELF);
            return false;
        }

        return $this->updateApplyStatus(self::APPLY_STATUS['APPROVE']) && $this->capture();
    }

    protected function rejectAction() : bool
    {
        if (!$this->isOnShelf()) {
            Core::setLastError(RESOURCE_STATUS_ON_SHELF);
            return false;
        }

        if (!$this->getRejectReasonDocumentAdapter()->add($this->getRejectReason())) {
            return false;
        }
        
        return $this->updateApplyStatus(self::APPLY_STATUS['REJECT']);
    }

    protected function updateApplyStatus(int $applyStatus) : bool
    {
        $this->setApplyStatus($applyStatus);
        $this->setStatus(self::SERVICE_STATUS['ON_SHELF']);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'rejectReason',
                'statusTime',
                'applyStatus',
                'status',
                'updateTime'
            )
        );
    }

    public function onShelf() : bool
    {
        if (!$this->isOffStock()) {
            Core::setLastError(RESOURCE_STATUS_ON_SHELF);
            return false;
        }

        return $this->updateApplyStatus(self::APPLY_STATUS['PENDING']);
    }

    public function offStock() : bool
    {
        if (!$this->isApprove() || !$this->isOnShelf()) {
            Core::setLastError(RESOURCE_STATUS_OFF_STOCK);
            return false;
        }

        return $this->updateStatus(self::SERVICE_STATUS['OFF_STOCK']);
    }

    public function revoke() : bool
    {
        if (!$this->isPending() || !$this->isOnShelf()) {
            Core::setLastError(RESOURCE_STATUS_NOT_PENDING);
            return false;
        }
        
        return $this->updateStatus(self::SERVICE_STATUS['REVOKED']);
    }

    public function close() : bool
    {
        if (!$this->isApprove() || !$this->isOffStock()) {
            Core::setLastError(RESOURCE_STATUS_NOT_NORMAL);
            return false;
        }
        
        return $this->updateStatus(self::SERVICE_STATUS['CLOSED']);
    }

    public function delete() : bool
    {
        if (!$this->isReject() && !$this->isRevoked() && !$this->isClosed()) {
            Core::setLastError(RESOURCE_STATUS_NOT_REJECT);
            return false;
        }
        
        return $this->updateStatus(self::SERVICE_STATUS['DELETED']);
    }

    public function isOnShelf() : bool
    {
        return $this->getStatus() == self::SERVICE_STATUS['ON_SHELF'];
    }

    public function isRevoked() : bool
    {
        return $this->getStatus() == self::SERVICE_STATUS['REVOKED'];
    }

    public function isClosed() : bool
    {
        return $this->getStatus() == self::SERVICE_STATUS['CLOSED'];
    }

    protected function resubmitAction() : bool
    {
        if (!$this->isOffStock() && !$this->isReject()) {
            Core::setLastError(RESOURCE_STATUS_OFF_STOCK);
            return false;
        }

        if (!$this->validate()) {
            return false;
        }

        $this->setUpdateTime(Core::$container->get('time'));
        $this->setStatusTime(Core::$container->get('time'));
        $this->setApplyStatus(self::APPLY_STATUS['PENDING']);
        $this->setStatus(self::SERVICE_STATUS['ON_SHELF']);

        return $this->getRepository()->edit($this);
    }

    public function updateVolume($volume) : bool
    {
        $this->setVolume($volume);

        return $this->getRepository()->edit(
            $this,
            array('volume')
        );
    }

    /**
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function addPageViews($ip, $memberId, $sessionId) : bool
    {
        $browseRecord = new BrowseRecord();
        $browseRecord->setIp($ip);
        $browseRecord->setSessionId($sessionId);
        $browseRecord->setType(IBrowseAble::TYPE['SERVICE']);
        $browseRecord->setBrowseObjectId($this->getId());
        $browseRecord->setMember(new Member($memberId));
        $browseRecord->setCreateTime(Core::$container->get('time'));

        if ($browseRecord->add()) {
            $pageViews = $this->getPageViews() + 1;
            $this->setPageViews($pageViews);
            return $this->getRepository()->edit(
                $this,
                array('pageViews')
            );
        }

        return false;
    }
}
