<?php
namespace Market\Service\Service\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Common\Model\NullApproveAbleTrait;
use Common\Model\NullOperatAbleTrait;
use Common\Model\NullOnShelfAbleTrait;
use Common\Model\NullResubmitAbleTrait;

class NullService extends Service implements INull
{
    use NullApproveAbleTrait, NullOperatAbleTrait, NullOnShelfAbleTrait, NullResubmitAbleTrait;
    
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function revoke() : bool
    {
        return $this->resourceNotExist();
    }

    public function close() : bool
    {
        return $this->resourceNotExist();
    }

    public function delete() : bool
    {
        return $this->resourceNotExist();
    }
}
