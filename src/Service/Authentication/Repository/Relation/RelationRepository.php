<?php
namespace Market\Service\Authentication\Repository\Relation;

use Common\Repository\OperatAbleRepositoryTrait;
use Marmot\Framework\Classes\Repository;
use Market\Service\Authentication\Adapter\Relation\IRelationAdapter;
use Market\Service\Authentication\Adapter\Relation\RelationDBAdapter;
use Market\Service\Authentication\Model\Relation;

class RelationRepository extends Repository implements IRelationAdapter
{
    use OperatAbleRepositoryTrait;

    private $adapter;

    public function __construct()
    {
        $this->adapter = new RelationDBAdapter();
    }

    protected function getActualAdapter()
    {
        return $this->adapter;
    }

    protected function getMockAdapter()
    {
        $this->adapter;
    }

    public function fetchOne($id) : Relation
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
}
