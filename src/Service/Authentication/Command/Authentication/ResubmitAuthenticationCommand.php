<?php
namespace Market\Service\Authentication\Command\Authentication;

class ResubmitAuthenticationCommand extends AuthenticationCommand
{
    public $qualificationImage;
    public $areaId;

    public function __construct(
        array $qualificationImage,
        int $areaId = 0,
        int $id = 0
    ) {
        parent::__construct($id);
        $this->areaId = $areaId;
        $this->qualificationImage = $qualificationImage;
    }
}
