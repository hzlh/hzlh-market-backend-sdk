<?php
namespace Market\Service\Authentication\Command\Relation;

use Marmot\Interfaces\ICommand;

class RelatedCommand implements ICommand
{
    public $serviceProviderId;

    public $strategyTypeId;

    public $crewId;

    public function __construct(
        $serviceProviderId,
        $strategyTypeId,
        $crewId
    ) {
        $this->serviceProviderId = $serviceProviderId;
        $this->strategyTypeId = $strategyTypeId;
        $this->crewId = $crewId;
    }
}
