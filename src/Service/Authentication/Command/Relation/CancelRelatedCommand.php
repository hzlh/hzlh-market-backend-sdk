<?php
namespace Market\Service\Authentication\Command\Relation;

use Marmot\Interfaces\ICommand;

class CancelRelatedCommand implements ICommand
{
    public $serviceProviderId;

    public $crewId;

    public function __construct(
        $serviceProviderId,
        $crewId
    ) {
        $this->serviceProviderId = $serviceProviderId;
        $this->crewId = $crewId;
    }
}
