<?php
namespace Market\Service\Authentication\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IResubmitAbleController;

use Market\Service\Authentication\Model\Authentication;
use Market\Service\Authentication\View\AuthenticationView;
use Market\Service\Authentication\Repository\Authentication\AuthenticationRepository;
use Market\Service\Authentication\Command\Authentication\ResubmitAuthenticationCommand;
use Market\Service\Authentication\CommandHandler\Authentication\AuthenticationCommandHandlerFactory;

class ResubmitController extends Controller implements IResubmitAbleController
{
    use JsonApiTrait, AuthenticationValidateTrait;

    private $repository;

    private $commandBus;
    
    public function __construct()
    {
        parent::__construct();
        $this->repository = new AuthenticationRepository();
        $this->commandBus = new CommandBus(new AuthenticationCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : AuthenticationRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    /**
     * 对应路由 /authentications/{id:\d+}/resubmit
     * 重新提交, 通过PATCH传参
     * @param int id
     * @param jsonApi
     * @return jsonApi
     */
    public function resubmit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        
        $qualificationImage = !empty($attributes['qualificationImage']['identify']) ?
        $attributes['qualificationImage'] :
        array();
        $areaId = $attributes['areaId'] ?? 0;

        if ($this->validateResubmitScenario(
            $qualificationImage
        )) {
            $command = new ResubmitAuthenticationCommand(
                $qualificationImage,
                $areaId,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $authentication = $this->getRepository()->fetchOne($command->id);
                if ($authentication instanceof Authentication) {
                    $this->render(new AuthenticationView($authentication));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
