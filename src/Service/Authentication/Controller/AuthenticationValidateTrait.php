<?php
namespace Market\Service\Authentication\Controller;

use WidgetRules\Common\WidgetRules as CommonWidgetRules;
use WidgetRules\Authentication\WidgetRules as AuthenticationWidgetRules;

trait AuthenticationValidateTrait
{
    protected function getCommonWidgetRules() : CommonWidgetRules
    {
        return CommonWidgetRules::getInstance();
    }

    protected function getAuthenticationWidgetRules() : AuthenticationWidgetRules
    {
        return AuthenticationWidgetRules::getInstance();
    }

    protected function validateAddScenario(
        $qualifications,
        $enterpriseId
    ) {
        return $this->getAuthenticationWidgetRules()->qualifications($qualifications)
        && $this->getCommonWidgetRules()->formatNumeric($enterpriseId, 'enterpriseId');
    }

    protected function validateResubmitScenario(
        $qualificationImage
    ) : bool {
        return (empty($qualificationImage) ? true :
            $this->getCommonWidgetRules()->image($qualificationImage, 'qualificationImage'));
    }

    protected function validateRelationScenario(
        $serviceProviderId,
        $strategyTypeId,
        $crewId
    ) : bool {
        return $this->getCommonWidgetRules()->formatNumeric($serviceProviderId, 'serviceProvider')
            && $this->getCommonWidgetRules()->formatNumeric($strategyTypeId, 'strategyType')
            && $this->getCommonWidgetRules()->formatNumeric($crewId, 'crew');
    }
}
