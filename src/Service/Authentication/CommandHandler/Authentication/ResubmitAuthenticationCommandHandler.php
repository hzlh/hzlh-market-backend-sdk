<?php
namespace Market\Service\Authentication\CommandHandler\Authentication;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\Authentication\Model\IdentityInfo;
use Market\Service\Authentication\Command\Authentication\ResubmitAuthenticationCommand;

class ResubmitAuthenticationCommandHandler implements ICommandHandler
{
    use AuthenticationCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitAuthenticationCommand)) {
            throw new \InvalidArgumentException;
        }

        $authentication = $this->fetchAuthentication($command->id);
        $authentication->setQualificationImage($command->qualificationImage);

        return $authentication->resubmit();
    }
}
