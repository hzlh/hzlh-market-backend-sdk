<?php
namespace Market\Service\Authentication\CommandHandler\Authentication;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class AuthenticationCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Service\Authentication\Command\Authentication\AddAuthenticationCommand'=>
        'Market\Service\Authentication\CommandHandler\Authentication\AddAuthenticationCommandHandler',
        'Market\Service\Authentication\Command\Authentication\ResubmitAuthenticationCommand'=>
        'Market\Service\Authentication\CommandHandler\Authentication\ResubmitAuthenticationCommandHandler',
        'Market\Service\Authentication\Command\Authentication\ApproveAuthenticationCommand'=>
        'Market\Service\Authentication\CommandHandler\Authentication\ApproveAuthenticationCommandHandler',
        'Market\Service\Authentication\Command\Authentication\RejectAuthenticationCommand'=>
        'Market\Service\Authentication\CommandHandler\Authentication\RejectAuthenticationCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
