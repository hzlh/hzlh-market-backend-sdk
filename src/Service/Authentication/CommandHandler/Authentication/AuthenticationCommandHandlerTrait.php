<?php
namespace Market\Service\Authentication\CommandHandler\Authentication;

use Market\Service\Authentication\Model\Authentication;
use Market\Service\Authentication\Repository\Authentication\AuthenticationRepository;

trait AuthenticationCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new AuthenticationRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : AuthenticationRepository
    {
        return $this->repository;
    }
    
    protected function fetchAuthentication(int $id) : Authentication
    {
        return $this->getRepository()->fetchOne($id);
    }
}
