<?php
namespace Market\Service\Authentication\CommandHandler\Authentication;

use Common\Model\IApproveAble;
use Common\CommandHandler\RejectCommandHandler;

use Market\Service\Authentication\Repository\Authentication\AuthenticationRepository;

class RejectAuthenticationCommandHandler extends RejectCommandHandler
{
    use AuthenticationCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchAuthentication($id);
    }
}
