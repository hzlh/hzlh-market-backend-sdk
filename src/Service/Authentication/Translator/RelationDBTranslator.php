<?php
namespace Market\Service\Authentication\Translator;

use Marmot\Interfaces\ITranslator;

use Market\Service\Authentication\Model\Authentication;
use Market\Service\Authentication\Model\NullRelation;
use Market\Service\Authentication\Model\Relation;
use Backend\Crew\Model\Crew;
use Strategy\Model\Type;

/**
 * @SuppressWarnings(PHPMD)
 */
class RelationDBTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $relation = null) : Relation
    {
        if (!isset($expression['relation_id'])) {
            return NullRelation::getInstance();
        }

        if ($relation == null) {
            $relation = new Relation($expression['relation_id']);
        }

        /**@var \Service\Authentication\Model\Relation $relation*/
        $relation->setStatus($expression['status']);
        $relation->setCreateTime($expression['create_time']);
        $relation->setUpdateTime($expression['update_time']);
        $relation->setStatusTime($expression['status_time']);
        $relation->setServiceProvider(new Authentication($expression['service_provider_id']));
        $relation->setId($expression['service_provider_id']);
        $relation->setStrategyType(new Type($expression['strategy_type_id']));
        $relation->setCrew(new Crew($expression['crew_id']));

        return $relation;
    }

    public function objectToArray($relation, array $keys = array()) : array
    {
        if (!$relation instanceof Relation) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'status',
                'createTime',
                'updateTime',
                'statusTime',
                'serviceProvider',
                'strategyType',
                'crew',
            );
        }
        $expression = array();

        if (in_array('id', $keys)) {
            $expression['relation_id'] = $relation->getId();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $relation->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $relation->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $relation->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $relation->getStatusTime();
        }
        if (in_array('serviceProvider', $keys)) {
            $expression['service_provider_id'] = $relation->getServiceProvider()->getId();
        }
        if (in_array('strategyType', $keys)) {
            $expression['strategy_type_id'] = $relation->getStrategyType()->getId();
        }
        if (in_array('crew', $keys)) {
            $expression['crew_id'] = $relation->getCrew()->getId();
        }
        return $expression;
    }
}
