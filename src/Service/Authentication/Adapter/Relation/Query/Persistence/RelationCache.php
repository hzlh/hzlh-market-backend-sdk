<?php
namespace Market\Service\Authentication\Adapter\Relation\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class RelationCache extends Cache
{
    public function __construct()
    {
        parent::__construct('service_provider_relation');
    }
}
