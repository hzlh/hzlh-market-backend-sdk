<?php
namespace Market\Service\Authentication\Adapter\Relation\Query;

use Marmot\Framework\Query\RowCacheQuery;

class RelationRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'relation_id',
            new Persistence\RelationCache(),
            new Persistence\RelationDb()
        );
    }

    /**
     * @param array $data 添加数据
     * 此处因为在设计的时候没有主键id,所以返回的是受影响行数,所以复写此方法
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function add(array $data, $lasetInsertId = false)
    {
        $result = $this->getDbLayer()->insert($data, $lasetInsertId);
        if (!$result) {
            return false;
        }

        return $result;
    }
}
