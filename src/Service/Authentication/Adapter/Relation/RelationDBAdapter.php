<?php
namespace Market\Service\Authentication\Adapter\Relation;

use Common\Model\IOperatAble;
use Marmot\Core;

use Common\Adapter\FetchRestfulAdapterTrait;
use Common\Adapter\OperatAbleRestfulAdapterTrait;
use Market\Service\Authentication\Adapter\Relation\Query\RelationRowCacheQuery;
use Market\Service\Authentication\Model\NullRelation;
use Market\Service\Authentication\Model\Relation;
use Market\Service\Authentication\Translator\RelationDBTranslator;
use Strategy\Repository\TypeRepository;

class RelationDBAdapter implements IRelationAdapter
{
    use FetchRestfulAdapterTrait, OperatAbleRestfulAdapterTrait;

    private $translator;
    
    private $rowCacheQuery;

    private $strategyTypeRepository;

    public function __construct()
    {
        $this->translator = new RelationDBTranslator();
        $this->rowCacheQuery = new RelationRowCacheQuery();
        $this->strategyTypeRepository = new TypeRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowCacheQuery);
        unset($this->strategyTypeRepository);
    }
    
    protected function getDBTranslator() : RelationDBTranslator
    {
        return $this->translator;
    }

    protected function getRowCacheQuery() : RelationRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getStrategyTypeRepository() : TypeRepository
    {
        return $this->strategyTypeRepository;
    }

    /**
     * function：查询单条
     * parameter：$id
     * start：
     *  fetchOne 查询数据库;
     *  if(空) return false；
     *  数组转对象
     *  set 企业信息
     *  set 策略类型信息
     *  return 企业的关联模型；
     * end
     */
    public function fetchOne($id) : Relation
    {
        $info = array();

        $info = $this->getRowCacheQuery()->getOne($id);
        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullRelation::getInstance();
        }

        /**@var \Service\Authentication\Model\Relation $relation*/
        $relation = $this->getDBTranslator()->arrayToObject($info);

        $strategyType = $this->getStrategyTypeRepository()->fetchOne($relation->getStrategyType()->getId());

        if (!empty($strategyType)) {
            $relation->setStrategyType($strategyType);
        }

        return $relation;
    }

    protected function addAction(IOperatAble $operatAbleObject, array $keys = array()) : bool
    {
        $info = array();


        $info = $this->getDBTranslator()->objectToArray($operatAbleObject, $keys);

        $id = $this->getRowCacheQuery()->add($info);

        if (!$id) {
            return false;
        }

        $operatAbleObject->setId($operatAbleObject->getId());
        return true;
    }

    public function fetchList(array $ids) : array
    {
        $list = array();

        $infoList = $this->getRowCacheQuery()->getList($ids);

        if (empty($infoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $translator = $this->getDBTranslator();
        foreach ($infoList as $info) {
            $data = $translator->arrayToObject($info);
            $list[] = $data;
        }

        return $list;
    }

    protected function formatFilter(array $filter) : string
    {
        unset($filter);
        return '';
    }

    protected function formatSort(array $sort) : string
    {
        unset($sort);
        return '';
    }
}
