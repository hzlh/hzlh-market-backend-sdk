<?php
namespace Market\Service\Authentication\Adapter\Authentication;

use Market\Service\Authentication\Model\Authentication;
use Market\Service\Authentication\Utils\MockFactory;

use Common\Model\IOperatAble;

class AuthenticationMockAdapter implements IAuthenticationAdapter
{
    public function add(IOperatAble $authentication, array $keys = array()) : bool
    {
        unset($authentication);

        return true;
    }

    public function edit(IOperatAble $authentication, array $keys = array()) : bool
    {
        unset($authentication);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : Authentication
    {
        return MockFactory::generateAuthentication($id);
    }

    public function fetchList(array $ids) : array
    {
        $authenticationList = array();

        foreach ($ids as $id) {
            $authenticationList[] = MockFactory::generateAuthentication($id);
        }

        return $authenticationList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }
}
