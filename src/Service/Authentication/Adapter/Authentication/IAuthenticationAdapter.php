<?php
namespace Market\Service\Authentication\Adapter\Authentication;

use Market\Service\Authentication\Model\Authentication;

use Common\Adapter\IOperatAbleAdapter;

interface IAuthenticationAdapter extends IOperatAbleAdapter
{
    public function fetchOne($id) : Authentication;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;
}
