<?php
namespace Market\Service\Authentication\Adapter\Authentication;

use Marmot\Core;

use Common\Adapter\FetchRestfulAdapterTrait;
use Common\Adapter\OperatAbleRestfulAdapterTrait;
use Common\Adapter\Document\RejectReasonDocumentAdapter;

use Market\Service\Authentication\Model\Authentication;
use Market\Service\Authentication\Model\NullAuthentication;
use Market\Service\Authentication\Translator\AuthenticationDBTranslator;
use Market\Service\Authentication\Adapter\Authentication\Query\AuthenticationRowCacheQuery;

use Member\Enterprise\Model\Enterprise;
use Member\Enterprise\Repository\Enterprise\EnterpriseRepository;

use Market\Service\ServiceCategory\Model\ServiceCategory;
use Market\Service\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;
use Strategy\Repository\TypeRepository;

use Market\Service\Authentication\Repository\Relation\RelationRepository;

/**
 * @SuppressWarnings(PHPMD)
 */
class AuthenticationDBAdapter implements IAuthenticationAdapter
{
    use OperatAbleRestfulAdapterTrait, FetchRestfulAdapterTrait;

    private $translator;

    private $rowCacheQuery;

    private $enterpriseRepository;

    private $serviceCategoryRepository;

    private $rejectReasonDocumentAdapter;

    private $strategyTypeRepository;

    public function __construct()
    {
        $this->translator = new AuthenticationDBTranslator();
        $this->rowCacheQuery = new AuthenticationRowCacheQuery();
        $this->enterpriseRepository = new EnterpriseRepository();
        $this->serviceCategoryRepository = new ServiceCategoryRepository();
        $this->rejectReasonDocumentAdapter = new RejectReasonDocumentAdapter();
        $this->strategyTypeRepository = new TypeRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowCacheQuery);
        unset($this->enterpriseRepository);
        unset($this->serviceCategoryRepository);
        unset($this->rejectReasonDocumentAdapter);
        unset($this->strategyTypeRepository);
    }
    
    protected function getDBTranslator() : AuthenticationDBTranslator
    {
        return $this->translator;
    }
    
    protected function getRowCacheQuery() : AuthenticationRowCacheQuery
    {
        return $this->rowCacheQuery;
    }
    
    protected function getRejectReasonDocumentAdapter() : RejectReasonDocumentAdapter
    {
        return $this->rejectReasonDocumentAdapter;
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }

    protected function getStrategyTypeRepository() : TypeRepository
    {
        return $this->strategyTypeRepository;
    }

    protected function fetchEnterprise(int $id) : Enterprise
    {
        return $this->getEnterpriseRepository()->fetchOne($id);
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return $this->serviceCategoryRepository;
    }

    protected function fetchServiceCategory(int $id) : ServiceCategory
    {
        return $this->getServiceCategoryRepository()->fetchOne($id);
    }

    public function fetchOne($id) : Authentication
    {
        $info = array();

        $info = $this->getRowCacheQuery()->getOne($id);

        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullAuthentication::getInstance();
        }

        $authentication = $this->getDBTranslator()->arrayToObject($info);

        if ($authentication instanceof Authentication) {
            if (!empty($authentication->getRejectReason()->getId())) {
                $this->getRejectReasonDocumentAdapter()->fetchOne($authentication->getRejectReason());
            }
        }
        
        $enterprise = $this->fetchEnterprise($authentication->getEnterprise()->getId());
        if (!empty($enterprise)) {
            $authentication->setEnterprise($enterprise);
        }
        
        $serviceCategory = $this->fetchServiceCategory($authentication->getServiceCategory()->getId());
        if (!empty($serviceCategory)) {
            $authentication->setServiceCategory($serviceCategory);
        }

        $relationRepository = new RelationRepository();
        $relation  = $relationRepository->fetchOne($id);

        $strategyType = $this->getStrategyTypeRepository()->fetchOne($relation->getStrategyType()->getId());

        if (!empty($strategyType)) {
            $authentication->setStrategyType($strategyType);
        }

        return $authentication;
    }

    public function fetchList(array $ids) : array
    {
        $authenticationList = array();
        
        $authenticationInfoList = $this->getRowCacheQuery()->getList($ids);

        if (empty($authenticationInfoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $translator = $this->getDBTranslator();
        foreach ($authenticationInfoList as $authenticationInfo) {
            $authentication = $translator->arrayToObject($authenticationInfo);

            $enterpriseIds[$authentication->getId()] = $authentication->getEnterprise()->getId();
            $serviceCategoryIds[$authentication->getId()] = $authentication->getServiceCategory()->getId();

            if (!empty($authentication->getRejectReason()->getId())) {
                $rejectReasonDocuments[] = $authentication->getRejectReason();
            }

            $authenticationList[] = $authentication;
        }

        if (!empty($rejectReasonDocuments)) {
            $this->getRejectReasonDocumentAdapter()->fetchList($rejectReasonDocuments);
        }
        
        $enterpriseList = $this->getEnterpriseRepository()->fetchList($enterpriseIds);
        if (!empty($enterpriseList)) {
            foreach ($authenticationList as $key => $authentication) {
                if (isset($enterpriseList[$key])) {
                    $authentication->setEnterprise($enterpriseList[$key]);
                }
            }
        }
        
        $serviceCategoryList = $this->getServiceCategoryRepository()->fetchList($serviceCategoryIds);
        if (!empty($serviceCategoryList)) {
            foreach ($authenticationList as $key => $authentication) {
                if (isset($serviceCategoryList[$key])) {
                    $authentication->setServiceCategory($serviceCategoryList[$key]);
                }
            }
        }
        $relationRepository = new RelationRepository();

        $relationList  = $relationRepository->fetchList($ids);

        $relationLists = array();
        foreach ($relationList as $relation) {
            $relationLists[$relation->getId()] = $relation;
        }

        if (!empty($relationList)) {
            foreach ($authenticationList as $authentication) {
                if (isset($relationLists[$authentication->getId()])) {
                    $authentication->setStrategyType($relationLists[$authentication->getId()]->getStrategyType());
                }
            }
        }

        return $authenticationList;
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $authentication = new Authentication();

            if (isset($filter['applyStatus'])) {
                $applyStatus = $filter['applyStatus'];
                if (is_numeric($applyStatus)) {
                    $authentication->setApplyStatus($filter['applyStatus']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $authentication,
                        array('applyStatus')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($applyStatus, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $authentication,
                        array('applyStatus')
                    );
                    $condition .= $conjection.key($info).' IN ('.$applyStatus.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['enterprise']) && !empty($filter['enterprise'])) {
                $authentication->getEnterprise()->setId($filter['enterprise']);
                $info = $this->getDBTranslator()->objectToArray(
                    $authentication,
                    array('enterprise')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['serviceCategory']) && !empty($filter['serviceCategory'])) {
                $authentication->getServiceCategory()->setId($filter['serviceCategory']);
                $info = $this->getDBTranslator()->objectToArray(
                    $authentication,
                    array('serviceCategory')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['enterpriseName']) && !empty($filter['enterpriseName'])) {
                $authentication->setEnterpriseName($filter['enterpriseName']);
                $info = $this->getDBTranslator()->objectToArray($authentication, array('enterpriseName'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['number'])) {
                $id = substr($filter['number'], 11);
                $id = is_numeric($id) ? $id : 0;
               
                $authentication->setId($id);
                $info = $this->getDBTranslator()->objectToArray(
                    $authentication,
                    array('id')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            //此处是为了解决首页获取同类企业数据时避免筛选到自己
            if (isset($filter['enterpriseId'])) {
                $authentication->getEnterprise()->setId($filter['enterpriseId']);
                $info = $this->getDBTranslator()->objectToArray($authentication, array('enterprise'));
                $condition .= $conjection.key($info).' <> '.current($info);
                $conjection = ' AND ';
            }
            // 专用于政策发消息时筛选服务商企业id
//            if (isset($filter['distinctEnterprise']) && isset($filter['applyStatus'])) {
//                $serviceAuthenticationDb = new AuthenticationDb();
//
//                $authentication->setApplyStatus($filter['applyStatus']);
//
//                $info = $this->getDBTranslator()->objectToArray($authentication, array('applyStatus'));
//
//                $primaryKeyInfo = $this->getDBTranslator()->objectToArray($authentication, array('id'));
//                $enterpriseInfo = $this->getDBTranslator()->objectToArray($authentication, array('enterprise'));
//                $condition .= $conjection . key($primaryKeyInfo)
//                    . ' IN (SELECT MIN('.key($primaryKeyInfo).') FROM '
//                    . $serviceAuthenticationDb->tname()
//                    . ' WHERE ' . key($info) . ' = ' . current($info)
//                    . ' GROUP BY ' . key($enterpriseInfo) .')';
//                $conjection = ' AND ';
//            }
        }
        
        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $authentication = new Authentication();
            foreach ($sort as $key => $val) {
                if ($key == 'updateTime') {
                    $info = $this->getDBTranslator()->objectToArray($authentication, array('updateTime'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'id') {
                    $info = $this->getDBTranslator()->objectToArray($authentication, array('id'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
            }
        }

        return $condition;
    }
}
