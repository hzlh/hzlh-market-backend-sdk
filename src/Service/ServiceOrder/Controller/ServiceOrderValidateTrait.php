<?php
namespace Market\Service\ServiceOrder\Controller;

use WidgetRules\Common\WidgetRules;
use WidgetRules\Order\WidgetRules as ServiceOrderWidgetRules;
use WidgetRules\Refund\WidgetRules as RefundWidgetRules;

trait ServiceOrderValidateTrait
{
    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function getServiceOrderWidgetRules() : ServiceOrderWidgetRules
    {
        return ServiceOrderWidgetRules::getInstance();
    }

    protected function getRefundWidgetRules() : RefundWidgetRules
    {
        return RefundWidgetRules::getInstance();
    }

    protected function validatePlaceServiceOrderScenario(
        $buyerMemberAccountId,
        $orderAddress,
        $orderCommodityList,
        $memberCouponIds
    ) {
        return $this->getWidgetRules()->formatNumeric($buyerMemberAccountId, 'buyerMemberAccountId')
        && $this->getServiceOrderWidgetRules()->orderAddress($orderAddress)
        && $this->getServiceOrderWidgetRules()->orderCommodityList($orderCommodityList)
        && $this->getServiceOrderWidgetRules()->orderMemberCoupons($memberCouponIds);
    }

    protected function validateUpdateOrderAmountScenario(
        $amount,
        $remark
    ) : bool {
        return $this->getServiceOrderWidgetRules()->amount($amount)
            && $this->getServiceOrderWidgetRules()->remark($remark);
    }

    protected function validateUpdateOrderAddressScenario(
        $orderAddress
    ) : bool {
        return $this->getServiceOrderWidgetRules()->orderAddress($orderAddress);
    }

    protected function validateBuyerCancelScenario(
        $cancelReason
    ) : bool {
        return $this->getServiceOrderWidgetRules()->buyerCancelReason($cancelReason);
    }

    protected function validateSellerCancelScenario(
        $cancelReason
    ) : bool {
        return $this->getServiceOrderWidgetRules()->sellerCancelReason($cancelReason);
    }

    protected function validateRejectReasonScenario(
        $rejectReason
    ) : bool {
        return $this->getRefundWidgetRules()->reason($rejectReason, 'rejectReason');
    }
}
