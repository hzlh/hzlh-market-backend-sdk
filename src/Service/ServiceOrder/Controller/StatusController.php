<?php
namespace Market\Service\ServiceOrder\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Market\Service\ServiceOrder\Model\ServiceOrder;
use Market\Service\ServiceOrder\View\ServiceOrderView;
use Market\Service\ServiceOrder\Repository\ServiceOrder\ServiceOrderRepository;
use Market\Service\ServiceOrder\Command\ServiceOrder\BuyerCancelServiceOrderCommand;
use Market\Service\ServiceOrder\Command\ServiceOrder\BuyerDeleteServiceOrderCommand;
use Market\Service\ServiceOrder\Command\ServiceOrder\SellerDeleteServiceOrderCommand;
use Market\Service\ServiceOrder\Command\ServiceOrder\SellerCancelServiceOrderCommand;
use Market\Service\ServiceOrder\Command\ServiceOrder\PerformanceEndServiceOrderCommand;
use Market\Service\ServiceOrder\Command\ServiceOrder\PerformanceBeginServiceOrderCommand;
use Market\Service\ServiceOrder\Command\ServiceOrder\PerformanceRejectBeginServiceOrderCommand;
use Market\Service\ServiceOrder\Command\ServiceOrder\BuyerConfirmationServiceOrderCommand;
use Market\Service\ServiceOrder\Command\ServiceOrder\BuyerPermanentDeleteServiceOrderCommand;
use Market\Service\ServiceOrder\Command\ServiceOrder\SellerPermanentDeleteServiceOrderCommand;
use Market\Service\ServiceOrder\CommandHandler\ServiceOrder\ServiceOrderCommandHandlerFactory;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class StatusController extends Controller
{
    use JsonApiTrait, ServiceOrderValidateTrait;

    private $repository;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceOrderRepository();
        $this->commandBus = new CommandBus(new ServiceOrderCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : ServiceOrderRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    /**
     * 对应路由 /serviceOrders/{id:\d+}/performanceBegin
     * 开始服务, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    public function performanceBegin(int $id)
    {
        if (!empty($id)) {
            $commandBus = $this->getCommandBus();

            $command = new PerformanceBeginServiceOrderCommand($id);

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();

                $serviceOrder = $repository->fetchOne($command->id);
                if ($serviceOrder instanceof ServiceOrder) {
                    $this->render(new ServiceOrderView($serviceOrder));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
    /**
     * 对应路由 /serviceOrders/{id:\d+}/performanceRejectBegin
     * 商家拒绝接单, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    public function performanceRejectBegin(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];

        $rejectReason = $attributes['rejectReason'];
        
        if ($this->validateRejectReasonScenario(
            $rejectReason
        )) {
            $commandBus = $this->getCommandBus();

            $command = new PerformanceRejectBeginServiceOrderCommand(
                $rejectReason,
                $id
            );

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $serviceOrder = $repository->fetchOne($command->id);
                if ($serviceOrder instanceof ServiceOrder) {
                    $this->render(new ServiceOrderView($serviceOrder));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
    /**
     * 对应路由 /serviceOrders/{id:\d+}/performanceEnd
     * 结束服务, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    public function performanceEnd(int $id)
    {
        if (!empty($id)) {
            $commandBus = $this->getCommandBus();

            $command = new PerformanceEndServiceOrderCommand($id);

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();

                $serviceOrder = $repository->fetchOne($command->id);
                if ($serviceOrder instanceof ServiceOrder) {
                    $this->render(new ServiceOrderView($serviceOrder));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /serviceOrders/{id:\d+}/buyerConfirmation
     * 买家确认, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    public function buyerConfirmation(int $id)
    {
        if (!empty($id)) {
            $command = new BuyerConfirmationServiceOrderCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $serviceOrder = $this->getRepository()->fetchOne($id);
                if ($serviceOrder instanceof ServiceOrder) {
                    $this->render(new ServiceOrderView($serviceOrder));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /serviceOrders/{id:\d+}/buyerDelete
     * 买家删除订单, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    public function buyerDelete(int $id)
    {
        if (!empty($id)) {
            $command = new BuyerDeleteServiceOrderCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $serviceOrder = $this->getRepository()->fetchOne($id);
                if ($serviceOrder instanceof ServiceOrder) {
                    $this->render(new ServiceOrderView($serviceOrder));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /serviceOrders/{id:\d+}/sellerDelete
     * 卖家删除订单, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    public function sellerDelete(int $id)
    {
        if (!empty($id)) {
            $command = new SellerDeleteServiceOrderCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $serviceOrder = $this->getRepository()->fetchOne($id);
                if ($serviceOrder instanceof ServiceOrder) {
                    $this->render(new ServiceOrderView($serviceOrder));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /serviceOrders/{id:\d+}/buyerPermanentDelete
     * 买家永久删除订单, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    public function buyerPermanentDelete(int $id)
    {
        if (!empty($id)) {
            $command = new BuyerPermanentDeleteServiceOrderCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $serviceOrder = $this->getRepository()->fetchOne($id);
                if ($serviceOrder instanceof ServiceOrder) {
                    $this->render(new ServiceOrderView($serviceOrder));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /serviceOrders/{id:\d+}/sellerPermanentDelete
     * 卖家永久删除订单, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    public function sellerPermanentDelete(int $id)
    {
        if (!empty($id)) {
            $command = new SellerPermanentDeleteServiceOrderCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $serviceOrder = $this->getRepository()->fetchOne($id);
                if ($serviceOrder instanceof ServiceOrder) {
                    $this->render(new ServiceOrderView($serviceOrder));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * /serviceOrders/{id:\d+}/buyerCancel
     * 买家取消订单, 通过PATCH传参
     * @param jsonApi
     * @return jsonApi
     */
    public function buyerCancel(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];

        $cancelReason = $attributes['cancelReason'];
        
        if ($this->validateBuyerCancelScenario(
            $cancelReason
        )) {
            $commandBus = $this->getCommandBus();

            $command = new BuyerCancelServiceOrderCommand(
                $cancelReason,
                $id
            );

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $serviceOrder = $repository->fetchOne($command->id);
                if ($serviceOrder instanceof ServiceOrder) {
                    $this->render(new ServiceOrderView($serviceOrder));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
    
    /**
     * /serviceOrders/{id:\d+}/sellerCancel
     * 卖家取消订单, 通过PATCH传参
     * @param jsonApi
     * @return jsonApi
     */
    public function sellerCancel(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];

        $cancelReason = $attributes['cancelReason'];
        
        if ($this->validateSellerCancelScenario(
            $cancelReason
        )) {
            $commandBus = $this->getCommandBus();

            $command = new SellerCancelServiceOrderCommand(
                $cancelReason,
                $id
            );

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $serviceOrder = $repository->fetchOne($command->id);
                if ($serviceOrder instanceof ServiceOrder) {
                    $this->render(new ServiceOrderView($serviceOrder));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
