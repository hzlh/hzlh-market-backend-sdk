<?php
namespace Market\Service\ServiceOrder\Controller;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IFetchAbleController;

use Market\Service\ServiceOrder\View\ServiceOrderView;
use Market\Service\ServiceOrder\Repository\ServiceOrder\ServiceOrderRepository;

class FetchController extends Controller implements IFetchAbleController
{
    use JsonApiTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceOrderRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ServiceOrderRepository
    {
        return $this->repository;
    }

    public function fetchOne(int $id)
    {
        $repository = $this->getRepository();
        $serviceOrder = $repository->fetchOne($id);

        if (!$serviceOrder instanceof INull) {
            $this->renderView(new ServiceOrderView($serviceOrder));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function fetchList(string $ids)
    {
        $ids = explode(',', $ids);

        $serviceOrderList = array();

        $repository = $this->getRepository();
        $serviceOrderList = $repository->fetchList($ids);

        if (!empty($serviceOrderList)) {
            $this->renderView(new ServiceOrderView($serviceOrderList));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function filter()
    {
        $repository = $this->getRepository();

        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();

        list($serviceOrderList, $count) = $repository->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = new ServiceOrderView($serviceOrderList);
            $view->pagination(
                'marketServiceOrders',
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
