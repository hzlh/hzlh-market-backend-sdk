<?php
namespace Market\Service\ServiceOrder\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IOperatAbleController;

use Market\Service\ServiceOrder\Model\ServiceOrder;
use Market\Service\ServiceOrder\View\ServiceOrderView;
use Market\Service\ServiceOrder\Repository\ServiceOrder\ServiceOrderRepository;
use Market\Service\ServiceOrder\Command\ServiceOrder\PlaceServiceOrderCommand;
use Market\Service\ServiceOrder\Command\ServiceOrder\UpdateOrderAmountCommand;
use Market\Service\ServiceOrder\Command\ServiceOrder\UpdateOrderAddressCommand;
use Market\Service\ServiceOrder\CommandHandler\ServiceOrder\ServiceOrderCommandHandlerFactory;

class OperationController extends Controller implements IOperatAbleController
{
    use JsonApiTrait, ServiceOrderValidateTrait;

    private $repository;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceOrderRepository();
        $this->commandBus = new CommandBus(new ServiceOrderCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : ServiceOrderRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    
    /**
     * 对应路由 /serviceOrders
     * 下单, 通过POST传参
     * @param jsonApi
     * @return jsonApi
     */
    public function add()
    {
        $data = $this->getRequest()->post('data');

        $remark = $data['attributes']['remark'];
        $relationships = $data['relationships'];

        $buyerMemberAccountId = $relationships['buyerMemberAccount']['data'][0]['id'];

        $memberCoupons = isset($relationships['memberCoupon']['data']) ?
                         $relationships['memberCoupon']['data'] :
                         array();

        $memberCouponIds = array();

        foreach ($memberCoupons as $memberCoupon) {
            $memberCouponIds[] = $memberCoupon['id'];
        }

        $orderAddressTemp = $relationships['orderAddress']['data'][0];

        $orderAddress['cellphone'] = $orderAddressTemp['attributes']['cellphone'];
        $orderAddress['snapshotId'] = $orderAddressTemp['relationships']['snapshot']['data'][0]['id'];

        $orderCommodityList = array();
        $orderCommodities = $relationships['orderCommodities']['data'];
        foreach ($orderCommodities as $orderCommodity) {
            $orderCommodityList[] = array(
                'number' => $orderCommodity['attributes']['number'],
                'skuIndex' => $orderCommodity['attributes']['skuIndex'],
                'snapshotId' => $orderCommodity['relationships']['snapshot']['data'][0]['id']
            );
        }

        if ($this->validatePlaceServiceOrderScenario(
            $buyerMemberAccountId,
            $orderAddress,
            $orderCommodityList,
            $memberCouponIds
        )) {
            $commandBus = $this->getCommandBus();

            $command = new PlaceServiceOrderCommand(
                $orderCommodityList,
                $orderAddress,
                $memberCouponIds,
                $remark,
                $buyerMemberAccountId
            );

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $serviceOrder = $repository->fetchOne($command->id);

                if ($serviceOrder instanceof ServiceOrder) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new ServiceOrderView($serviceOrder));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    public function edit(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    /**
     * /serviceOrders/{id:\d+}/updateOrderAmount
     * 修改订单金额功能, 通过PATCH传参
     * @param jsonApi
     * @return jsonApi
     */
    public function updateOrderAmount(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];

        $amount = $attributes['amount'];
        $remark = $attributes['remark'];
        
        if ($this->validateUpdateOrderAmountScenario(
            $amount,
            $remark
        )) {
            $commandBus = $this->getCommandBus();

            $command = new UpdateOrderAmountCommand(
                $amount,
                $remark,
                $id
            );
 
            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $serviceOrder = $repository->fetchOne($command->id);
                if ($serviceOrder instanceof ServiceOrder) {
                    $this->render(new ServiceOrderView($serviceOrder));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * /serviceOrders/{id:\d+}/updateOrderAddress
     * 修改收货地址, 通过PATCH传参
     * @param jsonApi
     * @return jsonApi
     */
    public function updateOrderAddress(int $id)
    {
        $data = $this->getRequest()->patch('data');

        $orderAddressTemp = $data['relationships']['orderAddress']['data'][0];

        $orderAddress['cellphone'] = $orderAddressTemp['attributes']['cellphone'];
        $orderAddress['snapshotId'] = $orderAddressTemp['relationships']['snapshot']['data'][0]['id'];

        if ($this->validateUpdateOrderAddressScenario(
            $orderAddress
        )) {
            $commandBus = $this->getCommandBus();

            $command = new UpdateOrderAddressCommand(
                $orderAddress,
                $id
            );

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $serviceOrder = $repository->fetchOne($command->id);
                if ($serviceOrder instanceof ServiceOrder) {
                    $this->render(new ServiceOrderView($serviceOrder));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
