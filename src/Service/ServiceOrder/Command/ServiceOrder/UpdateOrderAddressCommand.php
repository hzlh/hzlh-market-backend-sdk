<?php
namespace Market\Service\ServiceOrder\Command\ServiceOrder;

use Marmot\Interfaces\ICommand;

class UpdateOrderAddressCommand implements ICommand
{
    public $orderAddress;

    public $id;

    public function __construct(
        array $orderAddress = array(),
        int $id = 0
    ) {
        $this->orderAddress = $orderAddress;
        $this->id = $id;
    }
}
