<?php
namespace Market\Service\ServiceOrder\Command\ServiceOrder;

use Marmot\Interfaces\ICommand;

class BuyerCancelServiceOrderCommand implements ICommand
{
    public $cancelReason;

    public $id;

    public function __construct(
        int $cancelReason = 0,
        int $id = 0
    ) {
        $this->cancelReason = $cancelReason;
        $this->id = $id;
    }
}
