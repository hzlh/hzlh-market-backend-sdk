<?php
namespace Market\Service\ServiceOrder\Command\ServiceOrder;

use Marmot\Interfaces\ICommand;

class PerformanceRejectBeginServiceOrderCommand implements ICommand
{
    public $rejectReason;

    public $id;

    public function __construct(
        string $rejectReason,
        int $id = 0
    ) {
        $this->rejectReason = $rejectReason;
        $this->id = $id;
    }
}
