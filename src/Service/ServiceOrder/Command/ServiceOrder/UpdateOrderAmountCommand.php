<?php
namespace Market\Service\ServiceOrder\Command\ServiceOrder;

use Marmot\Interfaces\ICommand;

class UpdateOrderAmountCommand implements ICommand
{
    public $amount;

    public $remark;

    public $id;

    public function __construct(
        float $amount,
        string $remark = '',
        int $id = 0
    ) {
        $this->amount = $amount;
        $this->remark = $remark;
        $this->id = $id;
    }
}
