<?php
namespace Market\Service\ServiceOrder\Translator;

use Market\Service\ServiceOrder\Model\ServiceOrder;
use Market\Service\ServiceOrder\Model\NullServiceOrder;

use Market\Order\Translator\CommonOrderDBTranslator;

class ServiceOrderDBTranslator extends CommonOrderDBTranslator
{
    public function arrayToObject(array $expression, $serviceOrder = null)
    {
        if (!isset($expression['order_id'])) {
            return NullServiceOrder::getInstance();
        }

        if ($serviceOrder == null) {
            $serviceOrder = new ServiceOrder();
        }

        $serviceOrder = parent::arrayToObject($expression, $serviceOrder);
        
        return $serviceOrder;
    }

    public function objectToArray($serviceOrder, array $keys = array())
    {
        if (!$serviceOrder instanceof ServiceOrder) {
            return false;
        }

        $expression = parent::objectToArray($serviceOrder, $keys);

        return $expression;
    }
}
