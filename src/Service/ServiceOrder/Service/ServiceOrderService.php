<?php
namespace Market\Service\ServiceOrder\Service;

use Market\Service\ServiceOrder\Model\ServiceOrder;

use Market\TradeRecord\Model\TradeRecordTrait;
use Market\TradeRecord\Strategy\BuyerServiceOrderConfirmationRecordStrategy;
use Market\TradeRecord\Strategy\SellerServiceOrderConfirmationRecordStrategy;
use Market\TradeRecord\Strategy\CommissionServiceOrderConfirmationRecordStrategy;

use Member\Member\Model\MemberAccount;
use Member\Member\Repository\MemberAccount\MemberAccountRepository;

use Marmot\Interfaces\INull;
use Market\TradeRecord\Strategy\SubsidyServiceOrderConfirmationRecordStrategy;

class ServiceOrderService
{
    use TradeRecordTrait;

    public function __construct(ServiceOrder $serviceOrder)
    {
        $this->serviceOrder = $serviceOrder;
    }

    public function __destruct()
    {
        unset($this->serviceOrder);
    }

    protected function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    protected function getMemberAccountRepository() : MemberAccountRepository
    {
        return new MemberAccountRepository();
    }
    
    protected function fetchMemberAccount(int $id) : MemberAccount
    {
        return $this->getMemberAccountRepository()->fetchOne($id);
    }

    public function pay() : bool
    {
        return $this->getServiceOrder()->pay();
    }

    public function buyerConfirmation() : bool
    {
        $sellerEnterpriseId = $this->getServiceOrder()->getSellerEnterprise()->getId();
        $memberAccount = $this->fetchMemberAccount($sellerEnterpriseId);
        $amount = $this->getServiceOrder()->getRealAmount();

        if ($memberAccount instanceof INull) {
            $memberAccount = new MemberAccount();
            $memberAccount->setId($sellerEnterpriseId);
        }

        return $this->getServiceOrder()->buyerConfirmation()
            && $this->addTradeRecord()
            && $memberAccount->balancePlus($amount);
    }

    protected function fetchTradeRecordStrategies() : array
    {
        return [
//            new BuyerServiceOrderConfirmationRecordStrategy($this->getServiceOrder()),
//            new SellerServiceOrderConfirmationRecordStrategy($this->getServiceOrder()),
//            new CommissionServiceOrderConfirmationRecordStrategy($this->getServiceOrder()),
//            new SubsidyServiceOrderConfirmationRecordStrategy($this->getServiceOrder())
        ];
    }
}
