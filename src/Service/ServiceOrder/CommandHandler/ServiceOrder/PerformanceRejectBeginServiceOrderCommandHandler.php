<?php
namespace Market\Service\ServiceOrder\CommandHandler\ServiceOrder;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\ServiceOrder\Command\ServiceOrder\PerformanceRejectBeginServiceOrderCommand;

class PerformanceRejectBeginServiceOrderCommandHandler implements ICommandHandler
{
    use ServiceOrderCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof PerformanceRejectBeginServiceOrderCommand)) {
            throw new \InvalidArgumentException;
        }

        $serviceOrder = $this->fetchServiceOrder($command->id);
        //卖家拒绝原因即为退款原因
        $refund = $this->getRefund();
        $refund->setRefundReason($command->rejectReason);

        return $serviceOrder->performanceRejectBegin($refund);
    }
}
