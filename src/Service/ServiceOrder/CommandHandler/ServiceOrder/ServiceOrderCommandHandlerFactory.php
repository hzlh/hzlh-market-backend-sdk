<?php
namespace Market\Service\ServiceOrder\CommandHandler\ServiceOrder;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceOrderCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Service\ServiceOrder\Command\ServiceOrder\BuyerCancelServiceOrderCommand'=>
        'Market\Service\ServiceOrder\CommandHandler\ServiceOrder\BuyerCancelServiceOrderCommandHandler',
        'Market\Service\ServiceOrder\Command\ServiceOrder\BuyerConfirmationServiceOrderCommand'=>
        'Market\Service\ServiceOrder\CommandHandler\ServiceOrder\BuyerConfirmationServiceOrderCommandHandler',
        'Market\Service\ServiceOrder\Command\ServiceOrder\BuyerDeleteServiceOrderCommand'=>
        'Market\Service\ServiceOrder\CommandHandler\ServiceOrder\BuyerDeleteServiceOrderCommandHandler',
        'Market\Service\ServiceOrder\Command\ServiceOrder\BuyerPermanentDeleteServiceOrderCommand'=>
        'Market\Service\ServiceOrder\CommandHandler\ServiceOrder\BuyerPermanentDeleteServiceOrderCommandHandler',
        'Market\Service\ServiceOrder\Command\ServiceOrder\PerformanceBeginServiceOrderCommand'=>
        'Market\Service\ServiceOrder\CommandHandler\ServiceOrder\PerformanceBeginServiceOrderCommandHandler',
        'Market\Service\ServiceOrder\Command\ServiceOrder\PerformanceRejectBeginServiceOrderCommand'=>
        'Market\Service\ServiceOrder\CommandHandler\ServiceOrder\PerformanceRejectBeginServiceOrderCommandHandler',
        'Market\Service\ServiceOrder\Command\ServiceOrder\PerformanceEndServiceOrderCommand'=>
        'Market\Service\ServiceOrder\CommandHandler\ServiceOrder\PerformanceEndServiceOrderCommandHandler',
        'Market\Service\ServiceOrder\Command\ServiceOrder\PlaceServiceOrderCommand'=>
        'Market\Service\ServiceOrder\CommandHandler\ServiceOrder\PlaceServiceOrderCommandHandler',
        'Market\Service\ServiceOrder\Command\ServiceOrder\SellerCancelServiceOrderCommand'=>
        'Market\Service\ServiceOrder\CommandHandler\ServiceOrder\SellerCancelServiceOrderCommandHandler',
        'Market\Service\ServiceOrder\Command\ServiceOrder\SellerDeleteServiceOrderCommand'=>
        'Market\Service\ServiceOrder\CommandHandler\ServiceOrder\SellerDeleteServiceOrderCommandHandler',
        'Market\Service\ServiceOrder\Command\ServiceOrder\SellerPermanentDeleteServiceOrderCommand'=>
        'Market\Service\ServiceOrder\CommandHandler\ServiceOrder\SellerPermanentDeleteServiceOrderCommandHandler',
        'Market\Service\ServiceOrder\Command\ServiceOrder\UpdateOrderAmountCommand'=>
        'Market\Service\ServiceOrder\CommandHandler\ServiceOrder\UpdateOrderAmountCommandHandler',
        'Market\Service\ServiceOrder\Command\ServiceOrder\UpdateOrderAddressCommand'=>
        'Market\Service\ServiceOrder\CommandHandler\ServiceOrder\UpdateOrderAddressCommandHandler',
    );
    
    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
