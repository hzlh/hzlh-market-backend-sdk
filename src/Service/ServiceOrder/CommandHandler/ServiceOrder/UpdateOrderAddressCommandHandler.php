<?php
namespace Market\Service\ServiceOrder\CommandHandler\ServiceOrder;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\ServiceOrder\Command\ServiceOrder\UpdateOrderAddressCommand;

use Snapshot\Model\Snapshot;
use Snapshot\Repository\RepositoryFactory;
use Snapshot\Repository\Snapshot\SnapshotRepository;

class UpdateOrderAddressCommandHandler implements ICommandHandler
{
    use ServiceOrderCommandHandlerTrait;

    protected function getSnapshotRepository() : SnapshotRepository
    {
        return new SnapshotRepository();
    }

    protected function getRepositoryFactory() : RepositoryFactory
    {
        return new RepositoryFactory();
    }
    
    protected function fetchSnapshotObject($category, $snapshotObjectId)
    {
        $repository = $this->getRepositoryFactory()->getRepository($category);
        return $repository->fetchOne($snapshotObjectId);
    }

    protected function fetchSnapshot(int $id) : Snapshot
    {
        return $this->getSnapshotRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof UpdateOrderAddressCommand)) {
            throw new \InvalidArgumentException;
        }

        $serviceOrder = $this->fetchServiceOrder($command->id);

        $orderAddress = $serviceOrder->getOrderAddress();
        $orderAddress->setCellphone($command->orderAddress['cellphone']);
        $snapshot = $this->fetchSnapshot($command->orderAddress['snapshotId']);
        $orderAddress->setSnapshot($snapshot);
        $deliveryAddress = $this->fetchSnapshotObject(
            $snapshot->getCategory(),
            $snapshot->getSnapshotObject()->getId()
        );
        $orderAddress->setDeliveryAddress($deliveryAddress);
        
        $serviceOrder->setOrderAddress($orderAddress);

        return $serviceOrder->updateOrderAddress();
    }
}
