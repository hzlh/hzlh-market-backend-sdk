<?php
namespace Market\Service\ServiceOrder\CommandHandler\ServiceOrder;

use Market\Service\ServiceOrder\Model\ServiceOrder;
use Market\Service\ServiceOrder\Repository\ServiceOrder\ServiceOrderRepository;

use Market\Service\Refund\Model\Refund;

trait ServiceOrderCommandHandlerTrait
{
    private $repository;

    private $refund;

    public function __construct()
    {
        $this->repository = new ServiceOrderRepository();
        $this->refund = new Refund();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->refund);
    }

    protected function getRepository() : ServiceOrderRepository
    {
        return $this->repository;
    }

    protected function fetchServiceOrder($id) : ServiceOrder
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getRefund() : Refund
    {
        return $this->refund;
    }
}
