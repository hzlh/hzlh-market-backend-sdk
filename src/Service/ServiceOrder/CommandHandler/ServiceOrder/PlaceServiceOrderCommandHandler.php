<?php
namespace Market\Service\ServiceOrder\CommandHandler\ServiceOrder;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\ServiceOrder\Model\ServiceOrder;
use Market\Service\ServiceOrder\Command\ServiceOrder\PlaceServiceOrderCommand;

use Member\Member\Model\MemberAccount;
use Member\Member\Repository\MemberAccount\MemberAccountRepository;

use Market\Order\Model\ITradeAble;
use Market\Order\Model\OrderAddress;
use Market\Order\Model\OrderCommodity;
use Market\Order\CommandHandler\OrderCommodityCommandHandlerTrait;
use Market\Order\CommandHandler\OrderAddressCommandHandlerTrait;
use Market\Order\CommandHandler\OrderMemberCouponCommandHandlerTrait;

class PlaceServiceOrderCommandHandler implements ICommandHandler
{
    use OrderCommodityCommandHandlerTrait,
        OrderAddressCommandHandlerTrait,
        OrderMemberCouponCommandHandlerTrait;
    
    private $serviceOrder;

    private $memberAccountRepository;

    public function __construct()
    {
        $this->serviceOrder = new ServiceOrder();
        $this->memberAccountRepository = new MemberAccountRepository();
    }

    public function __destruct()
    {
        unset($this->serviceOrder);
        unset($this->memberAccountRepository);
    }

    protected function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    protected function getMemberAccountRepository() : MemberAccountRepository
    {
        return $this->memberAccountRepository;
    }

    private function fetchMemberAccount(int $id) : MemberAccount
    {
        return $this->getMemberAccountRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof PlaceServiceOrderCommand)) {
            throw new \InvalidArgumentException;
        }

        $serviceOrder = $this->getServiceOrder();

        $orderCommodities = $this->executeOrderCommodities($command->orderCommodities);

        $orderAddress = $this->executeOrderAddress($command->orderAddress);

        if (!empty($command->memberCouponIds)) {
            $orderMemberCoupons = $this->executeMemberCoupons($command->memberCouponIds);
            foreach ($orderMemberCoupons as $orderMemberCoupon) {
                $serviceOrder->addMemberCoupon($orderMemberCoupon);
            }
        }

        $buyerMemberAccount = $this->fetchMemberAccount($command->buyerMemberAccountId);

        $serviceOrder->setBuyerMemberAccount($buyerMemberAccount);
        $serviceOrder->setCategory(ITradeAble::ORDER_CATEGORY['SERVICE']);
        foreach ($orderCommodities as $orderCommodity) {
            $serviceOrder->addOrderCommodity($orderCommodity);
        }
        $serviceOrder->setOrderAddress($orderAddress);
        $serviceOrder->setRemark($command->remark);
        $serviceOrder->generateOrderno();

        if ($serviceOrder->placeOrder()) {
            $command->id = $serviceOrder->getId();
            return true;
        }

        return false;
    }
}
