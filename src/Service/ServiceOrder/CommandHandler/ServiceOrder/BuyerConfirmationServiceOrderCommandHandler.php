<?php
namespace Market\Service\ServiceOrder\CommandHandler\ServiceOrder;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\ServiceOrder\Command\ServiceOrder\BuyerConfirmationServiceOrderCommand;

use Market\Service\ServiceOrder\Service\ServiceOrderService;

class BuyerConfirmationServiceOrderCommandHandler implements ICommandHandler
{
    use ServiceOrderCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof BuyerConfirmationServiceOrderCommand)) {
            throw new \InvalidArgumentException;
        }

        $serviceOrder = $this->fetchServiceOrder($command->id);

        $serviceOrderService = new ServiceOrderService($serviceOrder);

        return $serviceOrderService->buyerConfirmation();
    }
}
