<?php
namespace Market\Service\ServiceOrder\CommandHandler\ServiceOrder;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\ServiceOrder\Command\ServiceOrder\BuyerDeleteServiceOrderCommand;

class BuyerDeleteServiceOrderCommandHandler implements ICommandHandler
{
    use ServiceOrderCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof BuyerDeleteServiceOrderCommand)) {
            throw new \InvalidArgumentException;
        }

        $serviceOrder = $this->fetchServiceOrder($command->id);

        return $serviceOrder->buyerDelete();
    }
}
