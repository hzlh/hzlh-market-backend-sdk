<?php
namespace Market\Service\ServiceOrder\CommandHandler\ServiceOrder;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\ServiceOrder\Command\ServiceOrder\BuyerCancelServiceOrderCommand;

class BuyerCancelServiceOrderCommandHandler implements ICommandHandler
{
    use ServiceOrderCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof BuyerCancelServiceOrderCommand)) {
            throw new \InvalidArgumentException;
        }

        $serviceOrder = $this->fetchServiceOrder($command->id);
        $serviceOrder->setCancelReason($command->cancelReason);

        return $serviceOrder->buyerCancel();
    }
}
