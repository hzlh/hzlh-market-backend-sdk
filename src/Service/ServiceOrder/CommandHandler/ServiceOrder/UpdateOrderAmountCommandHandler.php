<?php
namespace Market\Service\ServiceOrder\CommandHandler\ServiceOrder;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\ServiceOrder\Command\ServiceOrder\UpdateOrderAmountCommand;

class UpdateOrderAmountCommandHandler implements ICommandHandler
{
    use ServiceOrderCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof UpdateOrderAmountCommand)) {
            throw new \InvalidArgumentException;
        }

        $serviceOrder = $this->fetchServiceOrder($command->id);

        $serviceOrder->setPaidAmount($command->amount);
        $serviceOrder->setRemark($command->remark);

        return $serviceOrder->updateOrderAmount();
    }
}
