<?php
namespace Market\Service\ServiceOrder\CommandHandler\ServiceOrder;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\ServiceOrder\Command\ServiceOrder\SellerCancelServiceOrderCommand;

class SellerCancelServiceOrderCommandHandler implements ICommandHandler
{
    use ServiceOrderCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof SellerCancelServiceOrderCommand)) {
            throw new \InvalidArgumentException;
        }

        $serviceOrder = $this->fetchServiceOrder($command->id);
        $serviceOrder->setCancelReason($command->cancelReason);

        return $serviceOrder->sellerCancel();
    }
}
