<?php
namespace Market\Service\ServiceOrder\Adapter\ServiceOrder;

use Market\Service\ServiceOrder\Model\ServiceOrder;
use Market\Service\ServiceOrder\Utils\MockFactory;

class ServiceOrderMockAdapter implements IServiceOrderAdapter
{
    public function add(ServiceOrder $serviceOrder, array $keys = array()) : bool
    {
        unset($serviceOrder);

        return true;
    }

    public function edit(ServiceOrder $serviceOrder, array $keys = array()) : bool
    {
        unset($serviceOrder);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : ServiceOrder
    {
        return MockFactory::generateServiceOrder($id);
    }

    public function fetchList(array $ids) : array
    {
        $serviceOrderList = array();

        foreach ($ids as $id) {
            $serviceOrderList[] = MockFactory::generateServiceOrder($id);
        }

        return $serviceOrderList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
