<?php
namespace Market\Service\ServiceOrder\Adapter\ServiceOrder;

use Marmot\Core;

use Market\Order\Adapter\Order\Query\Persistence\OrderCache;
use Market\Service\ServiceOrder\Model\ServiceOrder;
use Market\Service\ServiceOrder\Model\NullServiceOrder;
use Market\Service\ServiceOrder\Translator\ServiceOrderDBTranslator;

use Market\Order\Adapter\Order\OrderAdapterTrait;
use Market\Order\Adapter\Order\Query\OrderRowQuery;
use Market\Order\Adapter\Order\Query\OrderRowCacheQuery;
use Market\Order\Repository\OrderSearch\OrderSearchRepository;

use Market\Order\Model\OrderUpdateAmountRecord;
use Market\Order\Model\NullOrderUpdateAmountRecord;
use Market\Order\Repository\OrderUpdateAmountRecord\OrderUpdateAmountRecordRepository;

use Market\Payment\Model\Payment;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class ServiceOrderDBAdapter implements IServiceOrderAdapter
{
    const PLATFORM_CANCEL_ORDER_TIME_LIMIT = 48;
    const PLATFORM_CONFIRMATIONL_ORDER_TIME_LIMIT = 15;
    
    use OrderAdapterTrait;
    
    private $translator;

    private $rowQuery;

    private $rowCacheQuery;

    private $orderSearchRepository;

    private $orderUpdateAmountRecordRepository;

    public function __construct()
    {
        $this->translator = new ServiceOrderDBTranslator();
        $this->rowQuery = new OrderRowQuery();
        $this->rowCacheQuery = new OrderRowCacheQuery();
        $this->orderSearchRepository = new OrderSearchRepository();
        $this->orderUpdateAmountRecordRepository = new OrderUpdateAmountRecordRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowCacheQuery);
        unset($this->orderSearchRepository);
        unset($this->orderUpdateAmountRecordRepository);
    }
    
    protected function getDBTranslator() : ServiceOrderDBTranslator
    {
        return $this->translator;
    }
    
    protected function getRowCacheQuery() : OrderRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getRowQuery() : OrderRowQuery
    {
        return $this->rowQuery;
    }
    
    protected function getOrderSearchRepository() : OrderSearchRepository
    {
        return $this->orderSearchRepository;
    }

    protected function getOrderUpdateAmountRecordRepository() : OrderUpdateAmountRecordRepository
    {
        return $this->orderUpdateAmountRecordRepository;
    }

    public function add(ServiceOrder $serviceOrder, array $keys = array()) : bool
    {
        $info = array();
        
        $info = $this->getDBTranslator()->objectToArray($serviceOrder, $keys);

        $id = $this->getRowQuery()->add($info);

        if (!$id) {
            return false;
        }

        $serviceOrder->setId($id);
        $this->getOrderSearchRepository()->add($serviceOrder);
        return true;
    }

    public function getCacheKey($id) : string
    {
        $cache = new OrderCache();
        return $cache->getCacheKey($id);
    }

    public function edit(ServiceOrder $serviceOrder, array $keys = array()) : bool
    {
        $info = array();

        //非缓存获取
        $conditionArray[$this->getRowQuery()->getPrimaryKey()] = $serviceOrder->getId();

        $info = $this->getDBTranslator()->objectToArray($serviceOrder, $keys);
        $result = $this->getRowQuery()->update($info, $conditionArray);

        if (!$result) {
            return false;
        }

        return true;
    }

    public function fetchOne($id) : ServiceOrder
    {
        $info = array();

        $info = $this->getRowQuery()->getOne($id);

        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullServiceOrder::getInstance();
        }

        $serviceOrder = $this->getDBTranslator()->arrayToObject($info);

        $buyerMemberAccount = $this->fetchMemberAccount($serviceOrder->getBuyerMemberAccount()->getId());
        if (!empty($buyerMemberAccount)) {
            $serviceOrder->setBuyerMemberAccount($buyerMemberAccount);
        }

        $sellerEnterprise = $this->fetchEnterprise($serviceOrder->getSellerEnterprise()->getId());
        if (!empty($sellerEnterprise)) {
            $serviceOrder->setSellerEnterprise($sellerEnterprise);
        }

        $orderAddress = $this->orderAddressList($serviceOrder->getId());

        if (!empty($orderAddress)) {
            $serviceOrder->setOrderAddress($orderAddress[$serviceOrder->getId()]);
        }

        $orderCommodityList = $this->orderCommodityList($serviceOrder->getId());

        if (!empty($orderCommodityList)) {
            $orderCommodities = $orderCommodityList[$serviceOrder->getId()];
            foreach ($orderCommodities as $orderCommodity) {
                $serviceOrder->addOrderCommodity($orderCommodity);
            }
        }

        $orderMemberCouponList = $this->orderMemberCouponList($serviceOrder->getId());

        if (!empty($orderMemberCouponList)) {
            $orderMemberCoupons = isset($orderMemberCouponList[$serviceOrder->getId()]) ?
                                  $orderMemberCouponList[$serviceOrder->getId()] :
                                  array();
            foreach ($orderMemberCoupons as $orderMemberCoupon) {
                $serviceOrder->addMemberCoupon($orderMemberCoupon);
            }
        }

        $orderUpdateAmountRecord = $this->fetchOrderUpdateAmountRecord($serviceOrder->getId());

        $serviceOrder->setOrderUpdateAmountRecord($orderUpdateAmountRecord);

        return $serviceOrder;
    }

    protected function fetchOrderUpdateAmountRecord($orderId) : OrderUpdateAmountRecord
    {
        $filter['order'] = $orderId;
        $sort['id'] = -1;
        $page = 0;
        $size = 1;

        list($list, $count) = $this->getOrderUpdateAmountRecordRepository()->filter(
            $filter,
            $sort,
            $page,
            $size
        );

        return $count > 0 ? array_shift($list) : NullOrderUpdateAmountRecord::getInstance();
    }

    public function fetchList(array $ids) : array
    {
        $serviceOrderList = array();
        $serviceOrderIds = array();
        $buyerMemberAccountIds = array();
        $sellerEnterpriseIds = array();
        
        //$serviceOrderInfoList = $this->getRowCacheQuery()->getList($ids);
        $serviceOrderInfoList = $this->getRowQuery()->getList($ids);

        if (empty($serviceOrderInfoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $translator = $this->getDBTranslator();
        foreach ($serviceOrderInfoList as $serviceOrderInfo) {
            $serviceOrder = $translator->arrayToObject($serviceOrderInfo);
            
            $buyerMemberAccountIds[$serviceOrder->getId()] = $serviceOrder->getBuyerMemberAccount()->getId();
            $sellerEnterpriseIds[$serviceOrder->getId()] = $serviceOrder->getSellerEnterprise()->getId();
            $serviceOrderIds[] = $serviceOrder->getId();

            $orderUpdateAmountRecord = $this->fetchOrderUpdateAmountRecord($serviceOrder->getId());

            $serviceOrder->setOrderUpdateAmountRecord($orderUpdateAmountRecord);

            $serviceOrderList[] = $serviceOrder;
        }

        $buyerMemberAccount = $this->getMemberAccountRepository()->fetchList($buyerMemberAccountIds);
        if (!empty($buyerMemberAccount)) {
            foreach ($serviceOrderList as $key => $serviceOrder) {
                if (isset($buyerMemberAccount[$key])) {
                    $serviceOrder->setBuyerMemberAccount($buyerMemberAccount[$key]);
                }
            }
        }
        
        $sellerEnterprise = $this->getEnterpriseRepository()->fetchList($sellerEnterpriseIds);
        if (!empty($sellerEnterprise)) {
            foreach ($serviceOrderList as $key => $serviceOrder) {
                if (isset($sellerEnterprise[$key])) {
                    $serviceOrder->setSellerEnterprise($sellerEnterprise[$key]);
                }
            }
        }

        $orderAddressList = $this->orderAddressList($serviceOrderIds);

        if (!empty($orderAddressList)) {
            foreach ($serviceOrderList as $serviceOrder) {
                if (isset($orderAddressList[$serviceOrder->getId()])) {
                    $serviceOrder->setOrderAddress($orderAddressList[$serviceOrder->getId()]);
                }
            }
        }

        $orderCommodityListTemp = $this->orderCommodityList($serviceOrderIds);

        if (!empty($orderCommodityListTemp)) {
            foreach ($serviceOrderList as $serviceOrder) {
                $orderCommodityList = $orderCommodityListTemp[$serviceOrder->getId()];
                foreach ($orderCommodityList as $orderCommodity) {
                    $serviceOrder->addOrderCommodity($orderCommodity);
                }
            }
        }

        $orderMemberCouponListTemp = $this->orderMemberCouponList($serviceOrderIds);

        if (!empty($orderMemberCouponListTemp)) {
            foreach ($serviceOrderList as $serviceOrder) {
                $orderMemberCouponList = isset($orderMemberCouponListTemp[$serviceOrder->getId()]) ?
                                         $orderMemberCouponListTemp[$serviceOrder->getId()] :
                                         array();
                foreach ($orderMemberCouponList as $orderMemberCoupon) {
                    $serviceOrder->addMemberCoupon($orderMemberCoupon);
                }
            }
        }

        return $serviceOrderList;
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $serviceOrder = new ServiceOrder();
                
            if (isset($filter['category']) && !empty($filter['category'])) {
                $category = $filter['category'];
                if (is_numeric($category)) {
                    $serviceOrder->setCategory($filter['category']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('category')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($category, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('category')
                    );
                    $condition .= $conjection.key($info).' IN ('.$category.')';
                }
                $conjection = ' AND ';
            }

            if (isset($filter['paymentType']) && !empty($filter['paymentType'])) {
                $paymentType = $filter['paymentType'];
                if (is_numeric($paymentType)) {
                    $payment = new Payment($filter['paymentType']);
                    $serviceOrder->setPayment($payment);
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('paymentType')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($paymentType, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('paymentType')
                    );
                    $condition .= $conjection.key($info).' IN ('.$paymentType.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $status = $filter['status'];
                if (is_numeric($status)) {
                    $serviceOrder->setStatus($filter['status']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('status')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($status, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('status')
                    );
                    $condition .= $conjection.key($info).' IN ('.$status.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['buyerOrderStatus'])) {
                if (!empty($condition)) {
                    $conjection = ' OR ';
                }
                $buyerOrderStatus = $filter['buyerOrderStatus'];
                if (is_numeric($buyerOrderStatus)) {
                    $serviceOrder->setBuyerOrderStatus($filter['buyerOrderStatus']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('buyerOrderStatus')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($buyerOrderStatus, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('buyerOrderStatus')
                    );
                    $condition .= $conjection.key($info).' IN ('.$buyerOrderStatus.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['sellerOrderStatus'])) {
                if (!empty($condition)) {
                    $conjection = ' OR ';
                }
                $sellerOrderStatus = $filter['sellerOrderStatus'];
                if (is_numeric($sellerOrderStatus)) {
                    $serviceOrder->setSellerOrderStatus($filter['sellerOrderStatus']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('sellerOrderStatus')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($sellerOrderStatus, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('sellerOrderStatus')
                    );
                    $condition .= $conjection.key($info).' IN ('.$sellerOrderStatus.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['buyerOrderStatusPortal'])) {
                $buyerOrderStatus = $filter['buyerOrderStatusPortal'];
                if (is_numeric($buyerOrderStatus)) {
                    $serviceOrder->setBuyerOrderStatus($filter['buyerOrderStatusPortal']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('buyerOrderStatus')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($buyerOrderStatus, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('buyerOrderStatus')
                    );
                    $condition .= $conjection.key($info).' IN ('.$buyerOrderStatus.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['sellerOrderStatusPortal'])) {
                $sellerOrderStatus = $filter['sellerOrderStatusPortal'];
                if (is_numeric($sellerOrderStatus)) {
                    $serviceOrder->setSellerOrderStatus($filter['sellerOrderStatusPortal']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('sellerOrderStatus')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($sellerOrderStatus, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $serviceOrder,
                        array('sellerOrderStatus')
                    );
                    $condition .= $conjection.key($info).' IN ('.$sellerOrderStatus.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['buyerMemberAccount']) && !empty($filter['buyerMemberAccount'])) {
                $serviceOrder->getBuyerMemberAccount()->setId($filter['buyerMemberAccount']);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceOrder,
                    array('buyerMemberAccount')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }

            if (isset($filter['sellerEnterprise']) && !empty($filter['sellerEnterprise'])) {
                $serviceOrder->getSellerEnterprise()->setId($filter['sellerEnterprise']);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceOrder,
                    array('sellerEnterprise')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['startTime']) && !empty($filter['startTime'])) {
                $serviceOrder->setCreateTime($filter['startTime']);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceOrder,
                    array('createTime')
                );
                $condition .= $conjection.key($info).' >= '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['endTime']) && !empty($filter['endTime'])) {
                $serviceOrder->setCreateTime($filter['endTime']);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceOrder,
                    array('createTime')
                );
                $condition .= $conjection.key($info).' <= '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['orderno'])) {
                $serviceOrder->setOrderno($filter['orderno']);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceOrder,
                    array('orderno')
                );
                $condition .= $conjection.key($info).' = "'.current($info).'"';
                $conjection = ' AND ';
            }
            if (isset($filter['dealStartTime']) && !empty($filter['dealStartTime'])) {
                $payment = new Payment(0, $filter['dealStartTime']);
                $serviceOrder->setPayment($payment);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceOrder,
                    array('paymentTime')
                );
                $condition .= $conjection.key($info).' >= '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['dealEndTime']) && !empty($filter['dealEndTime'])) {
                $payment = new Payment(0, $filter['dealEndTime']);
                $serviceOrder->setPayment($payment);
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceOrder,
                    array('paymentTime')
                );
                $condition .= $conjection.key($info).' <= '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['serviceCategory']) && !empty($filter['serviceCategory'])) {
                $filterInfo = array(
                    'serviceCategories' => $filter['serviceCategory']
                );
                $orderIds = $this->getOrderSearchRepository()->orderSearchGetOrder($filterInfo);

                $filter['orderIds'] = 0;

                if (!empty($orderIds)) {
                    $filter['orderIds'] = implode(',', $orderIds);
                }
            }
            if (isset($filter['serviceName']) && !empty($filter['serviceName'])) {
                $filterInfo = array(
                    'serviceNames' => $filter['serviceName']
                );

                $orderIds = $this->getOrderSearchRepository()->orderSearchGetOrder($filterInfo);

                if (isset($filter['orderIds']) && !empty($filter['orderIds'])) {
                    $orderIds = array(explode(',', $filter['orderIds']),$orderIds);
                }

                $filter['orderIds'] = 0;
                
                if (!empty($orderIds)) {
                    $filter['orderIds'] = implode(',', $orderIds);
                }
            }

            if (isset($filter['sellerEnterpriseName']) && !empty($filter['sellerEnterpriseName'])) {
                $filterInfo = array(
                    'sellerEnterpriseName' => $filter['sellerEnterpriseName']
                );

                $orderIds = $this->getOrderSearchRepository()->orderSearchGetOrder($filterInfo);

                if (isset($filter['orderIds']) && !empty($filter['orderIds'])) {
                    $orderIds = array(explode(',', $filter['orderIds']),$orderIds);
                }

                $filter['orderIds'] = 0;
                
                if (!empty($orderIds)) {
                    $filter['orderIds'] = implode(',', $orderIds);
                }
            }
            if (isset($filter['deliveryAddressCellphone']) && !empty($filter['deliveryAddressCellphone'])) {
                $filterInfo = array(
                    'deliveryAddressCellphone' => $filter['deliveryAddressCellphone']
                );

                $orderIds = $this->getOrderSearchRepository()->orderSearchGetOrder($filterInfo);

                if (isset($filter['orderIds']) && !empty($filter['orderIds'])) {
                    $orderIds = array(explode(',', $filter['orderIds']),$orderIds);
                }

                $filter['orderIds'] = 0;
                
                if (!empty($orderIds)) {
                    $filter['orderIds'] = implode(',', $orderIds);
                }
            }
            if (isset($filter['orderIds'])) {
                $ids = $filter['orderIds'];
                $info = $this->getDBTranslator()->objectToArray(
                    $serviceOrder,
                    array('id')
                );
                $condition .= $conjection.key($info).' IN ('.$ids.')';
                $conjection = ' AND ';
            }

            if (isset($filter['oldOrderno'])) {
                $id = substr($filter['oldOrderno'], 8);
                $id = is_numeric($id) ? $id : 0;
               
                $serviceOrder->setId($id);
                $info = $this->getDBTranslator()->objectToArray($serviceOrder, array('id'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            //定时任务,查询下单未支付超出48小时的订单
            if (isset($filter['validityCreateTime'])) {
                $condition .= $conjection.'TIMESTAMPDIFF(HOUR,FROM_UNIXTIME(
                    create_time,
                    "%Y-%m-%d %H:%i:%s")
                ,NOW()) >= '.self::PLATFORM_CANCEL_ORDER_TIME_LIMIT;
                $conjection = ' AND ';
            }

            //定时任务,查询等待买家确认订单超出15天的订单
            if (isset($filter['validityUpdateTime'])) {
                $condition .= $conjection.'TIMESTAMPDIFF(DAY,FROM_UNIXTIME(
                    update_time,
                    "%Y-%m-%d %H:%i:%s")
                ,NOW()) >='. self::PLATFORM_CONFIRMATIONL_ORDER_TIME_LIMIT;
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    private function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $order = new ServiceOrder();
            if (isset($sort['createTime'])) {
                $info = $this->getDBTranslator()->objectToArray($order, array('createTime'));
                $condition .= $conjection.key($info).' '.($sort['createTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['updateTime'])) {
                $info = $this->getDBTranslator()->objectToArray($order, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['id'])) {
                $info = $this->getDBTranslator()->objectToArray($order, array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        //$rowCacheQuery = $this->getRowCacheQuery();
        $rowCacheQuery = $this->getRowQuery();

        $condition = $this->formatFilter($filter);
        $condition .= $this->formatSort($sort);

        $serviceList = $rowCacheQuery->find($condition, $offset, $size);
        if (empty($serviceList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array(array(), 0);
        }

        $ids = array();
        $primaryKey = $rowCacheQuery->getPrimaryKey();
        foreach ($serviceList as $info) {
            $ids[] = $info[$primaryKey];
        }

        $count = 0;

        $count = sizeof($ids);
        if ($count  == $size || $offset > 0) {
            $count = $rowCacheQuery->count($condition);
        }

        return array($this->fetchList($ids), $count);
    }
}
