<?php
namespace Market\Service\ServiceOrder\Adapter\ServiceOrder;

use Market\Service\ServiceOrder\Model\ServiceOrder;

interface IServiceOrderAdapter
{
    public function add(ServiceOrder $serviceOrder, array $keys = array()) : bool;

    public function edit(ServiceOrder $serviceOrder, array $keys = array()) : bool;
    
    public function fetchOne($id) : ServiceOrder;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;
}
