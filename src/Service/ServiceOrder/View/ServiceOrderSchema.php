<?php
namespace Market\Service\ServiceOrder\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class ServiceOrderSchema extends SchemaProvider
{
    protected $resourceType = 'marketServiceOrders';

    public function getId($serviceOrder)
    {
        return $serviceOrder->getId();
    }

    public function getAttributes($serviceOrder) : array
    {
        return [
            'orderno'  => $serviceOrder->getOrderno(),
            'paymentId' => $serviceOrder->getPaymentId(),
            'totalPrice'  => $serviceOrder->getTotalPrice(),
            'paidAmount' => $serviceOrder->getPaidAmount(),
            'collectedAmount' => $serviceOrder->getCollectedAmount(),
            'commission' => $serviceOrder->getCommission(),
            'subsidy' => $serviceOrder->getSubsidy(),
            'realAmount' => $serviceOrder->getRealAmount(),
            'platformPreferentialAmount' => $serviceOrder->generatePlatformPreferentialAmount(),
            'businessPreferentialAmount' => $serviceOrder->generateBusinessPreferentialAmount(),
            'paymentType' => $serviceOrder->getPayment()->getType(),
            'transactionNumber' => $serviceOrder->getPayment()->getTransactionNumber(),
            'transactionInfo' => $serviceOrder->getPayment()->getTransactionInfo(),
            'paymentTime' => $serviceOrder->getPayment()->getTime(),
            'status' => $serviceOrder->getStatus(),
            'remark' => $serviceOrder->getRemark(),
            'failureReason'  => $serviceOrder->getFailureReason(),
            'timeRecord' => $serviceOrder->getTimeRecord(),
            'cancelReason' => $serviceOrder->getCancelReason(),
            'buyerOrderStatus' => $serviceOrder->getBuyerOrderStatus(),
            'sellerOrderStatus' => $serviceOrder->getSellerOrderStatus(),
            'createTime' => $serviceOrder->getCreateTime(),
            'updateTime' => $serviceOrder->getUpdateTime(),
            'statusTime' => $serviceOrder->getStatusTime()
        ];
    }
    
    public function getRelationships($serviceOrder, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'buyerMemberAccount' => [self::DATA => $serviceOrder->getBuyerMemberAccount()],
            'sellerEnterprise' => [self::DATA => $serviceOrder->getSellerEnterprise()],
            'orderCommodities' => [self::DATA => $serviceOrder->getOrderCommodities()],
            'orderAddress' => [self::DATA => $serviceOrder->getOrderAddress()],
            'orderUpdateAmountRecord' => [self::DATA => $serviceOrder->getOrderUpdateAmountRecord()],
            'memberCoupons' => [self::DATA => $serviceOrder->getMemberCoupons()],
        ];
    }
}
