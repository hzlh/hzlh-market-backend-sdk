<?php
namespace Market\Service\ServiceOrder\Strategy;

use Market\Payment\Model\Payment;

use Market\Service\ServiceOrder\Model\ServiceOrder;

use Member\Member\Model\MemberAccount;
use Member\Member\Repository\MemberAccount\MemberAccountRepository;

class BalancePaymentStrategy implements IPaymentStrategy
{
    protected function getMemberAccountRepository() : MemberAccountRepository
    {
        return new MemberAccountRepository();
    }

    protected function fetchMemberAccount(int $id) : MemberAccount
    {
        return $this->getMemberAccountRepository()->fetchOne($id);
    }

    public function algorithm(ServiceOrder $serviceOrder) : bool
    {
        $memberAccount = $this->fetchMemberAccount($serviceOrder->getBuyerMemberAccount()->getId());

        return $memberAccount->balanceReduce($serviceOrder->getPaidAmount());
    }
}
