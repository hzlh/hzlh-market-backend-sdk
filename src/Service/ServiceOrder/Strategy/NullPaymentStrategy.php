<?php
namespace Market\Service\ServiceOrder\Strategy;

use Market\Payment\Model\Payment;

use Market\Service\ServiceOrder\Model\ServiceOrder;

class NullPaymentStrategy implements IPaymentStrategy
{
    public function algorithm(ServiceOrder $serviceOrder) : bool
    {
        unset($serviceOrder);
        return false;
    }
}
