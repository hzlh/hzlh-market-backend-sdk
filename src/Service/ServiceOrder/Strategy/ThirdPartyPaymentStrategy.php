<?php
namespace Market\Service\ServiceOrder\Strategy;

use Marmot\Interfaces\INull;

use Market\Payment\Model\Payment;

use Market\Service\ServiceOrder\Model\ServiceOrder;

use Market\TradeRecord\Model\TradeRecordTrait;

use Market\TradeRecord\Strategy\BuyerServiceOrderPayRecordStrategy;
use Market\TradeRecord\Strategy\PlatformServiceOrderPayRecordStrategy;

class ThirdPartyPaymentStrategy implements IPaymentStrategy
{
    use TradeRecordTrait;
    
    private $serviceOrder;

    public function __construct()
    {
        $this->serviceOrder = new ServiceOrder();
    }

    public function __destruct()
    {
        unset($this->serviceOrder);
    }

    protected function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    public function algorithm(ServiceOrder $serviceOrder) : bool
    {
        $this->serviceOrder = $serviceOrder;
        
        return $this->addTradeRecord();
    }

    protected function fetchTradeRecordStrategies() : array
    {
        return [
            new BuyerServiceOrderPayRecordStrategy($this->getServiceOrder()),
            new PlatformServiceOrderPayRecordStrategy($this->getServiceOrder())
        ];
    }
}
