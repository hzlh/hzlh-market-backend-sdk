<?php
namespace Market\Service\ServiceOrder\Strategy;

use Market\Payment\Model\Payment;

class PaymentStrategyFactory
{
    const MAPS = array(
        Payment::PAYMENT_TYPE['WECHAT']=>'\Market\Service\ServiceOrder\Strategy\ThirdPartyPaymentStrategy',
        Payment::PAYMENT_TYPE['BALANCE']=>'\Market\Service\ServiceOrder\Strategy\BalancePaymentStrategy',
        Payment::PAYMENT_TYPE['ALIPAY']=>'\Market\Service\ServiceOrder\Strategy\ThirdPartyPaymentStrategy',
    );

    public static function getStrategy(int $type) : IPaymentStrategy
    {
        $strategy = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';
        return class_exists($strategy) ? new $strategy : new NullPaymentStrategy();
    }
}
