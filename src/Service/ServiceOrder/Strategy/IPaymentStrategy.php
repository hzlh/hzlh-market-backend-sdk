<?php
namespace Market\Service\ServiceOrder\Strategy;

use Market\Service\ServiceOrder\Model\ServiceOrder;

interface IPaymentStrategy
{
    public function algorithm(ServiceOrder $serviceOrder) : bool;
}
