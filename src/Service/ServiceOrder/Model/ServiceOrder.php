<?php
namespace Market\Service\ServiceOrder\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Common\Observer\ISplSubject;
use Common\Observer\SubjectTrait;

use Market\Order\Model\CommonOrder;
use Market\Order\Model\OrderCommodity;
use Market\Order\Model\OrderUpdateAmountRecord;

use Market\Service\Authentication\Adapter\Authentication\IAuthenticationAdapter;
use Market\Service\Authentication\Model\Authentication;
use Market\Service\Authentication\Model\NullAuthentication;
use Market\Service\Authentication\Repository\Authentication\AuthenticationRepository;
use Market\Service\Refund\Model\Refund;
use Market\Service\Service\Model\Service;
use Market\Service\Service\Model\ServiceStatisticTrait;
use Market\Service\ServiceCategory\Model\ServiceCategory;
use Market\Service\ServiceOrder\Strategy\PaymentStrategyFactory;
use Market\Service\ServiceOrder\Repository\ServiceOrder\ServiceOrderRepository;
use Market\Service\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;

use Member\Enterprise\Model\Enterprise;
use Member\Enterprise\Repository\Enterprise\EnterpriseRepository;
use Member\Member\Model\Member;
use Member\MemberCoupon\Model\MemberCoupon;
use Member\MemberCoupon\Model\AutoReleaseCouponTrait;
use Member\MemberCoupon\Repository\MemberCoupon\MemberCouponRepository;

use Strategy\Adapter\Type\ITypeAdapter;
use Strategy\Model\BasicConfig;
use Strategy\Model\NullType;
use Strategy\Repository\TypeRepository;
use Market\TradeRecord\Model\ITradeAble;
use Market\TradeRecord\Model\TradeRecordTrait;
use Market\TradeRecord\Model\TradeRefundRecordTrait;
use Market\TradeRecord\Strategy\BuyerServiceOrderConfirmationRecordStrategy;
use Market\TradeRecord\Strategy\SellerServiceOrderConfirmationRecordStrategy;
use Market\TradeRecord\Strategy\CommissionServiceOrderConfirmationRecordStrategy;
use Market\TradeRecord\Strategy\PlatformServiceOrderRefundRecordStrategy;
use Market\TradeRecord\Strategy\BuyerServiceOrderRefundRecordStrategy;

use BusinessNotice\CommandHandler\BusinessNotice\AutoTriggerAddBusinessNoticeCommandHandler;

use Market\Payment\Model\IPayAble;
use Market\Order\Model\ITradeAble as IOrderTradeAble;
use Coupon\ReleaseCoupon\Model\ReleaseCoupon;
use Market\TradeRecord\Strategy\SubsidyServiceOrderConfirmationRecordStrategy;

/**
 * @SuppressWarnings(PHPMD)
 */
class ServiceOrder extends CommonOrder implements IPayAble, ITradeAble, ISplSubject
{
    const PROBAILITY = 100;
    const MIN_AMOUNT = 0.01;
    const CAN_USE_COUPON_AMOUNT = 20;
    const UPDATE_ORDER_AMOUNT_LOWER_LIMINT = 0.01;
    const ORDER_AMOUNT_LOWER_LIMINT = 0;
    const MAX_COUPON_AMOUNT = 200;

    const MEMBER_COUPON_USE_MAX_COUNT = 2;
    const MEMBER_COUPON_NOT_SUPERPOSITION_COUNT = 1;

    use ServiceStatisticTrait,
        TradeRecordTrait,
        TradeRefundRecordTrait,
        AutoReleaseCouponTrait,
        SubjectTrait;

    private $orderUpdateAmountRecord;

    private $repository;

    private $paymentStrategyFactory;

    private $enterpriseRepository;
    
    private $memberCouponRepository;
    
    private $serviceCategoryRepository;

    public function __construct()
    {
        parent::__construct();
        $this->orderUpdateAmountRecord = new OrderUpdateAmountRecord();
        $this->repository = new ServiceOrderRepository();
        $this->paymentStrategyFactory = new PaymentStrategyFactory();
        $this->enterpriseRepository = new EnterpriseRepository();
        $this->memberCouponRepository = new MemberCouponRepository();
        $this->serviceCategoryRepository = new ServiceCategoryRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->orderUpdateAmountRecord);
        unset($this->repository);
        unset($this->paymentStrategyFactory);
        unset($this->enterpriseRepository);
        unset($this->memberCouponRepository);
        unset($this->serviceCategoryRepository);
    }

    private function addBusinessNotice() : bool
    {
        $this->attach(new AutoTriggerAddBusinessNoticeCommandHandler());

        if ($this->notify()) {
            return true;
        };

        return false;
    }

    public function setOrderUpdateAmountRecord(OrderUpdateAmountRecord $orderUpdateAmountRecord) : void
    {
        $this->orderUpdateAmountRecord = $orderUpdateAmountRecord;
    }

    public function getOrderUpdateAmountRecord() : OrderUpdateAmountRecord
    {
        return $this->orderUpdateAmountRecord;
    }

    protected function getRepository() : ServiceOrderRepository
    {
        return $this->repository;
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }
    
    protected function getPaymentStrategyFactory() : PaymentStrategyFactory
    {
        return $this->paymentStrategyFactory;
    }

    protected function getMemberCouponRepository() : MemberCouponRepository
    {
        return $this->memberCouponRepository;
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return $this->serviceCategoryRepository;
    }

    public function getPaymentId() : string
    {
        return IPayAble::TYPE['ORDER'].'_'.$this->getId();
    }

    public function generateBusinessPreferentialAmount() : float
    {
        $totalPrice = $this->getOrderUpdateAmountRecord()->getAmount();

        return $this->calculateBusinessPreferentialAmount($totalPrice);
    }

    public function generatePlatformPreferentialAmount() : float
    {
        return $this->getCollectedAmount()-$this->getPaidAmount();
    }
    //下单未付款
    protected function placeOrderAction() : bool
    {
        $this->calculateTotalPriceAndSellerEnterprise();
        $this->calculateAmount();

        $timeRecord = $this->getTimeRecord();
        $timeRecord[self::STATUS['PENDING']] = Core::$container->get('time');

        $this->setTimeRecord($timeRecord);

        if ($this->getSellerEnterprise() instanceof INull || $this->getBuyerMemberAccount() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY);
            return false;
        }
        
        if (!$this->isEnterpriseCertificationRequired()) {
            return false;
        }

        if (!$this->isCanUseCoupon()) {
            return false;
        }
        //设置佣金和补贴比例
        $this->setCommissionAndSubsidy();

        return $this->getRepository()->add($this, array(
            'id',
            'orderno',
            'buyerMemberAccount',
            'sellerEnterprise',
            'remark',
            'totalPrice',
            'paidAmount',
            'collectedAmount',
            'commission',
            'subsidy',
            'realAmount',
            'paymentType',
            'paymentTime',
            'transactionNumber',
            'transactionInfo',
            'cancelReason',
            'category',
            'status',
            'failureReason',
            'buyerOrderStatus',
            'sellerOrderStatus',
            'createTime',
            'updateTime',
            'statusTime',
            'timeRecord',
        )) && $this->addOrderCommodities()
           && $this->getOrderAddress()->add($this)
           && $this->bindMemberCoupons()
           && $this->updateMemberCouponStatus(MemberCoupon::STATUS['CONFIRMATION'])
           && $this->addBusinessNotice();
    }

    private function isEnterpriseCertificationRequired() : bool
    {
        $orderCommodities = $this->getOrderCommodities();
        foreach ($orderCommodities as $orderCommodity) {
            $serviceCategoryId = $orderCommodity->getSnapshot()->getSnapshotObject()->getServiceCategory()->getId(); //phpcs:ignore
            $serviceCategory = $this->getServiceCategoryRepository()->fetchOne($serviceCategoryId);

            $authenticationStatus = $this->getBuyerMemberAccount()->getMember()->getEnterpriseStatus();
            $isEnterpriseVerify = $serviceCategory->getIsEnterpriseVerify();

            if ($authenticationStatus != Member::AUTHENTICATION_STATUS['APPROVE']
                && $isEnterpriseVerify == ServiceCategory::IS_ENTERPRISE_VERIFY['YES']
            ) {
                Core::setLastError(USER_NOT_ENTERPRISE_AUTHENTICATION);
                return false;
            }
        }

        return true;
    }

    private function isCanUseCoupon() : bool
    {
        if (!$this->isSuperposition()) {
            return false;
        }

        if (!$this->isUsed()) {
            return false;
        }

        if (!$this->isExpired()) {
            return false;
        }

        if (!$this->isMeetConcession()) {
            return false;
        }
        
        if (!$this->isApplyScope()) {
            return false;
        }

        if ($this->getPaidAmount() < self::ORDER_AMOUNT_LOWER_LIMINT) {
            Core::setLastError(ORDER_PAIND_AMOUNT_INCORRECT);
            return false;
        }
        
        return true;
    }

    private function isSuperposition() : bool
    {
        $memberCoupons = $this->getMemberCoupons();
        $count = count($memberCoupons);

        if ($count > self::MEMBER_COUPON_USE_MAX_COUNT) {
            Core::setLastError(MEMBER_COUPON_MORE_THAN_SPECIFIED);
            return false;
        }

        if ($count > self::MEMBER_COUPON_NOT_SUPERPOSITION_COUNT) {
            $isSuperpositions = array();
            foreach ($memberCoupons as $memberCoupon) {
                $isSuperpositions[] = $memberCoupon->getReleaseCoupon()->getIsSuperposition();
            }

            if (in_array(ReleaseCoupon::IS_SUPERPOSITION['NO'], $isSuperpositions)) {
                Core::setLastError(MEMBER_COUPON_NOT_SUPERPOSITION_USE);
                return false;
            }
        }

        return true;
    }

    private function isUsed() : bool
    {
        foreach ($this->getMemberCoupons() as $memberCoupon) {
            if ($memberCoupon->getStatus() != MemberCoupon::STATUS['NORMAL']) {
                Core::setLastError(MEMBER_COUPON_STATUS_NOT_UNUSER);
                return false;
            }
        }

        return true;
    }

    private function isExpired() : bool
    {
        foreach ($this->getMemberCoupons() as $memberCoupon) {
            $validityStartTime = $memberCoupon->getReleaseCoupon()->getValidityStartTime();
            $validityEndTime = $memberCoupon->getReleaseCoupon()->getValidityEndTime();
            $currentTime = Core::$container->get('time');

            if ($validityStartTime > $currentTime) {
                Core::setLastError(MEMBER_COUPON_NOT_IN_USE_PERIOD);
                return false;
            }

            if ($validityEndTime!= 0 && $validityEndTime < $currentTime) {
                Core::setLastError(MEMBER_COUPON_NOT_IN_USE_PERIOD);
                return false;
            }
        }

        return true;
    }

    private function isMeetConcession() : bool
    {
        foreach ($this->getMemberCoupons() as $memberCoupon) {
            if ($this->getTotalPrice() < $memberCoupon->getReleaseCoupon()->getUseStandard()) {
                Core::setLastError(MEMBER_COUPON_NOT_MEET_CONCESSION);
                return false;
            }
        }

        return true;
    }

    private function isApplyScope() : bool
    {
        foreach ($this->getMemberCoupons() as $memberCoupon) {
            if ($memberCoupon->getReleaseCoupon()->getApplyScope() == ReleaseCoupon::APPLY_SCOPE['STORE_CURRENCY']) {
                $sellerEnterpriseId = $this->getSellerEnterprise()->getId();

                if (!in_array($sellerEnterpriseId, $memberCoupon->getReleaseCoupon()->getApplySituation())) {
                    Core::setLastError(MEMBER_COUPON_NOT_APPLY_SCOPE);
                    return false;
                }
            }
        }

        return true;
    }

    private function bindMemberCoupons() : bool
    {
        if (!empty($this->getMemberCoupons())) {
            foreach ($this->getMemberCoupons() as $memberCoupon) {
                if (!$memberCoupon->addMemberCouponToOrder($this)) {
                    return false;
                }
            }
        }
      
        return true;
    }

    private function updateMemberCouponStatus($status) : bool
    {
        if (!empty($this->getMemberCoupons())) {
            foreach ($this->getMemberCoupons() as $memberCoupon) {
                if (!$memberCoupon->updateStatus($status)) {
                    return false;
                }
            }
        }

        return true;
    }

    private function calculateTotalPriceAndSellerEnterprise() : void
    {
        $totalPrice = 0;
        $sellerEnterpriseId = 0;

        $orderCommodities = $this->getOrderCommodities();

        foreach ($orderCommodities as $orderCommodity) {
            $priceList = $orderCommodity->getSnapshot()->getSnapshotObject()->getPrice()->getPrice();
            $unitPrice = $priceList[$orderCommodity->getSkuIndex()]['value'];
            $totalPrice+=$unitPrice*$orderCommodity->getNumber();
            $sellerEnterpriseId = $orderCommodity->getCommodity()->getEnterprise()->getId();
            //$sellerEnterpriseId = $orderCommodity->getSnapshot()->getSnapshotObject()->getEnterprise()->getId();
        }

        $sellerEnterprise = $this->getEnterpriseRepository()->fetchOne($sellerEnterpriseId);
        $this->setTotalPrice($totalPrice);
        $this->setSellerEnterprise($sellerEnterprise);
    }

    private function calculateAmount() : void
    {
        $totalPrice = $this->getTotalPrice();

        $businessPreferentialAmount = $this->calculateBusinessPreferentialAmount();
        $platformPreferentialAmount = $this->calculatePlatformPreferentialAmount();

        $paidAmount = $totalPrice-$businessPreferentialAmount-$platformPreferentialAmount;
        $paidAmount = $paidAmount <= 0 ? self::UPDATE_ORDER_AMOUNT_LOWER_LIMINT : $paidAmount;
        $collectedAmount = $paidAmount+$platformPreferentialAmount;

        $this->setPaidAmount($paidAmount);
        $this->setCollectedAmount($collectedAmount);
    }

    private function calculateBusinessPreferentialAmount($totalPrice = 0)
    {
        $totalPrice = !empty($totalPrice) ? $totalPrice : $this->getTotalPrice();

        $memberCoupons = $this->getMemberCoupons();
        $businessPreferentialAmount = 0;

        foreach ($memberCoupons as $memberCoupon) {
            if ($memberCoupon->getReleaseType() == ReleaseCoupon::RELEASE_TYPE['STORE']) {
                if ($memberCoupon->getReleaseCoupon()->getType() == ReleaseCoupon::TYPE['RULL_REDUCED']) {
                    $businessPreferentialAmount = $memberCoupon->getReleaseCoupon()->getDenomination();
                }
                if ($memberCoupon->getReleaseCoupon()->getType() == ReleaseCoupon::TYPE['DISCOUNT']) {
                    $discount = $memberCoupon->getReleaseCoupon()->getDiscount();
                    $businessPreferentialAmount = $totalPrice*((self::PROBAILITY-$discount)/100);
                }
            }
        }
        
        return $businessPreferentialAmount;
    }

    private function calculatePlatformPreferentialAmount($totalPrice = 0)
    {
        $totalPrice = !empty($totalPrice) ? $totalPrice : $this->getTotalPrice();

        $memberCoupons = $this->getMemberCoupons();
        $platformPreferentialAmount = 0;

        foreach ($memberCoupons as $memberCoupon) {
            if ($memberCoupon->getReleaseType() == ReleaseCoupon::RELEASE_TYPE['OA']) {
                if ($memberCoupon->getReleaseCoupon()->getType() == ReleaseCoupon::TYPE['RULL_REDUCED']) {
                    $platformPreferentialAmount = $memberCoupon->getReleaseCoupon()->getDenomination();
                }
                if ($memberCoupon->getReleaseCoupon()->getType() == ReleaseCoupon::TYPE['DISCOUNT']) {
                    $discount = $memberCoupon->getReleaseCoupon()->getDiscount();
                    $platformPreferentialAmount = $totalPrice*((self::PROBAILITY-$discount)/100);
                    $platformPreferentialAmount = $platformPreferentialAmount > self::MAX_COUPON_AMOUNT ?
                                                  self::MAX_COUPON_AMOUNT :
                                                  $platformPreferentialAmount;
                }
            }
        }
        
        return $platformPreferentialAmount;
    }

    private function addOrderCommodities() : bool
    {
        foreach ($this->getOrderCommodities() as $orderCommodity) {
            if (!$orderCommodity->add($this)) {
                return false;
            }
        }

        return true;
    }
    //付款成功
    protected function payAction() : bool
    {
        return $this->getRepository()->edit(
            $this,
            array(
                'paymentType',
                'paymentTime',
                'transactionNumber',
                'transactionInfo',
                'status',
                'statusTime',
                'updateTime',
                'timeRecord'
            )
        ) && $this->getPaymentStrategyFactory()->getStrategy($this->getPayment()->getType())->algorithm($this)
          && $this->updateMemberCouponStatus(MemberCoupon::STATUS['USED'])
          && $this->addBusinessNotice();
    }

    //付款失败
    public function paymentFailure() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(ORDER_STATUS_NOT_PENDING);
            return false;
        }

        $timeRecord = $this->getTimeRecord();
        $timeRecord[self::STATUS['PAY_FAILURE']] = Core::$container->get('time');

        $this->setTimeRecord($timeRecord);
        $this->setStatus(self::STATUS['PAY_FAILURE']);
        $this->setUpdateTime(Core::$container->get('time'));
        $this->setStatusTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this, array(
            'failureReason',
            'status',
            'updateTime',
            'statusTime',
            'timeRecord'
        ));
    }

    //买家确认
    public function buyerConfirmation() : bool
    {
        if (!$this->isPerformanceEnd()) {
            Core::setLastError(ORDER_STATUS_NOT_PERFORMANCE_END);
            return false;
        }

        return $this->updateStatus(self::STATUS['BUYER_CONFIRMATION'])
            && $this->updateServiceVolume()
            && $this->addTradeRecord()
            && $this->autoReleaseCoupon(
                $this->getBuyerMemberAccount()->getId(),
                ReleaseCoupon::USE_SCENARIO['ORDER_CONFIRMATION']
            )
            && $this->addBusinessNotice();
    }

    //平台自动确认
    public function platformConfirmation() : bool
    {
        if (!$this->isPerformanceEnd()) {
            Core::setLastError(ORDER_STATUS_NOT_PERFORMANCE_END);
            return false;
        }
        
        return $this->updateStatus(self::STATUS['PLATFORM_CONFIRMATION'])
            && $this->updateServiceVolume()
            && $this->addTradeRecord()
            && $this->autoReleaseCoupon(
                $this->getBuyerMemberAccount()->getId(),
                ReleaseCoupon::USE_SCENARIO['ORDER_CONFIRMATION']
            )
            && $this->addBusinessNotice();
    }

    protected function fetchTradeRecordStrategies() : array
    {
        return [
            new BuyerServiceOrderConfirmationRecordStrategy($this),
            new SellerServiceOrderConfirmationRecordStrategy($this),
            new CommissionServiceOrderConfirmationRecordStrategy($this),
            new SubsidyServiceOrderConfirmationRecordStrategy($this),
        ];
    }

    //商家履约开始
    public function performanceBegin() : bool
    {
        if (!$this->isPaid()) {
            Core::setLastError(ORDER_STATUS_NOT_PAID);
            return false;
        }

        if ($this->getSellerEnterprise()->getRealNameAuthenticationStatus() !=
            Enterprise::AUTHENTICATION_STATUS['APPROVE']
        ) {
            Core::setLastError(USER_NOT_ENTERPRISE_REAL_NAME_AUTHENTICATION);
            return false;
        }

        return $this->updateStatus(self::STATUS['PERFORMANCE_BEGIN'])
               && $this->addBusinessNotice();
    }

    //商家拒绝履约
    public function performanceRejectBegin(Refund $refund) : bool
    {
        if (!$this->isPaid()) {
            Core::setLastError(ORDER_STATUS_NOT_PAID);
            return false;
        }

        if ($this->getSellerEnterprise()->getRealNameAuthenticationStatus() !=
            Enterprise::AUTHENTICATION_STATUS['APPROVE']
        ) {
            Core::setLastError(USER_NOT_ENTERPRISE_REAL_NAME_AUTHENTICATION);
            return false;
        }

        return $this->addRefund($refund);
    }

    protected function addRefund(Refund $refund)
    {
        $refund->setMember($this->getBuyerMemberAccount()->getMember());
        $refund->setOrder($this);
        $refund->setEnterprise($this->getSellerEnterprise());
        $refund->setStatus(Refund::STATUS['AGREE']);
        $refund->setOrderStatus($this->getStatus());
        $refund->setOrderNumber($this->getOrderno());
        $refund->setOrderCategory($this->getCategory());
        $refund->setGoodsName($this->getOrderCommodities()[0]->getSnapshot()->getSnapshotObject()->getTitle());

        if ($refund->add()) {
            return true;
        }

        return false;
    }
    //商家履约结束
    public function performanceEnd() : bool
    {
        if (!$this->isPerformanceBegin()) {
            Core::setLastError(ORDER_STATUS_NOT_PERFORMANCE_BEGIN);
            return false;
        }

        return $this->updateStatus(self::STATUS['PERFORMANCE_END'])
               && $this->addBusinessNotice();
    }
    //买家申请退款
    public function refund() : bool
    {
        return $this->updateStatus(self::STATUS['REFUNDING']);
    }
    //商家拒绝退款
    public function refuse(int $status) : bool
    {
        return $this->updateStatus($status);
    }
    //平台确认打款
    public function confirmPayment() : bool
    {
        return $this->updateStatus(self::STATUS['REFUNDED'])
            && $this->addRefundTradeRecord();
    }

    protected function fetchRefundTradeRecordStrategies() : array
    {
        return [
            new PlatformServiceOrderRefundRecordStrategy($this),
            new BuyerServiceOrderRefundRecordStrategy($this)
        ];
    }

    protected function updateServiceVolume() : bool
    {
        $serviceIds = array();

        foreach ($this->getOrderCommodities() as $orderCommodity) {
            $serviceIds[] = $orderCommodity->getSnapshot()->getSnapshotObjectId();
        }

        $serviceIdsCount = array_count_values($serviceIds);
        $serviceIds = array_unique($serviceIds);

        return $this->updateVolume($serviceIdsCount, $serviceIds);
    }
    
    /**
     * 卖家改价
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function updateOrderAmount() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(ORDER_STATUS_NOT_PENDING);
            return false;
        }

        if (!$this->isCanUpdateOrderAmount()) {
            Core::setLastError(UPDATE_ORDER_AMOUNT_OUT_OF_RANGE);
            return false;
        }

        $memberCoupons = array();

        foreach ($this->getMemberCoupons() as $memberCoupon) {
            if ($this->getPaidAmount() < $memberCoupon->getReleaseCoupon()->getUseStandard()) {
                if (!$memberCoupon->deleteMemberCouponToOrder($this)) {
                    return false;
                }
                if (!$memberCoupon->updateStatus(MemberCoupon::STATUS['NORMAL'])) {
                    return false;
                }
            }

            if ($this->getPaidAmount() >= $memberCoupon->getReleaseCoupon()->getUseStandard()) {
                $memberCoupons[] = $memberCoupon;
            }
        }

        $this->clearMemberCoupons();

        $totalPrice = $this->getPaidAmount();

        if (!empty($memberCoupons)) {
            foreach ($memberCoupons as $memberCoupon) {
                $this->addMemberCoupon($memberCoupon);
            }
        }

        $businessPreferentialAmount = $this->calculateBusinessPreferentialAmount($totalPrice);
        $platformPreferentialAmount = $this->calculatePlatformPreferentialAmount($totalPrice);

        $paidAmount = $totalPrice-$businessPreferentialAmount-$platformPreferentialAmount;
        $paidAmount = $paidAmount <= 0 ? self::UPDATE_ORDER_AMOUNT_LOWER_LIMINT : $paidAmount;
        $collectedAmount = $paidAmount+$platformPreferentialAmount;

        $this->setPaidAmount($paidAmount);
        $this->setCollectedAmount($collectedAmount);
        $this->setUpdateTime(Core::$container->get('time'));

        // 改价后重新计算扣除佣金和补贴的实际金额
        $realAmountInfo = $this->calculateCommissionAndSubsidy(
            $collectedAmount,
            $this->getCommissionProportion(),
            $this->getSubsidyProportion()
        );
        $this->setRealAmount($realAmountInfo['amount']);

        return $this->getRepository()->edit(
            $this,
            array(
                'paidAmount',
                'collectedAmount',
                'realAmount',
                'updateTime'
            )
        ) && $this->addUpdateAmountRecord($totalPrice, $this->getRemark());
    }

    private function isCanUpdateOrderAmount() : bool
    {
        return $this->getPaidAmount() >= self::UPDATE_ORDER_AMOUNT_LOWER_LIMINT;
    }

    protected function addUpdateAmountRecord(float $amount, string $remark) : bool
    {
        $orderUpdateAmountRecord = new OrderUpdateAmountRecord();
        $orderUpdateAmountRecord->setAmount($amount);
        $orderUpdateAmountRecord->setRemark($remark);

        return $orderUpdateAmountRecord->add($this);
    }

    public function updateOrderAddress() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(ORDER_STATUS_NOT_PENDING);
            return false;
        }

        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this, array('updateTime')) &&
               $this->getOrderAddress()->edit();
    }

    public function buyerCancel() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(ORDER_STATUS_NOT_PENDING);
            return false;
        }

        return $this->updateStatus(self::STATUS['BUYER_CANCEL_ORDER'])
            && $this->updateMemberCouponStatus(MemberCoupon::STATUS['NORMAL']);
    }

    public function sellerCancel() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(ORDER_STATUS_NOT_PENDING);
            return false;
        }

        return $this->updateStatus(self::STATUS['SELLER_CANCEL_ORDER'])
            && $this->updateMemberCouponStatus(MemberCoupon::STATUS['NORMAL']);
    }

    public function platformCancel() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(ORDER_STATUS_NOT_PENDING);
            return false;
        }

        return $this->updateStatus(self::STATUS['PLATFORM_CANCEL_ORDER'])
            && $this->updateMemberCouponStatus(MemberCoupon::STATUS['NORMAL']);
    }

    public function orderCompletion() : bool
    {
        return $this->updateStatus(self::STATUS['ORDER_COMPLETION']);
    }

    public function fetchCommissionAndSubsidy() : array
    {
        // 获取订单（没有佣金和补贴）商家应该得到的价钱
        $realAmount = $this->getCollectedAmount();
        $commission = $subsidy = 0;

        // 必须是服务订单
        if ($this->getCategory() == IOrderTradeAble::ORDER_CATEGORY['SERVICE']) {
            // 1.获取卖家企业id
            $enterpriseId = $this->getSellerEnterprise()->getId();
            // 2.获取订单信息
            $orderCommodity = $this->getOrderCommodities();
            /**@var OrderCommodity $realOrderCommodity*/
            $realOrderCommodity = $orderCommodity[0];
            // 3.获取服务
            /**@var Service $service*/
            $service = $realOrderCommodity->getCommodity();
            // 4.获取服务类型
            $serviceCategory = $service->getServiceCategory()->getId();
            // 5.获取服务商
            $filter = array(
                'enterprise' => $enterpriseId,
                'applyStatus' => Authentication::APPLY_STATUS['APPROVE'],
                'serviceCategory' => $serviceCategory
            );
            $realServiceProvider = $this->getOrderServiceProvider($filter);

            $selfSetType = false;
            // 6. 佣金补贴比例
            if ($realServiceProvider->getStrategyType()->getId() > 0) {
                $strategyType = $this->getStrategyTypeRepository()->fetchOne(
                    $realServiceProvider->getStrategyType()->getId()
                );

                if (!$strategyType instanceof NullType && $strategyType->isEnabled()) {
                    $commission = $strategyType->getCommission();
                    $subsidy = $strategyType->getSubsidy();
                    // 因为不知道佣金和比例是否被设置了0 所以需要标识来区分0是被设置的还是初始化的值
                    $selfSetType = true;
                }
            }
            // 服务商验证失败 或者 服务商未关联有效的策略，均执行系统默认的策略
            if (!$this->validateServiceProviderStrategy($realServiceProvider) || !$selfSetType) {
                /**@var BasicConfig $basicConfig*/
                $basicConfig = $realServiceProvider->getStrategyType()->getBasicConfig();
                $commission = $basicConfig->getCommission();
                $subsidy = $basicConfig->getSubsidy();
            }
        }

        return $this->calculateCommissionAndSubsidy($realAmount, $commission, $subsidy);
    }

    protected function getServiceProviderRepository() : IAuthenticationAdapter
    {
        return new AuthenticationRepository();
    }

    protected function getStrategyTypeRepository() : ITypeAdapter
    {
        return new TypeRepository();
    }

    protected function getOrderServiceProvider(array $serviceProviderFilter = array()) : Authentication
    {
        $realServiceProvider = new NullAuthentication();

        list($serviceProviderList, $count) = $this->getServiceProviderRepository()->filter($serviceProviderFilter);
        unset($count);
        // 此处查出来是一个数组，虽然可以直接取第一条数据，但是为了代码的健壮性，循环获取企业id与订单一致的信息即可
        /**@var Authentication $serviceProvider*/
        foreach ($serviceProviderList as $serviceProvider) {
            if ($serviceProvider->getEnterprise()->getId() == $this->getSellerEnterprise()->getId()) {
                $realServiceProvider = $serviceProvider;
            }
        }

        return  $realServiceProvider;
    }

    protected function validateServiceProviderStrategy(Authentication $serviceProvider) : bool
    {
        if ($serviceProvider instanceof NullAuthentication) {
            return false;
        }

        return true;
    }

    protected function calculateCommissionAndSubsidy($collectedAmount, $commission = 0, $subsidy = 0): array
    {
        // 默认佣金和补贴为0
        $subCommission = $addSubsidy = 0;

        // 10.计算扣除的佣金
        if (bccomp($commission, '0', 2) > 0) {
            $subCommission = bcmul($collectedAmount, bcdiv($commission, 100, 2), 2);
        }
        // 11.计算增加的补贴
        if (bccomp($subsidy, '0', 2) > 0) {
            $addSubsidy = bcmul($collectedAmount, bcdiv($subsidy, 100, 2), 2);
        }
        // 13.计算最终的实收金额
        $realAmount = bcadd(bcsub($collectedAmount, $subCommission, 2), $addSubsidy, 2);

        return array(
            'commission' => $subCommission,
            'subsidy' => $addSubsidy,
            'amount' => $realAmount,
            'commissionProportion' => $commission,
            'subsidyProportion' => $subsidy,
        );
    }

    //创建订单时初次计算佣金和补贴
    public function setCommissionAndSubsidy() : void
    {
        $commissionAndSubsidy = $this->fetchCommissionAndSubsidy();
        $this->setCommission($commissionAndSubsidy['commissionProportion']);
        $this->setSubsidy($commissionAndSubsidy['subsidyProportion']);
        $this->setRealAmount($commissionAndSubsidy['amount']);
    }
}
