<?php
namespace Market\Service\ServiceOrder\Repository\ServiceOrder;

use Market\Service\ServiceOrder\Model\ServiceOrder;
use Market\Service\ServiceOrder\Adapter\ServiceOrder\IServiceOrderAdapter;
use Market\Service\ServiceOrder\Adapter\ServiceOrder\ServiceOrderDBAdapter;
use Market\Service\ServiceOrder\Adapter\ServiceOrder\ServiceOrderMockAdapter;

use Evaluation\Evaluation\Adapter\Evaluation\IEvaluationCommonAdapter;

use Marmot\Framework\Classes\Repository;

class ServiceOrderRepository extends Repository implements IServiceOrderAdapter,IEvaluationCommonAdapter
{
    private $adapter;
    
    public function __construct()
    {
        $this->adapter = new ServiceOrderDBAdapter();
    }

    public function setAdapter(IServiceOrderAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    protected function getActualAdapter() : IServiceOrderAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IServiceOrderAdapter
    {
        return new ServiceOrderMockAdapter();
    }

    public function add(ServiceOrder $serviceOrder, array $keys = array()) : bool
    {
        return $this->getAdapter()->add($serviceOrder, $keys);
    }

    public function edit(ServiceOrder $serviceOrder, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($serviceOrder, $keys);
    }

    public function fetchOne($id) : ServiceOrder
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
}
