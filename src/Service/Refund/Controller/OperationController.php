<?php
namespace Market\Service\Refund\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IOperatAbleController;

use Market\Service\Refund\Model\Refund;
use Market\Service\Refund\View\RefundView;
use Market\Service\Refund\Repository\Refund\RefundRepository;
use Market\Service\Refund\Command\Refund\AddRefundCommand;
use Market\Service\Refund\Command\Refund\AgreeRefundCommand;
use Market\Service\Refund\Command\Refund\RefuseRefundCommand;
use Market\Service\Refund\Command\Refund\ConfirmPaymentRefundCommand;
use Market\Service\Refund\CommandHandler\Refund\RefundCommandHandlerFactory;

class OperationController extends Controller implements IOperatAbleController
{
    use JsonApiTrait, RefundValidateTrait;

    private $repository;

    private $commandBus;
    
    public function __construct()
    {
        parent::__construct();
        $this->repository = new RefundRepository();
        $this->commandBus = new CommandBus(new RefundCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : RefundRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    /**
     * 对应路由 /refunds
     * 申请退款功能, 通过POST传参
     * @param jsonApi
     * @return jsonApi
     */
    public function add()
    {
        $data = $this->getRequest()->post('data');

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];
        
        $refundReason = $attributes['refundReason'];
        $orderId = $relationships['order']['data'][0]['id'];
        $memberId = $relationships['member']['data'][0]['id'];
        $enterpriseId = $relationships['enterprise']['data'][0]['id'];

        if ($this->validateAddScenario(
            $refundReason,
            $orderId,
            $memberId,
            $enterpriseId
        )) {
            $command = new AddRefundCommand(
                $refundReason,
                $orderId,
                $memberId,
                $enterpriseId
            );

            if ($this->getCommandBus()->send($command)) {
                $refund = $this->getRepository()->fetchOne($command->id);
                if ($refund instanceof Refund) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new RefundView($refund));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    public function edit(int $id)
    {
        unset($id);
        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /refunds/{id}/agree
     * 同意退款功能, 通过PATCH传参
     * @param jsonApi
     * @return jsonApi
     */
    public function agree(int $id)
    {
        if (!empty($id)) {
            $command = new AgreeRefundCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $refund = $this->getRepository()->fetchOne($command->id);
                if ($refund instanceof Refund) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new RefundView($refund));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /refunds/{id}/refuse
     * 拒绝退款功能, 通过PATCH传参
     * @param jsonApi
     * @return jsonApi
     */
    public function refuse(int $id)
    {
        $data = $this->getRequest()->patch('data');

        $attributes = $data['attributes'];
        
        $refuseReason = $attributes['refuseReason'];

        if ($this->validateRefuseScenario($refuseReason)) {
            $command = new RefuseRefundCommand(
                $refuseReason,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $refund = $this->getRepository()->fetchOne($command->id);
                if ($refund instanceof Refund) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new RefundView($refund));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /refunds/{id}/confirmPayment
     * 确认打款功能, 通过PATCH传参
     * @param jsonApi
     * @return jsonApi
     */
    public function confirmPayment(int $id)
    {
        $data = $this->getRequest()->patch('data');

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];
        
        $voucher = $attributes['voucher'];
        $crewId = $relationships['crew']['data'][0]['id'];

        if ($this->validateConfirmPaymentScenario(
            $voucher,
            $crewId
        )) {
            $command = new ConfirmPaymentRefundCommand(
                $voucher,
                $crewId,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $refund = $this->getRepository()->fetchOne($command->id);
                if ($refund instanceof Refund) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new RefundView($refund));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}
