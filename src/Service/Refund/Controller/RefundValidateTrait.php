<?php
namespace Market\Service\Refund\Controller;

use WidgetRules\Common\WidgetRules;
use WidgetRules\Refund\WidgetRules as RefundWidgetRules;

trait RefundValidateTrait
{
    protected function getRefundWidgetRules() : RefundWidgetRules
    {
        return RefundWidgetRules::getInstance();
    }

    protected function getWidgetRules() : WidgetRules
    {
        return WidgetRules::getInstance();
    }

    protected function validateAddScenario(
        $refundReason,
        $orderId,
        $memberId,
        $enterpriseId
    ) : bool {
        return $this->getRefundWidgetRules()->reason($refundReason, 'refundReason')
            && $this->getWidgetRules()->formatNumeric($orderId, 'orderId')
            && $this->getWidgetRules()->formatNumeric($memberId, 'memberId')
            && $this->getWidgetRules()->formatNumeric($enterpriseId, 'enterpriseId');
    }

    protected function validateRefuseScenario($refuseReason)
    {
        return $this->getRefundWidgetRules()->reason($refuseReason, 'refuseReason');
    }

    protected function validateConfirmPaymentScenario(
        $voucher,
        $crewId
    ) {
        return $this->getWidgetRules()->image($voucher, 'voucher')
            && $this->getWidgetRules()->formatNumeric($crewId, 'crewId');
    }
}
