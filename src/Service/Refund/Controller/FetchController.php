<?php
namespace Market\Service\Refund\Controller;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IFetchAbleController;

use Market\Service\Refund\View\RefundView;
use Market\Service\Refund\Adapter\Refund\IRefundAdapter;
use Market\Service\Refund\Repository\Refund\RefundRepository;

class FetchController extends Controller implements IFetchAbleController
{
    use JsonApiTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new RefundRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IRefundAdapter
    {
        return $this->repository;
    }

    public function fetchOne(int $id)
    {
        $refund = $this->getRepository()->fetchOne($id);

        if (!$refund instanceof INull) {
            $this->renderView(new RefundView($refund));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function fetchList(string $ids)
    {
        $ids = explode(',', $ids);

        $refunds = array();

        $refunds = $this->getRepository()->fetchList($ids);

        if (!empty($refunds)) {
            $this->renderView(new RefundView($refunds));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function filter()
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();

        list($refundList, $count) = $this->getRepository()->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = new RefundView($refundList);
            $view->pagination(
                'productMarketRefunds',
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
