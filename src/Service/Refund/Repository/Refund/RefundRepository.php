<?php
namespace Market\Service\Refund\Repository\Refund;

use Marmot\Framework\Classes\Repository;

use Common\Repository\OperatAbleRepositoryTrait;

use Market\Service\Refund\Model\Refund;
use Market\Service\Refund\Adapter\Refund\IRefundAdapter;
use Market\Service\Refund\Adapter\Refund\RefundDBAdapter;
use Market\Service\Refund\Adapter\Refund\RefundMockAdapter;

class RefundRepository extends Repository implements IRefundAdapter
{
    use OperatAbleRepositoryTrait;

    private $adapter;

    public function __construct()
    {
        $this->adapter = new RefundDBAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IRefundAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    protected function getActualAdapter() : IRefundAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IRefundAdapter
    {
        return new RefundMockAdapter();
    }

    public function fetchOne($id) : Refund
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
}
