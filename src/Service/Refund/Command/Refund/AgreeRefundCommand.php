<?php
namespace Market\Service\Refund\Command\Refund;

use Marmot\Interfaces\ICommand;

class AgreeRefundCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id = 0
    ) {
        $this->id = $id;
    }
}
