<?php
namespace Market\Service\Refund\Command\Refund;

use Marmot\Interfaces\ICommand;

class AddRefundCommand implements ICommand
{
    public $refundReason;

    public $orderId;

    public $memberId;

    public $enterpriseId;

    public $id;

    public function __construct(
        string $refundReason,
        int $orderId = 0,
        int $memberId = 0,
        int $enterpriseId = 0,
        int $id = 0
    ) {
        $this->refundReason = $refundReason;
        $this->orderId = $orderId;
        $this->memberId = $memberId;
        $this->enterpriseId = $enterpriseId;
        $this->id = $id;
    }
}
