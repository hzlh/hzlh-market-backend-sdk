<?php
namespace Market\Service\Refund\Command\Refund;

use Marmot\Interfaces\ICommand;

class RefuseRefundCommand implements ICommand
{
    public $refuseReason;

    public $id;

    public function __construct(
        string $refuseReason,
        int $id = 0
    ) {
        $this->refuseReason = $refuseReason;
        $this->id = $id;
    }
}
