<?php
namespace Market\Service\Refund\Command\Refund;

use Marmot\Interfaces\ICommand;

class ConfirmPaymentRefundCommand implements ICommand
{
    public $voucher;

    public $crewId;

    public $id;

    public function __construct(
        array $voucher,
        int $crewId = 0,
        int $id = 0
    ) {
        $this->voucher = $voucher;
        $this->crewId = $crewId;
        $this->id = $id;
    }
}
