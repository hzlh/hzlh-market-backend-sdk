<?php
namespace Market\Service\Refund\Adapter\Refund;

use Marmot\Core;

use Common\Adapter\FetchRestfulAdapterTrait;
use Common\Adapter\OperatAbleRestfulAdapterTrait;

use Market\Service\Refund\Model\Refund;
use Market\Service\Refund\Model\NullRefund;
use Market\Service\Refund\Translator\RefundDBTranslator;
use Market\Service\Refund\Adapter\Refund\Query\RefundRowCacheQuery;

use Backend\Crew\Repository\Crew\CrewRepository;
use Member\Member\Repository\Member\MemberRepository;
use Member\Enterprise\Repository\Enterprise\EnterpriseRepository;
use Market\Service\ServiceOrder\Repository\ServiceOrder\ServiceOrderRepository;

use Backend\Crew\Model\Crew;
use Member\Member\Model\Member;
use Member\Enterprise\Model\Enterprise;
use Market\Service\ServiceOrder\Model\ServiceOrder;

/**
 * @SuppressWarnings(PHPMD)
 */
class RefundDBAdapter implements IRefundAdapter
{
    use OperatAbleRestfulAdapterTrait, FetchRestfulAdapterTrait;

    private $translator;

    private $rowCacheQuery;

    private $enterpriseRepository;

    private $serviceOrderRepository;

    private $memberRepository;

    private $crewRepository;

    public function __construct()
    {
        $this->translator = new RefundDBTranslator();
        $this->rowCacheQuery = new RefundRowCacheQuery();
        $this->enterpriseRepository = new EnterpriseRepository();
        $this->serviceOrderRepository = new ServiceOrderRepository();
        $this->memberRepository = new MemberRepository();
        $this->crewRepository = new CrewRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowCacheQuery);
        unset($this->enterpriseRepository);
        unset($this->serviceOrderRepository);
        unset($this->memberRepository);
        unset($this->crewRepository);
    }

    protected function getDBTranslator()
    {
        return $this->translator;
    }

    protected function getRowCacheQuery()
    {
        return $this->rowCacheQuery;
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }

    protected function fetchEnterprise(int $id) : Enterprise
    {
        return $this->getEnterpriseRepository()->fetchOne($id);
    }

    protected function getServiceOrderRepository() : ServiceOrderRepository
    {
        return $this->serviceOrderRepository;
    }

    protected function fetchServiceOrder(int $id) : ServiceOrder
    {
        return $this->getServiceOrderRepository()->fetchOne($id);
    }

    protected function getMemberRepository() : MemberRepository
    {
        return $this->memberRepository;
    }

    protected function fetchMember(int $id) : Member
    {
        return $this->getMemberRepository()->fetchOne($id);
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function fetchCrew(int $id) : Crew
    {
        return $this->getCrewRepository()->fetchOne($id);
    }

    public function fetchOne($id) : Refund
    {
        $info = array();

        $info = $this->getRowCacheQuery()->getOne($id);

        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullRefund::getInstance();
        }

        $refund = $this->getDBTranslator()->arrayToObject($info);

        $member = $this->fetchMember($refund->getMember()->getId());
        if (!empty($member)) {
            $refund->setMember($member);
        }

        $enterprise = $this->fetchEnterprise($refund->getEnterprise()->getId());
        if (!empty($enterprise)) {
            $refund->setEnterprise($enterprise);
        }

        $serviceOrder = $this->fetchServiceOrder($refund->getOrder()->getId());
        if (!empty($serviceOrder)) {
            $refund->setOrder($serviceOrder);
        }

        $crewId = $refund->getCrew()->getId();
        if (!empty($crewId)) {
            $crew = $this->fetchCrew($crewId);
            if (!empty($crew)) {
                $refund->setCrew($crew);
            }
        }

        return $refund;
    }

    public function fetchList(array $ids) : array
    {
        $refundList = array();
        $enterpriseIds = array();
        $memberIds = array();
        $orderIds = array();
        $crewIds = array();
        
        $refundInfoList = $this->getRowCacheQuery()->getList($ids);

        if (empty($refundInfoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $translator = $this->getDBTranslator();
        foreach ($refundInfoList as $refundInfo) {
            $refund = $translator->arrayToObject($refundInfo);

            $enterpriseIds[$refund->getId()] = $refund->getEnterprise()->getId();
            $memberIds[$refund->getId()] = $refund->getMember()->getId();
            $orderIds[$refund->getId()] = $refund->getOrder()->getId();
            $crewIds[$refund->getId()] = $refund->getCrew()->getId();

            $refundList[] = $refund;
        }
        //获取member
        $member = $this->getMemberRepository()->fetchList($memberIds);
        if (!empty($member)) {
            foreach ($refundList as $key => $refund) {
                if (isset($member[$key])) {
                    $refund->setMember($member[$key]);
                }
            }
        }
        //获取enterprise
        $enterprise = $this->getEnterpriseRepository()->fetchList($enterpriseIds);
        if (!empty($enterprise)) {
            foreach ($refundList as $key => $refund) {
                if (isset($enterprise[$key])) {
                    $refund->setEnterprise($enterprise[$key]);
                }
            }
        }
        //获取order
        $order = $this->getServiceOrderRepository()->fetchList($orderIds);
        if (!empty($order)) {
            foreach ($refundList as $key => $refund) {
                if (isset($order[$key])) {
                    $refund->setOrder($order[$key]);
                }
            }
        }
        //获取crew
        $crew = $this->getCrewRepository()->fetchList($crewIds);
        if (!empty($crew)) {
            foreach ($refundList as $key => $refund) {
                if (isset($crew[$key])) {
                    $refund->setCrew($crew[$key]);
                }
            }
        }

        return $refundList;
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $refund = new Refund();

            if (isset($filter['status'])) {
                $status = $filter['status'];
                if (is_numeric($status)) {
                    $refund->setStatus($filter['status']);
                    $info = $this->getDBTranslator()->objectToArray(
                        $refund,
                        array('status')
                    );
                    $condition .= $conjection.key($info).' = '.current($info);
                }
                if (strpos($status, ',')) {
                    $info = $this->getDBTranslator()->objectToArray(
                        $refund,
                        array('status')
                    );
                    $condition .= $conjection.key($info).' IN ('.$status.')';
                }
                $conjection = ' AND ';
            }
            if (isset($filter['enterprise'])) {
                $refund->getEnterprise()->setId($filter['enterprise']);
                $info = $this->getDBTranslator()->objectToArray($refund, array('enterprise'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['member'])) {
                $refund->getMember()->setId($filter['member']);
                $info = $this->getDBTranslator()->objectToArray($refund, array('member'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['order'])) {
                $refund->getOrder()->setId($filter['order']);
                $info = $this->getDBTranslator()->objectToArray($refund, array('order'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['orderNumber'])) {
                $refund->setOrderNumber($filter['orderNumber']);
                $info = $this->getDBTranslator()->objectToArray($refund, array('orderNumber'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['orderCategory'])) {
                $refund->setOrderCategory($filter['orderCategory']);
                $info = $this->getDBTranslator()->objectToArray($refund, array('orderCategory'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['goodsName']) && !empty($filter['goodsName'])) {
                $refund->setGoodsName($filter['goodsName']);
                $info = $this->getDBTranslator()->objectToArray($refund, array('goodsName'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['createTime'])) {
                $refund->setCreateTime($filter['createTime']);
                $info = $this->getDBTranslator()->objectToArray($refund, array('createTime'));
                $condition .= $conjection.'FROM_UNIXTIME('.key($info).', "%Y-%m-%d") = "'.date("Y-m-d", $filter['createTime']).'"'; //phpcs:ignore
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $refund = new Refund();
            foreach ($sort as $key => $val) {
                if ($key == 'updateTime') {
                    $info = $this->getDBTranslator()->objectToArray($refund, array('updateTime'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'id') {
                    $info = $this->getDBTranslator()->objectToArray($refund, array('id'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
            }
        }

        return $condition;
    }
}
