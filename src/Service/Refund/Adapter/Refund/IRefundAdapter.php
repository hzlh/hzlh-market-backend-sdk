<?php
namespace Market\Service\Refund\Adapter\Refund;

use Market\Service\Refund\Model\Refund;

use Common\Adapter\IOperatAbleAdapter;

interface IRefundAdapter extends IOperatAbleAdapter
{
    public function fetchOne($id) : Refund;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;
}
