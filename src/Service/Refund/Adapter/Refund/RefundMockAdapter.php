<?php
namespace Market\Service\Refund\Adapter\Refund;

use Market\Service\Refund\Model\Refund;
use Market\Service\Refund\Utils\MockFactory;

use Common\Model\IOperatAble;

class RefundMockAdapter implements IRefundAdapter
{
    public function add(IOperatAble $refund, array $keys = array()) : bool
    {
        unset($refund);
        unset($keys);

        return true;
    }

    public function edit(IOperatAble $refund, array $keys = array()) : bool
    {
        unset($refund);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : Refund
    {
        return MockFactory::generateRefund($id);
    }

    public function fetchList(array $ids) : array
    {
        $refundList = array();

        foreach ($ids as $id) {
            $refundList[] = MockFactory::generateRefund($id);
        }

        return $refundList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }
}
