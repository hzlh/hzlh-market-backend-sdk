<?php
namespace Market\Service\Refund\Adapter\Refund\Query;

use Marmot\Framework\Query\RowCacheQuery;

class RefundRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'refund_id',
            new Persistence\RefundCache(),
            new Persistence\RefundDb()
        );
    }
}
