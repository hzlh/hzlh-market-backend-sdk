<?php
namespace Market\Service\Refund\Adapter\Refund\Query\Persistence;

use Marmot\Framework\Classes\Db;

class RefundDb extends Db
{
    public function __construct()
    {
        parent::__construct('market_refund');
    }
}
