<?php
namespace Market\Service\Refund\Adapter\Refund\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class RefundCache extends Cache
{
    /**
     * 构造函数初始化key和表名一致
     */
    public function __construct()
    {
        parent::__construct('market_refund');
    }
}
