<?php
namespace Market\Service\Refund\Translator;

use Marmot\Interfaces\ITranslator;

use Market\Service\Refund\Model\Refund;
use Market\Service\Refund\Model\NullRefund;

class RefundDBTranslator implements ITranslator
{
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function arrayToObject(array $expression, $refund = null)
    {
        if (!isset($expression['refund_id'])) {
            return NullRefund::getInstance();
        }

        if ($refund == null) {
            $refund = new Refund($expression['refund_id']);
        }

        if (isset($expression['order_id'])) {
            $refund->getOrder()->setId($expression['order_id']);
        }
        if (isset($expression['member_id'])) {
            $refund->getMember()->setId($expression['member_id']);
        }
        if (isset($expression['enterprise_id'])) {
            $refund->getEnterprise()->setId($expression['enterprise_id']);
        }
        if (isset($expression['crew_id'])) {
            $refund->getCrew()->setId($expression['crew_id']);
        }
        if (isset($expression['refund_reason'])) {
            $refund->setRefundReason($expression['refund_reason']);
        }
        if (isset($expression['refuse_reason'])) {
            $refund->setRefuseReason($expression['refuse_reason']);
        }
        if (isset($expression['order_status'])) {
            $refund->setOrderStatus($expression['order_status']);
        }
        if (isset($expression['order_number'])) {
            $refund->setOrderNumber($expression['order_number']);
        }
        if (isset($expression['order_category'])) {
            $refund->setOrderCategory($expression['order_category']);
        }
        if (isset($expression['goods_name'])) {
            $refund->setGoodsName($expression['goods_name']);
        }
        $timeRecord = array();
        if (is_string($expression['time_record'])) {
            $timeRecord = json_decode($expression['time_record'], true);
        }
        if (is_array($expression['time_record'])) {
            $timeRecord = $expression['time_record'];
        }
        $refund->setTimeRecord($timeRecord);
        $voucher = array();
        if (is_string($expression['voucher'])) {
            $voucher = json_decode($expression['voucher'], true);
        }
        if (is_array($expression['voucher'])) {
            $voucher = $expression['voucher'];
        }
        $refund->setVoucher($voucher);
        if (isset($expression['status'])) {
            $refund->setStatus($expression['status']);
        }
        if (isset($expression['create_time'])) {
            $refund->setCreateTime($expression['create_time']);
        }
        if (isset($expression['update_time'])) {
            $refund->setUpdateTime($expression['update_time']);
        }
        if (isset($expression['status_time'])) {
            $refund->setStatusTime($expression['status_time']);
        }

        return $refund;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($refund, array $keys = array())
    {
        if (!$refund instanceof Refund) {
            return false;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'refundReason',
                'refuseReason',
                'voucher',
                'crew',
                'order',
                'member',
                'enterprise',
                'orderStatus',
                'orderNumber',
                'orderCategory',
                'goodsName',
                'timeRecord',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['refund_id'] = $refund->getId();
        }
        if (in_array('refundReason', $keys)) {
            $expression['refund_reason'] = $refund->getRefundReason();
        }
        if (in_array('refuseReason', $keys)) {
            $expression['refuse_reason'] = $refund->getRefuseReason();
        }
        if (in_array('voucher', $keys)) {
            $expression['voucher'] = $refund->getVoucher();
        }
        if (in_array('crew', $keys)) {
            $expression['crew_id'] = $refund->getCrew()->getId();
        }
        if (in_array('order', $keys)) {
            $expression['order_id'] = $refund->getOrder()->getId();
        }
        if (in_array('member', $keys)) {
            $expression['member_id'] = $refund->getMember()->getId();
        }
        if (in_array('enterprise', $keys)) {
            $expression['enterprise_id'] = $refund->getEnterprise()->getId();
        }
        if (in_array('orderStatus', $keys)) {
            $expression['order_status'] = $refund->getOrderStatus();
        }
        if (in_array('orderNumber', $keys)) {
            $expression['order_number'] = $refund->getOrderNumber();
        }
        if (in_array('orderCategory', $keys)) {
            $expression['order_category'] = $refund->getOrderCategory();
        }
        if (in_array('goodsName', $keys)) {
            $expression['goods_name'] = $refund->getGoodsName();
        }
        if (in_array('timeRecord', $keys)) {
            $expression['time_record'] = $refund->getTimeRecord();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $refund->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $refund->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $refund->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $refund->getStatusTime();
        }

        return $expression;
    }
}
