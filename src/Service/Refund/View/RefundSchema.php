<?php
namespace Market\Service\Refund\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class RefundSchema extends SchemaProvider
{
    protected $resourceType = 'productMarketRefunds';

    public function getId($refund) : int
    {
        return $refund->getId();
    }

    public function getAttributes($refund) : array
    {
        return [
            'refundReason' => $refund->getRefundReason(),
            'refuseReason' =>$refund->getRefuseReason(),
            'voucher' =>$refund->getVoucher(),
            'orderNumber' =>$refund->getOrderNumber(),
            'orderCategory' =>$refund->getOrderCategory(),
            'goodsName' =>$refund->getGoodsName(),
            'timeRecord' =>$refund->getTimeRecord(),
            'status' => $refund->getStatus(),
            'createTime' => $refund->getCreateTime(),
            'updateTime' => $refund->getUpdateTime(),
            'statusTime' => $refund->getStatusTime()
        ];
    }

    public function getRelationships($refund, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        return [
            'crew' => [self::DATA => $refund->getCrew()],
            'order' => [self::DATA => $refund->getOrder()],
            'member' => [self::DATA => $refund->getMember()],
            'enterprise' => [self::DATA => $refund->getEnterprise()]
        ];
    }
}
