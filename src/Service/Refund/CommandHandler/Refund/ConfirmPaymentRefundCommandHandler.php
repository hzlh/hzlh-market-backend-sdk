<?php
namespace Market\Service\Refund\CommandHandler\Refund;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\Refund\Command\Refund\ConfirmPaymentRefundCommand;

class ConfirmPaymentRefundCommandHandler implements ICommandHandler
{
    use RefundCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ConfirmPaymentRefundCommand)) {
            throw new \InvalidArgumentException;
        }

        $crew = $this->fetchCrew($command->crewId);

        $refund = $this->fetchRefund($command->id);
        $refund->setCrew($crew);
        $refund->setVoucher($command->voucher);
        
        return $refund->confirmPayment();
    }
}
