<?php
namespace Market\Service\Refund\CommandHandler\Refund;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\Refund\Command\Refund\AddRefundCommand;

class AddRefundCommandHandler implements ICommandHandler
{
    use RefundCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddRefundCommand)) {
            throw new \InvalidArgumentException;
        }

        $member = $this->fetchMember($command->memberId);
        $order = $this->fetchServiceOrder($command->orderId);
        $enterprise = $this->fetchEnterprise($command->enterpriseId);

        $title = $order->getOrderCommodities()[0]->getSnapshot()->getSnapshotObject()->getTitle();

        $refund = $this->getRefund();
        $refund->setMember($member);
        $refund->setOrder($order);
        $refund->setEnterprise($enterprise);
        $refund->setRefundReason($command->refundReason);
        $refund->setOrderStatus($order->getStatus());
        $refund->setOrderNumber($order->getOrderno());
        $refund->setOrderCategory($order->getCategory());
        $refund->setGoodsName($title);

        if ($refund->add()) {
            $command->id = $refund->getId();
            return true;
        }
        
        return false;
    }
}
