<?php
namespace Market\Service\Refund\CommandHandler\Refund;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\Refund\Command\Refund\AgreeRefundCommand;

class AgreeRefundCommandHandler implements ICommandHandler
{
    use RefundCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AgreeRefundCommand)) {
            throw new \InvalidArgumentException;
        }

        $refund = $this->fetchRefund($command->id);
        
        return $refund->agree();
    }
}
