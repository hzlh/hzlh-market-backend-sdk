<?php
namespace Market\Service\Refund\CommandHandler\Refund;

use Backend\Crew\Model\Crew;
use Backend\Crew\Repository\Crew\CrewRepository;

use Member\Member\Model\Member;
use Member\Member\Repository\Member\MemberRepository;

use Member\Enterprise\Model\Enterprise;
use Member\Enterprise\Repository\Enterprise\EnterpriseRepository;

use Market\Service\ServiceOrder\Model\ServiceOrder;
use Market\Service\ServiceOrder\Repository\ServiceOrder\ServiceOrderRepository;

use Market\Service\Refund\Model\Refund;
use Market\Service\Refund\Repository\Refund\RefundRepository;

trait RefundCommandHandlerTrait
{
    private $refund;

    private $repository;

    private $memberRepository;

    private $enterpriseRepository;

    private $serviceOrderRepository;

    private $crewRepository;

    public function __construct()
    {
        $this->refund = new Refund();
        $this->repository = new RefundRepository();
        $this->memberRepository = new MemberRepository();
        $this->enterpriseRepository = new EnterpriseRepository();
        $this->serviceOrderRepository = new ServiceOrderRepository();
        $this->crewRepository = new CrewRepository();
    }

    public function __destruct()
    {
        unset($this->refund);
        unset($this->repository);
        unset($this->memberRepository);
        unset($this->enterpriseRepository);
        unset($this->serviceOrderRepository);
        unset($this->crewRepository);
    }

    protected function getRefund() : Refund
    {
        return $this->refund;
    }

    protected function getRepository() : RefundRepository
    {
        return $this->repository;
    }

    protected function fetchRefund(int $id) : Refund
    {
        return $this->getRepository()->fetchOne($id);
    }

    protected function getMemberRepository() : MemberRepository
    {
        return $this->memberRepository;
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }

    protected function getServiceOrderRepository() : ServiceOrderRepository
    {
        return $this->serviceOrderRepository;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function fetchMember(int $id) : Member
    {
        return $this->getMemberRepository()->fetchOne($id);
    }

    protected function fetchEnterprise(int $id) : Enterprise
    {
        return $this->getEnterpriseRepository()->fetchOne($id);
    }

    protected function fetchServiceOrder(int $id) : ServiceOrder
    {
        return $this->getServiceOrderRepository()->fetchOne($id);
    }

    protected function fetchCrew(int $id) : Crew
    {
        return $this->getCrewRepository()->fetchOne($id);
    }
}
