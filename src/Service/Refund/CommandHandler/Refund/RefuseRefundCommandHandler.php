<?php
namespace Market\Service\Refund\CommandHandler\Refund;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\Refund\Command\Refund\RefuseRefundCommand;

class RefuseRefundCommandHandler implements ICommandHandler
{
    use RefundCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof RefuseRefundCommand)) {
            throw new \InvalidArgumentException;
        }

        $refund = $this->fetchRefund($command->id);
        $refund->setRefuseReason($command->refuseReason);
        
        return $refund->refuse();
    }
}
