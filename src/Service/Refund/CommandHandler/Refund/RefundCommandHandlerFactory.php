<?php
namespace Market\Service\Refund\CommandHandler\Refund;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class RefundCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Service\Refund\Command\Refund\AddRefundCommand'=>
            'Market\Service\Refund\CommandHandler\Refund\AddRefundCommandHandler',
        'Market\Service\Refund\Command\Refund\AgreeRefundCommand'=>
            'Market\Service\Refund\CommandHandler\Refund\AgreeRefundCommandHandler',
        'Market\Service\Refund\Command\Refund\RefuseRefundCommand'=>
            'Market\Service\Refund\CommandHandler\Refund\RefuseRefundCommandHandler',
        'Market\Service\Refund\Command\Refund\ConfirmPaymentRefundCommand'=>
            'Market\Service\Refund\CommandHandler\Refund\ConfirmPaymentRefundCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
