<?php
namespace Market\Service\Refund\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Common\Model\IOperatAble;
use Common\Model\OperatAbleTrait;

use Backend\Crew\Model\Crew;
use Member\Member\Model\Member;
use Member\Enterprise\Model\Enterprise;
use Market\Service\ServiceOrder\Model\ServiceOrder;

use Market\Service\Refund\Adapter\Refund\IRefundAdapter;
use Market\Service\Refund\Repository\Refund\RefundRepository;

/**
 * @SuppressWarnings(PHPMD)
 */
class Refund implements IOperatAble, IObject
{
    use OperatAbleTrait, Object;

    const STATUS = [
        'UNDER_REVIEW' => 0,
        'AGREE' => 2,
        'PAYMENT' => 4,
        'REFUSE' => -2
    ];

    const TIME_RECORD = array(
        self::STATUS['UNDER_REVIEW'] => '',
        self::STATUS['AGREE'] => '',
        self::STATUS['PAYMENT'] => '',
        self::STATUS['REFUSE'] => ''
    );

    private $id;
    
    private $refundReason;
    
    private $order;
    
    private $member;
    
    private $enterprise;
    
    private $crew;
    
    private $refuseReason;
    
    private $voucher;
    
    private $orderStatus;
    
    private $orderNumber;

    private $orderCategory;

    private $goodsName;

    private $timeRecord;
    
    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->refundReason = '';
        $this->order = new ServiceOrder();
        $this->member = new Member();
        $this->enterprise = new Enterprise();
        $this->crew = new Crew();
        $this->refuseReason = '';
        $this->voucher = array();
        $this->orderStatus = 0;
        $this->orderNumber = '';
        $this->orderCategory = 0;
        $this->goodsName = '';
        $this->timeRecord = self::TIME_RECORD;
        $this->status = self::STATUS['UNDER_REVIEW'];
        $this->updateTime = Core::$container->get('time');
        $this->createTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->repository = new RefundRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->refundReason);
        unset($this->order);
        unset($this->member);
        unset($this->enterprise);
        unset($this->crew);
        unset($this->refuseReason);
        unset($this->voucher);
        unset($this->orderStatus);
        unset($this->orderNumber);
        unset($this->orderCategory);
        unset($this->goodsName);
        unset($this->timeRecord);
        unset($this->status);
        unset($this->updateTime);
        unset($this->createTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setRefundReason(string $refundReason) : void
    {
        $this->refundReason = $refundReason;
    }

    public function getRefundReason() : string
    {
        return $this->refundReason;
    }

    public function setOrder(ServiceOrder $order) : void
    {
        $this->order = $order;
    }

    public function getOrder() : ServiceOrder
    {
        return $this->order;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setRefuseReason(string $refuseReason) : void
    {
        $this->refuseReason = $refuseReason;
    }

    public function getRefuseReason() : string
    {
        return $this->refuseReason;
    }

    public function setVoucher(array $voucher) : void
    {
        $this->voucher = $voucher;
    }

    public function getVoucher() : array
    {
        return $this->voucher;
    }

    public function setOrderStatus(int $orderStatus) : void
    {
        $this->orderStatus = $orderStatus;
    }

    public function getOrderStatus() : int
    {
        return $this->orderStatus;
    }

    public function setOrderNumber(string $orderNumber) : void
    {
        $this->orderNumber = $orderNumber;
    }

    public function getOrderNumber() : string
    {
        return $this->orderNumber;
    }

    public function setOrderCategory(int $orderCategory) : void
    {
        $this->orderCategory = $orderCategory;
    }

    public function getOrderCategory() : int
    {
        return $this->orderCategory;
    }

    public function setGoodsName(string $goodsName) : void
    {
        $this->goodsName = $goodsName;
    }

    public function getGoodsName() : string
    {
        return $this->goodsName;
    }

    public function setTimeRecord(array $timeRecord) : void
    {
        $this->timeRecord = $timeRecord;
    }

    public function getTimeRecord() : array
    {
        return $this->timeRecord;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['UNDER_REVIEW'];
    }

    protected function getRepository() : IRefundAdapter
    {
        return $this->repository;
    }
    /**
     * addAction
     * 添加数据
     * 验证order,enterprise,member
     */
    protected function addAction() : bool
    {
        if ($this->getOrder() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'order'));
            return false;
        }

        if ($this->getMember() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'member'));
            return false;
        }

        if ($this->getEnterprise() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'enterprise'));
            return false;
        }

        $orderStatus = $this->getOrder()->getStatus();
        if ($orderStatus < ServiceOrder::STATUS['PAID']
            || $orderStatus > ServiceOrder::STATUS['BUYER_CONFIRMATION']
        ) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'order'));
            return false;
        }

        $timeRecord = $this->getTimeRecord();
        $timeRecord[self::STATUS['UNDER_REVIEW']] = Core::$container->get('time');
        $this->setTimeRecord($timeRecord);

        return $this->getRepository()->add($this)
            && $this->getOrder()->refund();
    }

    protected function editAction() : bool
    {
        return false;
    }
    /**
     * agree
     * 同意退款
     * 验证状态是否为待卖家审核
     */
    public function agree() : bool
    {
        if (!$this->isUnderReview()) {
            Core::setLastError(STATUS_NOT_UNDER_REVIEW);
            return false;
        }

        $timeRecord = $this->getTimeRecord();
        $timeRecord[self::STATUS['AGREE']] = Core::$container->get('time');

        $this->setTimeRecord($timeRecord);
        $this->setStatus(self::STATUS['AGREE']);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this);
    }
    /**
     * refuse
     * 拒绝退款
     * 验证状态是否为待卖家审核
     */
    public function refuse() : bool
    {
        if (!$this->isUnderReview()) {
            Core::setLastError(STATUS_NOT_UNDER_REVIEW);
            return false;
        }

        $timeRecord = $this->getTimeRecord();
        $timeRecord[self::STATUS['REFUSE']] = Core::$container->get('time');
        
        $this->setTimeRecord($timeRecord);
        $this->setStatus(self::STATUS['REFUSE']);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this)
            && $this->getOrder()->refuse($this->getOrderStatus());
    }
    /**
     * confirmPayment
     * 确认打款
     * 验证crew，验证状态是否为同意退款
     */
    public function confirmPayment() : bool
    {
        if ($this->getCrew() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'crew'));
            return false;
        }

        if (!$this->isAgree()) {
            Core::setLastError(STATUS_NOT_AGREED);
            return false;
        }

        $timeRecord = $this->getTimeRecord();
        $timeRecord[self::STATUS['PAYMENT']] = Core::$container->get('time');
        
        $this->setTimeRecord($timeRecord);
        $this->setStatus(self::STATUS['PAYMENT']);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this)
            && $this->getOrder()->confirmPayment();
    }

    protected function isUnderReview() : bool
    {
        return $this->getStatus() == self::STATUS['UNDER_REVIEW'];
    }

    protected function isAgree() : bool
    {
        return $this->getStatus() == self::STATUS['AGREE'];
    }
}
