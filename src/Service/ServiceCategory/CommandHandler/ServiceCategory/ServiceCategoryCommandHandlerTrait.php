<?php
namespace Market\Service\ServiceCategory\CommandHandler\ServiceCategory;

use Market\Service\ServiceCategory\Model\ServiceCategory;
use Market\Service\ServiceCategory\Repository\ServiceCategory\ServiceCategoryRepository;

trait ServiceCategoryCommandHandlerTrait
{
    private $repository;

    public function __construct()
    {
        $this->repository = new ServiceCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : ServiceCategoryRepository
    {
        return $this->repository;
    }

    private function fetchServiceCategory($id) : ServiceCategory
    {
        return $this->getRepository()->fetchOne($id);
    }
}
