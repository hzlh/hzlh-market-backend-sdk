<?php
namespace Market\Service\ServiceCategory\CommandHandler\ServiceCategory;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Market\Service\ServiceCategory\Model\ServiceCategory;
use Market\Service\ServiceCategory\Command\ServiceCategory\AddServiceCategoryCommand;

use Market\Service\ServiceCategory\Model\ParentCategory;
use Market\Service\ServiceCategory\Repository\ParentCategory\ParentCategoryRepository;

class AddServiceCategoryCommandHandler implements ICommandHandler
{
    private $serviceCategory;

    private $parentCategoryRepository;

    public function __construct()
    {
        $this->serviceCategory = new ServiceCategory();
        $this->parentCategoryRepository = new ParentCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->serviceCategory);
        unset($this->parentCategoryRepository);
    }

    protected function getServiceCategory() : ServiceCategory
    {
        return $this->serviceCategory;
    }

    protected function getParentCategoryRepository() : ParentCategoryRepository
    {
        return $this->parentCategoryRepository;
    }

    private function fetchParentCategory(int $id) : ParentCategory
    {
        return $this->getParentCategoryRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddServiceCategoryCommand)) {
            throw new \InvalidArgumentException;
        }

        $parentCategory = $this->fetchParentCategory($command->parentId);

        $serviceCategory = $this->getServiceCategory();
        $serviceCategory->setName($command->name);
        $serviceCategory->setQualificationName($command->qualificationName);
        $serviceCategory->setIsQualification($command->isQualification);
        $serviceCategory->setIsEnterpriseVerify($command->isEnterpriseVerify);
        $serviceCategory->setCommission($command->commission);
        $serviceCategory->setStatus($command->status);
        $serviceCategory->setParentCategory($parentCategory);

        if ($serviceCategory->add()) {
            $command->id = $serviceCategory->getId();
            return true;
        }
        return false;
    }
}
