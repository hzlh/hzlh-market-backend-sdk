<?php
namespace Market\Service\ServiceCategory\CommandHandler\ServiceCategory;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceCategoryCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Service\ServiceCategory\Command\ServiceCategory\AddServiceCategoryCommand'=>
        'Market\Service\ServiceCategory\CommandHandler\ServiceCategory\AddServiceCategoryCommandHandler',
        'Market\Service\ServiceCategory\Command\ServiceCategory\EditServiceCategoryCommand'=>
        'Market\Service\ServiceCategory\CommandHandler\ServiceCategory\EditServiceCategoryCommandHandler',
        'Market\Service\ServiceCategory\Command\ServiceCategory\EnableServiceCategoryCommand'=>
        'Market\Service\ServiceCategory\CommandHandler\ServiceCategory\EnableServiceCategoryCommandHandler',
        'Market\Service\ServiceCategory\Command\ServiceCategory\DisableServiceCategoryCommand'=>
        'Market\Service\ServiceCategory\CommandHandler\ServiceCategory\DisableServiceCategoryCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
