<?php
namespace Market\Service\ServiceCategory\CommandHandler\ServiceCategory;

use Common\Model\IEnableAble;
use Common\CommandHandler\DisableCommandHandler;

class DisableServiceCategoryCommandHandler extends DisableCommandHandler
{
    use ServiceCategoryCommandHandlerTrait;

    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchServiceCategory($id);
    }
}
