<?php
namespace Market\Service\ServiceCategory\CommandHandler\ServiceCategory;

use Common\Model\IEnableAble;
use Common\CommandHandler\EnableCommandHandler;

class EnableServiceCategoryCommandHandler extends EnableCommandHandler
{
    use ServiceCategoryCommandHandlerTrait;

    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchServiceCategory($id);
    }
}
