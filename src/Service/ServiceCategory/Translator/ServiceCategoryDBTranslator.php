<?php
namespace Market\Service\ServiceCategory\Translator;

use Marmot\Interfaces\ITranslator;

use Market\Service\ServiceCategory\Model\ServiceCategory;
use Market\Service\ServiceCategory\Model\NullServiceCategory;

class ServiceCategoryDBTranslator implements ITranslator
{
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function arrayToObject(array $expression, $serviceCategory = null)
    {
        if (!isset($expression['service_category_id'])) {
            return NullServiceCategory::getInstance();
        }

        if ($serviceCategory == null) {
            $serviceCategory = new ServiceCategory($expression['service_category_id']);
        }
        
        if (isset($expression['name'])) {
            $serviceCategory->setName($expression['name']);
        }
        if (isset($expression['parent_category'])) {
            $serviceCategory->getParentCategory()->setId($expression['parent_category']);
        }
        if (isset($expression['is_qualification'])) {
            $serviceCategory->setIsQualification($expression['is_qualification']);
        }
        if (isset($expression['is_enterprise_verify'])) {
            $serviceCategory->setIsEnterpriseVerify($expression['is_enterprise_verify']);
        }
        if (isset($expression['qualification_name'])) {
            $serviceCategory->setQualificationName($expression['qualification_name']);
        }
        if (isset($expression['commission'])) {
            $serviceCategory->setCommission($expression['commission']);
        }
        if (isset($expression['status'])) {
            $serviceCategory->setStatus($expression['status']);
        }
        if (isset($expression['status_time'])) {
            $serviceCategory->setStatusTime($expression['status_time']);
        }
        if (isset($expression['create_time'])) {
            $serviceCategory->setCreateTime($expression['create_time']);
        }
        if (isset($expression['update_time'])) {
            $serviceCategory->setUpdateTime($expression['update_time']);
        }

        return $serviceCategory;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function objectToArray($serviceCategory, array $keys = array())
    {
        if (!$serviceCategory instanceof ServiceCategory) {
            return false;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'parentCategory',
                'qualificationName',
                'isQualification',
                'isEnterpriseVerify',
                'commission',
                'status',
                'statusTime',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['service_category_id'] = $serviceCategory->getId();
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $serviceCategory->getName();
        }
        if (in_array('parentCategory', $keys)) {
            $expression['parent_category'] = $serviceCategory->getParentCategory()->getId();
        }
        if (in_array('qualificationName', $keys)) {
            $expression['qualification_name'] = $serviceCategory->getQualificationName();
        }
        if (in_array('isQualification', $keys)) {
            $expression['is_qualification'] = $serviceCategory->getIsQualification();
        }
        if (in_array('isEnterpriseVerify', $keys)) {
            $expression['is_enterprise_verify'] = $serviceCategory->getIsEnterpriseVerify();
        }
        if (in_array('commission', $keys)) {
            $expression['commission'] = $serviceCategory->getCommission();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $serviceCategory->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $serviceCategory->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $serviceCategory->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $serviceCategory->getUpdateTime();
        }

        return $expression;
    }
}
