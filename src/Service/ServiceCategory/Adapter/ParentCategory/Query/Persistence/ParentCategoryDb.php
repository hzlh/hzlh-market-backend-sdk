<?php
namespace Market\Service\ServiceCategory\Adapter\ParentCategory\Query\Persistence;

use Marmot\Framework\Classes\Db;

class ParentCategoryDb extends Db
{
    public function __construct()
    {
        parent::__construct('market_parent_category');
    }
}
