<?php
namespace Market\Service\ServiceCategory\Adapter\ParentCategory\Query;

use Marmot\Framework\Query\RowCacheQuery;

class ParentCategoryRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'parent_category_id',
            new Persistence\ParentCategoryCache(),
            new Persistence\ParentCategoryDb()
        );
    }
}
