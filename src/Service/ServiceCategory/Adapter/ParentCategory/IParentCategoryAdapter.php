<?php
namespace Market\Service\ServiceCategory\Adapter\ParentCategory;

use Market\Service\ServiceCategory\Model\ParentCategory;

use Common\Adapter\IOperatAbleAdapter;

interface IParentCategoryAdapter extends IOperatAbleAdapter
{
    public function fetchOne($id) : ParentCategory;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;
}
