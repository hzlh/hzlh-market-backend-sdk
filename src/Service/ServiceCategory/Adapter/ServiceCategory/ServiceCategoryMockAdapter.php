<?php
namespace Market\Service\ServiceCategory\Adapter\ServiceCategory;

use Market\Service\ServiceCategory\Model\ServiceCategory;
use Market\Service\ServiceCategory\Utils\ServiceCategory\MockFactory;
use Market\Service\ServiceCategory\Translator\ServiceCategoryDBTranslator;
use Market\Service\ServiceCategory\Adapter\ServiceCategory\Query\ServiceCategoryRowCacheQuery;

use Common\Model\IOperatAble;
use Common\Adapter\OperatAbleRestfulAdapterTrait;

class ServiceCategoryMockAdapter implements IServiceCategoryAdapter
{
    use OperatAbleRestfulAdapterTrait;
    
    private $translator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->translator = new ServiceCategoryDBTranslator();
        $this->rowCacheQuery = new ServiceCategoryRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDBTranslator() : ServiceCategoryDBTranslator
    {
        return $this->translator;
    }
    
    protected function getRowCacheQuery() : ServiceCategoryRowCacheQuery
    {
        return $this->rowCacheQuery;
    }
    
    public function add(IOperatAble $serviceCategory, array $keys = array()) : bool
    {
        unset($serviceCategory);
        unset($keys);

        return true;
    }

    public function edit(IOperatAble $serviceCategory, array $keys = array()) : bool
    {
        unset($serviceCategory);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : ServiceCategory
    {
        return MockFactory::generateServiceCategory($id);
    }

    public function fetchList(array $ids) : array
    {
        $serviceCategoryList = array();

        foreach ($ids as $id) {
            $serviceCategoryList[] = MockFactory::generateServiceCategory($id);
        }

        return $serviceCategoryList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
