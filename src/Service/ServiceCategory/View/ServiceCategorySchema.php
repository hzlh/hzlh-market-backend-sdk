<?php
namespace Market\Service\ServiceCategory\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class ServiceCategorySchema extends SchemaProvider
{
    protected $resourceType = 'marketServiceCategories';

    public function getId($serviceCategory) : int
    {
        return $serviceCategory->getId();
    }

    public function getAttributes($serviceCategory) : array
    {
        return [
            'name' => $serviceCategory->getName(),
            'qualificationName' => $serviceCategory->getQualificationName(),
            'isQualification' => $serviceCategory->getIsQualification(),
            'isEnterpriseVerify' => $serviceCategory->getIsEnterpriseVerify(),
            'commission' => $serviceCategory->getCommission(),
            'status' => $serviceCategory->getStatus(),
            'createTime' => $serviceCategory->getCreateTime(),
            'updateTime' => $serviceCategory->getUpdateTime(),
            'statusTime' => $serviceCategory->getStatusTime()
        ];
    }

    public function getRelationships($serviceCategory, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        return [
            'parentCategory' => [self::DATA => $serviceCategory->getParentCategory()]
        ];
    }
}
