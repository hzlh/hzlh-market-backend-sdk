<?php
namespace Market\Service\ServiceCategory\Controller\ParentCategory;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IOperatAbleController;

use Market\Service\ServiceCategory\Model\ParentCategory;
use Market\Service\ServiceCategory\View\ParentCategoryView;
use Market\Service\ServiceCategory\Repository\ParentCategory\ParentCategoryRepository;
use Market\Service\ServiceCategory\Command\ParentCategory\AddParentCategoryCommand;
use Market\Service\ServiceCategory\Command\ParentCategory\EditParentCategoryCommand;
use Market\Service\ServiceCategory\CommandHandler\ParentCategory\ParentCategoryCommandHandlerFactory;

use WidgetRules\ServiceCategory\WidgetRules as ServiceCategoryWidgetRules;

class OperationController extends Controller implements IOperatAbleController
{
    use JsonApiTrait;

    private $serviceCategoryWidgetRules;

    private $repository;

    private $commandBus;
    
    public function __construct()
    {
        parent::__construct();
        $this->serviceCategoryWidgetRules = new ServiceCategoryWidgetRules();
        $this->repository = new ParentCategoryRepository();
        $this->commandBus = new CommandBus(new ParentCategoryCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->serviceCategoryWidgetRules);
        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getServiceCategoryWidgetRules() : ServiceCategoryWidgetRules
    {
        return $this->serviceCategoryWidgetRules;
    }

    protected function getRepository() : ParentCategoryRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    /**
     * 对应路由 /parentCategories
     * 服务分类新增功能, 通过POST传参
     * @param jsonApi
     * @return jsonApi
     */
    public function add()
    {
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        
        $name = $attributes['name'];

        if ($this->validateOperateScenario(
            $name
        )) {
            $command = new AddParentCategoryCommand(
                $name
            );

            if ($this->getCommandBus()->send($command)) {
                $parentCategory = $this->getRepository()->fetchOne($command->id);
                if ($parentCategory instanceof ParentCategory) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new ParentCategoryView($parentCategory));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * /parentCategories/{id:\d+}
     * 服务分类编辑功能, 通过PATCH传参
     * @param jsonApi
     * @return jsonApi
     */
    public function edit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        
        $name = $attributes['name'];

        if ($this->validateOperateScenario(
            $name
        )) {
            $command = new EditParentCategoryCommand(
                $name,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $parentCategory = $this->getRepository()->fetchOne($id);
                if ($parentCategory instanceof ParentCategory) {
                    $this->render(new ParentCategoryView($parentCategory));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateOperateScenario(
        $name
    ) : bool {
        return $this->getServiceCategoryWidgetRules()->name($name);
    }
}
