<?php
namespace Market\Service\ServiceCategory\Repository\ParentCategory;

use Market\Service\ServiceCategory\Model\ParentCategory;
use Market\Service\ServiceCategory\Adapter\ParentCategory\IParentCategoryAdapter;
use Market\Service\ServiceCategory\Adapter\ParentCategory\ParentCategoryDBAdapter;
use Market\Service\ServiceCategory\Adapter\ParentCategory\ParentCategoryMockAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Common\Repository\OperatAbleRepositoryTrait;

class ParentCategoryRepository extends Repository implements IParentCategoryAdapter
{
    use OperatAbleRepositoryTrait;
    
    private $adapter;
    
    public function __construct()
    {
        $this->adapter = new ParentCategoryDBAdapter();
    }
    
    protected function getActualAdapter() : IParentCategoryAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IParentCategoryAdapter
    {
        return new ParentCategoryMockAdapter();
    }

    public function fetchOne($id) : ParentCategory
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
}
