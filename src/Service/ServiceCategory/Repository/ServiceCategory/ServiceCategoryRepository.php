<?php
namespace Market\Service\ServiceCategory\Repository\ServiceCategory;

use Market\Service\ServiceCategory\Model\ServiceCategory;
use Market\Service\ServiceCategory\Adapter\ServiceCategory\IServiceCategoryAdapter;
use Market\Service\ServiceCategory\Adapter\ServiceCategory\ServiceCategoryDBAdapter;
use Market\Service\ServiceCategory\Adapter\ServiceCategory\ServiceCategoryMockAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Common\Repository\OperatAbleRepositoryTrait;

class ServiceCategoryRepository extends Repository implements IServiceCategoryAdapter
{
    use OperatAbleRepositoryTrait;
    
    private $adapter;
    
    public function __construct()
    {
        $this->adapter = new ServiceCategoryDBAdapter();
    }

    protected function getActualAdapter() : IServiceCategoryAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IServiceCategoryAdapter
    {
        return new ServiceCategoryMockAdapter();
    }

    public function fetchOne($id) : ServiceCategory
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
}
