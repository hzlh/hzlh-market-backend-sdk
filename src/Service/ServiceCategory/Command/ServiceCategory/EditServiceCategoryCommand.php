<?php
namespace Market\Service\ServiceCategory\Command\ServiceCategory;

use Marmot\Interfaces\ICommand;

class EditServiceCategoryCommand implements ICommand
{
    public $name;

    public $qualificationName;

    public $isQualification;

    public $isEnterpriseVerify;

    public $commission;

    public $status;

    public $id;

    public function __construct(
        string $name,
        string $qualificationName,
        int $isQualification,
        int $isEnterpriseVerify,
        float $commission,
        int $status,
        int $id
    ) {
        $this->name = $name;
        $this->qualificationName = $qualificationName;
        $this->isQualification = $isQualification;
        $this->isEnterpriseVerify = $isEnterpriseVerify;
        $this->commission = $commission;
        $this->status = $status;
        $this->id = $id;
    }
}
