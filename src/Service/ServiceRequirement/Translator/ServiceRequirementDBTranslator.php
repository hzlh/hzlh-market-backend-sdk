<?php
namespace Market\Service\ServiceRequirement\Translator;

use Marmot\Interfaces\ITranslator;

use Market\Service\ServiceRequirement\Model\ContactsInfo;
use Market\Service\ServiceRequirement\Model\ServiceRequirement;
use Market\Service\ServiceRequirement\Model\NullServiceRequirement;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ServiceRequirementDBTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $serviceRequirement = null)
    {
        if (!isset($expression['service_requirement_id'])) {
            return NullServiceRequirement::getInstance();
        }

        if ($serviceRequirement == null) {
            $serviceRequirement = new ServiceRequirement();
        }

        $serviceRequirement->setId($expression['service_requirement_id']);

        if (isset($expression['member_id'])) {
            $serviceRequirement->getMember()->setId($expression['member_id']);
        }
        if (isset($expression['service_category_id'])) {
            $serviceRequirement->getServiceCategory()->setId($expression['service_category_id']);
        }
        if (isset($expression['title'])) {
            $serviceRequirement->setTitle($expression['title']);
        }
        $detail = array();
        if (is_string($expression['detail'])) {
            $detail = json_decode($expression['detail'], true);
        }
        if (is_array($expression['detail'])) {
            $detail = $expression['detail'];
        }
        $serviceRequirement->setDetail($detail);

        $name = isset($expression['contact_name']) ? $expression['contact_name'] : '';
        $phone = isset($expression['contact_phone']) ? $expression['contact_phone'] : '';
        $serviceRequirement->setContactsInfo(new ContactsInfo($name, $phone));

        if (isset($expression['min_price'])) {
            $serviceRequirement->setMinPrice($expression['min_price']);
        }
        if (isset($expression['max_price'])) {
            $serviceRequirement->setMaxPrice($expression['max_price']);
        }
        if (isset($expression['validity_start_time'])) {
            $serviceRequirement->setValidityStartTime($expression['validity_start_time']);
        }
        if (isset($expression['validity_end_time'])) {
            $serviceRequirement->setValidityEndTime($expression['validity_end_time']);
        }
        if (isset($expression['reject_reason'])) {
            $serviceRequirement->getRejectReason()->setId($expression['reject_reason']);
        }
        if (isset($expression['apply_status'])) {
            $serviceRequirement->setApplyStatus($expression['apply_status']);
        }
        if (isset($expression['status'])) {
            $serviceRequirement->setStatus($expression['status']);
        }
        if (isset($expression['status_time'])) {
            $serviceRequirement->setStatusTime($expression['status_time']);
        }
        if (isset($expression['create_time'])) {
            $serviceRequirement->setCreateTime($expression['create_time']);
        }
        if (isset($expression['update_time'])) {
            $serviceRequirement->setUpdateTime($expression['update_time']);
        }
        
        return $serviceRequirement;
    }

    public function objectToArray($serviceRequirement, array $keys = array())
    {
        if (!$serviceRequirement instanceof ServiceRequirement) {
            return false;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'member',
                'serviceCategory',
                'title',
                'detail',
                'contactName',
                'contactPhone',
                'minPrice',
                'maxPrice',
                'validityStartTime',
                'validityEndTime',
                'applyStatus',
                'rejectReason',
                'createTime',
                'updateTime',
                'status',
                'statusTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['service_requirement_id'] = $serviceRequirement->getId();
        }
        if (in_array('member', $keys)) {
            $expression['member_id'] = $serviceRequirement->getMember()->getId();
        }
        if (in_array('serviceCategory', $keys)) {
            $expression['service_category_id'] = $serviceRequirement->getServiceCategory()->getId();
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $serviceRequirement->getTitle();
        }
        if (in_array('detail', $keys)) {
            $expression['detail'] = $serviceRequirement->getDetail();
        }
        if (in_array('contactName', $keys)) {
            $expression['contact_name'] = $serviceRequirement->getContactsInfo()->getName();
        }
        if (in_array('contactPhone', $keys)) {
            $expression['contact_phone'] = $serviceRequirement->getContactsInfo()->getPhone();
        }
        if (in_array('minPrice', $keys)) {
            $expression['min_price'] = $serviceRequirement->getMinPrice();
        }
        if (in_array('maxPrice', $keys)) {
            $expression['max_price'] = $serviceRequirement->getMaxPrice();
        }
        if (in_array('validityStartTime', $keys)) {
            $expression['validity_start_time'] = $serviceRequirement->getValidityStartTime();
        }
        if (in_array('validityEndTime', $keys)) {
            $expression['validity_end_time'] = $serviceRequirement->getValidityEndTime();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['apply_status'] = $serviceRequirement->getApplyStatus();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['reject_reason'] = $serviceRequirement->getRejectReason()->getId();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $serviceRequirement->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $serviceRequirement->getUpdateTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $serviceRequirement->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $serviceRequirement->getStatusTime();
        }

        return $expression;
    }
}
