<?php
namespace Market\Service\ServiceRequirement\Model;

class ContactsInfo
{
    private $name;

    private $phone;

    public function __construct(
        string $name = '',
        string $phone = ''
    ) {
        $this->name = $name;
        $this->phone = $phone;
    }

    public function __destruct()
    {
        unset($this->name);
        unset($this->phone);
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getPhone() : string
    {
        return $this->phone;
    }
}
