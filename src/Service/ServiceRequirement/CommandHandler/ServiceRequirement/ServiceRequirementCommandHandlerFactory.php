<?php
namespace Market\Service\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceRequirementCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'Market\Service\ServiceRequirement\Command\ServiceRequirement\AddServiceRequirementCommand'=>
        'Market\Service\ServiceRequirement\CommandHandler\ServiceRequirement\AddServiceRequirementCommandHandler',
        'Market\Service\ServiceRequirement\Command\ServiceRequirement\ApproveServiceRequirementCommand'=>
        'Market\Service\ServiceRequirement\CommandHandler\ServiceRequirement\ApproveServiceRequirementCommandHandler',
        'Market\Service\ServiceRequirement\Command\ServiceRequirement\RejectServiceRequirementCommand'=>
        'Market\Service\ServiceRequirement\CommandHandler\ServiceRequirement\RejectServiceRequirementCommandHandler',
        'Market\Service\ServiceRequirement\Command\ServiceRequirement\RevokeServiceRequirementCommand'=>
        'Market\Service\ServiceRequirement\CommandHandler\ServiceRequirement\RevokeServiceRequirementCommandHandler',
        'Market\Service\ServiceRequirement\Command\ServiceRequirement\CloseServiceRequirementCommand'=>
        'Market\Service\ServiceRequirement\CommandHandler\ServiceRequirement\CloseServiceRequirementCommandHandler',
        'Market\Service\ServiceRequirement\Command\ServiceRequirement\DeleteServiceRequirementCommand'=>
        'Market\Service\ServiceRequirement\CommandHandler\ServiceRequirement\DeleteServiceRequirementCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
