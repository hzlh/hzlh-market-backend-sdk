<?php
namespace Market\Service\ServiceRequirement\CommandHandler\ServiceRequirement;

use Market\Service\ServiceRequirement\Model\ServiceRequirement;
use Market\Service\ServiceRequirement\Repository\ServiceRequirement\ServiceRequirementRepository;

trait ServiceRequirementCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new ServiceRequirementRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : ServiceRequirementRepository
    {
        return $this->repository;
    }
    
    protected function fetchServiceRequirement(int $id) : ServiceRequirement
    {
        return $this->getRepository()->fetchOne($id);
    }
}
