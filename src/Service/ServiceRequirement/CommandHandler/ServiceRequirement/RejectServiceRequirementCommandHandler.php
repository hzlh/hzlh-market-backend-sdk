<?php
namespace Market\Service\ServiceRequirement\CommandHandler\ServiceRequirement;

use Common\Model\IApproveAble;
use Common\CommandHandler\RejectCommandHandler;

use Market\Service\ServiceRequirement\Repository\ServiceRequirement\ServiceRequirementRepository;

class RejectServiceRequirementCommandHandler extends RejectCommandHandler
{
    use ServiceRequirementCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchServiceRequirement($id);
    }
}
