<?php
namespace Market\Service\ServiceRequirement\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IOperatAbleController;

use Market\Service\ServiceRequirement\Model\ServiceRequirement;
use Market\Service\ServiceRequirement\View\ServiceRequirementView;
use Market\Service\ServiceRequirement\Repository\ServiceRequirement\ServiceRequirementRepository;
use Market\Service\ServiceRequirement\Command\ServiceRequirement\AddServiceRequirementCommand;
use Market\Service\ServiceRequirement\CommandHandler\ServiceRequirement\ServiceRequirementCommandHandlerFactory;

class OperationController extends Controller implements IOperatAbleController
{
    use JsonApiTrait, ServiceRequirementValidateTrait;

    private $repository;

    private $commandBus;
    
    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRequirementRepository();
        $this->commandBus = new CommandBus(new ServiceRequirementCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : ServiceRequirementRepository
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    /**
     * 对应路由 /serviceRequirements
     * 新闻新增功能, 通过POST传参
     * @param jsonApi
     * @return jsonApi
     */
    public function add()
    {
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $title = $attributes['title'];
        $detail = $attributes['detail'];
        $contactName = $attributes['contactName'];
        $contactPhone = $attributes['contactPhone'];
        $minPrice = $attributes['minPrice'];
        $maxPrice = $attributes['maxPrice'];
        $validityStartTime = $attributes['validityStartTime'];
        $validityEndTime = $attributes['validityEndTime'];

        $memberId = $relationships['member']['data'][0]['id'];
        $serviceCategoryId = $relationships['serviceCategory']['data'][0]['id'];

        if ($this->validateAddScenario(
            $title,
            $detail,
            $contactName,
            $contactPhone,
            $minPrice,
            $maxPrice,
            $validityStartTime,
            $validityEndTime,
            $memberId,
            $serviceCategoryId
        )) {
            $commandBus = $this->getCommandBus();

            $command = new AddServiceRequirementCommand(
                $title,
                $detail,
                $contactName,
                $contactPhone,
                $minPrice,
                $maxPrice,
                $validityStartTime,
                $validityEndTime,
                $memberId,
                $serviceCategoryId
            );

            if ($commandBus->send($command)) {
                $serviceRequirement = $this->getRepository()->fetchOne($command->id);
                if ($serviceRequirement instanceof ServiceRequirement) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new ServiceRequirementView($serviceRequirement));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    public function edit(int $id)
    {
        unset($id);
        $this->displayError();
        return false;
    }
}
