<?php
namespace Market\Service\ServiceRequirement\Controller;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IFetchAbleController;

use Market\Service\ServiceRequirement\View\ServiceRequirementView;
use Market\Service\ServiceRequirement\Repository\ServiceRequirement\ServiceRequirementRepository;

class FetchController extends Controller implements IFetchAbleController
{
    use JsonApiTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRequirementRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ServiceRequirementRepository
    {
        return $this->repository;
    }

    public function fetchOne(int $id)
    {
        $serviceRequirement = $this->getRepository()->fetchOne($id);

        if (!$serviceRequirement instanceof INull) {
            $this->renderView(new ServiceRequirementView($serviceRequirement));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function fetchList(string $ids)
    {
        $ids = explode(',', $ids);

        $serviceRequirementList = array();

        $serviceRequirementList = $this->getRepository()->fetchList($ids);

        if (!empty($serviceRequirementList)) {
            $this->renderView(new ServiceRequirementView($serviceRequirementList));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function filter()
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();

        list($serviceRequirementList, $count) = $this->getRepository()->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = new ServiceRequirementView($serviceRequirementList);
            $view->pagination(
                'serviceRequirements',
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );
            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
