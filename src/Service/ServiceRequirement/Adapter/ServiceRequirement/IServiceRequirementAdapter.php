<?php
namespace Market\Service\ServiceRequirement\Adapter\ServiceRequirement;

use Market\Service\ServiceRequirement\Model\ServiceRequirement;

use Common\Adapter\IOperatAbleAdapter;

interface IServiceRequirementAdapter extends IOperatAbleAdapter
{
    public function fetchOne($id) : ServiceRequirement;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;
}
