<?php
namespace Market\Service\ServiceRequirement\Adapter\ServiceRequirement\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class ServiceRequirementCache extends Cache
{
    /**
     * 构造函数初始化key和表名一致
     */
    public function __construct()
    {
        parent::__construct('market_service_requirement');
    }
}
