<?php
namespace Market\Service\ServiceRequirement\Adapter\ServiceRequirement;

use Market\Service\ServiceRequirement\Model\ServiceRequirement;
use Market\Service\ServiceRequirement\Utils\MockFactory;
use Market\Service\ServiceRequirement\Translator\ServiceRequirementDBTranslator;
use Market\Service\ServiceRequirement\Adapter\ServiceRequirement\Query\ServiceRequirementRowCacheQuery;

use Common\Model\IOperatAble;
use Common\Adapter\OperatAbleRestfulAdapterTrait;

class ServiceRequirementMockAdapter implements IServiceRequirementAdapter
{
    use OperatAbleRestfulAdapterTrait;
    
    private $translator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->translator = new ServiceRequirementDBTranslator();
        $this->rowCacheQuery = new ServiceRequirementRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDBTranslator() : ServiceRequirementDBTranslator
    {
        return $this->translator;
    }
    
    protected function getRowCacheQuery() : ServiceRequirementRowCacheQuery
    {
        return $this->rowCacheQuery;
    }
    
    public function add(IOperatAble $serviceRequirement, array $keys = array()) : bool
    {
        unset($serviceRequirement);

        return true;
    }

    public function edit(IOperatAble $serviceRequirement, array $keys = array()) : bool
    {
        unset($serviceRequirement);
        unset($keys);

        return true;
    }

    public function fetchOne($id) : ServiceRequirement
    {
        return MockFactory::generateServiceRequirement($id);
    }

    public function fetchList(array $ids) : array
    {
        $serviceRequirementList = array();

        foreach ($ids as $id) {
            $serviceRequirementList[] = MockFactory::generateServiceRequirement($id);
        }

        return $serviceRequirementList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }
}
