<?php
namespace Market\Service\ServiceRequirement\Command\ServiceRequirement;

use Marmot\Interfaces\ICommand;

abstract class ServiceRequirementCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id = 0
    ) {
        $this->id = $id;
    }
}
