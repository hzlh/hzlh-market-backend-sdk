<?php
namespace Market\Service\ServiceRequirement\Repository\ServiceRequirement;

use Marmot\Framework\Classes\Repository;

use Common\Repository\OperatAbleRepositoryTrait;

use Market\Service\ServiceRequirement\Model\ServiceRequirement;
use Market\Service\ServiceRequirement\Adapter\ServiceRequirement\IServiceRequirementAdapter;
use Market\Service\ServiceRequirement\Adapter\ServiceRequirement\ServiceRequirementDBAdapter;
use Market\Service\ServiceRequirement\Adapter\ServiceRequirement\ServiceRequirementMockAdapter;

class ServiceRequirementRepository extends Repository implements IServiceRequirementAdapter
{
    use OperatAbleRepositoryTrait;
    
    private $adapter;
    
    public function __construct()
    {
        $this->adapter = new ServiceRequirementDBAdapter();
    }

    public function setAdapter(IServiceRequirementAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    protected function getActualAdapter() : IServiceRequirementAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IServiceRequirementAdapter
    {
        return new ServiceRequirementMockAdapter();
    }

    public function fetchOne($id) : ServiceRequirement
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
}
