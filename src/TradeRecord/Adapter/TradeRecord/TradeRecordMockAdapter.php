<?php
namespace Market\TradeRecord\Adapter\TradeRecord;

use Market\TradeRecord\Model\TradeRecord;
use Market\TradeRecord\Utils\MockFactory;
use Market\TradeRecord\Model\DefaultTradeRecord;

class TradeRecordMockAdapter implements ITradeRecordAdapter
{
    public function add(TradeRecord $tradeRecord, array $keys = array()) : bool
    {
        unset($tradeRecord);
        unset($keys);
        return true;
    }

    public function fetchOne($id) : DefaultTradeRecord
    {
        return MockFactory::generateTradeRecord($id);
    }

    public function fetchList(array $ids) : array
    {
        $tradeRecordList = array();

        foreach ($ids as $id) {
            $tradeRecordList[] = MockFactory::generateTradeRecord($id);
        }

        return $tradeRecordList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {

        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
