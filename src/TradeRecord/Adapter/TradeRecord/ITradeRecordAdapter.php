<?php
namespace Market\TradeRecord\Adapter\TradeRecord;

use Market\TradeRecord\Model\TradeRecord;
use Market\TradeRecord\Model\DefaultTradeRecord;

interface ITradeRecordAdapter
{
    public function add(TradeRecord $depositRecord, array $keys = array()) : bool;

    public function fetchOne($id) : DefaultTradeRecord;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;
}
