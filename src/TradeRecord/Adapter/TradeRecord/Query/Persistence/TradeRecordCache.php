<?php
namespace Market\TradeRecord\Adapter\TradeRecord\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class TradeRecordCache extends Cache
{
    /**
     * 构造函数初始化key和表名一致
     */
    public function __construct()
    {
        parent::__construct('market_trade_record');
    }
}
