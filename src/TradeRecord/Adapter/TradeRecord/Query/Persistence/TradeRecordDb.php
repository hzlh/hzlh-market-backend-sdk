<?php
namespace Market\TradeRecord\Adapter\TradeRecord\Query\Persistence;

use Marmot\Framework\Classes\Db;

class TradeRecordDb extends Db
{
    public function __construct()
    {
        parent::__construct('market_trade_record');
    }
}
