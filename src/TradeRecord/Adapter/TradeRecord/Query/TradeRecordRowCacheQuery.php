<?php
namespace Market\TradeRecord\Adapter\TradeRecord\Query;

use Marmot\Framework\Query\RowCacheQuery;

class TradeRecordRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'trade_record_id',
            new Persistence\TradeRecordCache(),
            new Persistence\TradeRecordDb()
        );
    }
}
