<?php
namespace Market\TradeRecord\Adapter\TradeRecord;

use Marmot\Core;

use Market\TradeRecord\Model\TradeRecord;
use Market\TradeRecord\Model\DefaultTradeRecord;
use Market\TradeRecord\Model\NullDefaultTradeRecord;
use Market\TradeRecord\Repository\RepositoryFactory;
use Market\TradeRecord\Translator\TradeRecordDBTranslator;
use Market\TradeRecord\Adapter\TradeRecord\Query\TradeRecordRowCacheQuery;

use Member\Member\Model\MemberAccount;
use Member\Member\Repository\MemberAccount\MemberAccountRepository;

use Common\Adapter\FetchRestfulAdapterTrait;

class TradeRecordDBAdapter implements ITradeRecordAdapter
{
    use FetchRestfulAdapterTrait;

    private $translator;

    private $rowCacheQuery;

    private $repositoryFactory;

    private $memberAccountRepository;

    public function __construct()
    {
        $this->translator = new TradeRecordDBTranslator();
        $this->rowCacheQuery = new TradeRecordRowCacheQuery();
        $this->repositoryFactory = new RepositoryFactory();
        $this->memberAccountRepository = new MemberAccountRepository();
    }

    public function __destruct()
    {
        unset($this->translator);
        unset($this->rowCacheQuery);
        unset($this->repositoryFactory);
        unset($this->memberAccountRepository);
    }
    
    protected function getDBTranslator() : TradeRecordDBTranslator
    {
        return $this->translator;
    }
    
    protected function getRowCacheQuery() : TradeRecordRowCacheQuery
    {
        return $this->rowCacheQuery;
    }
    
    protected function getRepositoryFactory() : RepositoryFactory
    {
        return $this->repositoryFactory;
    }
    
    protected function getMemberAccountRepository() : MemberAccountRepository
    {
        return $this->memberAccountRepository;
    }

    protected function fetchMemberAccount(int $id) : MemberAccount
    {
        return $this->getMemberAccountRepository()->fetchOne($id);
    }

    public function add(TradeRecord $tradeRecord, array $keys = array()) : bool
    {
        $info = array();
        
        $info = $this->getDBTranslator()->objectToArray($tradeRecord, $keys);

        $id = $this->getRowCacheQuery()->add($info);
    
        if (!$id) {
            return false;
        }
        
        return true;
    }

    public function fetchOne($id) : DefaultTradeRecord
    {
        $info = array();

        $info = $this->getRowCacheQuery()->getOne($id);

        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullDefaultTradeRecord::getInstance();
        }

        $tradeRecord = $this->getDBTranslator()->arrayToObject($info);

        $memberAccount = $this->fetchMemberAccount($tradeRecord->getMemberAccount()->getId());

        if (!empty($memberAccount)) {
            $tradeRecord->setMemberAccount($memberAccount);
        }

        $referenceRepository = $this->getRepositoryFactory()->getRepository(
            RepositoryFactory::TYPE[$tradeRecord->getType()]
        );
        $reference = $referenceRepository->fetchOne($tradeRecord->getReferenceId());

        if (!empty($reference)) {
            $tradeRecord->setReference($reference);
        }

        return $tradeRecord;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function fetchList(array $ids) : array
    {
        $tradeRecordList = array();
        $memberAccountIds = array();
        $referenceIds = array();
        
        $tradeRecordInfoList = $this->getRowCacheQuery()->getList($ids);
        if (empty($tradeRecordInfoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        $translator = $this->getDBTranslator();
        foreach ($tradeRecordInfoList as $tradeRecordInfo) {
            $tradeRecord = $translator->arrayToObject($tradeRecordInfo);

            $memberAccountIds[$tradeRecord->getId()] = $tradeRecord->getMemberAccount()->getId();

            $referenceIds[RepositoryFactory::TYPE[$tradeRecord->getType()]][] = array(
                'referenceId' => $tradeRecord->getReferenceId(),
                'tradeRecordId' => $tradeRecord->getId()
            );
            
            $tradeRecordList[] = $tradeRecord;
        }

        $tradeRecordReferenceList = array();

        foreach ($referenceIds as $category => $val) {
            $ids = array_unique(array_column($val, 'referenceId'));
            $referenceList = $this->fetchReferenceList($category, $ids);

            foreach ($val as $v) {
                if (isset($referenceList[$v['referenceId']])) {
                    $tradeRecordReferenceList[$v['tradeRecordId']] = $referenceList[$v['referenceId']];
                }
            }
        }

        if (!empty($tradeRecordReferenceList)) {
            foreach ($tradeRecordList as $tradeRecord) {
                if (isset($tradeRecordReferenceList[$tradeRecord->getId()])) {
                    $tradeRecord->setReference($tradeRecordReferenceList[$tradeRecord->getId()]);
                }
            }
        }
        
        $memberAccount = $this->getMemberAccountRepository()->fetchList($memberAccountIds);
        if (!empty($memberAccount)) {
            foreach ($tradeRecordList as $key => $tradeRecord) {
                if (isset($memberAccount[$key])) {
                    $tradeRecord->setMemberAccount($memberAccount[$key]);
                }
            }
        }
        
        return $tradeRecordList;
    }

    private function fetchReferenceList($category, $ids) : array
    {
        $referenceRepository = $this->getRepositoryFactory()->getRepository($category);
        $referenceList = array();
        $referenceListTemp = $referenceRepository->fetchList($ids);

        foreach ($referenceListTemp as $referenceObject) {
            $referenceList[$referenceObject->getId()] = $referenceObject;
        }

        return $referenceList;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $tradeRecord = new DefaultTradeRecord();

            if (isset($filter['memberAccount'])) {
                $tradeRecord->getMemberAccount()->setId($filter['memberAccount']);
                $info = $this->getDBTranslator()->objectToArray(
                    $tradeRecord,
                    array('memberAccount')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }
        if (isset($filter['type']) && !empty($filter['type'])) {
            $type = $filter['type'];
            if (is_numeric($type)) {
                $tradeRecord->setType($filter['type']);
                $info = $this->getDBTranslator()->objectToArray(
                    $tradeRecord,
                    array('type')
                );
                $condition .= $conjection.key($info).' = '.current($info);
            }
            if (strpos($type, ',')) {
                $info = $this->getDBTranslator()->objectToArray(
                    $tradeRecord,
                    array('type')
                );
                $condition .= $conjection.key($info).' IN ('.$type.')';
            }
            $conjection = ' AND ';
        }
        if (isset($filter['startTime']) && !empty($filter['startTime'])) {
            $tradeRecord->setTradeTime($filter['startTime']);
            $info = $this->getDBTranslator()->objectToArray(
                $tradeRecord,
                array('tradeTime')
            );
            $condition .= $conjection.key($info).' > '.current($info);
            $conjection = ' AND ';
        }
        if (isset($filter['endTime']) && !empty($filter['endTime'])) {
            $tradeRecord->setTradeTime($filter['endTime']);
            $info = $this->getDBTranslator()->objectToArray(
                $tradeRecord,
                array('tradeTime')
            );
            $condition .= $conjection.key($info).' < '.current($info);
            $conjection = ' AND ';
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $tradeRecord = new DefaultTradeRecord();
            if (isset($sort['updateTime'])) {
                $info = $this->getDBTranslator()->objectToArray($tradeRecord, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['id'])) {
                $info = $this->getDBTranslator()->objectToArray($tradeRecord, array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}
