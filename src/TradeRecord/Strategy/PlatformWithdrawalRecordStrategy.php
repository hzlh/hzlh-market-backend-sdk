<?php
namespace Market\TradeRecord\Strategy;

use Market\TradeRecord\Model\PayTypeTrait;
use Market\TradeRecord\Model\ITradeRecordAble;
use Market\TradeRecord\Model\PlatformBalanceTrait;

use Member\Withdrawal\Model\Withdrawal;

use Member\Member\Model\MemberAccount;

class PlatformWithdrawalRecordStrategy implements ITradeRecordAble
{
    use PayTypeTrait, PlatformBalanceTrait;

    const TEMPLATES = array(
        'DEBTOR' => '汇众联合账户',
        'COMMENT' => '向账户(%s)的%s(%s)转账'
    );

    const BANK_CARD_NUMBER_SUBSTR_HEAD_STOP_COUNT = 4;
    const BANK_CARD_NUMBER_SUBSTR_HEAD_START_COUNT = 0;
    const BANK_CARD_NUMBER_SUBSTR_END_COUNT = -4;

    private $withdrawal;
    
    public function __construct(Withdrawal $withdrawal)
    {
        $this->withdrawal = $withdrawal;
    }

    protected function getWithdrawal() : Withdrawal
    {
        return $this->withdrawal;
    }

    public function getReferenceId() : int
    {
        return $this->getWithdrawal()->getId();
    }
    
    public function getMemberAccount() : MemberAccount
    {
        return new MemberAccount();
    }

    public function getTradeTime() : int
    {
        return $this->getWithdrawal()->getCreateTime();
    }

    public function getType() : int
    {
        return ITradeRecordAble::TRADE_RECORD_TYPES['WITHDRAWAL_PLATFORM'];
    }

    public function getTradeMoney() : float
    {
        return $this->getWithdrawal()->getAmount();
    }

    public function getDebtor() : string
    {
        return self::TEMPLATES['DEBTOR'];
    }

    public function getCreditor() : string
    {
        $bankName = $this->getWithdrawal()->getBankCard()->getBank()->getName();
        $cardNo = substr(
            $this->getWithdrawal()->getBankCard()->getCardNumber(),
            self::BANK_CARD_NUMBER_SUBSTR_HEAD_START_COUNT,
            self::BANK_CARD_NUMBER_SUBSTR_HEAD_STOP_COUNT
        ).'**** ****'.substr(
            $this->getWithdrawal()->getBankCard()->getCardNumber(),
            self::BANK_CARD_NUMBER_SUBSTR_END_COUNT
        );

        return $bankName.$cardNo;
    }

    public function getBalance() : float
    {
        return $this->fetchPlatformBalance()-$this->getWithdrawal()->getAmount();
    }

    public function getComment() : string
    {
        $bankName = $this->getWithdrawal()->getBankCard()->getBank()->getName();

        $cardNo = substr(
            $this->getWithdrawal()->getBankCard()->getCardNumber(),
            self::BANK_CARD_NUMBER_SUBSTR_END_COUNT
        );

        $celphone = $this->getWithdrawal()->getMemberAccount()->getMember()->getCellphone();

        return sprintf(self::TEMPLATES['COMMENT'], $celphone, $bankName, $cardNo);
    }
}
