<?php
namespace Market\TradeRecord\Strategy;

use Market\TradeRecord\Model\ITradeRecordAble;
use Market\TradeRecord\Model\MemberAccountBalanceTrait;

use Member\Withdrawal\Model\Withdrawal;

use Member\Member\Model\MemberAccount;

class MemberWithdrawalRecordStrategy implements ITradeRecordAble
{
    use MemberAccountBalanceTrait;

    const TEMPLATES = array(
        'DEBTOR'=>'账户(%s)',
        'CREDITOR' => '%s(%s)',
        'COMMENT' => '向%s(%s)提现'
    );

    const BANK_CARD_NUMBER_SUBSTR_END_COUNT = -4;

    private $withdrawal;
    
    public function __construct(Withdrawal $withdrawal)
    {
        $this->withdrawal = $withdrawal;
    }

    protected function getWithdrawal() : Withdrawal
    {
        return $this->withdrawal;
    }

    public function getReferenceId() : int
    {
        return $this->getWithdrawal()->getId();
    }
    
    public function getMemberAccount() : MemberAccount
    {
        return $this->getWithdrawal()->getMemberAccount();
    }

    public function getTradeTime() : int
    {
        return $this->getWithdrawal()->getCreateTime();
    }

    public function getType() : int
    {
        return ITradeRecordAble::TRADE_RECORD_TYPES['WITHDRAWAL'];
    }

    public function getTradeMoney() : float
    {
        return $this->getWithdrawal()->getAmount();
    }

    public function getDebtor() : string
    {
        $cellphone = $this->getMemberAccount()->getMember()->getCellphone();
        return sprintf(self::TEMPLATES['DEBTOR'], $cellphone);
    }

    public function getCreditor() : string
    {
        $bankName = $this->getBankName();
        $cardNo = $this->getCardNumber();
        
        return sprintf(self::TEMPLATES['CREDITOR'], $bankName, $cardNo);
    }

    public function getBalance() : float
    {
        return $this->fetchMemberAccountBalance($this->getWithdrawal()->getMemberAccount()->getId());
    }

    public function getComment() : string
    {
        $bankName = $this->getBankName();
        $cardNo = $this->getCardNumber();
        
        return sprintf(self::TEMPLATES['COMMENT'], $bankName, $cardNo);
    }

    protected function getBankName() : string
    {
        return $this->getWithdrawal()->getBankCard()->getBank()->getName();
    }

    protected function getCardNumber() : string
    {
        return substr($this->getWithdrawal()->getBankCard()->getCardNumber(), self::BANK_CARD_NUMBER_SUBSTR_END_COUNT);
    }
}
