<?php
namespace Market\TradeRecord\Strategy;

use Market\TradeRecord\Model\PayTypeTrait;
use Market\TradeRecord\Model\ITradeRecordAble;
use Market\TradeRecord\Model\MemberAccountBalanceTrait;

use Market\Service\ServiceOrder\Model\ServiceOrder;

use Member\member\Model\memberAccount;

class CommissionServiceOrderConfirmationRecordStrategy implements ITradeRecordAble
{
    use PayTypeTrait, MemberAccountBalanceTrait;
    
    const TEMPLATES = array(
        'DEBTOR' => '%s账户',
        'CREDITOR' => '汇众联合账户',
        'COMMENT' => '向平台账户支付订单%s交易服务费'
    );

    private $serviceOrder;
    
    public function __construct(ServiceOrder $serviceOrder)
    {
        $this->serviceOrder = $serviceOrder;
    }

    protected function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    public function getReferenceId() : int
    {
        return $this->getServiceOrder()->getId();
    }
    
    public function getMemberAccount() : memberAccount
    {
        $memberAccountId = $this->getServiceOrder()->getSellerEnterprise()->getId();
        return $this->getMemberAccountRepository()->fetchOne($memberAccountId);
    }

    public function getTradeTime() : int
    {
        return $this->getServiceOrder()->getCreateTime();
    }

    public function getType() : int
    {
        return ITradeRecordAble::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_PLATFORM'];
    }

    public function getTradeMoney() : float
    {
        return $this->getServiceOrder()->getCommission();
    }

    public function getDebtor() : string
    {
        $enterpriseName = $this->getServiceOrder()->getSellerEnterprise()->getName();
        
        return sprintf(self::TEMPLATES['DEBTOR'], $enterpriseName);
    }

    public function getCreditor() : string
    {
        return self::TEMPLATES['CREDITOR'];
    }

    public function getBalance() : float
    {
        $memberAccountId = $this->getServiceOrder()->getSellerEnterprise()->getId();

        //商家账户+应得-服务费
        return bcsub(
            bcadd(
                $this->fetchMemberAccountBalance($memberAccountId),
                $this->getServiceOrder()->getCollectedAmount(),
                2
            ),
            $this->serviceOrder->getCommission(),
            2
        );
    }

    public function getComment() : string
    {
        $orderNo = $this->getServiceOrder()->getOrderno();
        
        return sprintf(self::TEMPLATES['COMMENT'], $orderNo);
    }
}
