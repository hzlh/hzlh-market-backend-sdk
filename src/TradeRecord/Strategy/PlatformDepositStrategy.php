<?php
namespace Market\TradeRecord\Strategy;

use Market\TradeRecord\Model\PayTypeTrait;
use Market\TradeRecord\Model\ITradeRecordAble;
use Market\TradeRecord\Model\PlatformBalanceTrait;

use Member\Deposit\Model\Deposit;

use Member\Member\Model\MemberAccount;

class PlatformDepositStrategy implements ITradeRecordAble
{
    use PayTypeTrait, PlatformBalanceTrait;

    const TEMPLATES = array(
        'DEBTOR' => '账户(%s)的%s账户',
        'CREDITOR' => '汇众联合账户',
        'COMMENT' => '账户(%s)%s充值'
    );

    private $deposit;
    
    public function __construct(Deposit $deposit)
    {
        $this->deposit = $deposit;
    }

    protected function getDeposit() : Deposit
    {
        return $this->deposit;
    }

    public function getReferenceId() : int
    {
        return $this->getDeposit()->getId();
    }
    
    public function getMemberAccount() : MemberAccount
    {
        return new MemberAccount();
    }

    public function getTradeTime() : int
    {
        return $this->getDeposit()->getCreateTime();
    }

    public function getType() : int
    {
        return ITradeRecordAble::TRADE_RECORD_TYPES['DEPOSIT_PLATFORM'];
    }

    public function getTradeMoney() : float
    {
        return $this->getDeposit()->getAmount();
    }

    public function getDebtor() : string
    {
        $paymentTypeCn = $this->fetchPaymentTypeCn();
        $cellphone = $this->fetchMemberAccountCellphone();

        return sprintf(self::TEMPLATES['DEBTOR'], $cellphone, $paymentTypeCn);
    }

    public function getCreditor() : string
    {
        return self::TEMPLATES['CREDITOR'];
    }

    public function getBalance() : float
    {
        return $this->fetchPlatformBalance()+$this->getDeposit()->getAmount();
    }

    public function getComment() : string
    {
        $paymentTypeCn = $this->fetchPaymentTypeCn();
        $cellphone = $this->fetchMemberAccountCellphone();

        return sprintf(self::TEMPLATES['COMMENT'], $cellphone, $paymentTypeCn);
    }

    protected function fetchPaymentTypeCn() : string
    {
        $paymentType = $this->getDeposit()->getPayment()->getType();
        return $this->getPayTypeCN($paymentType);
    }

    protected function fetchMemberAccountCellphone() : string
    {
        return $this->getDeposit()->getMemberAccount()->getMember()->getCellphone();
    }
}
