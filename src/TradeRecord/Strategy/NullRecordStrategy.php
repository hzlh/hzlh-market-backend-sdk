<?php
namespace Market\TradeRecord\Strategy;

use Marmot\Interfaces\INull;

use Market\TradeRecord\Model\ITradeRecordAble;

use Member\Member\Model\MemberAccount;

class NullRecordStrategy implements ITradeRecordAble, INull
{
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getReferenceId() : int
    {
        return 0;
    }
    
    public function getMemberAccount() : MemberAccount
    {
        return new MemberAccount();
    }

    public function getTradeTime() : int
    {
        return 0;
    }

    public function getType() : int
    {
        return ITradeRecordAble::TRADE_RECORD_TYPES['NULL'];
    }

    public function getTradeMoney() : float
    {
        return 0;
    }

    public function getDebtor() : string
    {
        return '';
    }

    public function getCreditor() : string
    {
        return '';
    }

    public function getBalance() : float
    {
        return 0;
    }

    public function getComment() : string
    {
        return '';
    }
}
