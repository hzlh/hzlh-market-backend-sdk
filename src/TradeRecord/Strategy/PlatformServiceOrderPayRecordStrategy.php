<?php
namespace Market\TradeRecord\Strategy;

use Market\TradeRecord\Model\PayTypeTrait;
use Market\TradeRecord\Model\ITradeRecordAble;
use Market\TradeRecord\Model\PlatformBalanceTrait;

use Market\Service\ServiceOrder\Model\ServiceOrder;

use Member\Member\Model\MemberAccount;

class PlatformServiceOrderPayRecordStrategy implements ITradeRecordAble
{
    use PayTypeTrait, PlatformBalanceTrait;

    const TEMPLATES = array(
        'DEBTOR' => '账户(%s)的%s账户',
        'CREDITOR' => '汇众联合账户',
        'COMMENT' => '账户(%s)%s付款'
    );

    private $serviceOrder;
    
    public function __construct(ServiceOrder $serviceOrder)
    {
        $this->serviceOrder = $serviceOrder;
    }

    protected function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    public function getReferenceId() : int
    {
        return $this->getServiceOrder()->getId();
    }
    
    public function getMemberAccount() : MemberAccount
    {
        return new MemberAccount();
    }

    public function getTradeTime() : int
    {
        return $this->getServiceOrder()->getCreateTime();
    }

    public function getType() : int
    {
        return ITradeRecordAble::TRADE_RECORD_TYPES['ORDER_PAY_PLATFORM'];
    }

    public function getTradeMoney() : float
    {
        return $this->getServiceOrder()->getPaidAmount();
    }

    public function getDebtor() : string
    {
        $paymentType = $this->getServiceOrder()->getPayment()->getType();
        $paymentTypeCn = $this->getPayTypeCN($paymentType);

        $cellphone = $this->getMemberAccountCellphone();
        
        return sprintf(self::TEMPLATES['DEBTOR'], $cellphone, $paymentTypeCn);
    }

    public function getCreditor() : string
    {
        return self::TEMPLATES['CREDITOR'];
    }

    public function getBalance() : float
    {
        return $this->fetchPlatformBalance()+$this->getServiceOrder()->getPaidAmount();
    }

    public function getComment() : string
    {
        $paymentType = $this->getServiceOrder()->getPayment()->getType();
        $paymentTypeCn = $this->getPayTypeCN($paymentType);
        
        $cellphone = $this->getMemberAccountCellphone();

        return sprintf(self::TEMPLATES['COMMENT'], $cellphone, $paymentTypeCn);
    }

    protected function getMemberAccountCellphone() : string
    {
        return $this->getServiceOrder()->getBuyerMemberAccount()->getMember()->getCellphone();
    }
}
