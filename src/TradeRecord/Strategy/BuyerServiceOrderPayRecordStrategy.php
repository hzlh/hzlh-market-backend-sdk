<?php
namespace Market\TradeRecord\Strategy;

use Market\TradeRecord\Model\PayTypeTrait;
use Market\TradeRecord\Model\ITradeRecordAble;
use Market\TradeRecord\Model\MemberAccountBalanceTrait;

use Market\Service\ServiceOrder\Model\ServiceOrder;

use Member\Member\Model\MemberAccount;

class BuyerServiceOrderPayRecordStrategy implements ITradeRecordAble
{
    use PayTypeTrait, MemberAccountBalanceTrait;

    const TEMPLATES = array(
        'DEBTOR' => '账户(%s)的%s账户',
        'CREDITOR' => '账户(%s)',
        'COMMENT' => '订单%s支付完成,金额转入副账户冻结'
    );

    private $serviceOrder;
    
    public function __construct(ServiceOrder $serviceOrder)
    {
        $this->serviceOrder = $serviceOrder;
    }

    protected function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    public function getReferenceId() : int
    {
        return $this->getServiceOrder()->getId();
    }
    
    public function getMemberAccount() : MemberAccount
    {
        return $this->getServiceOrder()->getBuyerMemberAccount();
    }

    public function getTradeTime() : int
    {
        return $this->getServiceOrder()->getCreateTime();
    }

    public function getType() : int
    {
        return ITradeRecordAble::TRADE_RECORD_TYPES['ORDER_PAY_ENTERPRISE'];
    }

    public function getTradeMoney() : float
    {
        return $this->getServiceOrder()->getPaidAmount();
    }

    public function getDebtor() : string
    {
        $paymentType = $this->getServiceOrder()->getPayment()->getType();
        $paymentTypeCn = $this->getPayTypeCN($paymentType);
        $cellphone = $this->getServiceOrder()->getBuyerMemberAccount()->getMember()->getCellphone();
        
        return sprintf(self::TEMPLATES['DEBTOR'], $cellphone, $paymentTypeCn);
    }

    public function getCreditor() : string
    {
        $cellphone = $this->getServiceOrder()->getBuyerMemberAccount()->getMember()->getCellphone();
        
        return sprintf(self::TEMPLATES['CREDITOR'], $cellphone);
    }

    public function getBalance() : float
    {
        return $this->fetchMemberAccountBalance($this->getServiceOrder()->getBuyerMemberAccount()->getId());
    }

    public function getComment() : string
    {
        $orderNo = $this->getServiceOrder()->getOrderno();
        
        return sprintf(self::TEMPLATES['COMMENT'], $orderNo);
    }
}
