<?php
namespace Market\TradeRecord\Strategy;

use Market\TradeRecord\Model\PayTypeTrait;
use Market\TradeRecord\Model\ITradeRecordAble;
use Market\TradeRecord\Model\MemberAccountBalanceTrait;
use Market\Service\ServiceOrder\Model\ServiceOrder;
use Member\member\Model\memberAccount;

class SubsidyServiceOrderConfirmationRecordStrategy implements ITradeRecordAble
{
    use PayTypeTrait, MemberAccountBalanceTrait;

    const TEMPLATES = array(
        'DEBTOR' => '汇众联合账户',
        'CREDITOR' => '商家账户（%s）',
        'COMMENT' => '订单（%s）平台补贴支付完成，从平台账户支付商家账户（%s）'
    );

    private $serviceOrder;
    
    public function __construct(ServiceOrder $serviceOrder)
    {
        $this->serviceOrder = $serviceOrder;
    }

    protected function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    public function getReferenceId() : int
    {
        return $this->getServiceOrder()->getId();
    }
    
    public function getMemberAccount() : memberAccount
    {
        $memberAccountId = $this->getServiceOrder()->getSellerEnterprise()->getId();
        return $this->getMemberAccountRepository()->fetchOne($memberAccountId);
    }

    public function getTradeTime() : int
    {
        return $this->getServiceOrder()->getCreateTime();
    }

    public function getType() : int
    {
        return ITradeRecordAble::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_SUBSIDY'];
    }

    public function getTradeMoney() : float
    {
        return $this->getServiceOrder()->getSubsidy();
    }

    public function getDebtor() : string
    {
        return self::TEMPLATES['DEBTOR'];
    }

    public function getCreditor() : string
    {
        $cellphone = $this->getServiceOrder()->getBuyerMemberAccount()->getMember()->getCellphone();
        return sprintf(self::TEMPLATES['CREDITOR'], $cellphone);
    }

    public function getBalance() : float
    {
        $memberAccountId = $this->getServiceOrder()->getSellerEnterprise()->getId();
        $balance = $this->fetchMemberAccountBalance($memberAccountId);
        return bcadd(
            bcsub(
                bcadd($balance, $this->getServiceOrder()->getCollectedAmount(), 2),
                $this->getServiceOrder()->getCommission(),
                2
            ),
            $this->serviceOrder->getSubsidy(),
            2
        );
    }

    public function getComment() : string
    {
        $orderNo = $this->getServiceOrder()->getOrderno();
        $cellphone = $this->getServiceOrder()->getBuyerMemberAccount()->getMember()->getCellphone();
        return sprintf(self::TEMPLATES['COMMENT'], $orderNo, $cellphone);
    }
}
