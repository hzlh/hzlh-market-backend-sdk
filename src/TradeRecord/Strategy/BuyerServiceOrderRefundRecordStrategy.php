<?php
namespace Market\TradeRecord\Strategy;

use Market\TradeRecord\Model\PayTypeTrait;
use Market\TradeRecord\Model\ITradeRecordAble;
use Market\TradeRecord\Model\MemberAccountBalanceTrait;

use Market\Service\ServiceOrder\Model\ServiceOrder;

use Member\Member\Model\MemberAccount;

class BuyerServiceOrderRefundRecordStrategy implements ITradeRecordAble
{
    use PayTypeTrait, MemberAccountBalanceTrait;
    
    const TEMPLATES = array(
        'DEBTOR' => '账号（%s）',
        'CREDITOR' => '账号（%s）',
        'COMMENT' => '订单（%s）退款成功,金额从副账户转出'
    );

    private $serviceOrder;
    
    public function __construct(ServiceOrder $serviceOrder)
    {
        $this->serviceOrder = $serviceOrder;
    }

    protected function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    public function getReferenceId() : int
    {
        return $this->getServiceOrder()->getId();
    }
    
    public function getMemberAccount() : MemberAccount
    {
        return $this->getServiceOrder()->getBuyerMemberAccount();
    }

    public function getTradeTime() : int
    {
        return $this->getServiceOrder()->getCreateTime();
    }

    public function getType() : int
    {
        return ITradeRecordAble::TRADE_RECORD_TYPES['BUYER_REFUND'];
    }

    public function getTradeMoney() : float
    {
        return $this->getServiceOrder()->getPaidAmount();
    }

    public function getDebtor() : string
    {
        $cellphone = $this->getServiceOrder()->getBuyerMemberAccount()->getMember()->getCellphone();
        
        return sprintf(self::TEMPLATES['DEBTOR'], $cellphone);
    }

    public function getCreditor() : string
    {
        $cellphone = $this->getServiceOrder()->getBuyerMemberAccount()->getMember()->getCellphone();
        
        return sprintf(self::TEMPLATES['CREDITOR'], $cellphone);
    }

    public function getBalance() : float
    {
        return $this->fetchMemberAccountBalance(
            $this->getServiceOrder()->getBuyerMemberAccount()->getId()
        );
    }

    public function getComment() : string
    {
        $orderNo = $this->getServiceOrder()->getOrderno();
        
        return sprintf(self::TEMPLATES['COMMENT'], $orderNo);
    }
}
