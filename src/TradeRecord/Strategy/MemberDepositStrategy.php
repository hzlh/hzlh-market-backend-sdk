<?php
namespace Market\TradeRecord\Strategy;

use Market\TradeRecord\Model\PayTypeTrait;
use Market\TradeRecord\Model\ITradeRecordAble;
use Market\TradeRecord\Model\MemberAccountBalanceTrait;

use Member\Deposit\Model\Deposit;

use Member\Member\Model\MemberAccount;

class MemberDepositStrategy implements ITradeRecordAble
{
    use PayTypeTrait, MemberAccountBalanceTrait;

    const TEMPLATES = array(
        'DEBTOR' => '账户(%s)的%s账户',
        'CREDITOR' => '账户(%s)',
        'COMMENT' => '账户(%s)%s充值'
    );

    private $deposit;
    
    public function __construct(Deposit $deposit)
    {
        $this->deposit = $deposit;
    }

    protected function getDeposit() : Deposit
    {
        return $this->deposit;
    }

    public function getReferenceId() : int
    {
        return $this->getDeposit()->getId();
    }
    
    public function getMemberAccount() : MemberAccount
    {
        return $this->getDeposit()->getMemberAccount();
    }

    public function getTradeTime() : int
    {
        return $this->getDeposit()->getCreateTime();
    }

    public function getType() : int
    {
        return ITradeRecordAble::TRADE_RECORD_TYPES['DEPOSIT'];
    }

    public function getTradeMoney() : float
    {
        return $this->getDeposit()->getAmount();
    }

    public function getDebtor() : string
    {
        $paymentType = $this->getDeposit()->getPayment()->getType();
        $paymentTypeCn = $this->getPayTypeCN($paymentType);
        
        $cellphone = $this->getMemberCellphone();

        return sprintf(self::TEMPLATES['DEBTOR'], $cellphone, $paymentTypeCn);
    }

    public function getCreditor() : string
    {
        $cellphone = $this->getMemberCellphone();

        return sprintf(self::TEMPLATES['CREDITOR'], $cellphone);
    }

    public function getBalance() : float
    {
        return $this->fetchMemberAccountBalance($this->getDeposit()->getMemberAccount()->getId());
    }

    public function getComment() : string
    {
        $paymentType = $this->getDeposit()->getPayment()->getType();
        $paymentTypeCn = $this->getPayTypeCN($paymentType);
        
        $cellphone = $this->getMemberCellphone();

        return sprintf(self::TEMPLATES['COMMENT'], $cellphone, $paymentTypeCn);
    }

    protected function getMemberCellphone() : string
    {
        return $this->getDeposit()->getMemberAccount()->getMember()->getCellphone();
    }
}
