<?php
namespace Market\TradeRecord\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class TradeRecordSchema extends SchemaProvider
{
    protected $resourceType = 'productMarketTradeRecords';

    public function getId($tradeRecord)
    {
        return $tradeRecord->getId();
    }

    public function getAttributes($tradeRecord) : array
    {
        return [
            'referenceId'  => $tradeRecord->getReferenceId(),
            'tradeTime'  => $tradeRecord->getTradeTime(),
            'tradeType'  => $tradeRecord->getType(),
            'tradeMoney'  => $tradeRecord->getTradeMoney(),
            'debtor'  => $tradeRecord->getDebtor(),
            'creditor'  => $tradeRecord->getCreditor(),
            'balance'  => $tradeRecord->getBalance(),
            'comment'  => $tradeRecord->getComment(),
            'status' => $tradeRecord->getStatus(),
            'createTime' => $tradeRecord->getCreateTime(),
            'updateTime' => $tradeRecord->getUpdateTime(),
            'statusTime' => $tradeRecord->getStatusTime()
        ];
    }

    public function getRelationships($tradeRecord, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'memberAccount' => [self::DATA => $tradeRecord->getMemberAccount()],
            'reference' => [self::DATA => $tradeRecord->getReference()]
        ];
    }
}
