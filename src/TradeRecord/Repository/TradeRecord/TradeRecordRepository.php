<?php
namespace Market\TradeRecord\Repository\TradeRecord;

use Marmot\Framework\Classes\Repository;

use Market\TradeRecord\Model\TradeRecord;
use Market\TradeRecord\Model\DefaultTradeRecord;
use Market\TradeRecord\Adapter\TradeRecord\ITradeRecordAdapter;
use Market\TradeRecord\Adapter\TradeRecord\TradeRecordDBAdapter;
use Market\TradeRecord\Adapter\TradeRecord\TradeRecordMockAdapter;

class TradeRecordRepository extends Repository implements ITradeRecordAdapter
{
    private $adapter;
    
    public function __construct()
    {
        $this->adapter = new TradeRecordDBAdapter();
    }

    protected function getActualAdapter() : ITradeRecordAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : ITradeRecordAdapter
    {
        return new TradeRecordMockAdapter();
    }

    public function add(TradeRecord $tradeRecord, array $keys = array()) : bool
    {
        return $this->getAdapter()->add($tradeRecord, $keys);
    }

    public function fetchOne($id) : DefaultTradeRecord
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
}
