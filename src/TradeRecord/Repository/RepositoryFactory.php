<?php
namespace Market\TradeRecord\Repository;

use Market\TradeRecord\Model\ITradeRecordAble;

use Common\Repository\NullRepository;

class RepositoryFactory
{
    const CATEGORY = array(
        'SERVICE_ORDER' => 1,
        'DEPOSIT' => 2,
        'WITHDRAWAL' => 3
    );

    const TYPE = array(
        ITradeRecordAble::TRADE_RECORD_TYPES['DEPOSIT']=> self::CATEGORY['DEPOSIT'],
        ITradeRecordAble::TRADE_RECORD_TYPES['DEPOSIT_PLATFORM']=> self::CATEGORY['DEPOSIT'],
        ITradeRecordAble::TRADE_RECORD_TYPES['ORDER_PAY_ENTERPRISE']=> self::CATEGORY['SERVICE_ORDER'],
        ITradeRecordAble::TRADE_RECORD_TYPES['ORDER_PAY_PLATFORM']=> self::CATEGORY['SERVICE_ORDER'],
        ITradeRecordAble::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_BUYER_ENTERPRISE']=> self::CATEGORY['SERVICE_ORDER'],
        ITradeRecordAble::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_SELLER_ENTERPRISE']=> self::CATEGORY['SERVICE_ORDER'],
        ITradeRecordAble::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_PLATFORM']=> self::CATEGORY['SERVICE_ORDER'],
        ITradeRecordAble::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_SUBSIDY']=> self::CATEGORY['SERVICE_ORDER'],
        ITradeRecordAble::TRADE_RECORD_TYPES['BUYER_REFUND']=> self::CATEGORY['SERVICE_ORDER'],
        ITradeRecordAble::TRADE_RECORD_TYPES['PLATFORM_REFUND']=> self::CATEGORY['SERVICE_ORDER'],
        ITradeRecordAble::TRADE_RECORD_TYPES['WITHDRAWAL']=> self::CATEGORY['WITHDRAWAL'],
        ITradeRecordAble::TRADE_RECORD_TYPES['WITHDRAWAL_PLATFORM']=> self::CATEGORY['WITHDRAWAL'],
    );

    const MAPS = array(
        self::CATEGORY['DEPOSIT'] =>
        'Member\Deposit\Repository\Deposit\DepositRepository',
        self::CATEGORY['SERVICE_ORDER'] =>
        'Market\Service\ServiceOrder\Repository\ServiceOrder\ServiceOrderRepository',
        self::CATEGORY['WITHDRAWAL'] =>
        'Member\Withdrawal\Repository\Withdrawal\WithdrawalRepository',
    );

    public function getRepository(int $category)
    {
        $repository = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($repository) ? new $repository : NullRepository::getInstance();
    }
}
