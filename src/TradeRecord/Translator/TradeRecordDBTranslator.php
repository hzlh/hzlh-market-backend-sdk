<?php
namespace Market\TradeRecord\Translator;

use Marmot\Interfaces\ITranslator;

use Market\TradeRecord\Model\TradeRecord;
use Market\TradeRecord\Model\DefaultTradeRecord;
use Market\TradeRecord\Model\NullDefaultTradeRecord;

class TradeRecordDBTranslator implements ITranslator
{
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function arrayToObject(array $expression, $tradeRecord = null)
    {
        if (!isset($expression['trade_record_id'])) {
            return NullDefaultTradeRecord::getInstance();
        }

        if ($tradeRecord == null) {
            $tradeRecord = new DefaultTradeRecord();
        }

        $tradeRecord->setId($expression['trade_record_id']);

        if (isset($expression['reference_id'])) {
            $tradeRecord->setReferenceId($expression['reference_id']);
        }
        if (isset($expression['member_account_id'])) {
            $tradeRecord->getMemberAccount()->setId($expression['member_account_id']);
        }
        if (isset($expression['trade_time'])) {
            $tradeRecord->setTradeTime($expression['trade_time']);
        }
        if (isset($expression['type'])) {
            $tradeRecord->setType($expression['type']);
        }
        if (isset($expression['trade_money'])) {
            $tradeRecord->setTradeMoney($expression['trade_money']);
        }
        if (isset($expression['debtor'])) {
            $tradeRecord->setDebtor($expression['debtor']);
        }
        if (isset($expression['creditor'])) {
            $tradeRecord->setCreditor($expression['creditor']);
        }
        if (isset($expression['balance'])) {
            $tradeRecord->setBalance($expression['balance']);
        }
        if (isset($expression['comment'])) {
            $tradeRecord->setComment($expression['comment']);
        }
        if (isset($expression['create_time'])) {
            $tradeRecord->setCreateTime($expression['create_time']);
        }
        if (isset($expression['update_time'])) {
            $tradeRecord->setUpdateTime($expression['update_time']);
        }
        if (isset($expression['status'])) {
            $tradeRecord->setStatus($expression['status']);
        }
        if (isset($expression['status_time'])) {
            $tradeRecord->setStatusTime($expression['status_time']);
        }
            
        return $tradeRecord;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($tradeRecord, array $keys = array())
    {
        if (!$tradeRecord instanceof DefaultTradeRecord && !$tradeRecord instanceof TradeRecord) {
            return false;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'referenceId',
                'memberAccount',
                'tradeTime',
                'type',
                'tradeMoney',
                'debtor',
                'creditor',
                'balance',
                'comment',
                'createTime',
                'updateTime',
                'status',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['trade_record_id'] = $tradeRecord->getId();
        }
        if (in_array('referenceId', $keys)) {
            $expression['reference_id'] = $tradeRecord->getReferenceId();
        }
        if (in_array('memberAccount', $keys)) {
            $expression['member_account_id'] = $tradeRecord->getMemberAccount()->getId();
        }
        if (in_array('tradeTime', $keys)) {
            $expression['trade_time'] = $tradeRecord->getTradeTime();
        }
        if (in_array('type', $keys)) {
            $expression['type'] = $tradeRecord->getType();
        }
        if (in_array('tradeMoney', $keys)) {
            $expression['trade_money'] = $tradeRecord->getTradeMoney();
        }
        if (in_array('debtor', $keys)) {
            $expression['debtor'] = $tradeRecord->getDebtor();
        }
        if (in_array('creditor', $keys)) {
            $expression['creditor'] = $tradeRecord->getCreditor();
        }
        if (in_array('balance', $keys)) {
            $expression['balance'] = $tradeRecord->getBalance();
        }
        if (in_array('comment', $keys)) {
            $expression['comment'] = $tradeRecord->getComment();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $tradeRecord->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $tradeRecord->getUpdateTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $tradeRecord->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $tradeRecord->getStatusTime();
        }

        return $expression;
    }
}
