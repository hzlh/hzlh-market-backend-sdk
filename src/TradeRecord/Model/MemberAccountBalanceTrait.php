<?php
namespace Market\TradeRecord\Model;

use Member\Member\Model\MemberAccount;
use Member\Member\Repository\MemberAccount\MemberAccountRepository;

trait MemberAccountBalanceTrait
{
    protected function getMemberAccountRepository() : MemberAccountRepository
    {
        return new MemberAccountRepository();
    }

    protected function fetchMemberAccountBalance(int $id) : float
    {
        $account = $this->getMemberAccountRepository()->fetchOne($id);
        
        return $account->getAccountBalance();
    }
}
