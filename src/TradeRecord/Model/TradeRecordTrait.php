<?php
namespace Market\TradeRecord\Model;

use Market\TradeRecord\Model\ITradeAble;
use Market\TradeRecord\Model\TradeRecord;

trait TradeRecordTrait
{
    protected function getTradeRecord() : TradeRecord
    {
        return new TradeRecord();
    }

    public function addTradeRecord() : bool
    {
        foreach ($this->fetchTradeRecordStrategies() as $strategy) {
            $tradeRecord = $this->getTradeRecord();
            $tradeRecord->setStrategy(
                $strategy
            );

            if (!$tradeRecord->add()) {
                return false;
            }
        }

        return true;
    }

    abstract protected function fetchTradeRecordStrategies() : array;
}
