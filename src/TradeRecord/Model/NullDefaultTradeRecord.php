<?php
namespace Market\TradeRecord\Model;

use Marmot\Interfaces\INull;

class NullDefaultTradeRecord extends DefaultTradeRecord implements INull
{
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
