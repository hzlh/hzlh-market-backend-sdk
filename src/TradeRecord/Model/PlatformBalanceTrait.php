<?php
namespace Market\TradeRecord\Model;

use PlatformAccount\Repository\PlatformAccount\PlatformAccountRepository;

trait PlatformBalanceTrait
{
    protected function getPlatformAccountRepository() : PlatformAccountRepository
    {
        return new PlatformAccountRepository();
    }

    protected function fetchPlatformBalance() : float
    {
        $account = $this->getPlatformAccountRepository()->fetchPlatformAccount();
        
        return $account->getAccountBalance();
    }
}
