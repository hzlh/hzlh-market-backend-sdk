<?php
namespace Market\TradeRecord\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Member\Member\Model\MemberAccount;

use Market\Service\ServiceOrder\Model\NullServiceOrder;

use Market\TradeRecord\Model\ITradeAble;

class DefaultTradeRecord implements IObject
{
    use Object;

    const STATUS_NORMAL = 0; //正常
    
    private $id;
    
    private $referenceId;

    private $reference;

    private $memberAccount;

    private $tradeTime;

    private $type;

    private $tradeMoney;

    private $debtor;

    private $creditor;

    private $balance;

    private $comment;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->referenceId = 0;
        $this->reference = new NullServiceOrder();
        $this->memberAccount = new MemberAccount();
        $this->tradeTime = 0;
        $this->type = ITradeRecordAble::TRADE_RECORD_TYPES['NULL'];
        $this->tradeMoney = 0.0;
        $this->debtor = '';
        $this->creditor = '';
        $this->balance = 0.0;
        $this->comment = '';
        $this->status = self::STATUS_NORMAL;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->referenceId);
        unset($this->reference);
        unset($this->memberAccount);
        unset($this->tradeTime);
        unset($this->type);
        unset($this->tradeMoney);
        unset($this->debtor);
        unset($this->creditor);
        unset($this->balance);
        unset($this->comment);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setReferenceId(int $referenceId) : void
    {
        $this->referenceId = $referenceId;
    }

    public function getReferenceId() : int
    {
        return $this->referenceId;
    }

    public function setReference(ITradeAble $reference)
    {
        $this->reference = $reference;
    }

    public function getReference() : ITradeAble
    {
        return $this->reference;
    }

    public function setMemberAccount(MemberAccount $memberAccount)
    {
        $this->memberAccount = $memberAccount;
    }

    public function getMemberAccount() : MemberAccount
    {
        return $this->memberAccount;
    }
    
    public function setTradeTime(int $tradeTime) : void
    {
        $this->tradeTime = $tradeTime;
    }

    public function getTradeTime() : int
    {
        return $this->tradeTime;
    }
    
    public function setType(int $type) : void
    {
        $this->type = in_array($type, ITradeRecordAble::TRADE_RECORD_TYPES) ?
                    $type :
                    ITradeRecordAble::TRADE_RECORD_TYPES['NULL'];
    }

    public function getType() : int
    {
        return $this->type;
    }
    
    public function setTradeMoney(float $tradeMoney) : void
    {
        $this->tradeMoney = $tradeMoney;
    }

    public function getTradeMoney() : float
    {
        return $this->tradeMoney;
    }
    
    public function setDebtor(string $debtor) : void
    {
        $this->debtor = $debtor;
    }

    public function getDebtor() : string
    {
        return $this->debtor;
    }

    public function setCreditor(string $creditor) : void
    {
        $this->creditor = $creditor;
    }

    public function getCreditor() : string
    {
        return $this->creditor;
    }
    
    public function setBalance(float $balance) : void
    {
        $this->balance = $balance;
    }

    public function getBalance() : float
    {
        return $this->balance;
    }
    
    public function setComment(string $comment) : void
    {
        $this->comment = $comment;
    }

    public function getComment() : string
    {
        return $this->comment;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
