<?php
namespace Market\TradeRecord\Model;

use Member\Member\Model\MemberAccount;

interface ITradeRecordAble
{
    const TRADE_RECORD_TYPES = array(
        'NULL' => 0,
        'DEPOSIT' => 1, //用户充值
        'DEPOSIT_PLATFORM' => 2, //平台转入(充值)
        'ORDER_PAY_ENTERPRISE' => 10, //支付(第三方支付）
        'ORDER_PAY_PLATFORM' => 12, //转入(第三方支付)
        'ORDER_CONFIRMATION_BUYER_ENTERPRISE' => 14, //支付(订单）
        'ORDER_CONFIRMATION_SELLER_ENTERPRISE' => 16, //收入(卖家)
        'ORDER_CONFIRMATION_PLATFORM' => 18, //支付(交易服务费）
        'ORDER_CONFIRMATION_SUBSIDY' => 19, //收入(补贴）
        'WITHDRAWAL' => 20, //用户提现
        'WITHDRAWAL_PLATFORM' => 22, //平台转出(提现)
        'BUYER_REFUND' => 24, //买家退款状态
        'PLATFORM_REFUND' => 26, //平台退款状态
    );

    public function getReferenceId() : int;
    
    public function getMemberAccount() : MemberAccount;

    public function getTradeTime() : int;

    public function getType() : int;

    public function getTradeMoney() : float;

    public function getDebtor() : string;

    public function getCreditor() : string;

    public function getBalance() : float;

    public function getComment() : string;
}
