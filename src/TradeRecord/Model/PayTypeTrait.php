<?php
namespace Market\TradeRecord\Model;

use Market\Payment\Model\Payment;

trait PayTypeTrait
{
    public function getPayTypeCN($paymentType) : string
    {
        return isset(Payment::PAYMENT_TYPE_CN[$paymentType]) ?
                Payment::PAYMENT_TYPE_CN[$paymentType] :
                Payment::PAYMENT_TYPE_CN[Payment::PAYMENT_TYPE['NULL']];
    }
}
