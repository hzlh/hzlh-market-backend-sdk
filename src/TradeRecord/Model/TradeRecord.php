<?php
namespace Market\TradeRecord\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Market\TradeRecord\Strategy\NullRecordStrategy;
use Market\TradeRecord\Repository\TradeRecord\TradeRecordRepository;

use Member\Member\Model\MemberAccount;

class TradeRecord implements ITradeRecordAble, IObject
{
    use Object;
    
    const STATUS_NORMAL = 0;
    
    private $strategy;

    private $repository;

    public function __construct($strategy = null)
    {
        $this->strategy = is_null($strategy) ? new NullRecordStrategy() : $strategy;
        $this->id = 0;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->status = self::STATUS_NORMAL;
        $this->repository = new TradeRecordRepository();
    }

    public function __destruct()
    {
        unset($this->strategy);
        unset($this->id);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setStrategy(ITradeRecordAble $strategy) : void
    {
        $this->strategy = $strategy;
    }

    public function getStrategy() : ITradeRecordAble
    {
        return $this->strategy;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getReferenceId() : int
    {
        return $this->getStrategy()->getReferenceId();
    }
    
    public function getMemberAccount() : MemberAccount
    {
        return $this->getStrategy()->getMemberAccount();
    }

    public function getTradeTime() : int
    {
        return $this->getStrategy()->getTradeTime();
    }

    public function getType() : int
    {
        return $this->getStrategy()->getType();
    }

    public function getTradeMoney() : float
    {
        return $this->getStrategy()->getTradeMoney();
    }

    public function getDebtor() : string
    {
        return $this->getStrategy()->getDebtor();
    }

    public function getCreditor() : string
    {
        return $this->getStrategy()->getCreditor();
    }

    public function getBalance() : float
    {
        return $this->getStrategy()->getBalance();
    }

    public function getComment() : string
    {
        return $this->getStrategy()->getComment();
    }

    protected function getRepository() : TradeRecordRepository
    {
        return $this->repository;
    }

    public function add() : bool
    {
        return $this->getRepository()->add($this);
    }
}
