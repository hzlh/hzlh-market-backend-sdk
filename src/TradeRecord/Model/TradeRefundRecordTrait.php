<?php
namespace Market\TradeRecord\Model;

use Market\TradeRecord\Model\ITradeAble;
use Market\TradeRecord\Model\TradeRecord;

trait TradeRefundRecordTrait
{
    protected function getRefundTradeRecord() : TradeRecord
    {
        return new TradeRecord();
    }
    //添加退款流水
    public function addRefundTradeRecord() : bool
    {
        foreach ($this->fetchRefundTradeRecordStrategies() as $strategy) {
            $tradeRecord = $this->getRefundTradeRecord();
            $tradeRecord->setStrategy(
                $strategy
            );

            if (!$tradeRecord->add()) {
                return false;
            }
        }

        return true;
    }

    abstract protected function fetchRefundTradeRecordStrategies() : array;
}
