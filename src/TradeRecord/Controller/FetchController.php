<?php
namespace Market\TradeRecord\Controller;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Common\Controller\Interfaces\IFetchAbleController;

use Market\TradeRecord\View\TradeRecordView;
use Market\TradeRecord\Repository\TradeRecord\TradeRecordRepository;

class FetchController extends Controller implements IFetchAbleController
{
    use JsonApiTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new TradeRecordRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : TradeRecordRepository
    {
        return $this->repository;
    }

    public function fetchOne(int $id)
    {
        $repository = $this->getRepository();
        $tradeRecord = $repository->fetchOne($id);

        if (!$tradeRecord instanceof INull) {
            $this->renderView(new TradeRecordView($tradeRecord));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function fetchList(string $ids)
    {
        $ids = explode(',', $ids);

        $tradeRecordList = array();

        $repository = $this->getRepository();
        $tradeRecordList = $repository->fetchList($ids);

        if (!empty($tradeRecordList)) {
            $this->renderView(new TradeRecordView($tradeRecordList));
            return true;
        }

        $this->displayError();
        return false;
    }

    public function filter()
    {
        $repository = $this->getRepository();

        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();

        list($tradeRecordList, $count) = $repository->filter(
            $filter,
            $sort,
            ($curpage-1)*$perpage,
            $perpage
        );

        if ($count > 0) {
            $view = new TradeRecordView($tradeRecordList);
            $view->pagination(
                'productMarketTradeRecords',
                $this->getRequest()->get(),
                $count,
                $perpage,
                $curpage
            );

            $this->renderView($view);
            return true;
        }

        $this->displayError();
        return false;
    }
}
