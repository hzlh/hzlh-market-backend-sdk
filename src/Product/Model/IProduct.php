<?php
namespace Market\Product\Model;

use Common\Model\IOperatAble;
use Common\Model\IOnShelfAble;
use Common\Model\IApproveAble;
use Common\Model\IResubmitAble;

use Snapshot\Model\ISnapshotAble;

use Market\Order\Model\ITradeAble;

interface IProduct extends IOperatAble, ISnapshotAble, IOnShelfAble, IApproveAble, IResubmitAble, ITradeAble
{
}
