<?php
namespace Market\Statistical\Translator;

use Marmot\Interfaces\ITranslator;

use Market\Statistical\Model\Statistical;

class StaticsServiceAuthenticationCountTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $statistical = null)
    {
        if ($statistical == null) {
            $statistical = new Statistical();
        }

        $result = $expression;
        
        $statistical->setResult($result);

        return $statistical;
    }


    public function objectToArray($statistical, array $keys = array())
    {
        unset($statistical);
        unset($keys);
    }
}
