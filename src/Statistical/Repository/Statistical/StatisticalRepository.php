<?php
namespace Market\Statistical\Repository\Statistical;

use Market\Statistical\Model\Statistical;
use Market\Statistical\Adapter\Statistical\IStatisticalAdapter;

class StatisticalRepository implements IStatisticalAdapter
{
    private $adapter;
    
    public function __construct(IStatisticalAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function setAdapter(IStatisticalAdapter $adapter) : void
    {
        $this->adapter = $adapter;
    }

    private function getAdapter() : IStatisticalAdapter
    {
        return $this->adapter;
    }

    public function analyse(array $filter = array()) : Statistical
    {
        return $this->getAdapter()->analyse($filter);
    }
}
