<?php
namespace Market\Statistical\Controller;

use Market\Statistical\Adapter\Statistical\IStatisticalAdapter;
use Market\Statistical\Adapter\Statistical\NullStatisticalAdapter;

class IAdapterFactory
{
    const MAPS = array(
        
        //产销超市
        'staticsProductMarketAuthenticationCount'=>
        'Market\Statistical\Adapter\Statistical\Markets\Product\StaticsServiceAuthenticationCountAdapter',
        'staticsProductMarketRequirementCount'=>
        'Market\Statistical\Adapter\Statistical\Markets\Product\StaticsServiceRequirementCountAdapter',
        'staticsProductMarketCount'=>
        'Market\Statistical\Adapter\Statistical\Markets\Product\StaticsServiceCountAdapter',
        //人才超市
        'staticsTalentMarketRequirementCount'=>
        'Market\Statistical\Adapter\Statistical\Markets\Talent\StaticsServiceRequirementCountAdapter',
        //学习港
        'staticsLearnMarketRequirementCount'=>
        'Market\Statistical\Adapter\Statistical\Markets\Learn\StaticsServiceRequirementCountAdapter',
    );

    public function getAdapter(string $type) : IStatisticalAdapter
    {
        $adapter = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($adapter) ? new $adapter : new NullStatisticalAdapter();
    }
}
