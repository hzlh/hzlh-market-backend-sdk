<?php
namespace Market\Statistical\Controller;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use Market\Statistical\View\StatisticalView;
use Market\Statistical\Repository\Statistical\StatisticalRepository;

class StatisticalController extends Controller
{
    use JsonApiTrait;

    protected function getStatisticalRepository(string $type) : StatisticalRepository
    {
        $adapterFactory = new IAdapterFactory();
        $adapter = $adapterFactory->getAdapter($type);
        return new StatisticalRepository(new $adapter);
    }

    public function analyse(string $type)
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();
       
        //部分接口需要分页
        $filter['curpage'] = ($curpage-1)*$perpage;
        $filter['perpage'] = $perpage;

        //浏览和分享量统计需要sort
        $filter['sort'] = $sort;

        $repository = $this->getStatisticalRepository($type);

        $statistical = $repository->analyse($filter);

        if (!$statistical instanceof INull) {
            $this->renderView(new StatisticalView($statistical));
            return true;
        }

        $this->displayError();
        return false;
    }
}
