<?php
namespace Market\Statistical\Model;

class Statistical
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var array $result 统计结果
     */
    private $result;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->result = array();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->result);
    }

    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setResult(array $result) : void
    {
        $this->result = $result;
    }

    public function getResult() : array
    {
        return $this->result;
    }
}
