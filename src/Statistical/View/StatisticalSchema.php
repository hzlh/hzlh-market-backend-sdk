<?php
namespace Market\Statistical\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class StatisticalSchema extends SchemaProvider
{
    protected $resourceType = 'marketStatisticals';

    public function getId($statistical) : int
    {
        return $statistical->getId();
    }

    public function getAttributes($statistical) : array
    {
        return [
            'result'  => $statistical->getResult()
        ];
    }
}
