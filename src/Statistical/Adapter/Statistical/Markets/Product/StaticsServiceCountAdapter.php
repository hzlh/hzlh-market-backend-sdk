<?php
namespace Market\Statistical\Adapter\Statistical\Markets\Product;

use Marmot\Core;

use Market\Statistical\Model\Statistical;
use Market\Statistical\Model\NullStatistical;
use Market\Statistical\Translator\StaticsServiceCountTranslator;
use Market\Statistical\Adapter\Statistical\IStatisticalAdapter;

class StaticsServiceCountAdapter implements IStatisticalAdapter
{
    private $staticsServiceTranslator;

    public function __construct()
    {
        $this->staticsServiceTranslator = new StaticsServiceCountTranslator();
    }

    public function __destruct()
    {
        unset($this->staticsServiceTranslator);
    }
    
    protected function getStaticsServiceCountTranslator() : StaticsServiceCountTranslator
    {
        return $this->staticsServiceTranslator;
    }

    public function analyse(array $filter = array()) : Statistical
    {
        $info = array();

        if (isset($filter['enterpriseId'])) {
            $info = Core::$dbDriver->query('call statics_market_service_count("'.$filter['enterpriseId'].'");');
        }

        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullStatistical::getInstance();
        }

        return $this->getStaticsServiceCountTranslator()->arrayToObject($info);
    }
}
