<?php
namespace Market\Statistical\Adapter\Statistical\Markets\Product;

use Marmot\Core;

use Market\Statistical\Model\Statistical;
use Market\Statistical\Model\NullStatistical;
use Market\Statistical\Translator\StaticsServiceRequirementCountTranslator;
use Market\Statistical\Adapter\Statistical\IStatisticalAdapter;

class StaticsServiceRequirementCountAdapter implements IStatisticalAdapter
{
    private $staticsRequirementTranslator;

    public function __construct()
    {
        $this->staticsRequirementTranslator = new StaticsServiceRequirementCountTranslator();
    }

    public function __destruct()
    {
        unset($this->staticsRequirementTranslator);
    }
    
    protected function getStaticsServiceRequirementCountTranslator() : StaticsServiceRequirementCountTranslator
    {
        return $this->staticsRequirementTranslator;
    }

    public function analyse(array $filter = array()) : Statistical
    {
        $info = array();

        if (isset($filter['memberId'])) {
            $info = Core::$dbDriver->query('call statics_market_service_requirement_count("'.$filter['memberId'].'");');
        }

        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullStatistical::getInstance();
        }

        return $this->getStaticsServiceRequirementCountTranslator()->arrayToObject($info);
    }
}
