<?php
namespace Market\Statistical\Adapter\Statistical\Markets\Product;

use Marmot\Core;

use Market\Statistical\Model\Statistical;
use Market\Statistical\Model\NullStatistical;
use Market\Statistical\Translator\StaticsServiceAuthenticationCountTranslator;
use Market\Statistical\Adapter\Statistical\IStatisticalAdapter;

class StaticsServiceAuthenticationCountAdapter implements IStatisticalAdapter
{
    private $staticsAuthenticationTranslator;

    public function __construct()
    {
        $this->staticsAuthenticationTranslator = new StaticsServiceAuthenticationCountTranslator();
    }

    public function __destruct()
    {
        unset($this->staticsAuthenticationTranslator);
    }
    
    protected function getStaticsServiceAuthenticationCountTranslator() : StaticsServiceAuthenticationCountTranslator
    {
        return $this->staticsAuthenticationTranslator;
    }

    public function analyse(array $filter = array()) : Statistical
    {
        $info = array();
       
        if (isset($filter['enterpriseId'])) {
            $info = Core::$dbDriver->query('call statics_market_service_authentication_count("'.$filter['enterpriseId'].'");');
        }

        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullStatistical::getInstance();
        }

        return $this->getStaticsServiceAuthenticationCountTranslator()->arrayToObject($info);
    }
}
