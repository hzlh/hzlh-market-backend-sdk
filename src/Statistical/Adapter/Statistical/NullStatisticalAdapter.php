<?php
namespace Market\Statistical\Adapter\Statistical;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Market\Statistical\Model\Statistical;
use Market\Statistical\Model\NullStatistical;

class NullStatisticalAdapter implements INull, IStatisticalAdapter
{
    public function analyse(array $filter = array()) : Statistical
    {
        return NullStatistical::getInstance();
    }
}
