<?php
namespace Market\Statistical\Adapter\Statistical;

use Market\Statistical\Model\Statistical;

interface IStatisticalAdapter
{
    public function analyse(array $filter = array()) : Statistical;
}
